import { FilloutStandardEmbed } from "@fillout/react";
import * as React from 'react'
import { createRoot } from 'react-dom/client'

function App() {

  return (
    <>
      <FilloutStandardEmbed
        filloutId="pqodtDmkB1us"
        dynamicResize="true"
        />
    </>
  );
}

const container = document.getElementById('app');
const root = createRoot(container);
root.render(<App/>);

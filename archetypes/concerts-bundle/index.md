---
title: "{{ .Name | time | dateFormat "_2 January 2006" | title }}"
date: {{ .Date }}
doors: 2pm
draft: true
categories:
- Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
- key: "Standard"
  value: "£12"
- key: "Vortex Members & Concessions"
  value: "£8"
concert_date: {{ .Name | time }}
concert_series: Mopomoso Live
year: {{ .Name | time | dateFormat "2006" }}
---

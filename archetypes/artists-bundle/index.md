---
title: "{{ replace .Name "-" " " | title }}"
name: "{{ index (last 1 (split .Name "-")) 0  | title }}, {{ delimit (first (sub ((split .Name "-") | len) 1) (split .Name "-")) " " | title }}"
date: {{ .Date }}
categories:
- Artist
tags: 
- "{{ replace .Name "-" " " | urlize }}"
artists: "{{ replace .Name "-" " " | title }}"
---

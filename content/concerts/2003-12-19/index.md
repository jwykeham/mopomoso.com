---
artists:
  - Angharad Davies
  - Phil Durrant
concert_date: 2003-12-19T20:00:00+01:00
concert_series: At The Red Rose
date: 2003-12-19T20:00:00+01:00
notes: null
sets:
  - - Angharad Davies
    - Phil Durrant
sources:
  - https://www.angharaddavies.com/past-events?page=11
title: 19 December 2003
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2003
---

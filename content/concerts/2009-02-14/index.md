---
title: 15 February 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-02-15T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - Robert Jarvis
  - Lawrence Casserley
  - Abaetetuba
  - John Russell
  - Stefan Keune
sets:
  - - Robert Jarvis
    - Lawrence Casserley
  - - Abaetetuba
  - - John Russell
    - Stefan Keune
year: 2009
---
Duo, sextet, duo.

### {{<artist "Robert Jarvis">}} & {{<artist "Lawrence Casserley">}}
{{<youtube p6_RuXNVhxs>}} 
### {{<artist "Abaetetuba">}}
{{<youtube MAwZ30DkCPc>}} 
### {{<artist "John Russell">}} & {{<artist "Stefan Keune">}}
{{<youtube gouHUfWPmcM>}} 

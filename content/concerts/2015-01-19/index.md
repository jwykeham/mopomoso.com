---
title: 15 February 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-02-15T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artist:
  - "Mick Beck"
  - "Anton Hunter"
  - "Johnny Hunter"
  - "Adrian Northover"
  - "Daniel Thompson"
  - "Gianni Mimmo"
  - "Harri Sjoestroem"
sets:
  - - Mick Beck
    - Anton Hunter
    - Johnny Hunter
  - - Adrian Northover
    - Daniel Thompson
  - - Gianni Mimmo
    - Harri Sjoestroem
year: 2015
---

### Beck Hunters: {{<artist "Mick Beck">}} (tenor sax, bassoon and whistles), {{<artist "Anton Hunter">}} (guitar) & {{<artist "Johnny Hunter">}} (drums)

{{<youtube 5aspNpm9PBs>}}

### {{<artist "Adrian Northover">}} (sax) & {{<artist "Daniel Thompson">}} (guitar)

{{<youtube rf3inzbI1_Q>}}

### {{<artist "Gianni Mimmo">}} & {{<artist "Harri Sjoestroem">}} (soprano saxes)

{{<youtube OVMNVuQnLok>}}

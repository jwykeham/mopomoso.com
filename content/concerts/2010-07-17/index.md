---
title: 18 July 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-07-18T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Josef Klammer"
  - "Seppo Gründler"
  - "Dave Tucker"
  - "Dave Solomon"
  - "Sonia Paco Rocchia"
  - "Ricardo Tejero"
  - "Pat Thomas"
  - "Satoko Fukuda"
  - "John Russell"
sets:
  - - Josef Klammer
    - Seppo Gründler
  - - Dave Tucker
    - Dave Solomon
    - Sonia Paco Rocchia
    - Ricardo Tejero
    - Pat Thomas
  - - Satoko Fukuda
    - John Russell
year: 2010
---
### {{<artist "Josef Klammer">}} (drums, electronics)/{{<artist "Seppo Gründler">}} (guitar to midi, electronics)

Klammer &amp; Gruendler have been working on the implementation of their “New Improvised Electronic Music” for over 20 years, creating pieces like Razionalnik – first multilateral telematic midi concert, Im Trockenraum – Austria’s first earphones concert for 100 earphones, RGB – synchronizing two ensembles (Berlin/Graz) via TV pictures, numerous sound installations, music for theatre and radiophonic pieces. A rare chance to hear the groundbreaking duo in this their UK debut.

{{<youtube MRbLgtckWso>}} 

### The Fantastique Quintette - {{<artist "Dave Tucker">}} (guitar)/{{<artist "Dave Solomon">}} (drums)/{{<artist "Sonia Paco Rocchia">}} (bassoon)/{{<artist "Ricardo Tejero">}} (reeds)/{{<artist "Pat Thomas">}} (keyboards)

Ex Fall guitarist and London Improvisers Orchestra stalwart Dave Tucker’s five piece in a stunning new incarnation with some of the music’s finest players in a set which promises to be full of energy and explosive delights.

{{<youtube oIsa5Vztd0k>}} 

### {{<artist "Satoko Fukuda">}} (violin)/{{<artist "John Russell">}} (guitar)

Satoko Fukuda is a remarkable musical talent who began playing the violin at the age of seven. Invited on a full scholarship to study at the Yehudi Menuhin School, where she led the Menuhin School orchestra, she is the recipient of many awards and accolades and has toured and broadcast extensively. A player of intense concentration and feeling with a love of improvising she is joined by regular playing partner John Russell and their set tonight will be recorded for future release. A not to be missed musical pairing!

{{<youtube Q7w6Ii7QtT0>}} 

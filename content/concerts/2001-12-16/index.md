---
artists:
  - Lol Coxhill
  - Hugh Davies
  - Sue Ferrar
  - Roger Turner
  - John Russell
  - Paul Smyth
  - Chris Burn
  - Rhodri Davies
  - Adam Bohman
  - Alan Wilkinson
  - Matt Hutchinson
concert_date: 2001-12-16T20:00:00Z
concert_series: At The Red Rose
date: 2001-12-16T20:00:00Z
notes: null
sets: []
sources:
  - https://thumped.com/bbs/threads/in-london-this-sunday-16th.801/
title: 16 December 2001
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2001
---
---
artists:
  - Phil Durrant
  - '? Renkel'
  - '? Davies'
  - '? Beins'
  - Markus Wettstein
concert_date: 1998-06-21T20:00:00+01:00
concert_series: At The Red Rose
date: 1998-06-21T20:00:00+01:00
notes: []
sets: []
sources:
  - https://www.newspapers.com/image/260769500
title: 21 June 1998
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1998
---
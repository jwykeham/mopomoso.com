---
title: Tuesday 20 August 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-08-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Russell"
year: 2013
---


### Tutti
{{<youtube olT5TzaDfaY>}}
  
### Smyth, Lauro, Musson, Solberg
{{<youtube YxLVyJPMLBc>}}
  
### Houtkamp, Burn, Wassermann, Linson , Casserley
{{<youtube GE5mrRFA5hw>}}

### Eldridge, Blunt, Russell
{{<youtube rTYLoKPR4AY>}}

### Houtkamp , Smyth , Musson , Lauro, Burn, Linson, Solberg
{{<youtube iWSpEW3ziM0>}}

### Blunt, Solberg, Casserley
{{<youtube WYbI3Q9ttxk>}}

### Smyth, Eldridge, Wassermann, Russell
{{<youtube u_FfuPKbXtc>}}

### Tutti
{{<youtube YbJ69q2JEYc>}}

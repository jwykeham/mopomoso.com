---
title: 20 October 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-10-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Kay Grant"
  - "Daniel Thompson"
  - "Benedict Taylor"
  - "Alex Ward"
  - "Yoko Miura"
  - "Charlie Collins"
  - "Richard Barrett"
  - "Paul Obermayer"
  - "John Russell"
sets:
  - - Kay Grant
    - Daniel Thompson
    - Benedict Taylor
    - Alex Ward
  - - Yoko Miura
    - Charlie Collins
  - - Richard Barrett
    - Paul Obermayer
    - John Russell
year: 2013
---

Continuing to present great music with three contrasting sets emphasising the intriguing diversity of approaches to be found in free improvisation.

### {{<artist "Kay Grant">}} (voice), {{<artist "Daniel Thompson">}} (guitar), {{<artist "Benedict Taylor">}} (viola) & {{<artist "Alex Ward">}} (clarinet)

{{<youtube 5gkm6-KoirY>}}

{{<youtube doinBPNQK10>}}

{{<youtube X4UHBZhfBtg>}}

### {{<artist "Yoko Miura">}} (piano) & {{<artist "Charlie Collins">}} (percussion)

{{<youtube ZGXzFrj6bjQ>}}

### FURT - {{<artist "Richard Barrett">}} and {{<artist "Paul Obermayer">}} (electronics) plus guest {{<artist "John Russell">}} (electric guitar)

{{<youtube dP9SOQG6gI4>}}

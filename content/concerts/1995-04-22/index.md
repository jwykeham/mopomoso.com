---
artists:
  - John Russell
concert_date: 1995-04-22T20:00:00+01:00
concert_series: At The Red Rose
date: 1995-04-22T20:00:00+01:00
notes:
  - '>Highly recommended experimental night hosts avant-violinist Durrant with guitar maverick John Russell, plus a full scale electronic-acoustic blow-out from Atton''s Leeds-based trio. - *The Guardian - Sat April 15 1995*'
  - Listing states "Phil Durrant Trio" not sure if this means Phil Durrant, John Russell and John Butcher
sets:
  - - Phil Durrant
    - John Russell
    - ???
  - - Chris Atton
    - Martin Hackett
    - Mike Jennings
sources:
  - https://www.newspapers.com/image/260767758
title: 22 April 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
costs:
  - key: "Standard"
    value: "£3.50"
  - key: "Concessions"
    value: "£2.50"
year: 1995
---

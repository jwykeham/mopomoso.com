---
title: 18 March 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-03-18T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Neil Metcalfe"
  - "John Rangecroft"
  - "Marcio Mattos"
  - "Matthias Boss"
  - "Adrian Northover"
  - "Marcello Magliocchi"
  - "Maresuke Okamoto"
  - "Kresten Michael Osgood"
  - "John Russell"
sets:
  - - Neil Metcalfe
    - John Rangecroft
    - Marcio Mattos
  - - Matthias Boss
    - Adrian Northover
    - Marcello Magliocchi
    - Maresuke Okamoto
  - - Kresten Michael Osgood
    - John Russell
year: 2018
---

Three contrasting sets to delight and inspire in the relaxed and intimate listening environment of the world famous Vortex Jazz club. Free improvisation is one of the true jewels in today’s contemporary cultural life and one where the musicians share the discovery of the music at the same time as the audience. The monthly Sunday afternoon sessions are a great way to enjoy the music in a friendly and welcoming atmosphere. 
<!--more-->

### {{<artist "Neil Metcalfe">}} (flute), {{<artist "John Rangecroft">}} (sax/clarinet) & {{<artist "Marcio Mattos">}} (bass)

![download](https://www.mopomoso.com/wp-content/uploads/2018/02/download.jpg)

These three musicians have been involved with playing freely improvised music since the early 70s and common threads in their development are John Stevens, Evan Parker and Roscoe Mitchell. Displaying a keen sense of pitch and line their approach can, in some ways, seem almost conversational. Certainly there is a relaxed ease in the interaction between these musicians, built over many years of shared music making. A delight.


### {{<artist "Matthias Boss">}} (vln), {{<artist "Adrian Northover">}} (sax), {{<artist "Marcello Magliocchi">}} (drums) & {{<artist "Maresuke Okamoto">}} (cello)

![adrian 4tet](https://www.mopomoso.com/wp-content/uploads/2018/02/adrian-4tet.jpeg)

Interesting international quartet with established musicians from Switzerland, Italy, Japan and the UK whose CVs look like a who’s who of improvised music. Suffice to say that this group comes with a pedigree many would envy. Their joint experiences are not employed in an overtly flashy way but to give the music an unstated strength and depth. There is a genuine poise and sense of purpose in the group and, while their shared skills and experience are present, they are employed lightly, as a tool to allow the music to develop fully on centre stage.



### {{<artist "Kresten Michael Osgood">}} (drums, cornet, keyboards) & {{<artist "John Russell">}} (guitar)

![john and kresten](https://www.mopomoso.com/wp-content/uploads/2018/02/john-and-kresten.jpeg)

Drummer Kresten Osgood has played and recorded with a wide variety of musicians at the cutting end of Jazz including Paul Bley, John Tchicai and Sam Rivers and last year played with renowned UK guitarist Russell for the first time, recording material in an informal quartet over two days, with New York multi-instrumentalist Cooper-Moore and French trumpet wizard Jean Luc Cappozzo in Denmark. Their music never stands still, mixing keen intellegence and a generous curiosity. Join the fun!

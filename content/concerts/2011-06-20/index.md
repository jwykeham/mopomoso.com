---
title: 17 July 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-07-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Charlie Collins"
  - "John Jasnoch"
  - "Alexander Hawkins"
  - "Luc Houtkamp"
  - "John Russell"
sets:
  - - Charlie Collins
    - John Jasnoch
  - - Alexander Hawkins
  - - Luc Houtkamp
    - John Russell
year: 2011
---

### {{<artist "Charlie Collins">}} / {{<artist "John Jasnoch">}}

{{<youtube NSQFC8QqCGg>}} 

### {{<artist "Alexander Hawkins">}} (piano)

{{<youtube xXOPs7VCHkw>}} 

### {{<artist "Luc Houtkamp">}} / {{<artist "John Russell">}}

{{<youtube szwv4VptjJs>}} 

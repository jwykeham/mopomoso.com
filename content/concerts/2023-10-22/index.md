---
title: "22 October 2023"
date: 2023-10-01T06:09:19+01:00
doors: 2pm
categories:
- Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
- key: "Standard"
  value: "£12"
- key: "Vortex Members & Concessions"
  value: "£8"
concert_date: 2023-10-22 00:00:00 +0100 BST
concert_series: Mopomoso Live
ticket_link: https://www.ticketweb.uk/event/mopomoso-october-vortex-jazz-club-tickets/13692288
artists:
- "Jackson Burton"
- "Ash Reid"
- "Harry Gilonis"
- "Eddie Prévost"
- "Geoff Eales"
- "Paul Jolly"
- "Ray Jones"
- "Maria Sappho"
sets:
- - "Jackson Burton"
  - "Ash Reid"
- - "Harry Gilonis"
  - "Eddie Prévost"
- - "Geoff Eales"
  - "Paul Jolly"
  - "Ray Jones"
- - "Maria Sappho"
---
### {{<artist "Jackson Burton">}} (voice) & {{<artist "Ash Reid">}} (clarinet, voice etc)

Ash and Jackson have been collaborating together for over 10 years, working with scores, text pieces and action based performances that address class and gender in relation to where they grew up. Recent performances have included Cafe OTO, The Fruit Market and Hundred Years Gallery.

Ash Reid is a performer and sometime writer, avid texter, current bar tender and former thought owner.

### {{<artist "Harry Gilonis">}} (voice) & {{<artist "Eddie Prévost">}} (percussion)
{{<artist/description "Harry Gilonis">}} 

{{<artist/description "Eddie Prévost">}} 

### {{<artist "Geoff Eales">}} (piano), {{<artist "Paul Jolly">}} (reeds) & {{<artist "Ray Jones">}} (spoken word)

Ray will be performing his poetry and that of Bob Kaufman and Dylan Thomas alongside music and words from Geoff Eales and Paul Jolly.

### {{<artist "Maria Sappho">}} (piano)
{{<artist/description "Maria Sappho">}} 

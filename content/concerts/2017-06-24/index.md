---
title: 16 July 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-07-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "James O’Sullivan"
  - "Chris Prosser"
  - "Terry Day"
  - "Dominic Lash"
  - "Tania Chen"
  - "John Russell"
sets:
  - - James O’Sullivan
    - Chris Prosser
  - - Terry Day
    - Dominic Lash
  - - Tania Chen
    - John Russell
year: 2017
---

> "Great music and a wonderful way to spend the afternoon" - **George Coote**. 

Indeed correct George! A Sunday afternoon at The Vortex listening up close and personal to some great musicians soothes all savage breasts.

### {{<artist "James O’Sullivan">}} (guitar) & {{<artist "Chris Prosser">}} (violin)

![chris james](https://www.mopomoso.com/wp-content/uploads/2017/06/chris-james-300x142.jpeg)

The New Zealand based violinist and London based guitarist renew their partnership in a duo that promises to further enlarge upon their shared delight with the process and practices of free improvisation. An initial inspiration and driving force that was nurtured in the workshops of Eddie Prevost. Catch them while you can.

### {{<artist "Terry Day">}} (percussion) & {{<artist "Dominic Lash">}} (bass)

![terry and dom](https://www.mopomoso.com/wp-content/uploads/2017/06/terry-and-dom-300x164.jpeg)

A welcome return for two Mopomoso favourites here for the first time as a duo. Both musicians are highly regarded members of the improvising community, both here and abroad, where they lend their respective talents to a wide range of groups and projects. Promises to be a lot of fun!

### {{<artist "Tania Chen">}} (piano) & {{<artist "John Russell">}} (guitar)

![tania john](https://www.mopomoso.com/wp-content/uploads/2017/06/tania-john-300x117.jpeg)

The remarkable and diverse talents of Tania Chen, here on a visit from the US, meet those of the redoubtable guitarist John Russell. Again a debut duo performance, Tania and John played together at The Vortex in Fete Quaqua 2010.  
The range of options open for the musicians is so large it is impossible to predict what the resulting music might be but, suffice to say, it will be nothing less than a fully engaging experience based upon close listening and an openness to a shared musical experience.

---
title: "18 August 2024"
date: 2024-04-24T10:59:11+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-08-18 00:00:00 +0100 BST
concert_series: Mopomoso Live
year: 2024
ticket_link: https://www.vortexjazz.co.uk/event/mopomoso-august-2024-08-18/2024-08-18/
sets:
- - Mark Hewins
  - Alex Ward
- - Neil Metcalfe
  - Barry John Edwards
  - Colin Somervell
- - Caroline Kraabel
  - Veryan Weston
artists:
- Mark Hewins
- Alex Ward
- Neil Metcalfe
- Barry John Edwards
- Colin Somervell
- Caroline Kraabel
- Veryan Weston
---

---
title: 19 May 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-05-19T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Caroline Kraabel"
  - "Neil Metcalfe"
  - "Armorel Weston"
  - "Kay Grant"
  - "Jim Dvorak"
  - "Otto Willberg"
  - "John Russell"
  - "Alex Ward"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Caroline Kraabel
    - Neil Metcalfe
  - - Armorel Weston
    - Kay Grant
    - Jim Dvorak
    - Otto Willberg
  - - John Russell
    - Alex Ward
year: 2019
---
### {{<artist "Caroline Kraabel">}} (sax) & {{<artist "Neil Metcalfe">}} (flute)

![](https://www.mopomoso.com/wp-content/uploads/2019/05/neil-and-caroline.png)

The first pairing of the afternoon sees two major players from the London scene who have that rare ability to mix melodic invention with both playfulness and a deep sense of soul.

### Lullula – {{<artist "Armorel Weston">}} (voice), {{<artist "Kay Grant">}} (voice), {{<artist "Jim Dvorak">}} (trumpet) & {{<artist "Otto Willberg">}} (bass/voice)

![](https://www.mopomoso.com/wp-content/uploads/2019/05/lullula-weston-grant-dvorak-willberg.jpg)

The name is taken from the woodlark Lullula, known for its heavenly song, and this group use free improvisation to create a joyous and delightful sound, never complacent and full of surprises.

### {{<artist "John Russell">}} (guitar) & {{<artist "Alex Ward">}} (clarinet)

![](https://www.mopomoso.com/wp-content/uploads/2019/05/john-and-alex.png)

For the last set we have renowned guitarist / improviser John Russell with the ever surprising Alex Ward. They will be deciding on the day what instruments to use so it could be a mix of acoustic and electric guitars with Alex’s clarinet somewhere in there as well. It might be all acoustic or electric. Whatever the instrumentation it will be a lot of fun!

---
classic-editor-remember:
  - classic-editor
title: 19 January 2020
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2020-01-19T14:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Caroline Kraabel"
  - "Rowland Sutherland"
  - "Dave Tucker"
  - "Hannah Marshall"
  - "Jackie Walduck"
  - "John Russell"
  - "Masaharu Shoji"
sets:
  - - "Caroline Kraabel"
    - "Rowland Sutherland"
  - - "Dave Tucker"
    - "Hannah Marshall"
    - "Jackie Walduck"
  - - "John Russell"
    - "Masaharu Shoji"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2020
---

## ‘The Afternoon Sessions’

Presenting some of the finest in free improvisation and now entering our 30th year in an unbroken series of monthly concerts

We are delighted to start this our 30th anniversary year with a programme reflecting the breadth of influences to be found from within the free improvising world.
<!--more-->
### {{<artist "Caroline Kraabel">}} (sax) & {{<artist "Rowland Sutherland">}} (flute)

**![](https://www.mopomoso.com/wp-content/uploads/2020/01/rowland-Caroline-duo.png)**

{{<artist "Caroline Kraabel">}} came to London from Seattle as a teenager, just too late to realise her punk dreams and instead discovering the saxophone, street performance and busking. There were ideas about freedom in the air. London’s vibrant improvised music scene and its many great musicians gave her opportunities to explore extended techniques (especially the use of voice with the sax) and to spend time thinking about acoustics and the interactions of electricity and music: reproduction, synthesis, and their implications.  
She is committed to improvisation as a way of living and working, making music in unexpected ways and places (Taking a Life for a Walk; Going Outside) but also composing and playing written music (Mass Producers and Saxophone Experiments in Space for large groups, and many pieces for smaller groups). For many yearsCaroline has been playing with and conducting the London Improvisers Orchestra.  
{{<artist "Rowland Sutherland">}} enjoys an international career in many different fields of music. He regularly performs in new music and classical ensembles, jazz groups, symphony orchestras, various non-Western groups, pop outfits and as a soloist. Many of Rowland’s solo contemporary flute performances have been broadcast on BBC Radio 3, London. He has composed and arranged music for groups, ensembles and for the BBC.

### {{<artist "Dave Tucker">}} (guitar), {{<artist "Hannah Marshall">}} (cello) & {{<artist "Jackie Walduck">}} (vibes)

**![](https://www.mopomoso.com/wp-content/uploads/2020/01/dave-tucker-trio.png)**

{{<artist "Dave Tucker">}} began performing in the punk movement of the 70’s in Manchester, his first release recorded in 1978 with Mellotron. In the 80’s he was a member of “The Fall” touring with them and recording “Slates” as well as various John Peel sessions. After moving to London in the mid – 80’s he studied and played with Philipp Wachsmann and with members of the London scene including Dudu Pukwana, Andy Sheppard, Nick Evans, Johnny Dyani &amp; John Stevens.  
A member of the Alan Tomlinson Trio since 1992 by this time he was also performing with many leading international musicians, including Keith Tippett, Otomo Yoshide, Charles Hayward, Barre Phillips, Lol Coxhill &amp; Dietmar Diesner. and touring internationally.
As a member of Scatter with Pat Thomas, Phil Minton and Roger Turner he appeared at 99 Uncool Festival in Switzerland and recorded sessions for BBC radio. Currently playing relationships include a trio with Louis Moholo &amp; Francine Luce, a duet with John Butcher, his own quartet “School of Velocity” with Evan Parker, John Edwards and Steve Noble and playing guitar and conducting the London Improvisers Orchestra and Winkhaus with performance artist Anna Homler &amp; Adrian Northover.
{{<artist "Hannah Marshall">}} is a cellist who is continuing to extract, invent, and exorcize as many sounds and emotional qualities from her instrument as she can. She has been a regular member of Alexander Hawkins’ Ensembles and has toured in Europe and South America with Luc Ex and Veryan Weston’s ensembles – SOL 6 &amp; 12. She plays with ‘String Terrorists’ – Barrel (a trio with Violinist Alison Blunt &amp; Violist/poet Ivor kallin). And has been invited by Fred Frith and Suichi Chino in their residencies at café Oto. She also plays with Terry Day, Tim Hodgkinson, Roger Turner, Paul May, Kay Grant, and the London Improvisers Orchestra.  
{{<artist "Jackie Walduck">}} is a composer and vibraphone player whose work explores the interface between composition and improvisation. Her PhD Role-Taking in Free Improvisation and Collaborative Composition was completed in 2008 and forms the basis of a lifetime’s professional work and research. She works, through improvisation, with classical, contemporary and jazz musicians, and collaborates with dancers and film-makers to create new work. Her film score for The Dress was premiered at Cannes in 2007. Recent collaborations have been with Kala Ramnath, Amjad Ali Khan and musicians from Shivanova. She leads the New Music Ensemble Ignite.  
Jackie trained at Guildhall School of Music and Drama, and has been invited to lead creative workshops for many of the UK’s leading music organisations – London Sinfonietta, Birmingham Contemporary Music Group, The Academy of St Martin in the Fields. She established Ignite Ensemble with Wigmore Hall Learning, and leads their residency at the Hall working in a range of informal settings. Her work for the British Council in Oman (2001-3) helped develop creative music learning in schools, engaging professional Omani musicians to work with children. She teaches at the Royal Academy of Music and examines their Music in Community course. She engages musicians of all abilities in improvisation and collaborative composition, and was a Lead Composer on Sound and Music’s Listen Imagine Compose research project. Beyond schools based work, she has special interests in working with homeless adults, and young people with Autistic Spectrum Disorders.

### {{<artist "John Russell">}} (guitar) & {{<artist "Masaharu Shoji">}} (reeds and woodwind)

**![](https://www.mopomoso.com/wp-content/uploads/2020/01/Screenshot-2020-01-10-at-20.05.07.png)**

{{<artist "Masaharu Shoji">}} (saxophonist): Born in Hiroshima, Japan in 1957. He studied under Keizo Inoue from the age of 18. In 1992, he debuted at Russia Jazz Festival. It is characterized by a Japanese -style “interval” and a sound full of emotion. He is a natural improvisation musician although the flow of free jazz is played. He loves nature and is always getting inspiration from it. He has been in an organic farming village for more than 20 years and has been interacting with people through performances. He has been engaged in a wide variety of local and international artists (art, film, drama, Butoh, poetry, natural sounds, etc.) and has been in and around. (from Japanese)  
A leading figure in the UK improvised music scene {{<artist "John Russell">}} is internationally respected for his guitar playing and continuing work within the field. He initiated and is responsible for running Mopomoso which, apart from hosting the UK’s longest unbroken concert series of freely improvised music, also organises special events and workshops. In 2016 he co-founded Weekertoft, a label specialising in high quality releases of improvised music, with Dublin based pianist Paul G Smyth.  
*‘He finds new tones in the same place, new relationships in the same gesture. A second trip across the fingerboard is always a different excursion. The harmonic is a transparent sound: silence and ambient sound pass through it. It accounts for Russell’s unhurried pace and his sense of order, even when he’s playing fast: there’s simply so much going on.’ Stuart Broomer – Point of Departure*

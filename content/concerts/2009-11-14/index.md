---
title: 15 November 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-11-15T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "John Russell"
  - "Roger Turner"
  - "Trio Filario Farinoppa"
  - "Claudia Ulla Binder"
  - "Alex Maguire"
  - "Simon Picard"
  - "Lawrence Casserley"
  - "Adam Linson"
sets:
  - - "John Russell"
    - "Roger Turner"
  - - "Trio Filario Farinoppa"
  - - "Claudia Ulla Binder"
  - - "Alex Maguire"
    - "Simon Picard"
  - - "Lawrence Casserley"
    - "Adam Linson"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2009
---

As our November concerts coincide with The London Jazz Festival we always make an extended concert.

The line up for this one was: {{<artist "John Russell">}} (guitar) and {{<artist "Roger Turner">}} (percussion). {{<artist "Trio Filario Farinoppa">}} (from Italy) - {{<artist "Claudia Ulla Binder">}} (piano) from Switzerland. {{<artist "Alex Maguire">}} (piano) and {{<artist "Simon Picard">}} (saxophone). {{<artist "Lawrence Casserley">}} (laptop) and {{<artist "Adam Linson">}} (bass).
### {{<artist "Lawrence Casserley">}} & {{<artist "Adam Linson">}}
{{<youtube ot1h-t3_SBk>}}
### {{<artist "Alex Maguire">}} & {{<artist "Simon Picard">}}
{{<youtube aEDX_m_sq6Q>}}
### {{<artist "Claudia Ulla Binder">}}
{{<youtube BrOfuhzEGJs>}}
### {{<artist "Trio Filario Farinoppa">}}
{{<youtube ELr_qVtwQt4>}}
### {{<artist "John Russell">}} & {{<artist "Roger Turner">}}
{{<youtube 1bvkQv_TSwI>}}

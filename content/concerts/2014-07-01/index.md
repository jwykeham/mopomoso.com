---
title: 17 - 19 August 2014
categories:
  - Fête Quaqua
date: "1970-01-01T00:00:00Z"
concert_date: 2014-08-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors:
  - 2014-08-17T14:00:00+01:00
  - 2014-08-18T20:00:00+01:00
  - 2014-08-19T20:00:00+01:00
concert_series: Fête Quaqua
costs:
  - key: Single Concert
    value: £8/£5 concessions
  - key: Three day pass
    value: £20/£15 concessions
artists:
  - "JJ Duerinckx"
  - "Alice Eldridge"
  - "Carl Ludwig Hubsch"
  - "Ken Ikeda"
  - "Adam Linson"
  - "Edward Lucas"
  - "Rachel Musson"
  - "John Russell"
  - "Paul G Smyth"
  - "Ståle Liavik Solberg"
  - "Alex Ward"
  - "Ute Wassermann"
year: 2014
---

# FÊTE QUAQUA 2014

Since it’s inception in 1982 guitarist John Russell's Fete Quaqua has brought together different musicians from around the world to share in a programme of free improvisation. Although some of the musicians have never played together before there are always existing collaborative strands to be found within the larger group. It is this development of existing traditions alongside the juxtaposition of previously divergent elements that Russell believes provide a fertile base for the musician’s improvisations. Each night starts and finishes with a short piece from the whole ensemble with different smaller combinations in between. Each night is different and all the musicians will be playing on all three nights.

## The Musicians

### {{<artist "JJ Duerinckx">}} (saxophone) Belgium

![JJ Duerinckx](https://www.mopomoso.com/wp-content/uploads/2014/07/jj-jp.jpg "JJ Duerinckx")

Following an initial inspiration from Steve Lacy and meetings with Lol Coxhill and Michel Doneda JJ Duerinckx decided to specialise on using the sopranino saxophone in freely improvised music. He is an instigating member of the concept One Moment Free Improv… a set of events around free improvisation in Brussels and is also a sound artist mainly influenced by Murray Schaffer. He also plays the baritone saxophone in alternative/experimental and free jazz bands. [Hear examples of his work on Soundcloud](https://soundcloud.com/duerinckx-jj "opens in new window/tab").

### {{<artist "Alice Eldridge">}} (cello) UK

![](https://www.mopomoso.com/wp-content/uploads/2014/07/alice-jp.jpg "alice jp")

Depending upon who is asking, Alice Eldridge is an improvising cellist, producer or researcher in biologically-inspired systems and sound. She has a PhD in cybernetically-inspired software for improvisation from University of Sussex, where she lectures in Generative Creativity. Following a few years performing with her solo cello-laptop AV duo Alice and her Self-Karaoke Machines, she presently favours improvising with humans: as one quarter of PRSF Women Make Music awardees Collectress, as one half of Duot with bass player Seth Bennett and as a vibrant member of Brighton’s Safehouse Collective. She has just returned from an ecological research project exploring soundscape as a proxy for habitat status in the Ecuadorian Cloud Forest.[ https://www.ecila.org/index.html](https://www.ecila.org/index.html)

### {{<artist "Carl Ludwig Hubsch">}} (tuba) Germany

![](https://www.mopomoso.com/wp-content/uploads/2014/07/carl-l-jp-e1407686940425.jpg "carl l jp")

“My playing is focused on music as a structure in time. All focus is on the genesis of the moment. While emphasis and plot are fragmented and given the freedom of a new point of departure, utmost care is given to awareness of musical flow and continuity of the play. ?Through the use of avant-garde and self-invented performance techniques, the tuba acquires completely new characteristics as a brass instrument. An innovative array of unexpected sounds is heard, the instrument is seen from a fresh perspective, and the audience is confronted with a novel way of perceiving time.” [www.clhuebsch.de ](<https://www.clhuebsch.de >)

### {{<artist "Ken Ikeda">}} (electronics) Japan

![](https://www.mopomoso.com/wp-content/uploads/2014/07/ken-jp.jpg "ken jp")

Ken Ikeda is a video artist and composer born in Tokyo (1964). He has exhibited sound art and visual installations around the world and has collaborated with, amongst others, painter Tadanoori Yokoo, artist Mariko Mori and composed and recorded for film maker David Lynch. He featured as part of Sonic Boom, at the Hayward Gallery, London in 2000 and as part of the Royal Academy’s ‘Apocalypse’ exhibition in 2001.[ https://www.last.fm/music/Ken+Ikeda ](<https://www.last.fm/music/Ken+Ikeda >)

### {{<artist "Adam Linson">}} (bass/electronics) Canada

![](https://www.mopomoso.com/wp-content/uploads/2014/07/adam-jp.jpg "adam jp")

Adam Linson is a double bassist, improvisor, and composer, who also designs, develops, and peforms with real-time interactive computer music systems. He performs regularly on the double bass, acoustically and with live electronics, as a soloist and in a wide variety ofensembles. Born in Los Angeles, he has been active on the double bass and with information technology since age 11. He studied music under George Lewis and Bertram Turetzky at the University of California, San  
Diego, where he performed jazz, improvised, and twentieth-century orchestral music, and completed a BA in philosophy. From 1999-2009 he was based in Berlin, Germany, where he began working in diverse improvising ensembles in Europe and North America, and also began his sustained involvement with interactive computer music. He has composed music for international contemporary dance productions, and had two residencies at STEIM, Amsterdam. Since 2010 he divides his time between Canada and the UK.[ www.percent-s.com](https://www.percent-s.com)

### {{<artist "Edward Lucas">}} (trombone) UK

![](https://www.mopomoso.com/wp-content/uploads/2014/07/edward-jp.jpg "edward jp")

I have known the trombone since my school days, but I came to improvisation later in life as a way of getting back into playing after a substantial break. It is now almost exclusively what I do, and I can thank the numerous musicians I have had the opportunity to play with. This has included groupings together with Tom Wheatley, Daniel Thompson, Eddie Prévost, Roland Ramanan, and James O’Sullivan, among many others. I also have a long running duo with Slovakian analogue synthesist Daniel Kordik. Our collaboration together extends to running the Earshots concert series and label, which we have been developing over the past year.[ https://kordiklucas.wordpress.com/](https://kordiklucas.wordpress.com/)

### {{<artist "Rachel Musson">}} (saxophone) UK

![](https://www.mopomoso.com/wp-content/uploads/2014/07/rachel-jp.jpg "rachel jp")

Involved with a variety of improvisation- based projects, including a trio with Liam Noble and Mark Sanders. Also a trio with Danish saxophonist Julie Kjaer and cellist Hannah Marshall, and a duo with bassist Olie Brice. She is a member of clarinetist Alex Ward?s new quintet and Eddie Prevost?s Atmospheric Disturbance She has also written for and recorded with her own band, Skein, which released a highly acclaimed album on F-IRE Records at the end of 2010. She was picked by BBC Jazz on Three to perform at Cheltenham Jazz Festival last year and has performed with a.o Alcyona Mick, Han Bennink, Liam Noble, Gail Brand, Eddie Prevost, Olie Brice, Federico Ughi, Mary Halvorson, John Russell, Adam Linson, Seb Rochford.[ https://rachelmusson.com/ ](<https://rachelmusson.com/ >)

### {{<artist "John Russell">}} (guitar) UK

![](https://www.mopomoso.com/wp-content/uploads/2014/07/jr-jp.jpg "jr jp")

John Russell got his first guitar in 1965 while living in Kent and began to play in and around London from 1971 onwards. An early involvement with the emerging free improvisation scene (from 1972) followed, seeing him play in such places as The Little Theatre Club, Ronnie Scott’s, The Institute of Contemporary Arts, The Musicians’ Co-Op and the London Musicians’ Collective. From 1974 his work extended into teaching, broadcasts (radio and television) and touring in the United Kingdom and, ever extensively, in other countries around the world . He has played with many of the world’s leading improvisers and his work can be heard on over 50 CDs. In 1981, he founded QUAQUA, a large bank of improvisers put together in different combinations for specific projects and, in 1991, he started MOPOMOSO which has become the UK’s longest running concert series featuring mainly improvised music. [www.john-russell.co.uk ](<https://www.john-russell.co.uk >)

### {{<artist "Paul G Smyth">}} (piano) Ireland

![](https://www.mopomoso.com/wp-content/uploads/2014/07/paul-g-jp.jpg "paul g jp")

Paul G. Smyth (b.1976) is a musician, artist and designer, based in Dublin. A member of 9-piece band The Jimmy Cake (described by Leagues O’Toole in the Irish Times as “the most powerful musical force in Ireland”), Smyth is also Ireland’s foremost free-improvising pianist. After studying fine-art painting in the National College of Art and Design, Smyth toured with New York free-jazz veteran Charles Gayle, and since then has played with such luminaries as Derek Bailey, Evan Parker, Keiji Haino, Barry Guy, John Russell and Damo Suzuki, been a featured composer in the National Concert Hall, written music for theatre, and performed in 17 countries. He was a founding member of the artist-run collective The Whispering Gallery, which was set up for the promotion of new music from both Ireland and abroad.

Other activity has included an exhibition of works on paper in Roundstone, Connemara, and the premiere of electroacoustic work, ‘The Dry Land’, at the Sligo New Music Festival 2006 and as a member of synth trio Boys Of Summer (“the sound of John Carpenter being buried alive” – Le Cool Magazine)[ https://www.paulgsmyth.com/ ](<https://www.paulgsmyth.com/ >)

### {{<artist "Ståle Liavik Solberg">}} (drums/percussion) Norway

![](https://www.mopomoso.com/wp-content/uploads/2014/07/stale-jp.jpg "stale jp")

Drummer and percussionist Ståle Liavik Solberg has established himself as strong voice in the field of improvised music. The duo Motsol and other collaborations with vocalist Stine Janvin Motland are important, but he also works regularly with musicians like Frode Gjerstad, Joe Williamson, Fred Lonberg-Holm, Steve Beresford, David Stackenäs, John Russell, Nils Henrik Asheim, Per Zanussi and Martin Küchen to name a few.

He has a masters degree in improvisation from the Norwegian Academy of Music and has a passionate interest in the combination of improvised music in relation to other art forms, which includes being in Inbal Pinto &amp; Avshalom Pollak Dance Company’s “Trout”. Solberg is one of the driving forces behind the concert series Blow Out! in Oslo, and he curates the festival of the same name together with Paal Nilssen-Love. <https://soundcloud.com/stllvkslbrg >

### {{<artist "Alex Ward">}} (clarinet) UK

![](https://www.mopomoso.com/wp-content/uploads/2014/07/alex-w-jp-e1407687076390.jpg "alex w jp")

Born in 1974, composer, improviser, and performing musician Alex Ward’s primary instruments are clarinet and guitar. His involvement in free improvisation dates back to 1986, when he met the guitarist Derek Bailey. As an improviser, he was initially principally a clarinettist, but since 2000 he has also been active as an improvising guitarist. On both instruments, hIs longest-standing collaborations in this field have been with the drummer Steve Noble. From 1993 to 2001, most of his activity as a composer took place in collaboration with Benjamin Hervé, mainly in the context of the rock band Camp Blackfoot. From 2002-2005, his writing was mostly solitary, and primarily focused on songs. Since 2006, he has been heavily involved in both solo and collaborative composition, predominantly of instrumental music. Much of his writing and performing during this time has been done with Dead Days Beyond Help, a duo with drummer Jem Doulton. He also currently leads a number of bands including Predicate, Forebrace, The Alex Ward Quintet /Sextet and Alex Ward and The Dead Ends.[ https://sites.google.com/site/alexwardmusician/home ](<https://sites.google.com/site/alexwardmusician/home >)

### {{<artist "Ute Wassermann">}} (voice) Germany

![](https://www.mopomoso.com/wp-content/uploads/2014/07/ute-w-jp-e1407687146101.jpg "ute w jp")

Ute Wassermann is a composer/performer, improviser and interpreter of contemporary music. She studied at the Hamburg Academy of Fine Arts with a,o Henning Christiansen, specialising in sound installation / vocal performance, and studied classical singing w. Carol Plantamura (San Diego) and Arnold van Mill (Hamburg). Since 1984 she has developed many multivoiced vocal techniques, catalogued by register, timbre and articulational sequences, which may be deconstructed and/or superimposed and used to explore spatial resonance phenomena. She has also worked extensively with various extensions of the voice including live electronics and the use of gongs and metal coils as vocal resonators. She has given many performances of her own solo work and works regularly with many improvising musicians, in venues ranging from international festivals (Japan, Australia, Hongkong, Buenos Aires) to lofts.[ https://femmes-savantes.net/en/les-femmes-savantes/ute-wassermann ](<https://femmes-savantes.net/en/les-femmes-savantes/ute-wassermann >)

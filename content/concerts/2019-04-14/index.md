---
title: 21 April 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-04-21T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Yoni Silver"
  - "Joshua Weitzel"
  - "Martin Speake"
  - "Alyson Cawley"
  - "Steve Beresford"
  - "Pascal Marzan"
  - "Phil Minton"
  - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Yoni Silver
    - Joshua Weitzel
  - - Martin Speake
    - Alyson Cawley
  - - Steve Beresford
    - Pascal Marzan
  - - Phil Minton
    - John Russell
year: 2019
---
Four world class duos showcasing the diversity and excellence found in free improvisation
<!--more-->
### {{<artist "Yoni Silver">}} & {{<artist "Joshua Weitzel">}}

![](https://www.mopomoso.com/wp-content/uploads/2019/04/joni-and-joshua.png)

Mixing music and action, sound and silence two performers who share an interest in the construction of music in real time

### {{<artist "Martin Speake">}} & {{<artist "Alyson Cawley">}}

![](https://www.mopomoso.com/wp-content/uploads/2019/04/martin-and-alyson.png)

A duo versed in jazz but who also take their music into to the exploration of world rhythms, they here bring these shared experiences to bear on free improvisation.

### {{<artist "Steve Beresford">}} & {{<artist "Pascal Marzan">}}

![](https://www.mopomoso.com/wp-content/uploads/2019/04/steve-and-pascal.jpg)

A fine pairing of two Mopomoso favourites, they have worked together over a number of years and show a keen understanding of each other’s musicality enabling them to allow each other the opportunity to give full expression to the unfolding music.

### {{<artist "Phil Minton">}} & {{<artist "John Russell">}}

![](https://www.mopomoso.com/wp-content/uploads/2019/04/maxresdefault.jpg)

This duo have been performing for many years and have appeared around the world presenting their dynamic and engaging music on a variety of platforms to much acclaim. A clip of a previous Vortex performance went viral reaching over 370,000 views on Facebook alone. Don’t miss them while you have the chance!

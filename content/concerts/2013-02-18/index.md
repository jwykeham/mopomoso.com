---
title: 17 March 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-03-17T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Guillermo Torres"
  - "Marcio Mattos"
  - "Adrian Northover"
  - "Gianni Mimmo"
  - "Gianni Lenoci"
  - "Christiano Calcagnile"
  - "Mark Sanders"
  - "Alison Blunt"
  - "Elaine Mitchener"
sets:
  - - Guillermo Torres
    - Marcio Mattos
    - Adrian Northover
  - - Gianni Mimmo
    - Gianni Lenoci
    - Christiano Calcagnile
  - - Mark Sanders
    - Alison Blunt
    - Elaine Mitchener
year: 2013
---

### {{<artist "Guillermo Torres">}} (flgl/tpt), {{<artist "Marcio Mattos">}} (cello) & {{<artist "Adrian Northover">}} (sax)

{{<youtube W5giyQATPzs>}}

### Reciprocal Uncles – {{<artist "Gianni Mimmo">}} (sop sax), {{<artist "Gianni Lenoci">}} (pno) & {{<artist "Christiano Calcagnile">}} (drms)

{{<youtube qBaeS4fl6pA>}}

### {{<artist "Mark Sanders">}} (drms), {{<artist "Alison Blunt">}} (vln), {{<artist "Elaine Mitchener">}} (voc) & John Russell (gtr)

{{<youtube SADmfoP5UsU>}}

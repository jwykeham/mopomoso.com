---
classic-editor-remember:
  - classic-editor
title: 15 November 2020
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2020-11-15T14:00:00Z
concert_series: Afternoon Sessions
venue: YouTube
doors: 2pm
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2020
---
Premiere of episode six from Mopomoso TV featuring improvised music from around the world. Come and join us in the chat room
  
{{<youtube JcUvMZwKnuE>}}
  

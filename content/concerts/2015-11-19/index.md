---
title: 20 December 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-12-20T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Xmas Xtravaganza
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Russell"
year: 2015
---
The Mopomoso Xmas Xtravaganza is a cross between a party and a celebration of all things improvised, celebrating the music in a series of 5 – 10 minute cameo sets:

### Lynch / Thompson

{{<youtube X6jI4nY44GQ>}}

### Nortey / Ren / Bennett

{{<youtube ToGwb8G9JE>}}

### {{<artist "Martin Vishnick">}}
{{<youtube nWmG_MFOg-Y>}}

### Casserley / Hackett

{{<youtube FVz1TsQTGAY>}}

### Stoddart / Ross

{{<youtube HMDtck8MnO8>}}

### Hallett / Hutchinson / Sands / Wang / Pignon

{{<youtube B8Zg8xo3OdM>}}

### Bohman / Northover

{{<youtube JYQP7bxGpq0>}}

### Eastley / Beresford / Solberg

{{<youtube r-c0kvMwDcU>}}

### Mackness / Minton / Blunt / Bennett

{{<youtube 9-af8AipJn0>}}

### Brie / Gurung / Day

{{<youtube 7nJ4WfV_G9g>}}

### Chapman / West / Hutchinson

{{<youtube Os3wwz22jQo>}}

### Panton / Day

{{<youtube bgUyB5q2XYc>}}

### Grant / Weston

{{<youtube TBMFVpg5nY0>}}

### Chen / Wang / Hallett / Morris / Russell

{{<youtube rsdEW9fRZK8>}}

### Tomlinson / Wilkinson / Tucker / Solberg

{{<youtube 96JuN3Y0sUA>}}

### The Mopomoso Team (Chondros/ Eyles/ Grant/ Russell/ Tucker)

{{<youtube j51bIPctyRY>}}

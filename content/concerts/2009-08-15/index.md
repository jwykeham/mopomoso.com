---
title: 16 August 2009
categories:
  - Fete QuaQua
date: "1970-01-01T00:00:00Z"
concert_date: 2009-08-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: "Afternoon Sessions"
artists:
  - "Sabu Toyozumi"
  - "Luo Chao yun"
  - "John Russell"
  - "Pat Thomas"
  - "keyboards"
  - "Lol Coxhill"
  - "John Butcher"
  - "Jean Borde"
  - "Ute Voelker"
  - "Satoko Fukuda"
  - "Angelika Sheridan"
  - "Shabaka Hutchins"
  - "Hannah Marshall"
  - "Hennry Lowther"
sets:
  - - John Butcher
    - Pat Thomas
    - Angelika Sheridan
    - Jean Bordé
  - - Lol Coxhill
    - Hannah Marshall
    - Luo Chao-yun
    - Sabu Toyozumi
  - - Henry Lowther
    - Ute Voelker
    - Jean Borde
    - Luo Chao-yun
    - Shabaka Hutchings
  - - John Russell
    - Lol Coxhill
    - Satoko Fukuda
    - Ute Völker
  - - Pat Thomas
    - Henry Lowther
    - John Butcher
    - Shabaka Hutchings
    - Hannah Marshall
  - - Angelika Sheridan
    - John Russell
    - Shabaka Hutchings
    - Hannah Marshall
    - Sabu Toyozumi
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2009
---
## FETE QUAQUA (Day one)

{{<artist "Sabu Toyozumi">}} (Japan) percussion, {{<artist "Luo Chao yun">}} (Taiwan) pipa, {{<artist "John Russell">}} (UK) guitar, {{<artist "Pat Thomas">}} (UK) {{<artist "keyboards">}}, {{<artist "Lol Coxhill">}} (UK) saxophone, {{<artist "John Butcher">}} (UK) saxophones, {{<artist "Jean Borde">}} (France) bass, {{<artist "Ute Voelker">}} (Germany) accordion, {{<artist "Satoko Fukuda">}} (UK) violin, {{<artist "Angelika Sheridan">}} (Germany) flutes, {{<artist "Shabaka Hutchins">}} (UK) saxophone and clarinet, {{<artist "Hannah Marshall">}} (UK) cello, {{<artist "Hennry Lowther">}} (UK) trumpet.

{{<artist "John Russell">}}, brought together another hand picked group of leading international improvisers from the worlds of jazz, experimental, classical and avant-guard music in his three day annual music festival – Fête Quaqua 2009. The festival featured both ensemble and smaller groupings to explore new collaborations over the three days

Sets in order of performance. Please allow time to download.

### Tutti
{{<youtube S1oL7xOBH1A>}} 
### {{<artist "John Butcher">}}, {{<artist "Pat Thomas">}}, {{<artist "Angelika Sheridan">}} & {{<artist "Jean Bordé">}}
{{<youtube jKtEJ0bGsiI>}} 
### {{<artist "Lol Coxhill">}}, {{<artist "Hannah Marshall">}}, {{<artist "Luo Chao-yun">}} & {{<artist "Sabu Toyozumi">}}
{{<youtube Is3wkisk3Ns>}}
### {{<artist "Henry Lowther">}}, {{<artist "Ute Voelker">}}, {{<artist "Jean Borde">}}, {{<artist "Luo Chao-yun">}} & {{<artist "Shabaka Hutchings">}}
No video available
### {{<artist "John Russell">}}, {{<artist "Lol Coxhill">}}, {{<artist "Satoko Fukuda">}} & {{<artist "Ute Völker">}}
{{<youtube m71e3M1SuVo>}}
### {{<artist "Pat Thomas">}}, {{<artist "Henry Lowther">}}, {{<artist "John Butcher">}}, {{<artist "Shabaka Hutchings">}} & {{<artist "Hannah Marshall">}}
{{<youtube TnZyZ-QRiKw>}}
### {{<artist " Angelika Sheridan">}}, {{<artist "John Russell">}}, {{<artist "Shabaka Hutchings">}}, {{<artist "Hannah Marshall">}} & {{<artist "Sabu Toyozumi">}}
No video available
### Tutti
{{<youtube 9cisWN5sgmM>}}

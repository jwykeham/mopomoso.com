---
title: 28 July 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-07-28T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Ian McGowan"
  - "Hannah Marshall"
  - "Marcio Mattos"
  - "John Rangecroft"
  - "Phil Minton"
  - "John Russell"
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Ian McGowan
    - Hannah Marshall
    - Marcio Mattos
  - - John Rangecroft
  - - Phil Minton
    - John Russell
year: 2019
---

## THE AFTERNOON SESSIONS
a continuing look into the world of free improvisation from the UK’s longest running concert series dedicated to all things free and improvised.

### {{<artist "Ian McGowan">}} (trumpet) {{<artist "Hannah Marshall">}} (cello) {{<artist "Marcio Mattos">}} (bass) trio

![](/uploads/2019/06/mmm-trio.png)

Three highly respected players generating a collective sensibility built on years of experience. The generosity of spirit and genuine love for sharing the mechanics of music making shine through. Real!

### {{<artist "John Rangecroft">}} (clarinet) solo

![](/uploads/2019/06/john-rangey.png)

A rare chance to hear John play solo and also on clarinet, rather than the saxophone Mopomoso regulars will be more familiar with. John’s material, while based in the melodic, offers an intriguing glimpse into a different narrative.

### {{<artist "Phil Minton">}} (voice) {{<artist "John Russell">}} (guitar) duo

![](/uploads/2019/06/maxresdefault-1.jpg)Not much to say about these two! One Youtube video, recorded at a Mopomoso session at the Vortex went viral on a Thai tourist site with over 370,000 and over 3,500 shares. Vocal chords meet guitar strings in an engaging immediacy we guarantee you won’t have heard before!

---
title: 17 June 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-06-17T14:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £6
artists:
  - "John Eyles"
  - "Petri Huurinainen"
  - "Emmanuelle Waeckerlé"
  - "Dave Tucker"
  - "Matt Hutchinson"
  - "Andrew Lisle"
  - "Henry Lowther"
  - "John Russell"
sets:
  - - John Eyles
    - Petri Huurinainen
    - Emmanuelle Waeckerlé
  - - Dave Tucker
    - Matt Hutchinson
    - Andrew Lisle
  - - Henry Lowther
    - John Russell
year: 2018
---
A continuing series featuring some of the best from the world of free improvisation
### Bouche Bée – {{<artist "John Eyles">}} (alto saxophone), {{<artist "Petri Huurinainen">}} (guitar) & {{<artist "Emmanuelle Waeckerlé">}} (voice)
![](https://www.mopomoso.com/wp-content/uploads/2018/06/bouchebeetrio3.jpg)
Bouche Bée (the name is a French expression meaning “open-mouthed” or “speechless”) was formed in 2005 by vocalist Emmanuelle Waeckerlé and guitarist Petri Huurinainen. Music-writer and saxophonist John Eyles joined in the Spring of 2017. Three individual voices listening and responding together shape Bouche Bée music.

In the summer of 2015, they met while rehearsing to perform Cornelius Cardew’s *The Great Learning* at Union Chapel in Islington.  They also attend Eddie Prevost's Friday evening improv workshop.

Emmanuelle is a London-based interdisciplinary artist, writer and performer. She is a Reader in photography and relational practices at University for the Creative Arts in Farnham. She is a member of MoWo (Mopomoso Workshop group). In 2017 her double CD *Ode (owed) to O* was released by edition wandelweiser records. [www.ewaeckerle.com](https://www.ewaeckerle.com/)

Petri is a Finnish musician and artist currently living in London. He works with live based sounds, extended-guitar performances, and drawings. His limited edition 12″ vinyl record *Exile Nothing* came out in 2016 and is available from Rough Trade. <https://www.phuurinainen.com/>

John was a founder member of the Mopomoso Workshop and is a member of MoWo. He is also in the thirteen-strong London Experimental Ensemble which performed Cardew's *Treatise* at Iklectik in January 2017, the recording of which was released by Split Rock records in March 2018.

### {{<artist "Dave Tucker">}} (bass), {{<artist "Matt Hutchinson">}} (piano) & {{<artist "Andrew Lisle">}} (drums)
![](https://www.mopomoso.com/wp-content/uploads/2018/06/dave-tucker-trio.jpeg)
Trio leader Dave Tucker’s first recorded release was in 1978 with Mellatron and in the early 80’s he was a member of “The Fall” touring and recording. He went on to experiment with other musics and, after moving to London in the mid – 80’s he studied &amp; performed with Phil Wachsmann. Since 1992 a member of the Alan Tomlinson trio he has performed with many musicians from around the world and is a valued contributor to The London Improvisers Orchestra and Mopomoso.

A member of The Sidewinders, inc. Marc Charig backing Stevie Wonder’s 2nd British tour to the Mike Osbourne quartet c. 1967/68 Matt ’s earliest excursions into free improvisation also came around this time. The early 70’s was mostly spent composing and playing for Jazz groups in and around London. Meeting Richard Beswick introduced another side of free improvisation in the forms of Tony Wren, Phil Wachsmann and Bead records. In the early 80’s met John Butcher in one of John Williams’s jazz ensembles. Further collaborations with Phil Wachsmann and also Chris Burn dominated a lot of his activities in the '90s,presenting opportunities to play in Europe and N.America and to work in bands such as King Ubu Orchestra, various permutations of the Chris Burn Ensemble collaborating with the likes of Lol Coxhill, Derek Bailey and Evan Parker, Also performed with the Mhu Mhu Orchester with Pauls Lytton and Rutherford and Johannes Bauer and was a regular performer at the LMC, the Red Rose Club and the Klinker club. Towards the end of the decade appeared in the London Contemporary Piano Festival in 1998 alongside Stan Tracy, Michael Finnissey, Mervn Afrika, Rene Reznek.The new millennium saw a continuation with Ensemble as well as work with smaller groups particularly duos with Chris B and Phil W and subsequent albums with both.

Andrew Lisle is a drummer from Northumberland, UK. In 2008 he began studying Jazz at Leeds College of Music discovering a passion and talent for free improvisation. While there he co-founded *Shatner’s Bassoon.*  To date the band have recorded two studio albums (and have toured the UK extensively. After graduating in 2011 he moved to Lisbon, Portugal where he spent much of his time at Clean Feed Records playing with a.o Rodrigo Pinheiro (Red Trio), Hernani Faustino (Red Trio), Rodrigo Amado and Luis Lopes. Moving to London in 2013 he has played with a.o Colin Webster, John Edwards, John Dikeman, Alex Ward, Dirk Serries, Alan Wilkinson, Matthew Bourne, Rachel Musson and Mark Sanders.



### {{<artist "Henry Lowther">}} (trumpet) & {{<artist "John Russell">}} (guitar)
![](https://www.mopomoso.com/wp-content/uploads/2018/06/john-and-henry.jpeg)
Quite simply one of the best trumpet players in the World today Henry Lowther has played with everyone and everywhere from The Ronnie Scott group to the Woodstock festival. A prolific arranger and gifted composer he also leads his own group Still Waters. His duo with Mopomoso founder Russell is based on a deep understanding built up over a number of years of playing both in a duo and in a trio with violinist Satoko Fukuda.<span class="Apple-converted-space"> </span>

Here’s a duo set from two years ago

{{<youtube yjTpkr3vqC0>}}

---
title: 20 May 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-05-20T14:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Sylvia Hallett"
  - "Ian McGowan"
  - "Kay Grant"
  - "Caroline Kraabel"
  - "John Russell"
  - "Stale Liavik Solberg"
  - "Alex Ward"
sets:
  - - Sylvia Hallett
    - Ian McGowan
  - - Kay Grant
    - Caroline Kraabel
  - - John Russell
    - Stale Liavik Solberg
    - Alex Ward
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2018
---

The very best from the world of improvised music in the intimate setting of "London's listening Jazz club"

### {{<artist "Sylvia Hallett">}} (violin) & {{<artist "Ian McGowan">}} (trumpet)

![sylvia and ian](https://www.mopomoso.com/wp-content/uploads/2018/04/sylvia-and-ian.jpg)

Two much loved figures on the London free improvisation scene both Sylvia and Ian display a real love of play within their music. Sometimes letting the music run ahead, sometimes reacting in a split second, they maintain a constant creative vigilance and a deep sense of musicality. Delighted they can return to play a set for us!

### {{<artist "Kay Grant">}} (voice) & {{<artist "Caroline Kraabel">}} (sax)

![kay and caroline](https://www.mopomoso.com/wp-content/uploads/2018/04/kay-and-caroline.jpeg)

Both Kay Grant and Caroline Kraabel have worked consistently for improvised music (Kay as a Mopomoso volunteer and Caroline in The London Improvisers Orchestra) and this dedication is implicit in their music. A commitment to sharing the moment and following the path suggested by the music in a very direct way that is also bound up in a genuine sense of fun.

### {{<artist "John Russell">}} (guitar), {{<artist "Stale Liavik Solberg">}} (percussion) & {{<artist "Alex Ward">}} (clarinet)

![russell solberg ward](https://www.mopomoso.com/wp-content/uploads/2018/04/russell-solberg-ward.jpeg)

How come this grouping has never happened before? The three musicians have worked together in other highly regarded combinations around the world but this is the first time they have played as a trio. Sometimes something that seems so right can be staring you in the face for a long time before the logic kicks in. This should be a cracker!

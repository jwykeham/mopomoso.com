---
title: "15 September 2024"
date: 2024-04-24T10:59:18+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-09-15 00:00:00 +0100 BST
concert_series: Mopomoso Live
year: 2024
sets:
- - LUFT
- - David Ryan
  - Dominic Lash
- - Andrew Ciccone
  - Gina Southgate
  - Ivor Kallin
  - Bettina Schroeder
artists:
- LUFT
- Chris Hill
- James O'Sullivan
- Alan Newcombe
- David Ryan
- Dominic Lash
- Andrew Ciccone
- Gina Southgate
- Ivor Kallin
- Bettina Schroeder
---

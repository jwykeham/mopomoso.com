---
title: 19 January 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-01-19T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Nicola Guazzaloca"
  - "Hannah Marshall"
  - "Dave Tucker"
  - "Urs Leimgruber"
  - "John Russell"
sets:
  - - Nicola Guazzaloca
    - Hannah Marshall
  - - Dave Tucker
  - - Urs Leimgruber
    - John Russell
year: 2014
---

### {{<artist "Nicola Guazzaloca">}} (piano) & {{<artist "Hannah Marshall">}} (cello)

{{<youtube Hm236omSkmU>}}

### {{<artist "Dave Tucker">}} (guitar)

{{<youtube ENDIm5Al7Gs>}}

{{<youtube yK4Z8-BRQzk>}}

### {{<artist "Urs Leimgruber">}} (sax) & {{<artist "John Russell">}} (guitar)

{{<youtube n5vEaczus0o>}} 

{{<youtube e8z6il8S7X8>}} 

{{<youtube 6Hi-NjTLvZA>}}

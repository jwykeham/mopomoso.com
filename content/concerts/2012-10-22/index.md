---
title: 18 November 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-11-18T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Eun-Jung Kim"
  - "John Jasnoch"
  - "Charlie Collins"
  - "Jean Michel van Schouwburg"
  - "Adam Bohman"
  - "Jean Jacques Duerinickx"
  - "Fernando Perales"
  - "Chefa Alonso"
  - "Barbara Meyer"
  - "Cova Villages"
  - "Edoardo Marraffa"
  - "Nicola Guazzaloca"
  - "Pascal Marzan"
  - "John Russell"
sets:
  - - Eun-Jung Kim
    - John Jasnoch
    - Charlie Collins
  - - Jean Michel van Schouwburg
    - Adam Bohman
    - Jean Jacques Duerinickx
  - - Fernando Perales
  - - Chefa Alonso
    - Barbara Meyer
    - Cova Villages
  - - Edoardo Marraffa
    - Nicola Guazzaloca
  - - Pascal Marzan
    - John Russell
year: 2012
---

### {{<artist "Eun-Jung Kim">}} (komungo), {{<artist "John Jasnoch">}} (oud, banjo – uke) & {{<artist "Charlie Collins">}} (waterphone, perc)

{{<youtube 2INVfgViX-I>}}

### {{<artist "Jean Michel van Schouwburg">}} (voice), {{<artist "Adam Bohman">}} (electronics) & {{<artist "Jean Jacques Duerinickx">}} (sax)

{{<youtube QJx8ngymjFQ>}}

### {{<artist "Fernando Perales">}} (prepared guitar, electronics)

{{<youtube Rmf2zGmYc6o>}}

### MOLIMO - {{<artist "Chefa Alonso">}} (sax-percussion), {{<artist "Barbara Meyer">}} (cello), {{<artist "Cova Villages">}} (voice)

{{<youtube 03Nyx5H6j3E>}}

### {{<artist "Edoardo Marraffa">}} (sax) & {{<artist "Nicola Guazzaloca">}} (piano)

{{<youtube Kb_3XXCOhrE>}}

### {{<artist "Pascal Marzan">}} (guitar) & {{<artist "John Russell">}} (guitar)

{{<youtube c5LO0TXfKeg>}}

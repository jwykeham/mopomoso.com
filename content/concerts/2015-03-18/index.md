---
title: 19 April 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-04-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Anthony Donovan"
  - "Charlie Collins"
  - "Ian Simpson"
  - "Stephen Grew"
  - "Phillip Marks"
  - "Seth Bennett"
  - "Matt Robinson"
  - "Alison Blunt"
  - "Mark Sanders"
  - "Adam Bohman"
  - "JJ Duerinckx"
  - "Tom Jackson"
sets:
  - - Anthony Donovan
    - Charlie Collins
    - Ian Simpson
  - - Stephen Grew
    - Phillip Marks
    - Seth Bennett
    - Matt Robinson
  - - Alison Blunt
    - Mark Sanders
  - - Adam Bohman
    - JJ Duerinckx
    - Tom Jackson
year: 2015
---

### Kurouzu – {{<artist "Anthony Donovan">}} (fretless half-guitar & electronics), {{<artist "Charlie Collins">}} (percussion & metal) & {{<artist "Ian Simpson">}} (prepared lapsteel)

{{<youtube TXgM75kcYiI>}}

### The Grew Quartet – {{<artist "Stephen Grew">}} (piano), {{<artist "Phillip Marks">}} (drums), {{<artist "Seth Bennett">}} (bass) & {{<artist "Matt Robinson">}} (clarinets)

{{<youtube K4nglDDDS-8>}}

### {{<artist "Alison Blunt">}} (violin) & {{<artist "Mark Sanders">}} (drums)

{{<youtube -GCJwA2Fqd4>}}

### {{<artist "Adam Bohman">}} (electronics), {{<artist "JJ Duerinckx">}} (sax) & {{<artist "Tom Jackson">}} (clarinets)

{{<youtube TSPLKcH-6dY>}}

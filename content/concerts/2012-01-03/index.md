---
title: 15 January 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-01-15T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Alison Blunt"
  - "Dave Leahy"
  - "Tony Marsh"
  - "Caroline Kraabel"
  - "Veryan Weston"
  - "Kay Grant"
  - "Hannah Marshall"
  - "Satoko Fukuda"
  - "Henry Lowther"
  - "John Russell"
sets:
  - - Alison Blunt
    - Dave Leahy
    - Tony Marsh
  - - Caroline Kraabel
    - Veryan Weston
  - - Kay Grant
    - Hannah Marshall
  - - Satoko Fukuda
    - Henry Lowther
    - John Russell
year: 2012
---

### {{<artist "Alison Blunt">}} (violin) / {{<artist "Dave Leahy">}} (bass) / {{<artist "Tony Marsh">}} (drums, percussion)

{{<youtube 6-Rg96VGhFk>}} 

### {{<artist "Caroline Kraabel">}} (saxophone) / {{<artist "Veryan Weston">}} (piano)

{{<youtube l6SuQWcHdHo>}} 

### {{<artist "Kay Grant">}} (voice) / {{<artist "Hannah Marshall">}} (cello)

{{<youtube d0i5eBEo7ck>}} 

### {{<artist "Satoko Fukuda">}} (violin) / {{<artist "Henry Lowther">}} (trumpet) / {{<artist "John Russell">}} (guitar)

{{<youtube asOv2n0FrwY>}} 

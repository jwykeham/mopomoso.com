---
artists:
  - Phil Minton
  - Matt Hutchinson
  - John Russell
  - Caroline Kraabel
concert_date: 1995-08-30T20:00:00+01:00
concert_series: At The Red Rose
date: 1995-08-30T20:00:00+01:00
notes: []
sets: []
sources:
  - https://www.newspapers.com/image/261139122
title: 30 August 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1995
---
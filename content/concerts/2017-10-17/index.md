---
title: 12 November 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-11-12T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: London Jazz Festival
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Susanna Ferrar"
  - "Otto Willberg"
  - "Jordan Muscatello"
  - "Sonic Pleasure"
  - "Dave Tucker"
  - "Kay Grant"
  - "Adrian Northover"
  - "Bouche Bée"
  - "Emmanuelle Waeckerlé"
  - "Petri Huurinainen"
  - "John Eyles"
  - "Chris Hill"
  - "Alec Kronacker"
  - "Adam Kinsey"
  - "Stephan Barrett"
  - "Deborah Chinn"
  - "Phil Durrant"
  - "Martin Vishnick"
  - "Martin Clarke"
  - "Sam Enthoven"
  - "Michael Jaquinto"
  - "Alan Newcombe"
  - "Charlotte Keeffe"
  - "Sebastian Sterkowicz"
  - "Dave Fowler"
  - "Samuel Plackett"
year: 2017
---

## Mopomoso special as part of The London Jazz Festival


### THE WORKSHOP INVITES

![mowo](https://www.mopomoso.com/wp-content/uploads/2017/10/mowo1.jpeg)

Formed in 2016 to mark the 25th anniversary of the first Mopomoso concerts the Mopomoso workshops have continued each week in London’s West End. Guests are invited to take part in a monthly concert open to the public and for this special show the workshop takes over Mopomoso's regular afternoon slot at the Vortex to present a diverse and engaging mix of improvised music featuring its members in collaboration with a specially invited group of guests from the wider improvising community.

A great chance to hear some exciting musicians newer to the scene in the company of some Mopomoso favourites this promises to be a truly amazing concert. Not to be missed!!!

### Special Guests include
- {{<artist "Susanna Ferrar">}}
- {{<artist "Otto Willberg">}}
- {{<artist "Jordan Muscatello">}}
- {{<artist "Sonic Pleasure">}}
- {{<artist "Dave Tucker">}}
- {{<artist "Kay Grant">}} and {{<artist "Adrian Northover">}}
- {{<artist "Bouche Bée">}} - {{<artist "Emmanuelle Waeckerlé">}} (Voice), {{<artist "Petri Huurinainen">}} (Guitar) & {{<artist "John Eyles">}} (Sax)

### Workshop Members include
- {{<artist "Chris Hill">}}
- {{<artist "Alec Kronacker">}}
- {{<artist "Adam Kinsey">}}
- {{<artist "Stephan Barrett">}}
- {{<artist "Deborah Chinn">}}
- {{<artist "Phil Durrant">}}
- {{<artist "Martin Vishnick">}}
- {{<artist "Emmanuelle Waeckerlé">}}
- {{<artist "Martin Clarke">}}
- {{<artist "John Eyles">}}
- {{<artist "Sam Enthoven">}}
- {{<artist "Michael Jaquinto">}}
- {{<artist "Alan Newcombe">}}
- {{<artist "Charlotte Keeffe">}}
- {{<artist "Sebastian Sterkowicz">}}
- {{<artist "Dave Fowler">}}
- {{<artist "Samuel Plackett">}}

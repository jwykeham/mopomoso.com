---
title: 20 July 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-07-20T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Alex Ward"
  - "Alexander Hawkins"
  - "Dominic Lash"
  - "Paul May"
  - "John Edwards"
  - "Adrian Northover"
  - "Chefa Alonso"
  - "Albert Kaul"
  - "John Russell"
sets:
  - - "Alex Ward"
    - "Alexander Hawkins"
    - "Dominic Lash"
    - "Paul May"
  - - "John Edwards"
    - "Adrian Northover"
  - - "Chefa Alonso"
    - "Albert Kaul"
    - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

### Barkingside - {{<artist "Alex Ward">}}, {{<artist "Alexander Hawkins">}}, {{<artist "Dominic Lash">}} & {{<artist "Paul May">}}

{{<youtube Aofm9Wr57sw>}}

### {{<artist "John Edwards">}} & {{<artist "Adrian Northover">}}

{{<youtube ElViP44mqiI>}}


### {{<artist "Chefa Alonso">}}, {{<artist "Albert Kaul">}}, {{<artist "John Russell">}}
{{<youtube blNhAirdsfg>}}

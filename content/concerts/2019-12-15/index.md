---
title: 15 December 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-12-15T14:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Jez Parfett"
  - "Pat Moonchy"
  - "Lucky Liguori"
  - "Steve Beresford"
  - "Satoko Fukuda"
  - "Paul Jolly"
  - "Alan Wilkinson"
  - "Paul Morris"
  - "Pascal Marzan"
  - "Matt Hutchinson"
  - "Michael Mendones"
  - "Ed Lucas"
  - "Maggie Nicols"
  - "Sid Thomas"
  - "Ross Lambert"
  - "Sylvia Hallett"
  - "Mike Skelton"
  - "Beibei Wang"
  - "John Eyles"
  - "Professor Nonowsky"
  - "Roland Ramanan"
  - "Dave Tucker"
  - "Kostas Chondros"
  - "James Wykeham"
  - "Benedict Taylor"
  - "Isidora Edwards"
  - "Martin Hackett"
  - "Lawrence Casserley"
  - "James Bailey"
  - "Adam Bohman"
  - "David Grundy"
  - "Laurel Uziel"
  - "Alex Maguire"
  - "Simon Picard"
  - "Rick Jensen"
  - "John Rangecroft"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2019
---
Every year Mopomoso hosts a cross section of players from the improvising music community in an end of year Christmas show. Around thirty or so musicians take part in a series of five to ten minute sets, some serious some humourous. 
<!--more-->
Both audience and participants are invited to bring food to share in a celebration of all things improvised. The programme will be changing up to and including the date of performance. A really great way to meet old friends and make new ones and to get a unique glimpse into this vibrant form of music making. 

So far expressing an interest in playing are : {{<artist "Jez Parfett">}}, {{<artist "Pat Moonchy">}}, {{<artist "Lucky Liguori">}}, {{<artist "Steve Beresford">}}, {{<artist "Satoko Fukuda">}}, {{<artist "Paul Jolly">}}, {{<artist "Alan Wilkinson">}}, {{<artist "Paul Morris">}}, {{<artist "Pascal Marzan">}}, {{<artist "Matt Hutchinson">}}, {{<artist "Michael Mendones">}}, {{<artist "Ed Lucas">}}, {{<artist "Maggie Nicols">}}, {{<artist "Sid Thomas">}}, {{<artist "Ross Lambert">}}, {{<artist "Sylvia Hallett">}}, {{<artist "Mike Skelton">}}, {{<artist "Beibei Wang">}}, {{<artist "John Eyles">}}, {{<artist "Professor Nonowsky">}}, {{<artist "Roland Ramanan">}}, {{<artist "Dave Tucker">}}, {{<artist "Kostas Chondros">}}, {{<artist "James Wykeham">}}, {{<artist "Benedict Taylor">}}, {{<artist "Isidora Edwards">}}, {{<artist "Martin Hackett">}}, {{<artist "Lawrence Casserley">}}, {{<artist "James Bailey">}}, {{<artist "Adam Bohman">}}, {{<artist "David Grundy">}}, {{<artist "Laurel Uziel">}}, {{<artist "Alex Maguire">}}, {{<artist "Simon Picard">}}, {{<artist "Rick Jensen">}}, {{<artist "John Rangecroft">}}.

Here’s the Toy Laptop Quartet being very silly at the 2007 show!
{{<youtube  bWsJqk4Onv8>}}

---
artists:
  - Steve Bereford
  - Nigel Coombes
  - Veryan Weston
  - Hugh Davies
  - John Russell
  - Roger Turner
sets:
  - - Steve Bereford
    - Nigel Coombes
  - - Veryan Weston
  - - Hugh Davies
    - John Russell
    - Roger Turner
concert_date: 1997-02-16T20:00:00Z
concert_series: At The Red Rose
date: 1997-02-16T20:00:00Z
notes:
  - '>Improvised music session with 3 duet for piano and violin solo pianist Weston and a trio for amplified objects, trash guitar and percussion. - *The Guardian - Sat Feb 15 1997*'
sets: []
sources:
  - https://www.newspapers.com/image/260738943
title: 16 February 1997
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
costs:
  - key: "Standard"
    value: "£4"
  - key: "Concessions"
    value: "£3"
year: 1997
---

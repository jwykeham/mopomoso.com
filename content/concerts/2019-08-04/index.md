---
title: 18 - 20 August 2019
categories:
  - Fete Quaqua
date: "1970-01-01T00:00:00Z"
concert_date: 2019-08-18T14:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Susanna Ferrar"
  - "Benedict Taylor"
  - "Jean-Michel Van Schouwburg"
  - "Kay Grant"
  - "Roland Ramanan"
  - "Masaharu Shoji"
  - "Sue Lynch"
  - "Alex Ward"
  - "Steve Beresford"
  - "Lee Boyd Allatson"
  - "Ken Ikeda"
  - "Blanca Regina"
  - "Pascal Marzan"
  - "John Russell"
  - "Harry Gilonis"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Susanna Ferrar
    - Benedict Taylor
    - Jean-Michel Van Schouwburg
    - Kay Grant
    - Roland Ramanan
  - - Masaharu Shoji
    - Sue Lynch
    - Alex Ward
    - Steve Beresford
    - Lee Boyd Allatson
  - - Ken Ikeda
    - Blanca Regina
    - Pascal Marzan
    - John Russell
    - Harry Gilonis
year: 2019
---

## Fête Quaqua 2019


![](/uploads/2019/08/Screenshot-2019-08-02-at-19.50.32.png)

### {{<artist "Susanna Ferrar">}} violin, {{<artist "Benedict Taylor">}} viola, {{<artist "Jean-Michel Van Schouwburg">}} voice, {{<artist "Kay Grant">}} voice, {{<artist "Roland Ramanan">}} trumpet

### {{<artist "Masaharu Shoji">}} sax, {{<artist "Sue Lynch">}} sax, {{<artist "Alex Ward">}} clarinet/guitar, {{<artist "Steve Beresford">}} piano/electronics, {{<artist "Lee Boyd Allatson">}} drums

### {{<artist "Ken Ikeda">}} electronics, {{<artist "Blanca Regina">}} electronics, {{<artist "Pascal Marzan">}} guitars, {{<artist "John Russell">}} guitars, {{<artist "Harry Gilonis">}} poetry/words

## Admission 
### each concert
£8 / £6 concessions
### Three concert pass
£20/£15 concessions


> The basic idea behind all my Quaqua projects is very simple. It is to put groups together that recognise existing creative relationships alongside introducing musicians to each other for the first time. Every year I make a mini festival *Fête Quaqua* based on these principles. Over three days the participants work together in different combinations, every concert being different. Apart from choosing the participants, the various combinations and giving an indication as to how long each piece should last everything else is freely improvised. Uniquely free improvisation offers the opportunity for both musicians and audience to discover the music as it happens and for those who buy a three day pass a chance to experience the natural development that the event is designed to encourage - John Russell

>Quaqua  - (Latin) whithersoever/whatever… Quaquaversal – pointing in all directions.

---
title: "25 June 2023"
date: 2023-06-25T14:00:00+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2023-06-25T14:00:00+01:00
concert_series: "Afternoon Sessions"
artists:
  - "Emil Karlsen"
  - "Dominic Lash"
  - "Susanna Ferrar"
  - "Phil Minton"
  - "Roger Turner"
sets:
  - - "Emil Karlsen"
    - "Dominic Lash"
  - - "Susanna Ferrar"
  - - "Phil Minton"
    - "Roger Turner"
year: 2023
---
### {{<artist "Emil Karlsen">}} & {{<artist "Dominic Lash">}}

### {{<artist "Susanna Ferrar">}}

### {{<artist "Phil Minton">}} & {{<artist "Roger Turner">}}

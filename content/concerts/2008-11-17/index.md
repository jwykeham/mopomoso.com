---
title: 16 November 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-11-16T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Swift are the Winds of Life"
  - "Charlie Collins"
  - "Beatrix Ward Fernandez"
  - "Yvonna Magda"
  - "Steve Beresford"
  - "Satoko Fukuda"
  - "The Alan Tomlinson Trio"
  - "Alan Tomlinson"
  - "Dave Tucker"
  - "Roger Turner"
  - "Richard Barrett"
  - "Paul Obermayer"
  - "Evan Parker"
  - "John Edwards"
  - "John Russell"
sets:
  - - "Swift are the Winds of Life"
    - "Charlie Collins"
    - "Beatrix Ward Fernandez"
    - "Yvonna Magda"
  - - "Steve Beresford"
    - "Satoko Fukuda"
  - - "The Alan Tomlinson Trio"
    - "Alan Tomlinson"
    - "Dave Tucker"
    - "Roger Turner"
  - - "Richard Barrett"
    - "Paul Obermayer"
  - - "Evan Parker"
    - "John Edwards"
    - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

The November concert is an extended one to tie in with the London Jazz festival.

### {{<artist "Swift are the Winds of Life">}} - {{<artist "Charlie Collins">}}, {{<artist "Beatrix Ward Fernandez">}} & {{<artist "Yvonna Magda">}}

{{<youtube yIXSsp90cTk>}}

### {{<artist "Steve Beresford">}} & {{<artist "Satoko Fukuda">}}

{{<youtube gVIESimLHpk>}}

### {{<artist "The Alan Tomlinson Trio">}} - {{<artist "Alan Tomlinson">}}, {{<artist "Dave Tucker">}} & {{<artist "Roger Turner">}}

{{<youtube 467QtEU7XCY>}}


### Furt - {{<artist "Richard Barrett">}} & {{<artist "Paul Obermayer">}}

{{<youtube 7sc_fh0M9v8>}}

### {{<artist "Evan Parker">}}, {{<artist "John Edwards">}} & {{<artist "John Russell">}}

{{<youtube Lyj3RCb0vzw>}}

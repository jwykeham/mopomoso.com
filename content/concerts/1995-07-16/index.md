---
artists: []
concert_date: 1995-07-16T20:00:00+01:00
concert_series: At The Red Rose
date: 1995-07-16T20:00:00+01:00
notes: null
sets:
  - - John Russell
    - Hugh Davies
  - - Marcio Mattos
    - Phil Durrant
    - '? Hayward'
  - - '? Jasnoch'
    - '? Collin'
sources:
  - https://www.newspapers.com/image/790512818
  - https://www.newspapers.com/image/261124331
title: 16 July 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1995
---
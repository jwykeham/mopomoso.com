---
title: 15 September 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-09-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Trevor Watts"
  - "Veryan Weston"
  - "Marcio Mattos"
  - "John Russell"
  - "Pascal Marzan"
sets:
  - - "Trevor Watts"
    - "Veryan Weston"
    - "Marcio Mattos"
  - - "John Russell"
    - "Pascal Marzan"
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Trevor Watts
    - Veryan Weston
    - Marcio Mattos
  - - John Russell
    - Pascal Marzan
year: 2019
---

### {{<artist "Trevor Watts">}} (sax) / {{<artist "Veryan Weston">}} (piano) / {{<artist "Marcio Mattos">}} (bass) / Dave Fowler (drums)

![](./images/trevor-watts-veryan-weston-marcio-mattos--dave-fowler.png)

A superb quartet featuring real legends in the development of the music. Highly recommended.

### {{<artist "John Russell">}} (guitar) / {{<artist "Pascal Marzan">}} (guitar)

![](./images/john-russell-pascal-marzan.jpg)

Long standing duo coming from different guitar traditions and styles who share a deep love for all things guitar. Very special stuff!

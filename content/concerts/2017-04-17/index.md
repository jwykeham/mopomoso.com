---
title: 21 May 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-05-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Charlotte Keeffe"
  - "Joe Smith Sands"
  - "Diego Sampieri"
  - "Marilza Gouvea"
  - "Marcio Mattos"
  - "Adrian Northover"
  - "Jean-Jacques Duerinckx"
  - "John Russell"
  - "Matthieu Safatly"
sets:
  - - Charlotte Keeffe
    - Joe Smith Sands
    - Diego Sampieri
  - - Marilza Gouvea
    - Marcio Mattos
    - Adrian Northover
  - - Jean-Jacques Duerinckx
    - John Russell
    - Matthieu Safatly
year: 2017
---

Three highly distinct and distinctive trios illustrating in a very real way the range of musical approaches to be found in free improvisation.
<!--more-->

### {{<artist "Charlotte Keeffe">}} (tpt,flg), {{<artist "Joe Smith Sands">}} (gtr) & {{<artist "Diego Sampieri">}} (gtr)

![charlotte joe and diego](https://www.mopomoso.com/wp-content/uploads/2017/04/charlotte-joe-and-diego-300x114.jpeg)

In 2016 Mopomoso began a regular workshop series to mark 25 years of unbroken monthly concerts dediacated to free improvisation and it is with great delight we are presenting a group led by one of that workshop’s core members. Trumpet / flugel horn player Charlotte Keeffe, who has recently completed her MA at The Guildhall School of Music, is already exciting much interest on the scene. A talented instrumentalist and dedicated musician she has asked two fine young guitar players to join her for a set that promises to be lively and full of fun.

### {{<artist "Marilza Gouvea">}} (voc), {{<artist "Marcio Mattos">}} (cello) & {{<artist "Adrian Northover">}} (sax)

![marcio marilza adrian](https://www.mopomoso.com/wp-content/uploads/2017/04/marcio-marilza-adrian-300x165.jpeg)

The three musicians playing the second set of the day should need no introduction to anyone who has been following the London free improvisation scene over the years. All are highly regarded in their own right and also as contributors to many of the capital’s better known bands like The LIO, The Bohman Brothers and The Horse Collective. They are here to celebrate the release of their debut CD as a trio, so why not hear them live and then pick up a copy at a special concert price? Elegant and poised, a music of directness and nuance.

### {{<artist "Jean-Jacques Duerinckx">}} (sax), {{<artist "John Russell">}} (gtr) & {{<artist "Matthieu Safatly">}} (cello)

![ji ji ms and jr](https://www.mopomoso.com/wp-content/uploads/2017/04/ji-ji-ms-and-jr-300x121.jpeg)

Duerinckx and Russell have been collaborators for a couple of years, notably in projects led by Jean-Michel Van Schouwburg and also in Russell’s Quaqua groups. They came together again last year to form this trio with Safatly for a well received debut at the Ars Musica festival in Brussels. Their music is one of delicacy and deep listening, fine detail and intimacy coupled with an honest intensity.

***
>‘Good atmosphere. Good music played by good musicians. I can’t think of a better way to spend a Sunday afternoon!’ **George Coote**

---
title: "16 June 2024"
date: 2024-04-24T10:59:01+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-06-16 00:00:00 +0100 BST
concert_series: Mopomoso Live
artists:
- Jamie Coleman
- Ross Lambert
- Bark!
- mcddavid
- Jörg Hufschmidt
sets:
  - - Jamie Coleman
    - Ross Lambert
  - - BARK!
  - - mcddavid
    - Jörg Hufschmidt
year: 2024
ticket_link: https://www.vortexjazz.co.uk/event/mopomoso-june-2024-06-16/
---
### {{<artist "Jamie Coleman">}} & {{<artist "Ross Lambert">}}
{{<artist/description "Jamie Coleman">}}

{{<artist/description "Ross Lambert">}}
### {{<artist "BARK!">}} - {{<artist "Rex Caswell">}}, {{<artist "Paul Obermayer">}} & {{<artist "Phil Marks">}}
{{<artist/description "BARK!">}}

{{<artist/description "Rex Caswell">}}

{{<artist/description "Paul Obermayer">}}

{{<artist/description "Phil Marks">}}

### {{<artist "mcddavid">}} & {{<artist "Jörg Hufschmidt">}}
{{<artist/description "mcddavid">}}

{{<artist/description "Jörg Hufschmidt">}}

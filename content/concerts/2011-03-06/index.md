---
title: 17 April 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-04-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Sebastien Branche"
  - "Artur Vidal"
  - "Chris Burn"
  - "Matthew Hutchinson"
  - "Kay Grant"
  - "Satoko Fukuda"
  - "Henry Lowther"
  - "John Russell"
sets:
  - - Sebastien Branche
    - Artur Vidal
  - - Chris Burn
    - Matthew Hutchinson
  - - Kay Grant
  - - Satoko Fukuda
    - Henry Lowther
    - John Russell
year: 2011
---

Mopomoso continues to present the best of improvised music – this month featuring four very different small group settings.

### Relentless - {{<artist "Sebastien Branche">}} (sax) / {{<artist "Artur Vidal">}} (sax)

{{<youtube nZMg8G7YJ8Q>}} 

### {{<artist "Chris Burn">}} (trumpets) / {{<artist "Matthew Hutchinson">}} (piano)

{{<youtube xOJv7n7pX7Q>}} 

### {{<artist "Kay Grant">}} (voice)

{{<youtube g4JAU5G9_2c>}} 

### {{<artist "Satoko Fukuda">}} (violin) / {{<artist "Henry Lowther">}} (trumpet) / {{<artist "John Russell">}} (guitar)

{{<youtube 64LtOP73ByA>}} 

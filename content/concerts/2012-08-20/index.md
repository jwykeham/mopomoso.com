---
title: 19 August 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-08-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Fête Quaqua
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2012
---
# Fête Quaqua 2012 Day One

{{<youtube A2-kPtaSh2k>}}
{{<youtube gXCc9esuzFc>}}
{{<youtube -aat5EpLwi4>}}
{{<youtube 8t3V6PeoG0M>}}
{{<youtube FxvOuR07wzE>}}
{{<youtube xaCWot4gWvU>}}
{{<youtube 8S2pYbueaHc>}}
{{<youtube 6_XCmyoAuWs>}}

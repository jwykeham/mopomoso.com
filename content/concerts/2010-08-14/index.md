---
title: 15 August 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-08-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Fete Quaqua
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Chris Burn"
  - "John Butcher"
  - "Tania Chen"
  - "Lol Coxhill"
  - "Aleks Kolkowski"
  - "Dominic Lash"
  - "John Russell"
  - "Akio Suzuki"
  - "Sabu Toyozumi"
  - "Alex Ward"
  - "Ute Wassermann"
year: 2010
---
First day of John Russell’s annual Fete Quaqua which took place on the 15th, 16th and 17th of August 2010.
  
Originally used as a name for a weekly club for improvised music, the first Quaqua groups performed in the early 1980’s under the heading ‘Fete Quaqua’ at the now defunct London Musicians’ Collective building in Camden Town. The basic idea behind all Quaqua projects being to extend existing collaborations in juxtaposition with new groupings and thus provide a fertile ground for free improvisation. This was an extension of the way a number of us had been working at the Little Theatre Club and the London Musicians’ Co-Op concerts at the Unity Theatre from around 1973 onwards, where personnel changed from concert to concert from a pool of musicians with different approaches, who shared a love of free improvisation. The 2010 ‘Fete Quaqua’ had eleven ‘original voices’, some of whom had worked together for many years, and others who had never played together before. Their musical backgrounds stretch across an enormous variety of styles and settings. Each concert started and finished with a short ensemble piece, the main part of the evening comprising smaller groupings from the larger ensemble. Excerpts from the three days, filmed by Helen Petts, are now up on Youtube and can be seen below. ‘Quaqua’ is a Latin word and means ‘whithersoever’. John Russell
  
Musicians taking part were:

### {{<artist "Chris Burn">}}, {{<artist "John Butcher">}}, {{<artist "Tania Chen">}}, {{<artist "Lol Coxhill">}}, {{<artist "Aleks Kolkowski">}}, {{<artist "Dominic Lash">}}, {{<artist "John Russell">}}, {{<artist "Akio Suzuki">}}, {{<artist "Sabu Toyozumi">}}, {{<artist "Alex Ward">}} and {{<artist "Ute Wassermann">}}.

Many thanks are due to The Great Britain Sasakawa Foundation for their help towards the travel costs for the two Japanese visitors.

{{<youtube xzKV31G10dU>}} 
{{<youtube 7qkInQkKpFk>}} 
{{<youtube 8LC5pfP7YbU>}} 
{{<youtube IFqjS-9ZCUA>}} 
{{<youtube IE6Q1jP_r9s>}} 
{{<youtube h1pvbSpWQ9I>}} 
{{<youtube jAbuGR7RZgc>}} 
{{<youtube 2X7rkmNMnKk>}} 
{{<youtube tAnJAXXY0OI>}} 

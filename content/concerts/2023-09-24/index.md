---
title: "24 September 2023"
date: 2023-09-09T07:00:00+01:00
doors: 2pm
categories:
- Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
- key: "Standard"
  value: "£12"
- key: "Vortex Members & Concessions"
  value: "£8"
concert_date: 2023-09-24 00:00:00 +0100 BST
concert_series: Mopomoso Live
artists:
- "Alan Tomlinson"
- "Steve Beresford"
- "Viv Corringham"
- "Lawrence Casserley"
- "Yoko Miura"
- "mcddavid"
- "Tim Fletcher"
- "Pascal Marzan"
- "James Wykeham"
sets:
- - "Alan Tomlinson"
  - "Steve Beresford"
- - "Viv Corringham"
  - "Lawrence Casserley"
  - "Yoko Miura"
- - "mcddavid"
  - "Tim Fletcher"
  - "Viv Corringham"
  - "Pascal Marzan"
  - "James Wykeham"

ticket_link: https://www.ticketweb.uk/event/mopomoso-september-vortex-jazz-club-tickets/13630458
---
### {{<artist "Alan Tomlinson">}} (trombone) & {{<artist "Steve Beresford">}} (piano)

### {{<artist "Viv Corringham">}} (voice) & {{<artist "Lawrence Casserley">}} (processing) with {{<artist "Yoko Miura">}} (piano/melodica)

### {{<artist "mcddavid">}} (guitar and things) & {{<artist "Yoko Miura">}} (piano/melodica)

### Un coup d'Ariel by mToo (Mopomoso Tv Organisers Orchestra) 
comprising:
- {{<artist "mcddavid">}}
- {{<artist "Tim Fletcher">}}
- {{<artist "Viv Corringham">}}
- {{<artist "Pascal Marzan">}}
- {{<artist "James Wykeham">}}


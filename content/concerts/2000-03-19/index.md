---
artists:
  - FURT
concert_date: 2000-03-19T20:00:00Z
concert_series: At The Red Rose
date: 2000-03-19T20:00:00Z
notes: null
sets: []
sources:
  - https://furtlogic.com/node/76.html
title: 19 March 2000
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2000
---
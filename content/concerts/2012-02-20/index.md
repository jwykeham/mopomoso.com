---
title: 18 March 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-03-18T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Nick Malcolm"
  - "Olie Brice"
  - "Roger Turner"
  - "Dave Tucker"
  - "Chefa Alonso"
  - "Barbara Meyer"
  - "Cova Villegas"
  - "John Russell"
sets:
  - - Nick Malcolm
    - Olie Brice
    - Roger Turner
  - - Dave Tucker
  - - Chefa Alonso
    - Barbara Meyer
    - Cova Villegas
  - - Roger Turner
    - John Russell
year: 2012
---

### {{<artist "Nick Malcolm">}} (trumpet) / {{<artist "Olie Brice">}} (bass) / {{<artist "Roger Turner">}} (percussion)

{{<youtube UNtVu3WRkuk>}}

### {{<artist "Dave Tucker">}} (piano)

{{<youtube mjOHSnZ5BBk>}}

### {{<artist "Chefa Alonso">}} (sax,percussion) / {{<artist "Barbara Meyer">}} (cello) / {{<artist "Cova Villegas">}} (voice)

{{<youtube thy9vxrJnbg>}}

### {{<artist "Roger Turner">}} (percussion) / {{<artist "John Russell">}} (guitar)

{{<youtube GHC6Hpe8tk0>}}

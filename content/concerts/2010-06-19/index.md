---
title: 20 June 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-06-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Gus Garside"
  - "Dan Powell"
  - "Kay Grant"
  - "Alex Ward"
  - "Luo Chao yun"
  - "John Russell"
sets:
  - - Gus Garside
    - Dan Powell
  - - Guillaume Viltard
  - - Kay Grant
    - Alex Ward
  - - Luo Chao yun
    - John Russell
year: 2010
---
### {{<artist "Gus Garside">}} (bass,electronics) / {{<artist "Dan Powell">}} (small percussion,laptop,guitar)

A well established Brighton based duo of some standing with revered bass player and sound installation artist

“… have an interest in modern composition as well as improvising and seem to combine both, recalling the rigorous acoustic soundscapes of the avant-garde and a number of sideways leaps into unknown territories.”  
-Gravitational Pull

{{<youtube 5D89GoQoo8g>}} 

### {{<artist "Guillaume Viltard">}} (bass) solo

French bass player, now based in London, where he has established a firm reputation for his inventive and imaginative music. His group ‘Treehouse’ played Mopomoso last year to wide acclaim and this is his eagerly awaited debut for us as a soloist.

{{<youtube 79bgFlh7DtI>}} 

### {{<artist "Kay Grant">}} (voice) / {{<artist "Alex Ward">}} (clarinet) duo

The New York vocalist – where she worked with a.o John Zorn – is now a UK resident and indispensable part of the London scene. She is joined here by regular partner, clarinet maestro Alex Ward for what promises to be a fine example of the improvisatory art.

{{<youtube eZnRb-67sTw>}} 

### {{<artist "Luo Chao yun">}} (pipa) / {{<artist "John Russell">}} (guitar)

A rare chance to catch this exciting duo. Taiwanese pipa virtuoso meets top British guitar improviser in an East meets West collaboration of frets, fingers, strings and plectra,  
positively bubbling with musical delights.

Luo Chao yun appears courtesy of the Taiwanese Ministry of Foreign Affairs

{{<youtube BIWkcHF8OPY>}} 

{{<youtube kSrB96GkQNI>}} 

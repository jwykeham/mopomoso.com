---
title: 17 September 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-09-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Discover Plus
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Stefan Keune"
  - "John Russell"
  - "Alison Blunt"
  - "Yoko Miura"
  - "Ola Paulson"
  - "Anders Lindsjö"
  - "Anders Uddeskog"
sets:
  - - Stefan Keune
    - John Russell
  - - Alison Blunt
    - Yoko Miura
  - - Ola Paulson
    - Anders Lindsjö
    - John Russell
    - Anders Uddeskog
year: 2017
---
# DISCOVERY PLUS
Our regular monthly afternoon series continues at The Vortex “London’s listening Jazz club’ with, in different guises, some of the participants from the previous day’s Discovery festival in Walthamstow.


### {{<artist "Stefan Keune">}} (sax) & {{<artist "John Russell">}} (acoustic guitar)
### {{<artist "Alison Blunt">}} (violin) & {{<artist "Yoko Miura">}} (piano)
### {{<artist "Ola Paulson">}} (alto saxophone), {{<artist "Anders Lindsjö">}} (electric bass guitar), {{<artist "John Russell">}} (electric guitar) & {{<artist "Anders Uddeskog">}} (drums & percussion)
### plus surprise guests!!!

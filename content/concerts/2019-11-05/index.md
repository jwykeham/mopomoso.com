---
title: 24 November 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-11-24T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
summary: An afternoon in the company of some amazing improvisors illustrating three very different approaches to small group playing.
artists:
  - "Olie Brice"
  - "Rachel Musson"
  - "John Jasnoch"
  - "Gillian Whitely"
  - "Geoff Bright"
  - "Isidora Edwards"
  - "John Russell"
sets:
  - - "Olie Brice"
    - "Rachel Musson"
  - - "John Jasnoch"
    - "Gillian Whitely"
    - "Geoff Bright"
  - - "Isidora Edwards"
    - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2019
---
An afternoon in the company of some amazing improvisors illustrating three very different approaches to small group playing.

With backgrounds and influences stretching from Country and Western through Jazz, Contemporary Classical, the Avant Garde and Folk musics, join the musicians in taking your ears on a journey of sonic discovery. 

There are no safety nets and no guarantees except to say that as the music is freely improvised we know you won”t have heard it before.


### {{<artist "Olie Brice">}} (bass) / {{<artist "Rachel Musson">}} (sax) duo

![](./images/olie-brice-rachel-musson.jpg)

### {{<artist "John Jasnoch">}} (12 string guitar, mandolin) / {{<artist "Gillian Whitely">}} (violin, accordion, hurdy-gurdy, voice) / {{<artist "Geoff Bright">}} (soprano and bass saxophones, voice) trio

![](./images/IMG_0691.jpg)

### {{<artist "Isidora Edwards">}} (cello) / {{<artist "John Russell">}} (guitar) duo

![](./images/john-russell.jpg) ![](./images/isidora-edwards.png)


---
title: 20 April 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-04-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Neil Metcalfe"
  - "Daniel Thompson"
  - "Viv Corringham"
  - "Lawrence Casserley"
  - "Caroline Kraabel"
  - "John Edwards"
  - "Cleveland Watkiss"
sets:
  - - Neil Metcalfe
    - Daniel Thompson
  - - Viv Corringham
    - Lawrence Casserley
  - - Caroline Kraabel
    - John Edwards
    - Cleveland Watkiss
year: 2014
---

Owing to technical problems we have no video or audio recordings of this concert. So in the words of Eric Dolphy, “When you hear music, after it’s over, *it’s gone* in the air. You can never capture it again.”

### {{<artist "Neil Metcalfe">}} (flute), {{<artist "Daniel Thompson">}} (guitar)

![](https://www.mopomoso.com/wp-content/uploads/2014/03/daniel-and-neil.jpg "daniel and neil")

Highly musical and non demonstrative duo that gets to the core of what it means to improvise. Close listening and a fine attention to detail coupled with a firm technical grasp make for an intimate and highly charged listening experience.

### {{<artist "Viv Corringham">}} (voice) & {{<artist "Lawrence Casserley">}} (electronics)

![](https://www.mopomoso.com/wp-content/uploads/2014/03/viv-lawrence.jpg "viv lawrence")]

Two Mopomoso favourites come together to explore the full range of the human voice with electronic treatments from the ever inventive Casserley. Nice to see Viv back in the UK and able to make this date. Delightful!

### {{<artist "Caroline Kraabel">}} (saxophone), {{<artist "John Edwards">}} (bass) & {{<artist "Cleveland Watkiss">}} (voice)

[![](https://www.mopomoso.com/wp-content/uploads/2014/03/cleveland-caroline-john.jpg "cleveland caroline john")](https://www.mopomoso.com/wp-content/uploads/2014/03/cleveland-caroline-john.jpg) With an ‘award winning Jazz singer’ and the ‘European free improvising bass player of choice’ this trio, put together by Caroline Kraabel, who is no mean musical slouch herself!, promises to deliver across a broad spectrum. From small quiet gestures to large demonstrative statements, no stone unturned and with an all encompassing sense of fun.

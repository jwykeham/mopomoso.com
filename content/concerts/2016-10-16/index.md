---
title: 2 - 4 December 2016
categories:
  - I’Klectik Ballistik
date: "1970-01-01T00:00:00Z"
concert_date: 2016-12-02T00:00:00Z
venue: I’Klectik Arts Lab, ‘Old Paradise Yard’, 20 Carlisle Lane (Royal Street corner) SE1 7LG
doors:
  - 2016-12-02T19:00:00Z
  - 2016-12-03T13:30:00Z
  - 2016-12-04T13:30:00Z
concert_series: I’Klectik Ballistik
ticket_link: https://www.eventbrite.co.uk/e/mopomoso-presents-iklectik-ballistik-tickets-28758529536?utm-medium=discovery&utm-campaign=social&utm-content=attendeeshare&aff=escb&utm-source=cp&utm-term=listing
costs:
  - key: Friday
    value: £8/10
  - key: Saturday
    value: £10/12
  - key: Sunday
    value: £10/12
  - key: 3-day pass
    value: £20/25
artists:
  - "The Mopomoso workshop group"
  - "Susanna Ferrar"
  - "Peter Marsh"
  - "David Ross"
  - "Andrew Sharpley"
  - "Sarah Gail Brand"
  - "Mark Sanders"
  - "Steve Beresford"
  - "Satoko Fukuda"
  - "John Russell"
  - "Stale Liavik Solberg"
  - "Paul Morris"
  - "Bruno Guastalla"
  - "Lawerence Casserley"
  - "Sharon Gal"
  - "Daniel James Ross"
  - "Ian Simpson"
  - "Alex Maguire"
  - "Martin Speake"
  - "Luiz Moretto"
  - "Marcio Mattos"
  - "Isidora Edwards"
  - "KILT"
  - "Daniel Kordik"
  - "Ken Ikeda"
  - "Ed Lucas"
  - "Illi Adato"
  - "Guy Harries"
  - "Dave Fowler"
  - "The Custodians"
  - "Adam Bohman"
  - "Adrian Northover"
  - "Sue Lynch"
  - "Alan Wilkinson"
  - "Dave Tucker"
  - "Rachel Musson"
  - "Claude Deppa"
  - "Ravi Low-beer"
  - "Kay Grant"
  - "Alex Ward"
  - "Paul G Smyth"
  - "Phil Minton"
  - "Sylvia Hallett"
  - "Clive Bell"
  - "South Leicestershire Improvisors Ensemble"
  - "Lee Allatson"
  - "Virginia Anderson"
  - "Bruce Coates"
  - "Christopher Hobbs"
  - "Rick Nance"
  - "Trevor Lines"
  - "Edward Lucas"
  - "Daniel Thompson"
  - "Guillaume Viltard"
  - "Mark Browne"
  - "Ross Lambert"
  - "Ute Kanngiesser"
  - "Steve Noble (perc)"
  - "Tom Jackson"
  - "JJ Duerinckx"
  - "Alison Blunt"
  - "Hannah Marshall"
  - "Trevor Watts"
  - "Eve Petermann"
year: 2016
---

## I’Klectik Ballistik

**Friday 2 / Saturday 3 / Sunday 4 December 2016**

![iklectik-ballistik](https://www.mopomoso.com/wp-content/uploads/2016/12/iklectik-ballistik.jpg)

3 days of improvised music and more.

A celebration of the diversity and collective power of improvised music.

### Dec 2 Friday (doors 7pm)
- {{<artist "The Mopomoso workshop group">}}
- {{<artist "Susanna Ferrar">}} (violin) performing with film shot during her recent visit to the Discovery Hut Antarctica
- {{<artist "Peter Marsh">}} (bass), {{<artist "David Ross">}} (electronics) & {{<artist "Andrew Sharpley">}} (electronics)
- {{<artist "Sarah Gail Brand">}} (trombone) & {{<artist "Mark Sanders">}} (drums)
- {{<artist "Steve Beresford">}} (piano) & {{<artist "Satoko Fukuda">}} (violin)
- {{<artist "John Russell">}} (guitar) & {{<artist "Stale Liavik Solberg">}} (percussion)

###  Dec 3 Saturday (doors 1.30pm)
- {{<artist "The Mopomoso workshop group">}}
- {{<artist "Paul Morris">}} (The Gadget)
- {{<artist "Bruno Guastalla">}} (cello) & {{<artist "Lawerence Casserley">}} (electronics)
- {{<artist "Sharon Gal">}} (voice-electronics)
- {{<artist "Daniel James Ross">}} (electronics) & {{<artist "Ian Simpson">}} (guitar)
- {{<artist "Alex Maguire">}} (piano) & {{<artist "Martin Speake">}} (sax)
- {{<artist "Luiz Moretto">}} (violin), {{<artist "Marcio Mattos">}} (cello) & {{<artist "Isidora Edwards">}} (cello)
- {{<artist "KILT">}} – {{<artist "Daniel Kordik">}} (electronics), {{<artist "Ken Ikeda">}} (electronics) & {{<artist "Ed Lucas">}} (trombone)
- {{<artist "Illi Adato">}} (electro acoustic rig), {{<artist "Guy Harries">}} (flute and electronics) & {{<artist "Dave Fowler">}} (drums)
- {{<artist "The Custodians">}} – {{<artist "Adam Bohman">}} (voice-electronics), {{<artist "Adrian Northover">}} (sax) & {{<artist "Sue Lynch">}} (sax)
- {{<artist "Alan Wilkinson">}} (sax)
- {{<artist "Dave Tucker">}} (bass), {{<artist "Rachel Musson">}} (sax), {{<artist "Claude Deppa">}} (trumpet) & {{<artist "Ravi Low-beer">}} (drums)
- {{<artist "Kay Grant">}} (voice) & {{<artist "Alex Ward">}} (clarinet)
- {{<artist "Paul G Smyth">}} (piano)
- {{<artist "Phil Minton">}} (voice) & {{<artist "John Russell">}} (guitar)

### **Dec 4 Sunday (doors 1.30pm)**
- {{<artist "The Mopomoso workshop group">}}
- {{<artist "Sylvia Hallett">}} (violin) & {{<artist "Clive Bell">}} (flute)
- {{<artist "South Leicestershire Improvisors Ensemble">}} – {{<artist "Lee Allatson">}} (drums), {{<artist "Virginia Anderson">}} (clarinets), {{<artist "Bruce Coates">}} (saxophones), {{<artist "Christopher Hobbs">}} (piano/whistles/small instruments), {{<artist "Rick Nance">}} (flugel horn) & {{<artist "Trevor Lines">}} (bass)
- {{<artist "Edward Lucas">}} (trombone), {{<artist "Daniel Thompson">}} (guitar) & {{<artist "Guillaume Viltard">}} (bass)
- {{<artist "Mark Browne">}} (sax)
- {{<artist "Ross Lambert">}} (guitar) & {{<artist "Ute Kanngiesser">}} (cello)
- {{<artist "Steve Noble (perc)">}} & {{<artist "Alex Ward">}} (clarinet)
- {{<artist "Adrian Northover">}} (sax), {{<artist "Tom Jackson">}} (clarinet) & {{<artist "JJ Duerinckx">}} (sax)
- {{<artist "Alison Blunt">}} (violin), {{<artist "Hannah Marshall">}} (cello) & {{<artist "Trevor Watts">}} (sax)
- {{<artist "Eve Petermann">}} (dance) & {{<artist "John Russell">}} (guitar)

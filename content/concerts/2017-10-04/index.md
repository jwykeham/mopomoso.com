---
title: 15 October 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-10-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Yves Charuest"
  - "Dominic Lash"
  - "Matt Hutchinson"
  - "Susanna Ferrar"
  - "Ken Ikeda"
  - "John Russell"
sets:
  - - Yves Charuest
    - Dominic Lash
  - - Matt Hutchinson
  - - Susanna Ferrar
    - Ken Ikeda
    - John Russell
year: 2017
---

### {{<artist "Yves Charuest">}} (sax) & {{<artist "Dominic Lash">}} (bass) duo

![yves and dom grab](https://www.mopomoso.com/wp-content/uploads/2017/10/yves-and-dom-grab.jpeg)

Montreal based alto saxophonist YVES CHARUEST was a member of the Peter Kowald Trio (1985-1990) with Peter Kowald and Louis Moholo, with whom he played extensively in Europe and in the USA. He has also worked with John Betsch, Agustí Fernández, Georg Graewe, Roscoe Mitchell, William Parker, Mathias Schubert and Nate Wooley, as well as with many of his fellow Canadian creative musicians such as Jean Derome, Lisle Ellis, Michel Ratté and Sam Shalabi. A precise player with a fine sense of melodic form he is ably accompanied here by bassist DOMINIC LASH whose empathetic and richly varied playing style is well known to Mopomoso regulars. A duo with an unfolding and engaging narrative.

### {{<artist "Matt Hutchinson">}} (piano)

![matt h grab](https://www.mopomoso.com/wp-content/uploads/2017/10/matt-h-grab.jpeg)

From backing Little Stevie Wonder on his second UK tour to Luciano Pavarotti’s recording engineer, from jazz with Mike Osborne to the Chris Burn Ensemble and other free improvisation luminaries such as Phil Wachsmann, pianist MATT HUTCHINSON has lost none of his enthusiasm and flair for music. A skilled and highly experienced musician his playing is, in equal measures, both playful and profound and very much in the moment.

### {{<artist "Susanna Ferrar">}} (violin), {{<artist "Ken Ikeda">}} (electronics) & {{<artist "John Russell">}} (guitar) trio

![sue john ken grab](https://www.mopomoso.com/wp-content/uploads/2017/10/sue-john-ken-grab.jpeg)

Two highly individual musicians join renowned guitarist JOHN RUSSELL for a trio that promises to involve close listening and an exploration of all the components that make up their combined instrumental voices. SUE FERRAR has worked with a.o The London Improvisers Orchestra and Evan Parker and has played her violin in Robert Falcon Scott’s hut in the Antarctic. KEN IKEDA has performed with many musicians across the world where his electronic treatments have served to extend and reveal new aspects of his colleagues’ work. A trio where space and intricacy go hand in hand.

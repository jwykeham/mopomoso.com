---
title: "21 July 2024"
date: 2024-04-24T10:59:06+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-07-21 00:00:00 +0100 BST
concert_series: Mopomoso Live
artists:
- Alati
- Matt Rogers
- Benedict Taylor
- Sophie Sleigh-Johnson
- Adam Bohman
- Chris Dowding
- Brigitte Beraha
- Dave O' Brien
sets:
  - - Alati
  - - Matt Rogers
    - Benedict Taylor
  - - Sophie Sleigh-Johnson
    - Adam Bohman
year: 2024
ticket_link: https://www.vortexjazz.co.uk/event/mopomoso-july-2024-07-21/
---

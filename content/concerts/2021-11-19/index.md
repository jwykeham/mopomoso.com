---
title: "MOPOMOSO – A TRIBUTE TO JOHN RUSSELL"
image: /uploads/2021/11/Mopomoso-21l11-750x417-1.jpg
date: "1970-01-01T00:00:00Z"
concert_date: 2021-11-19T14:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
categories:
  - Afternoon Sessions
artists:
  - "Maggie Nicols"
  - "Paul G. Smyth"
  - "Pascal Marzan"
  - "Dave Tucker"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2021
---
The Mopomoso team warmly invites some of John’s longer term collaborators

- {{<artist "Maggie Nicols">}}
- {{<artist "Paul G. Smyth">}}
- {{<artist "Pascal Marzan">}}
- {{<artist "Dave Tucker">}}

to perform at The Vortex in a Mopomoso concert especially for the London Jazz Festival, *A Tribute to John Russell*.

{{< youtube 53rhlhtquCE >}}


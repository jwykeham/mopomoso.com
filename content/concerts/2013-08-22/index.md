---
title: 15 September 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-09-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Stefan Keune"
  - "Dominic Lash"
  - "Steve Noble"
  - "Shih Yang Lee"
  - "Satoko Fukuda"
  - "Henry Lowther"
  - "John Russell"
sets:
  - - Stefan Keune
    - Dominic Lash
    - Steve Noble
  - - Shih Yang Lee
  - - Satoko Fukuda
    - Henry Lowther
    - John Russell
year: 2013
---

### {{<artist "Stefan Keune">}} (saxophone), {{<artist "Dominic Lash">}} (bass) & {{<artist "Steve Noble">}} (drums)

![stefan keune steve noble](https://www.mopomoso.com/wp-content/uploads/2013/08/stefan-and-steve.jpg "stefan and steve")

The ever inventive Keune arrives from his native Germany to try out this new trio. They were booked to play earlier in the year but sadly couldn’t owing to travel problems caused by the weather. Anticipating a set of fire and intensity with an underlying native wit from three talented and experienced musicians.

### {{<artist "Shih Yang Lee">}} (piano)

![shih yang lee](https://www.mopomoso.com/wp-content/uploads/2013/08/shih-yang-lee-jpeg.jpg "shih yang lee jpeg")

Taiwanese piano player Shih-Yang Lee is on his first trip to Europe and we are delighted to have him play a solo set for us. A player of depth and passion, style and enthusiasm whose duos with Belgian veteran Fred van Hove have been getting some very positive and well deserved responses from critics and listeners alike. A rare treat!

### {{<artist "Satoko Fukuda">}} (violin), {{<artist "Henry Lowther">}} (trumpet) & {{<artist "John Russell">}} (guitar)

![](https://www.mopomoso.com/wp-content/uploads/2013/08/henry-satoko-john.jpg "henry satoko john")

A truly world class group of virtuosity and depth from three musicians whose backgrounds cover almost the complete musical spectrum. Described as ‘music like light through stained glass’ the ‘colours’ come from acoustic instruments without electronics or other artifacts, relying solely on a keen empathy and a common aim. Direct and engaging music for heart and soul!

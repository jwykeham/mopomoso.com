---
title: "17 March 2024"
date: 2024-03-05T08:37:31Z
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-03-17 00:00:00 +0000 GMT
concert_series: Mopomoso Live
ticket_link: https://www.ticketweb.uk/event/mopomoso-march-vortex-jazz-club-tickets/13620903
sets:
  - - "Viv Corringham"
    - "Maggie Nicols"
  - - "Marilza Gouvea"
  - - "Maya-Leigh Rosenwasser"
    - "Sue Lynch"
artists:
  - "Viv Corringham"
  - "Maggie Nicols"
  - "Marilza Gouvea"
  - "Maya-Leigh Rosenwasser"
  - "Sue Lynch"
year: 2024
---
### {{<artist "Viv Corringham">}} & {{<artist "Maggie Nicols">}}

{{<artist/description "Viv Corringham">}}

{{<artist/description "Maggie Nicols">}}

Maggie and Viv recently performed together in a successful tour of Europe - 11 dates in 11 days.

### {{<artist "Marilza Gouvea">}}
{{<artist/description "Marilza Gouvea">}}

### {{<artist "Maya-Leigh Rosenwasser">}} & {{<artist "Sue Lynch">}}

{{<artist/description "Maya-Leigh Rosenwasser">}}

{{<artist/description "Sue Lynch">}} 








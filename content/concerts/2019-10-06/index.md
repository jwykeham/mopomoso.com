---
title: 20 October 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-10-20T14:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Lawrence Casserley"
  - "Jean-Michel Van Schouwburg"
  - "Marillsa Gouvea"
  - "Marcio Mattos"
  - "Adrian Northover"
  - "Benedict Taylor"
  - "Susanna Ferrar"
  - "John Russell"
sets:
  - - "Lawrence Casserley"
    - "Jean-Michel Van Schouwburg"
  - - "Marillsa Gouvea"
    - "Marcio Mattos"
    - "Adrian Northover"
    - "Benedict Taylor"
  - - "Susanna Ferrar"
    - "John Russell"
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2019
---
Continuing to celebrate all things improvised Mopomoso’s October concert chiefly comprises some major players associated with the London scene who are also respected members of the international improvising community. (Belgian vocalist Van Schouwburg has built so many connections here that he can be considered an honorary member of the capital’s creative community).
<!--more-->
For newcomers to the music the Mopomoso concerts offer a chance to hear a cross section of what is happening in this vibrant and essential area of music making. A unique feature of free improvisation is that both the musicians and the audience discover the music at the same time. Feeling free to use and order any available sounds and to express their own musicality their music is created for a specific time and place. In the words of the hashtag all you have to do is ‘bring your ears’.


### {{<artist "Lawrence Casserley">}} (electronics) {{<artist "Jean-Michel Van Schouwburg">}} (voice) duo

![](./images/lawrence-casserley-jean-michel-van-schouwburg.jpg)


### {{<artist "Marillsa Gouvea">}} (voice) {{<artist "Marcio Mattos">}} (cello) / {{<artist "Adrian Northover">}} (sax) {{<artist "Benedict Taylor">}} (viola) quartet

![](./images/marcio-4.png)

### {{<artist "Susanna Ferrar">}} (violin) {{<artist "John Russell">}} (guitar) duo

![](./images/john-and-sue.png)




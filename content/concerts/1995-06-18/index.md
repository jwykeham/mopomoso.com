---
artists: []
concert_date: 1995-06-18T20:00:00+01:00
concert_series: At The Red Rose
date: 1995-06-18T20:00:00+01:00
notes: []
sets:
  - - Phil Minton
  - - Veryan Weston
    - Lol Coxhill
sources:
  - https://www.newspapers.com/image/261096637
title: 18 June 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1995
---
---
title: 17 April 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-04-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Matthias Boss"
  - "Phil Gibbs"
  - "Marcello Magliocchi"
  - "Jean Michel van Schouwburg"
  - "Ola Paulson"
  - "Mark Lawrence"
  - "Dominic Lash"
  - "Nick Malcolm"
  - "Tony Orrell"
  - "Ken Ikeda"
  - "Simon Scott"
sets:
  - - Matthias Boss
    - Phil Gibbs
    - Marcello Magliocchi
    - Jean Michel van Schouwburg
  - - Ola Paulson
  - - Mark Lawrence
    - Dominic Lash
    - Nick Malcolm
    - Tony Orrell
  - - Ken Ikeda
    - Simon Scott
year: 2016
---

Four enthralling sets celebrating the diversity of musical approaches to be found in the world of free improvisation. All are making their Mopomoso debuts.

### {{<artist "Matthias Boss">}} (violin), {{<artist "Phil Gibbs">}} (guitar), {{<artist "Marcello Magliocchi">}} (percussion) & {{<artist "Jean Michel van Schouwburg">}} (voice)

![4tet minus one](https://www.mopomoso.com/wp-content/uploads/2016/03/unnamed.jpg)

Delighted to welcome this new quartet with members from Italy, Belgium, Switzerland and the UK. Expect a fiery, constantly changing sonic brew linked by keen listening and quick fire interaction.

### {{<artist "Ola Paulson">}} (saxophone)

![Ola Paulson](https://www.mopomoso.com/wp-content/uploads/2016/03/large.jpg)

Swedish saxophonist Ola Paulson works in the field between experimental jazz/improvisation and contemporary music with a focus on investigating the tension between composition and improvisation. He is the initiator and bandleader of Konvoj Ensemble who released the critically acclaimed album ”Colors of:” featuring Evan Parker and Sten Sandell in 2013.

### {{<artist "Mark Lawrence">}} (guitar), {{<artist "Dominic Lash">}} (bass), {{<artist "Nick Malcolm">}} (trumpet) & {{<artist "Tony Orrell">}} (drums) quartet

![bristol quartet](https://www.mopomoso.com/wp-content/uploads/2016/03/bristol-quartet.jpg)

Exciting group from the UK featuring some of our finest players. Between ‘delicate and precise’ to ‘deep down and dirty’ a group that always follows the demands of the music based on a deep empathy and constant awareness to the finer nuances within the moment. Intelligent and engaging.

### {{<artist "Ken Ikeda">}} (electronics) & {{<artist "Simon Scott">}} (electronics)

![ken and simon](https://www.mopomoso.com/wp-content/uploads/2016/03/ken-and-simon.jpg)

Ken Ikeda is a video artist and composer from Tokyo. He has exhibited sound art and visual installations around the world and collaborated with painter Tadanoori Yokoo and artist Mariko Mori. “Mist On The Window” (2007) and “Kosame” (2010) were released on Spekk, following “Tzuki” (2000) and “Merge” (2003) both on the Touch label. He has also appeared for Mopomoso as a member of John Russell’s Quaqua projects and with shamisen virtuoso Yumiko Tanaka.

He is partnered here with Simon Scott, a sound ecologist and multi-instrumentalist from Cambridge whose work explores the implications of recording the natural world using technology and the manipulation of natural sounds. Simon also plays the drums in Slowdive and has recently collaborated with artists James Blackshaw, Spire, Taylor Deupree (Between) and Isan amongst others.

---
title: 16 - 17 January 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-01-16T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Rodrigo Montoya"
  - "Thomas Rohrer"
  - "Michelle Agnes"
  - "Marcio Mattos"
  - "Chefa Alonso"
  - "Albert Kaul"
  - "Barbara Meyer"
  - "Claudia Binder"
  - "John Butcher"
  - "John Edwards"
  - "Gianni Mimmo"
  - "Gianni Lenoci"
  - "Michel Doneda"
  - "John Russell"
  - "Roger Turner"
sets:
  - - Rodrigo Montoya
    - Thomas Rohrer
    - Michelle Agnes
    - Marcio Mattos
  - - Chefa Alonso
    - Albert Kaul
    - Barbara Meyer
  - - Claudia Binder
    - John Butcher
    - John Edwards
  - - Gianni Mimmo
    - Gianni Lenoci
year: 2011
---

Continuing to celebrate the best in free improvisation Mopomoso starts the year in style with an international line up of from Switzerland, Brazil, Spain, Germany, Italy and the UK with their regular concert series and an extra CD launch concert.

# Sunday 16 January

### {{<artist "Rodrigo Montoya">}} / {{<artist "Thomas Rohrer">}} / {{<artist "Michelle Agnes">}} / {{<artist "Marcio Mattos">}}

A remarkable group with some of Brazil’s finest improvisers, Montoya and Rohrer’s far from traditional work on the shamisen and rabeca respectively exhibit a keen sense of open exploration, Agnes on piano and Mattos on cello are a delight. Busy, inventive and uncompromising music of the highest order.

{{<youtube _ugH5JmtW70>}} 

### {{<artist "Chefa Alonso">}} / {{<artist "Albert Kaul">}} / {{<artist "Barbara Meyer">}}

A welcome return visit for Spanish saxophonist Alonso and German pianist Kaul, this time with visuals from Meyer, presenting a UK debut for this collaboration, exploring the imaginative and expansive relationships between sight and sound.

{{<youtube ale0FbRkWg8>}} 

### {{<artist "Claudia Binder">}} / {{<artist "John Butcher">}} / {{<artist "John Edwards">}}

Swiss pianist Binder here playing with two great improvisers. Butcher and Edwards are regarded world wide as being amongst the very best exponents on their respective instruments and Binder’s work is noted for her ability to get to the heart of the music. Intimate and expressive music of many shades.

{{<youtube e9dXZ6DI9H8>}} 

### Reciprocal Uncles - {{<artist "Gianni Mimmo">}} / {{<artist "Gianni Lenoci">}} – Visit postponed

Another UK debut sees Amirani boss and soprano sax wizard Gianni Mimmo in the company of the highly regarded Gianni Lenoci whose credits include Steve Lacy, Harold Land, Kent Carter, Don Moye and Marcus Stockhausen to name a few.

As well as improvisation he is currently working on interpretations of J.S Bach and Morton Feldman. A richly nuanced multi-perspective approach giving a vivid listening experience.

# PLUS!

# Monday 17 January

### Doneda / Russell / Turner

Final Winter tour date and only UK appearance to launch the trio’s new CD ‘The Cigar that Talks’, this concert sees a rare UK appearance for saxophonist {{<artist "Michel Doneda">}}.

A leading figure in French new music, widely regarded for his highly personal approach to both his music and instrument, he has worked with a.o Fred van Hove, Max Eastley, John Zorn, Elvin Jones, Phil Wachsmann, Le Quan Ninh and Eliot Sharp.

Michel is joined by the incomparable pairing of {{<artist "John Russell">}} (guitar) and {{<artist "Roger Turner">}} (percussion), a duo of some thirty years standing, ‘The Cigars’ have played a number of European festivals and tours, where their music has captivated audiences with its constantly evolving music.

CD’s signed by the group will be on sale for a special one off price of £8. Booking advisable.

{{<youtube zgjeHqIWyX8>}} 

---
artists:
  - Evan Parker
  - Gail Brand
concert_date: 1998-11-15T20:00:00+01:00
concert_series: At The Red Rose
date: 1998-11-15T20:00:00+01:00
notes: []
sets: []
sources:
  - https://www.newspapers.com/image/260379107
  - https://www.newspapers.com/image/720137321
title: 15 November 1998
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
costs:
  - key: "Standard"
    value: "£4"
  - key: "Concessions"
    value: "£3"
year: 1998
---

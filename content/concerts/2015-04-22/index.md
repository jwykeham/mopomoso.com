---
title: 3 May 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-05-03T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Linda Sharrock Special
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Linda Sharrock"
  - "Charlie Collins"
  - "John Jasnoch"
  - "Derek Saw"
sets:
  - - Linda Sharrock
    - Charlie Collins
    - John Jasnoch
    - Derek Saw
year: 2015
---
## Linda Sharrock Special
### The Linda Sharrock Group
### {{<artist "Linda Sharrock">}}'s near cathartic melodic vocalisation and raw balladry on some of the earliest manifestations of free jazz find their natural home in the company of Berlin born saxophone master Mario Rechtern, ably assisted by members of Sonny Simmons legendary UK quartet – {{<artist "Charlie Collins">}}, {{<artist "John Jasnoch">}}, and {{<artist "Derek Saw">}}.

### First Set

{{<youtube iwwp6zhPiVo>}}

### Second Set
{{<youtube 1O-iOe3ZWR0>}}

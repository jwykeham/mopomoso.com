---
title: "23 July 2023"
date: 2023-07-09T13:48:42+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2023-07-23T00:00:00Z
concert_series: "Afternoon Sessions"
ticket_link: https://www.ticketweb.uk/event/mopomoso-july-vortex-jazz-club-tickets/13402638
artists:
  - "sarah gail brand"
  - "alex maguire"
  - "julian woods"
  - "george garford"
  - "john bisset"
  - "paul jolly"
  - "simon mccorry"
  - "jakub rokita"
sets:
  - - "Sarah Gail Brand"
    - "Alex Maguire"
  - - "Julian Woods"
    - "George Garford"
    - "John Bisset"
  - - "Paul Jolly"
    - "Simon McCorry"
    - "Jakub Rokita"
year: 2023
---
### {{<artist "Sarah Gail Brand">}} & {{<artist "Alex MaGuire">}}
{{<artist/description "Sarah Gail Brand">}}

{{<artist/description "Alex MaGuire">}}

### {{<artist "Julian Woods">}}, {{<artist "George Garford">}} & {{<artist "John Bisset">}}
{{<artist/description "Julian Woods">}}

{{<artist/description "George Garford">}}

{{<artist/description "John Bisset">}}

### **Acoustic Survey**: {{<artist "Paul Jolly">}}, {{<artist "Simon McCorry">}} & {{<artist "Jakub Rokita">}}
{{<artist/description "Paul Jolly">}}

{{<artist/description "Simon McCorry">}}

{{<artist/description "Jakub Rokita">}}

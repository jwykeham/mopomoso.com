---
title: 16 March 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-03-16T00:00:00Z
concert_series: "Afternoon Sessions"
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Revolver"
  - "Kim Johannesen"
  - "Fredrik Kirkevold"
  - "The Cristophe Charles Quintet"
  - "Christophe Charles"
  - "Yasutake Watanabe"
  - "Hoonoda Kim"
  - "Lisa Koiso"
  - "Woo Kyung Son"
  - "Jason Robinson"
  - "John Russell"
  - "Roger Turner"
sets:
  - - "Revolver"
    - "Kim Johannesen"
    - "Fredrik Kirkevold"
  - - "The Cristophe Charles Quintet"
    - "Christophe Charles"
    - "Yasutake Watanabe"
    - "Hoonoda Kim"
    - "Lisa Koiso"
    - "Woo Kyung Son"
  - - "Jason Robinson"
    - "John Russell"
    - "Roger Turner"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

### {{<artist "Revolver">}} - {{<artist "Kim Johannesen">}}, {{<artist "Fredrik Kirkevold">}}

{{< youtube F2GFEkvgiDA >}}

### {{<artist "The Cristophe Charles Quintet">}} - {{<artist "Christophe Charles">}}, {{<artist "Yasutake Watanabe">}}, {{<artist "Hoonoda Kim">}} plus {{<artist "Lisa Koiso">}} and {{<artist "Woo Kyung Son">}}

{{< youtube IImRGr65F4k >}}

### {{<artist "Jason Robinson">}}, {{<artist "John Russell">}} & {{<artist "Roger Turner">}}

{{< youtube DIV9w3oW0sQ >}}

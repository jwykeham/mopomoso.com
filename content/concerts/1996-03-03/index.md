---
artists: []
concert_date: 1996-03-03T20:00:00Z
concert_series: At The Red Rose
date: 1996-03-03T20:00:00Z
notes: null
sets:
  - - Steve Noble
    - Alex Maguire
  - - Steve Beresford
    - Francine Luce
sources:
  - https://www.newspapers.com/image/790523400
  - https://www.newspapers.com/image/260881048
title: ' 3 March 1996'
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
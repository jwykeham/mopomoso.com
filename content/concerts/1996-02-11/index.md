---
artists: []
concert_date: 1996-02-11T20:00:00Z
concert_series: At The Red Rose
date: 1996-02-11T20:00:00Z
notes: null
sets:
  - - Evan Parker
  - - Marcio Mattos
    - Robin Hayward
    - Nick Couldry
sources:
  - https://www.newspapers.com/image/790522035
  - https://www.newspapers.com/image/260867057
title: 11 February 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
costs:
  - key: "Standard"
    value: "£4"
  - key: "Concessions"
    value: "£3"
year: 1996
---

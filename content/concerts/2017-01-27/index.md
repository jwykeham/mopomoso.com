---
title: 19 February 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-02-19T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Rangecroft"
  - "Marcio Mattos"
  - "Dave Tucker"
  - "Pat Thomas"
  - "Kay Grant"
  - "Neil Metcalf"
  - "John Russell"
sets:
  - - John Rangecroft
    - Marcio Mattos
    - Dave Tucker
  - - Pat Thomas
    - Kay Grant
  - - Neil Metcalf
    - John Russell
year: 2017
---

This month’s Mopomoso concert in our ‘Afternoon Sessions at The Vortex’ is an all UK programme of small group interplay with seven highly regarded musicians. 

### The Paulinus trio – {{<artist "John Rangecroft">}} (reeds), {{<artist "Marcio Mattos">}} (bass) & {{<artist "Dave Tucker">}} (guitar)

![Paulinus trio](https://www.mopomoso.com/wp-content/uploads/2017/01/paulinus-grab-300x175.jpeg)

A welcome return to Mopomoso from this no nonsense group. Ears open, eyes down and full steam ahead! A constantly shifting musical narrative of depth and clarity.

### {{<artist "Pat Thomas">}} (keyboards) & {{<artist "Kay Grant">}} (voice)

![Kay Grant and Pat Thomas](https://www.mopomoso.com/wp-content/uploads/2017/01/kay-grant-pat-thomas-300x136.jpg)

Two musicians featured on the ‘Making Rooms’ 4 CD boxed set (Weekertoft), for which they have received rave reviews, here performing as a duo for the first time. Expect playful inventiveness, sympathetic interplay and large swathes of rich sonorities juxtaposed with moments of microscopic detail. Enjoy the journey!

### {{<artist "Neil Metcalf">}} (flute) & {{<artist "John Russell">}} (guitar)

![neil a](https://www.mopomoso.com/wp-content/uploads/2017/01/john-and-neil-300x223.jpeg)

Within the world of free improvisation Neil’s musicianship and total dedication to his art is of almost legendary status. He will constantly develop a melodic fragment or stretch its parameters in a seemingly endless stream of invention. Joined here by guitarist and Mopomoso founder Russell in a duo that promises to be a delight.

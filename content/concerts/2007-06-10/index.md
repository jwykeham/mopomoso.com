---
artists:
  - Henry Kaiser
  - Steve Beresford
  - Lol Coxhill
  - Satoko Fukuda
  - John Russell
  - Roger Turner
  - Hannah Marshall
concert_date: 2007-06-10T20:00:00+01:00
concert_series: At The Red Rose
date: 2007-06-10T20:00:00+01:00
notes: null
sets:
  - - Henry Kaiser
    - Steve Beresford
  - - Henry Kaiser
    - John Russell
  - - Steve Beresford
    - Roger Turner
  - - Henry Kaiser
    - Steve Beresford
    - John Russell
    - Roger Turner
    - Satoko Fukuda
  - - Hannah Marshall
    - Col Coxhill
sources:
  - https://www.flickr.com/photos/mattbrown1984/541679759/
  - http://offminor.purplebadger.com/archives/146
title: 10 June 2007
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2007
---
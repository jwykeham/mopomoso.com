---
title: "23 April 2023"
date: 2023-07-09T13:41:55+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2023-04-23T14:00:00+01:00
artists:
  - "Benedict Taylor"
  - "Yves Charuest"
  - "George Haslam"
  - "Marcio Mattos"
  - "Viv Corringham"
  - "Daniel Cano"
  - "Andrea Di Biase"
  - "Dave Storey"
sets:
  - - "Benedict Taylor"
    - "Yves Charuest"
  - - "George Haslam"
    - "Marcio Mattos"
  - - "Viv Corringham"
  - - "Daniel Cano"
    - "Andrea Di Biase"
    - "Dave Storey"
concert_series: Afternoon Sessions
year: 2023
---
### {{<artist "Benedict Taylor">}} & {{<artist "Yves Charuest">}}
{{<artist/description "Benedict Taylor">}}

{{<artist/description "Yves Charuest">}}

### {{<artist "George Haslam">}} & {{<artist "Marcio Mattos">}}
{{<artist/description "George Haslam">}}

{{<artist/description "Marcio Mattos">}}
### {{<artist "Viv Corringham">}}
{{<artist/description "Viv Corringham">}}

### {{<artist "Daniel Cano">}}, {{<artist "Andrea Di Biase">}} & {{<artist "Dave Storey">}}
{{<artist/description "Daniel Cano">}}

{{<artist/description "Andrea Di Biase">}}

{{<artist/description "Dave Storey">}}

---
title: 15 November 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-11-15T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Chefa Alonso"
  - "Barbara Meyer"
  - "Cova Villages"
  - "Gianni Mimmo"
  - "Gianni Lenoci"
  - "Cristiano Calcagnile"
  - "Ove Volquartz"
  - "Phil Marks"
  - "Rex Casswell"
  - "Paul Obermayer"
  - "Stefan Keune"
  - "John Russell"
  - "BARK!"
sets:
  - - Chefa Alonso
    - Barbara Meyer
    - Cova Villages
  - - Gianni Mimmo
    - Gianni Lenoci
    - Cristiano Calcagnile
    - Ove Volquartz
  - - Phil Marks
    - Rex Casswell
    - Paul Obermayer
  - - Stefan Keune
    - John Russell
year: 2015
---

Our November special this year is a multi national feast of free improvisation giving a glimpse into some of the different approaches to be found in this vibrant music. With a fuller programme than usual, the music will start at 2.00 pm so please arrive on time to to avoid disappointment. Tickets can be booked via the Vortex.

### MOLIMO - {{<artist "Chefa Alonso">}} (sax/perc) {{<artist "Barbara Meyer">}} (cello) {{<artist "Cova Villages">}} (voice)

{{<youtube fs_1zruDDwE>}}

This excellent all woman trio return to Mopomoso after three years to promote their new CD. Intellegent and playful, a treat to welcome them back.

### RECIPROCAL UNCLES - {{<artist "Gianni Mimmo">}} (sax) {{<artist "Gianni Lenoci">}} (piano) {{<artist "Cristiano Calcagnile">}} (drums) {{<artist "Ove Volquartz">}} (bass & contrabass clarinets)

{{<youtube isf_cNYBGA0>}}

Since their last visit in 2013 ‘the Uncles’ have grown from three to four with the edition of German clarinettist Ove Volquartz. Schooled but not scholarly, Reciprocal Uncles’ engaging manner and musicianship never fails to delight.

### {{<artist "BARK!">}} - {{<artist "Phil Marks">}} (drums) {{<artist "Rex Casswell">}} (guitar) {{<artist "Paul Obermayer">}} (electronics)

{{<youtube DesyHTrjqlY>}}

With apparent and beguiling ease BARK! unite electronics and percussion in a fine trio of rapid interactive music. Great fun!

### {{<artist "Stefan Keune">}} & {{<artist "John Russell">}}

{{<youtube HJ9TWUhC2Ds>}}

Saxophonist {{<artist "Stefan Keune">}} and guitarist {{<artist "John Russell">}} are regular playing partners who always try to get together at least once a year. This year they are recording privately for a 2016 release and this will be their only public performance in the UK. Not to be missed!

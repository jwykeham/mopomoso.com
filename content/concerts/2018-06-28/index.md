---
title: 15 July 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-07-15T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Paul Pignon"
  - "Richard Sanderson"
  - "Noel Meek"
  - "Alan Wilkinson"
  - "Johnny Lapio"
  - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Paul Pignon
    - Richard Sanderson
  - - Noel Meek
    - Alan Wilkinson
  - - Johnny Lapio
    - John Russell
year: 2018
---

As part of its ‘Afternoon Sessions’ Mopomoso presents three contrasting duos of freely improvised music.

### {{<artist "Paul Pignon">}} (clarinet, bass clarinet) & {{<artist "Richard Sanderson">}} (amplified melodeon)

![](https://www.mopomoso.com/wp-content/uploads/2018/06/paul-pignon-richard-sanderson-grab.jpeg)

Based in Sweden, Paul Pignon has been involved with improvised music since the 1960s. He is joined by Linear Obsessional founder and head man Richard Sanderson.

### {{<artist "Noel Meek">}} (electronics) & {{<artist "Alan Wilkinson">}} (sax) duo

![](https://www.mopomoso.com/wp-content/uploads/2018/06/noel-meek-alan-wilkinson-grab.jpeg)

New Zealander Noel Meek brings his special blend of electronics to bear on the colossal saxophone of Flim Flam boss Alan Wilkinson.

### {{<artist "Johnny Lapio">}} (trumpet) & {{<artist "John Russell">}} (guitar) duo

![](https://www.mopomoso.com/wp-content/uploads/2018/06/johnny-lapio-john-russell-grab.jpeg)

Italian trumpeter Johnny Lapio arrives for a debut duo with Mopomoso founder John Russell

Come and sample the delights of a music unlike any other in that the audience share the discovery of the music at the same time as the musicians themselves. A truly direct listening experience!

Paul Pignon has received travel support from The Swedish Arts Grants Committee

![](https://www.mopomoso.com/wp-content/uploads/2018/06/KON_logotype_stor_150dpi.jpg)

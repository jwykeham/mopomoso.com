---
title: 19 - 21 August 2018
categories:
  - Fête Quaqua
date: "1970-01-01T00:00:00Z"
concert_date: 2018-08-19T01:00:00+01:00
concert_series: Fête Quaqua
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors:
  - 2018-08-19T14:00:00+01:00
  - 2019-08-20T20:00:00+01:00
  - 2019-08-21T20:00:00+01:00
costs:
  - key: Full Price
    value: £8 / £6 Concessions
  - key: Three Day Pass
    value: £20 / £15 Concessions
artists:
  - "Mark Browne"
  - "Phil Durrant"
  - "Max Eastley"
  - "Sylvia Hallett"
  - "Matt Hutchinson"
  - "Paul Jolly"
  - "Dominic Lash"
  - "Martin Mayes"
  - "Phil Minton"
  - "Rachel Musson"
  - "Steve Noble"
  - "John Russell"
  - "Chun-Ting Wang"
  - "Alex Ward"
  - "Stephen Ying"
year: 2018
---

## Fête Quaqua 2018
19th – 21st August

![](https://www.mopomoso.com/wp-content/uploads/2018/07/fq-2018-collage-end.jpeg)

Put together each year by guitarist John Russell Fête Quaqua aims for old friends to get together and meet new ones in an atmosphere of in the moment, never to be repeated, musical invention. Free improvisation is unique in that the audience and musicians discover the music at the same time and this makes for a genuine engagement with the creative process.

The musicians taking part this year come from a mix of backgrounds and experiences which should lead to some very exciting exchanges. Each concert starts and finishes with a short piece by the full ensemble and the interim programme comprises changing combinations of musicians that are different for each event.

No two concerts will be the same in this never to be repeated event. Miss it and you have missed it!

An annual gathering of free improvising musicians this year featuring

- {{<artist "Mark Browne">}} (sax / percussion)
- {{<artist "Phil Durrant">}} (mandolin / electronics)
- {{<artist "Max Eastley">}} (self made instruments)
- {{<artist "Sylvia Hallett">}} (violin / electronics)
- {{<artist "Matt Hutchinson">}} (piano / electronics)
- {{<artist "Paul Jolly">}} (sax / clarinet)
- {{<artist "Dominic Lash">}} (bass)
- {{<artist "Martin Mayes">}} (french horn)
- {{<artist "Phil Minton">}} (voice) 
- {{<artist "Rachel Musson">}} (sax) 
- {{<artist "Steve Noble">}} (drums)
- {{<artist "John Russell">}} (guitar)
- {{<artist "Chun-Ting Wang">}} (bass)
- {{<artist "Alex Ward">}} (clarinet / guitar)
- {{<artist "Stephen Ying">}} (turntables)

### NB Phil Minton will not be appearing in the Sunday concert and Martin Mayes will not be appearing in the Tuesday concert. All the other performers will be taking part in all three performances.


 


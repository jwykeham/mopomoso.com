---
title: 20th August 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-08-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 8pm
concert_series: Fête Quaqua
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2012
---
# Fête Quaqua 2012 Day Two
{{<youtube jKHqLtFK2uE>}}
{{<youtube onZf1kzvamA>}}
{{<youtube F8tI5mM8OdE>}}
{{<youtube jElaeKCWhrw>}}
{{<youtube MS0G69zX8XM>}}
{{<youtube sCxhz9sRRTc>}}
{{<youtube HJUPbVjy8cI>}}
{{<youtube 1HK-_uVGy-Y>}}

---
title: 20 November 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-11-20T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: London Jazz Festival
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Niklas Fite"
  - "Isak Hedtjärn"
  - "Mark Wastell"
  - "Olie Brice"
  - "Ed Jones"
  - "Mark Sanders"
  - "Alex Ward"
  - "Yoko Arai"
  - "John Russell"
sets:
  - - Niklas Fite
    - Isak Hedtjärn
  - - Mark Wastell
    - Olie Brice
    - Ed Jones
    - Mark Sanders
    - Alex Ward
  - - Yoko Arai
    - John Russell
year: 2016
---

To coincide with the London Jazz Festival, which has almost been going as long as us, Mopomoso is delighted to be able to present this international bill with musicians from Sweden, Chile and Japan performing with their UK counterparts and celebrating both the unity and diversity to be found within the world of free improvisation. Continuing our 25th anniversary afternoon series at The Vortex – address in the footer at the bottom of this page

### {{<artist "Niklas Fite">}} (guitar), {{<artist "Isak Hedtjärn">}} (clarinet) & Phil Minton (voice)

{{<youtube etcWMjHzdK8>}}

The Fite, Hedtjarn duo are originally from Sweden but currently resident in Berlin, where they are continuing to develop their own take on the music. At one concert they were joined by one of the best vocal improvisers in the world today, Phil Minton. Needless to say the concert was a great success and they jumped at the opportunity to play again at Mopomoso – just as we jumped at the opportunity to have them here!

### Mark Wastell Quintet – {{<artist "Mark Wastell">}} (cello), {{<artist "Olie Brice">}} (bass), {{<artist "Ed Jones">}} (sax,clarinet), {{<artist "Mark Sanders">}} (percussion) & {{<artist "Alex Ward">}} (clarinet)

![wastell-5](https://www.mopomoso.com/wp-content/uploads/2016/10/wastell-5-300x109.jpeg)

Mark Wastell is known as a musician who prefers to influence and nudge, to gently coax and set up situations that bring out the best in his colleagues and in this, his current quintet, he has some very fine colleagues indeed. A group that very easily fits into the category ‘diversity and unity’.

### {{<artist "Yoko Arai">}} (piano) & {{<artist "John Russell">}} (guitar)

![arai-russell](https://www.mopomoso.com/wp-content/uploads/2016/10/arai-russell-300x143.jpeg)

The final set of the afternoon sees the highly respected Japan-based pianist Yoko Arai resume her playing acquaintance with UK guitarist Russell. Their last appearance here was three years ago and previous meetings – some involving larger groups – have taken place both here at Mopomoso and in Tokyo, where Yoko hosts a concert series.

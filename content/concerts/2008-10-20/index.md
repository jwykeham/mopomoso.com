---
title: 19 October 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-10-19T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Ensemble Progressivo"
  - "Ricardo Tejero"
  - "Alison Blunt"
  - "Adrian Northover"
  - "Marcjo Mattos"
  - "Javier Carmona"
  - "Chris Burn"
  - "John Russell"
  - "Stefan Keune"
sets:
  - - "Ensemble Progressivo"
    - "Ricardo Tejero"
    - "Alison Blunt"
    - "Adrian Northover"
    - "Marcjo Mattos"
    - "Javier Carmona"
  - - "Chris Burn"
  - - "John Russell"
    - "Stefan Keune"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

### {{<artist "Ensemble Progressivo">}} - {{<artist "Ricardo Tejero">}}, {{<artist "Alison Blunt">}}, {{<artist "Adrian Northover">}}, {{<artist "Marcjo Mattos">}} & {{<artist "Javier Carmona">}}

{{<youtube IhQq2iEsK4k>}}

### {{<artist "Chris Burn">}}

No video available

### {{<artist "John Russell">}} & {{<artist "Stefan Keune">}} 

{{<youtube lNlEp-w2KOY>}}

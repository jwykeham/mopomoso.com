---
title: "15 December 2024"
date: 2024-04-24T10:59:39+01:00
doors: 2pm
draft: true
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-12-15 00:00:00 +0000 GMT
concert_series: Mopomoso Live
year: 2024
---

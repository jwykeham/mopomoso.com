---
title: 16 September 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-09-16T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Phil Durrant"
  - "Martin Vishnick"
  - "Cath Roberts"
  - "Alex Ward"
  - "Mark Browne"
  - "Dan Gregory"
  - "Ian McGowan"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Phil Durrant
    - Martin Vishnick
  - - Cath Roberts
    - Alex Ward
  - - Mark Browne
    - Dan Gregory
    - Ian McGowan
year: 2018
---
A continuing series featuring some of the finest from the world of free improvisation.
Three very different sets illustrating some of the constant delights to be found in free improvisation. Do come and give your ears a treat!
<!--more-->
### {{<artist "Phil Durrant">}} (mandolin) & {{<artist "Martin Vishnick">}} (guitar)
![](https://www.mopomoso.com/wp-content/uploads/2018/09/phil-d-martin-m-duo.jpeg)
Mandolin and guitar are a traditional pairing in other musical fields but virtually unknown in the world of free improvisation so this pairing is quite a treat. The duo first met at the Skronk April event in 2017 and it became rapidly apparent that they shared common interests in extended techniques and were influenced by electronic music. They have have since become a fixture, performing at Flim Flam, Hundred Years Gallery, The Horse @Iklectik, SkronkFest and as part of larger groups at Mopomoso and Arch1. Poised and intricate playing from two dedicated and talented musicians.
### {{<artist "Cath Roberts">}} (sax) & {{<artist "Alex Ward">}} (clarinet-guitar)
![](https://www.mopomoso.com/wp-content/uploads/2018/09/alex-and-cath.jpeg)
World class clarinet player Alex Ward is joined by Sloth Racket and Lume luminary Cath Roberts on baritone saxophone for a set that should be full of fireworks. Alex will also be playing electric guitar so we can expect a broad sonic canvas with many twists and turns.

#### Alex Ward's solo cd *Proprioception* is released on the Weekertoft label
[![Propioception cover](https://www.mopomoso.com/wp-content/uploads/2018/09/alex-cd-cover.jpeg)](https://weekertoft.bandcamp.com/album/proprioception)
<https://weekertoft.bandcamp.com/album/proprioception>
### Lust Rollers – {{<artist "Mark Browne">}} (sax and collected objects), {{<artist "Dan Gregory">}} (junk- guitar remnants) plus special guest {{<artist "Ian McGowan">}} (trumpet-guitar)
![](https://www.mopomoso.com/wp-content/uploads/2018/09/lust-r-plus-ioan.jpeg)
Priding themselves on once being described as a meeting between the dead Franz Liszt and the Dadaists, Lust Rollers look to provide a head on collision between the virtuosic and the openly inept. Improvised Music utilising elements of absurd theatre, this is a visual group whose instrumentation encompasses large gongs, glass, bones and homemade instruments exemplifying an overtly domestic nature. For this concert they are joined by the extraordinary talent of Ian MacGowan on trumpet and guitar


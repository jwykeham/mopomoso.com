---
title: 15 May 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-05-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Mark Browne"
  - "Sonic Pleasure"
  - "Ian Smith"
  - "Matt Hutchinson"
  - "Dave Solomon"
  - "Katja Cruz"
  - "John Russell"
  - "Roger Turner"
sets:
  - - Mark Browne
    - Sonic Pleasure
    - Ian Smith
  - - Matt Hutchinson
    - Dave Solomon
  - - Katja Cruz
  - - John Russell
    - Roger Turner
year: 2011
---

### {{<artist "Mark Browne">}} (castrato saxophone / percussion) {{<artist "Sonic Pleasure">}} (bricks / masonry / metal) {{<artist "Ian Smith">}} (trumpet)

{{<youtube b4ppeVXL-8g>}} 

### {{<artist "Matt Hutchinson">}} (piano) / {{<artist "Dave Solomon">}} (drums) duo

{{<youtube D2F3RtJU2Yo>}} 

### {{<artist "Katja Cruz">}} (solo voice)

{{<youtube a4lDguitpdA>}} 

### {{<artist "John Russell">}} (guitar) {{<artist "Roger Turner">}} (percussion)

{{<youtube qkG5FEFH7wc>}} 

---
title: 18 January 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-01-18T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Hannah Marshall"
  - "Josep Maria Balanya"
  - "Matthias Boss"
  - "Marcello Magliocchi"
  - "Marcio Mattos"
  - "Rachel Musson"
  - "Alan Tomlinson"
  - "Dave Tucker"
  - "Phil Marks"
sets:
  - - Hannah Marshall
    - Josep Maria Balanya
  - - Matthias Boss
    - Marcello Magliocchi
    - Marcio Mattos
  - - Rachel Musson
  - - Alan Tomlinson
    - Dave Tucker
    - Phil Marks
year: 2015
---
We bring you a stunning programme of free improvisation to start 2015 in our new regular afternoon slot. **N.B Doors open at 2.00 pm 14.00hr**

### {{<artist "Hannah Marshall">}} (cello) & {{<artist "Josep Maria Balanya">}} (piano)

{{<youtube aPbtQyXucf4>}}

### {{<artist "Matthias Boss">}} (violin), {{<artist "Marcello Magliocchi">}} (percussion) & {{<artist "Marcio Mattos">}} (cello)

{{<youtube imJ9P7E98Go>}}

### {{<artist "Rachel Musson">}} (saxophone)

{{<youtube fJkFv5btBmo>}}

### Alan Tomlinson trio – {{<artist "Alan Tomlinson">}} (trombone), {{<artist "Dave Tucker">}} (guitar) & {{<artist "Phil Marks">}} (drums)

{{<youtube Aq5gOfxHBDk>}}

---
title: 21 March 2010
categories:
  - Afternoon Sessions
date: "2010-03-21T00:00:00Z"
concert_date: 2010-03-21T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Steve Beresford"
  - "Ute Kanngieser"
  - "Pascal Marzan"
  - "The Alan Wilkinson"
  - "Alan Wilkinson"
  - "Pat Thomas"
  - "Mark Sanders"
  - "John Russell"
  - "Paul G Smyth"
sets:
  - - Steve Beresford
    - Ute Kanngieser
  - - Pascal Marzan
  - - The Alan Wilkinson
    - Alan Wilkinson
    - Pat Thomas
    - Mark Sanders
  - - John Russell
    - Paul G Smyth
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2010
---

### {{<artist "Steve Beresford">}} (piano ) & {{<artist "Ute Kanngieser">}} (cello )
Steve Beresford is a highly imaginative musician with a depth of knowledge across all musical genres. Much in demand as a collaborator in a multitude of projects, working across the full spectrum from free improvisation through jazz-based material to pop, reggae and MOR songs and on to fully scored music for film and dance. He is joined for a first time duo pairing with London Improvisers Orchestra partner, cellist Ute Kanngieser (AMM and Treehouse)

{{<youtube mp-iVCWWdHE>}}

### {{<artist "Pascal Marzan">}} (guitar)

Paris based Pascal Marzan is a leading exponent of the nylon strung classical guitar in the world of free improvisation. Through his annual "Roc de Choc" festival and monthly concert series he is also responsible for encouraging the development of the music in France. A dynamic musician his music moves between small delicate sounds to orchestra like textures and is never anything less than engaging.

{{<youtube l_RL4x-ikGM>}}

### {{<artist "The Alan Wilkinson">}} trio - {{<artist "Alan Wilkinson">}} (saxophones), {{<artist "Pat Thomas">}} (piano) & {{<artist "Mark Sanders">}} (drums)
Alan Wilkinson has been blowing up a storm with this trio. A hard hitting, energy based music to warm the cockles! In the company of two of the best musicians in the country, ably helping to stoke up the temperature, this established group’s interplay is full of wit, and humour and nothing less than a real celebration of the human spirit. Impossible not to be moved!

{{<youtube F7d_r3graOY>}}

### {{<artist "John Russell">}} (guitar) & {{<artist "Paul G Smyth">}} (piano)
Paul G Smyth is Ireland’s leading improviser where he has played and toured with a.o {{<artist "Charles Gayle">}}, {{<artist "Derek Bailey">}}, {{<artist "Keiji Haino">}}, {{<artist "Evan Parker">}}, {{<artist "Barry Guy">}} and {{<artist "Damo Suzuki">}}.

Outside of improvisation, his group The Jimmy Cake was described in The Irish Times as being "the most powerful musical force in Ireland". He is joined by an old partner, Mopomoso boss and renowned guitarist John Russell, for a duo that first performed in 2000 at the Project festival in Dublin.

{{<youtube NkpqE5kVcLk>}}

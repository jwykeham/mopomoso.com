---
title: 21 June 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-06-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Kay Grant"
  - "Ntshuks Bonga"
  - "Georges Paul"
  - "John Russell"
  - "Roger Turner"
sets:
  - - Kay Grant
    - Ntshuks Bonga
  - - Georges Paul
  - - John Russell
    - Roger Turner
year: 2015
---

### {{<artist "Kay Grant">}} (voice) & {{<artist "Ntshuks Bonga">}} (saxophone)

{{<youtube hQTw0k9nsck>}}

### {{<artist "Georges Paul">}} (saxophones)

https://www.youtube.com/watch?v=9lz7iK1RN78

### {{<artist "John Russell">}} (guitar) & {{<artist "Roger Turner">}} (percussion)

{{<youtube aYcuH-hWKD4>}}

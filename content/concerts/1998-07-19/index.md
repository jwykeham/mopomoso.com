---
artists:
  - Maggie Nichols
  - Marcio Mattos
  - Veryan Weston
  - Gail Brand
concert_date: 1998-07-19T20:00:00Z
concert_series: At The Red Rose
date: 1998-07-19T20:00:00Z
notes: null
sets: []
sources:
  - https://www.newspapers.com/newspage/260814384/
  - https://www.newspapers.com/newspage/260814384/
title: 19 July 1998
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1998
---
---
title: 16 November 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-11-16T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: London Jazz Festival
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Martin Speake"
  - "Mark Sanders"
  - "Richard Scott"
  - "Adam Linson"
  - "Phil Marks"
  - "David Birchall"
  - "Kay Grant"
  - "Marcio Mattos"
  - "Phil Wachsmann"
  - "Matt Hutchinson"
sets:
  - - Martin Speake
    - Mark Sanders
  - - Richard Scott
    - Adam Linson
    - Phil Marks
    - David Birchall
  - - Kay Grant
    - Marcio Mattos
  - - Phil Wachsmann
    - Matt Hutchinson
year: 2014
---

To coincide with the London Jazz Festival we have programmed four sets of excellent music from the freer end of things.

### {{<artist "Martin Speake">}} (sax) & {{<artist "Mark Sanders">}} (drums, percussion)

{{<youtube _MJNwZgmaHA>}}

### Richard Scott’s Lightning Ensemble – {{<artist "Richard Scott">}} (buchla lightning, wigi electronics), {{<artist "Adam Linson">}} (bass), {{<artist "Phil Marks">}} (percussion), {{<artist "David Birchall">}} (guitar)

{{<youtube KY1iAPsgkMM>}}

### {{<artist "Kay Grant">}} (voice) & {{<artist "Marcio Mattos">}} (cello)

{{<youtube KOSQmBwCOsk>}}

### {{<artist "Phil Wachsmann">}} (violin) & {{<artist "Matt Hutchinson">}} (piano)

{{<youtube 21sA7oHiUOk>}}

---
title: "24 July 2022"
date: 2023-01-14T13:22:54Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-07-24T01:00:00+01:00
concert_series: Afternoon Sessions
artists:
  - "Caroline Kraabel"
  - "Steve Noble"
  - "Alan Wakeman"
  - "Mike Walter"
  - "Susanna Ferrar"
  - "Alex Ward"
  - "Steve Beresford"
  - "Mark Browne"
  - "Veryan Weston"
  - "Sylvia Hallett"
  - "David Holland"
  - "Hugh Metcalfe"
  - "George Haslam"
year: 2022
---
On Sunday 24th July, magical Mopomoso is thrilled to present to you a unique tribute show to the one and only Lol Coxhill!
<!--more-->
Please note, this special Mopomoso performance will start at midday/12pm, doors will open at 11:30am.

This fantastic afternoon of wondrous music making will feature the follow sublime improvisers:
- {{< artist "Caroline Kraabel" >}} (alto)
- {{< artist "Steve Noble" >}} (drums)
- {{< artist "Alan Wakeman" >}} (tenor)
- {{< artist "Mike Walter" >}} (tenor, soprano saxophones)
- {{< artist "Susanna Ferrar" >}} (violin)
- {{< artist "Alex Ward" >}} (clarinet, guitar)
- {{< artist "Steve Beresford" >}} (piano, electronics)
- {{< artist "Mark Browne" >}} (soprano saxophone etc)
- {{< artist "Veryan Weston" >}} (piano)
- {{< artist "Sylvia Hallett" >}} (violin etc)
- {{< artist "David Holland" >}} (piano)
- {{< artist "Hugh Metcalfe" >}} (guitar, films etc)
- {{< artist "George Haslam" >}} (saxophone) & maybe more…

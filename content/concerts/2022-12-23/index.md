---
title: "December 2022"
date: 2023-01-12T18:06:50Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-12-18T00:00:00Z
concert_series: Afternoon Sessions
artists:
  - "Sylvia Hallett"
  - "Viv Corringham"
  - "Charlotte Keeffe"
  - "John Bisset"
  - "Andrea Bolzoni"
  - "Paul Jolly"
  - "Dave Tucker"
  - "Phil Minton"
  - "Maggie Nicols"
  - "Tim Fletcher"
  - "Ivor Kallin"
  - "Adrian Northover"
  - "John Eyles"
  - "Julian Woods"
  - "George Garford"
  - "Douglas Benford"
year: 2022
---
Mopomoso is the UK’s longest running concert series dedicated to freely improvised music!

Come & join us on Sunday 18th December for our Christmas Party!
<!--more-->
Please note, we’ll be kicking off earlier this month at 12pm, so that we have more time for lots of musical bursts!

Expect to hear music from {{<artist "Sylvia Hallett">}}, {{<artist "Viv Corringham">}}, {{<artist "Charlotte Keeffe">}}, {{<artist "John Bisset">}}, {{<artist "Andrea Bolzoni">}}, {{<artist "Paul Jolly">}}, {{<artist "Dave Tucker">}}, {{<artist "Phil Minton">}}, {{<artist "Maggie Nicols">}}, {{<artist "Tim Fletcher">}}, {{<artist "Ivor Kallin">}}, {{<artist "Adrian Northover">}}, {{<artist "John Eyles">}}, {{<artist "Julian Woods">}}, {{<artist "George Garford">}}, {{<artist "Douglas Benford">}} and many more…

and many, many more…

Come & join the famous Mopomoso Christmas Party!

This will be another fantastic Mopomoso afternoon not to be missed – book your tickets now!

 


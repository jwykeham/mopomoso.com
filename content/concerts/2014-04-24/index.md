---
title: 18 May 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-05-18T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Chris Burn"
  - "Matt Hutchinson"
  - "Jim Dvorak"
  - "Harrison Smith"
  - "Paul Pignon"
  - "Thomas Bjelkeborn"
  - "Phil Minton"
  - "John Russell"
sets:
  - - Chris Burn
    - Matt Hutchinson
  - - Jim Dvorak
    - Harrison Smith
  - - Paul Pignon
    - Thomas Bjelkeborn
  - - Phil Minton
    - John Russell
year: 2014
---

An evening featuring four established duos from the finest in free improvisation

### {{<artist "Chris Burn">}} (trumpets), {{<artist "Matt Hutchinson">}} (piano)

{{<youtube QXDbZ2J-1vI>}}

### {{<artist "Jim Dvorak">}} (trumpet), {{<artist "Harrison Smith">}} (saxophone)

{{<youtube 33-lYJgbiiY>}}

### SQ – {{<artist "Paul Pignon">}} (single reeds and electronics) & {{<artist "Thomas Bjelkeborn">}} (electronics)

{{<youtube f7YY4gp_1_E>}}

We gratefully acknowledge financial assistance from The Swedish Arts Council towards this appearance.

[![swedish arts logo](https://www.mopomoso.com/wp-content/uploads/2014/04/swedsihartscouncil_logo.jpg "swedsihartscouncil_logo")](https://www.mopomoso.com/wp-content/uploads/2014/04/swedsihartscouncil_logo.jpg)

### {{<artist "Phil Minton">}} (voice) & {{<artist "John Russell">}} (guitar)

{{<youtube 7ypupDxWI2o>}}

---
artists:
  - Evan Parker
concert_date: 1996-12-15T15:00:00Z
concert_series: At The Red Rose
date: 1996-12-15T15:00:00Z
notes: null
sets: []
sources:
  - https://www.newspapers.com/image/790539044
  - https://www.newspapers.com/image/260603844
title: 15 December 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
costs:
  - key: "Standard"
    value: "£8"
  - key: "Vortex Members & Concessions"
    value: "£6"
year: 1996
---

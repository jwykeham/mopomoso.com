---
artists: []
concert_date: 1996-01-28T20:00:00Z
concert_series: At The Red Rose
date: 1996-01-28T20:00:00Z
notes:
  - '>Eccentric vocalist performs *riverrun* bashed on *Finnegan''s Wake*. - *The Independant 26 Jan 1996*'
sets:
  - - Phil Minton Quartet
  - - Lol Coxhill
sources:
  - https://www.newspapers.com/image/719375492
  - https://www.newspapers.com/image/260858642
title: 28 January 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
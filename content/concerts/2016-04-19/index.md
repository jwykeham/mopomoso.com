---
title: 15 May 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-05-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: 25th Anniversary
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Rangecroft"
  - "Dave Tucker"
  - "Marcio Mattos"
  - "Mark Sanders"
  - "Matilda Rolfsson"
  - "Hannah Marshall"
  - "Julie Kjaer"
  - "Henry Lowther"
  - "John Russell"
sets:
  - - John Rangecroft
    - Dave Tucker
    - Marcio Mattos
    - Mark Sanders
  - - Matilda Rolfsson
    - Hannah Marshall
    - Julie Kjaer
  - - Henry Lowther
    - John Russell
year: 2016
---

Mopomoso's 25th anniversary afternoon sessions continue with three very different sets from the wonderful world of free improvisation.

### {{<artist "John Rangecroft">}} (sax), {{<artist "Dave Tucker">}} (guitar), {{<artist "Marcio Mattos">}} (bass) & {{<artist "Mark Sanders">}} (drums)

![group one rangecroft jjppeegg](https://www.mopomoso.com/wp-content/uploads/2016/04/group-one-rangecroft-jjppeegg.jpg)

Highly respected reedsman Rangecroft, here on tenor saxophone, leading his own quartet which promises melodic and rhythmic invention of a high calibre. With three of the UK’s finest providing a propulsive energy, a great way to open proceedings.

### {{<artist "Matilda Rolfsson">}} (perc), {{<artist "Hannah Marshall">}} (cello) & {{<artist "Julie Kjaer">}} (sax)

![h,j and m](https://www.mopomoso.com/wp-content/uploads/2016/04/hj-and-m.jpg)

Outstanding Danish saxophonist, British cellist and Norwegian percussionist join forces in an ever changing trialogue full of ideas and invention, brimming with energy and a genuine sense of fun. Delightful!

### {{<artist "Henry Lowther">}} (trumpet) & {{<artist "John Russell">}} (guitar)

![h and j peggy](https://www.mopomoso.com/wp-content/uploads/2016/04/h-and-j-peggy-1024x594.jpg)

The last set of the Mopomoso Red Rose series was played by this long standing partnership. A poised and very open duo who are unaverse to risk taking while sharing a mutual respect and understanding for each other’s music. Their regular trio with violinist Satoko Fukuda plays later in the year so this is a rare chance to catch these two fine musicians in a duo.

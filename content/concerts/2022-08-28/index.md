---
title: "28 August 2022"
date: 2023-01-14T10:18:59Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-08-28T01:00:00+01:00
concert_series: Afternoon Sessions
artists:
  - "Rick Jensen"
  - "Jordan Muscatello"
  - "Paul May"
  - "Emily Shapiro"
  - "Phil Durrant"
  - "Jackie Walduck"
  - "Marjolaine Charbin"
sets:
  - - "Rick Jensen"
    - "Jordan Muscatello"
    - "Paul May"
  - - "Emily Shapiro"
  - - "Phil Durrant"
    - "Jackie Walduck"
    - "Marjolaine Charbin"
year: 2022
---
Mopomoso is the UK’s longest running concert series dedicated to freely improvised music! Come & join us on the 4th Sunday of every month for an afternoon full of captivating musical meetings…

On Sunday 28th August Mopomoso (live) is delighted to be presenting the following magical musicians;

### {{<artist "Rick Jensen">}} (tenor), {{<artist "Jordan Muscatello">}} (bass) & {{<artist "Paul May">}} (drums)

### {{<artist "Emily Shapiro">}} (bass clarinet)

### {{<artist "Phil Durrant">}} (electric mandolin), {{<artist "Jackie Walduck">}} (vibes) & {{<artist "Marjolaine Charbin">}} (piano)

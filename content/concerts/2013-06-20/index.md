---
title: 21 July 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-07-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Jim Dvorak"
  - "Mark Hewins"
  - "Dave Tucker"
  - "Pat Thomas"
  - "Phil Minton"
  - "Roger Turner"
  - "Maggie Nicols"
  - "John Russell"
  - "Mia Zabelka"
sets:
  - - Jim Dvorak
    - Mark Hewins
  - - Dave Tucker
    - Pat Thomas
    - Phil Minton
    - Roger Turner
  - - Maggie Nicols
    - John Russell
    - Mia Zabelka
year: 2013
---

### {{<artist "Jim Dvorak">}} (trumpet) & {{<artist "Mark Hewins">}} (guitar)

{{<youtube 8ob2TVbUUxM>}}

### SCATTER – {{<artist "Dave Tucker">}} (guitar), {{<artist "Pat Thomas">}} (keyboards), {{<artist "Phil Minton">}} (voice) & {{<artist "Roger Turner">}} (drums)

{{<youtube aiGLPpjExIY>}}

### TRIO BLURB – {{<artist "Maggie Nicols">}} (voice), {{<artist "John Russell">}} (guitar) & {{<artist "Mia Zabelka">}} (violin,voice)

{{<youtube 5ZwKU_2oHi0>}}

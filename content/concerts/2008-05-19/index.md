---
title: 18 May 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-05-18T01:00:00+01:00
concert_series: "Afternoon Sessions"
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Arc"
  - "Sylvia Hallet"
  - "Gus Garside"
  - "Danny Kingshill"
  - "John Coxon"
  - "Steve Noble"
  - "Pat Thomas"
  - "Alan Wilkinson"
  - "Lol Coxhill"
  - "John Russell"
sets:
  - - "Arc"
    - "Sylvia Hallet"
    - "Gus Garside"
    - "Danny Kingshill"
  - - "John Coxon"
    - "Steve Noble"
    - "Pat Thomas"
    - "Alan Wilkinson"
  - - "Lol Coxhill"
    - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

### {{<artist "Arc">}} - {{<artist "Sylvia Hallet">}}, {{<artist "Gus Garside">}} & {{<artist "Danny Kingshill">}}

{{<youtube MCltwtFNalQ>}}


### {{<artist "John Coxon">}}, {{<artist "Steve Noble">}}, {{<artist "Pat Thomas">}} & {{<artist "Alan Wilkinson">}}

{{<youtube OmBtfcnwuGg>}}


### {{<artist "Lol Coxhill">}} & {{<artist "John Russell">}}

{{<youtube hGg9H58nIlY>}}


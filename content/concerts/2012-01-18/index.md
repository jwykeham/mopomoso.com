---
title: 19 February 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-02-19T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Nina De Heney"
  - "Lisa Ullen"
  - "Gianni Mimmo"
  - "Harri Sjoestrom"
  - "John Butcher"
  - "Dominic Lash"
  - "John Russell"
year: 2012
---

### {{<artist "Nina De Heney">}} (bass) / {{<artist "Lisa Ullen">}} (piano)

{{<youtube 5UcVgNSDiv4>}}

### {{<artist "Gianni Mimmo">}} / {{<artist "Harri Sjoestrom">}} (soprano saxophones)

{{<youtube x7hbHCfBzt4>}}

### {{<artist "John Butcher">}} (sax) / {{<artist "Dominic Lash">}} (bass) / {{<artist "John Russell">}} (guitar) trio

{{<youtube skmK0a4tZv0>}}

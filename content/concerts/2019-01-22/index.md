---
title: 17 February 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-02-17T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Viv Corringham"
  - "Mia Zabelka"
  - "Alison Blunt"
  - "Lawrence Casserley"
  - "Hannah Marshall"
  - "Maggie Nicols"
  - "John Black"
  - "Emmanmuelle Pellegrini"
  - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2019
---
### {{<artist "Viv Corringham">}} & {{<artist "Mia Zabelka">}}

![](https://www.mopomoso.com/wp-content/uploads/2019/01/Screenshot-2019-01-16-at-19.55.37.png)

Viv Corringham (vocals) makes a welcome return to the Mopomoso stage, this time in the company of another of our ‘out of town regulars’ Austrian violinist / vocalist Mia Zabelka. Delighted to have them back in what will be their debut performance as a duo.

### {{<artist "Alison Blunt">}}, {{<artist "Lawrence Casserley">}} & {{<artist "Hannah Marshall">}}

![](https://www.mopomoso.com/wp-content/uploads/2019/01/alison-hannah-lawrence.png)

A fine trio put together by leading electronic music practitioner Casserley and featuring two of the UK’s leading string players in Blunt on violin and Marshall on cello. A sonic bath for the ears!

### {{<artist "Maggie Nicols">}} & {{<artist "John Black">}}

![](https://www.mopomoso.com/wp-content/uploads/2019/01/image1.jpeg)

Vocalist Maggie Nicols needs no introduction. She has been at the heart of free-improvisation since the 1960s and is as vital today as ever. Many performers cite her as a nurturing influence on their art.

John Black (guitar) is an arch-improviser and musical journeyman who has been active in free-improvisation for many years, alongside his work in sacred and Spanish music traditions

### {{<artist "Emmanmuelle Pellegrini">}} & {{<artist "John Russell">}}

![](https://www.mopomoso.com/wp-content/uploads/2019/01/Screenshot-2019-01-16-at-20.29.17.png)

E. Pellegrini (poetry, action, improvisation)
Emmanuelle Pellegrini’s performances are more or less poetic but for sure quite loud. She crosses the road of many improvisers, dancers, video artists or poets as well as visual artists. Her poetic work is mainly influenced by the sound poet Bernard Heidsieck. On stage she practices a form of improvised poetry, guided by listening and memories. Performed body is also important in her work focused on orality. She has made two crucial encounters : one with the french choreographer Emilie Borgo and the other with the italian percussionist Elisabeth Flunger with whom she has developed a number of different projects. More recently she has worked with Lee Patterson, Xavier Saïki or Fred Marty and has also played with Jean-Philippe Gross, Xavier Charles, Thomas Charmetant, Isabelle Duthoit

In the East of France she curates Densités, International Festival of improvised music and performing arts.

Guitarist John Russell has been organising and playing concerts of freely improvised music since the early 1970s. He founded Mopomoso in 1991 and co-directs the Weekertoft label with Dublin based pianist Paul G Smyth. As well as being a member of a number of established groups he runs a developmental project Quaqua and this June will be visiting professor at The Musashino Art University in Tokyo, where he will lecture and run workshops on the subject of free improvisation.

---
title: 21 February 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-02-21T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Jasnoch"
  - "Derek Saw"
  - "Adam Woolfe"
  - "Matt Hutchinson"
  - "Phil Minton"
  - "John Russell"
sets:
  - - John Jasnoch
    - Derek Saw
    - Adam Woolfe
  - - Matt Hutchinson
  - - Phil Minton
    - John Russell
year: 2016
---

### {{<artist "John Jasnoch">}} (guitar), {{<artist "Derek Saw">}} (valve trombone) & {{<artist "Adam Woolfe">}} (clarinet and piano)

{{<youtube fLbIWX9vUmk>}}

Excellent Sheffield-based trio with Saw and Jasnoch last appearing here as part of The Linda Sharrock group. Also forming part of the Sonny Simmons UK quartet, they are joined here by Adam Woolfe, whose other work has included the Kenneth Marx hip hop collective and releases under the name Tantric Doctors.

### {{<artist "Matt Hutchinson">}} (piano)

{{<youtube uozPEEKDg6k>}}

From art school with Terry Nillson-Love via jazz groups with, amongst others, Mike Osborne, these days Hutchinson is best known for both his electronic and acoustic work with Chris Burn and Phil Wachsmann. A rare chance to hear this ever inventive and sensitive player in a solo context.

### {{<artist "Phil Minton">}} (voice) & {{<artist "John Russell">}} (guitar)

{{<youtube nmhVhzjoOts>}}

Phil Minton is one of the most adventurous singers on the scene today. From early work in jazz through the Mike Westbrook groups and on to major contemporary projects including the founding of the Feral Choir, Minton has been amazing audiences around the world. Here with regular musical partner Russell in a duo that never fails to engage the audience at the cutting edge of creative music as it unfolds.

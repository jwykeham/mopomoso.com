---
artists:
  - John Russell
  - Kyoske Tokunaga
  - Hugh Davies
concert_date: 1995-10-15T20:00:00+01:00
concert_series: At The Red Rose
date: 1995-10-15T20:00:00+01:00
notes: null
sets: []
sources:
  - https://www.newspapers.com/image/790519910
  - https://www.newspapers.com/image/260688156
title: 15 October 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1995
---
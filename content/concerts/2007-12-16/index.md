---
artists:
  - Steve Beresford
  - John Russell
  - Kay Grant
  - Roger Turner
  - Dave Tucker
  - Dominic Lash
concert_date: 2007-12-16T20:00:00Z
concert_series: At The Red Rose
date: 2007-12-16T20:00:00Z
notes: null
sets:
  - - Steve Beresford
    - John Russell
    - Kay Grant
    - Roger Turner
  - - Dave Tucker
    - Dominic Lash
  - - Dave Tucker
    - Dominic Lash
    - Chris Cundy
    - Dave Fowler
    - Adrian Northover
  - - Alison Blunt
    - Ivor Kallin
    - Hannah Marshall
    - Dominic Lash
  - - Lol Coxhill
    - John Russell
    - Pascal Marzan
  - - Steve Beresford
    - Pascal Marzan
sources:
  - https://www.youtube.com/watch?v=bWsJqk4Onv8
  - https://www.thewire.co.uk/audio/tracks/john-russell-remembers-special-performances-at-mopomoso
  - http://dominiclash.blogspot.com/p/gig-archive.html
  - https://pascalmarzan.blogspot.com/2007/11/vendredi-14-dcembre-2007-national-film.html
title: 16 December 2007
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2007
---

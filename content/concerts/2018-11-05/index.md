---
title: 18 November 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-11-18T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Tom Jackson"
  - "Martin Hackett"
  - "Blanca Regina"
  - "Dave Tucker"
  - "Matt Hutchinson"
  - "Chris Burn"
  - "Kay Grant"
  - "Alex Ward"
  - "Niklas Fite"
  - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Tom Jackson
    - Martin Hackett
  - - Blanca Regina
    - Dave Tucker
  - - Matt Hutchinson
    - Chris Burn
  - - Kay Grant
    - Alex Ward
  - - Niklas Fite
    - John Russell
year: 2018
---
November's concert in the Mopomoso Afternoon Sessions series highlights the music of the duo with five contrasting sets from ten musicians who have each developed their own personal approach to playing. An engaging mixture of the electric and the acoustic, music doesn't get **liver** or more lively than this!

### {{<artist "Tom Jackson">}} (clarinet) & {{<artist "Martin Hackett">}} (synthesiser)

![](https://www.mopomoso.com/wp-content/uploads/2018/11/Tom-and-Martin.png)

### {{<artist "Blanca Regina">}} (voice and electronics) & {{<artist "Dave Tucker">}} (piano and devices)

![](https://www.mopomoso.com/wp-content/uploads/2018/11/blanca-and-dave-keys.png)

### {{<artist "Matt Hutchinson">}} (piano) & {{<artist "Chris Burn">}} (trumpet)

![](https://www.mopomoso.com/wp-content/uploads/2018/11/matt-and-chris-copy-bw.jpg)

### {{<artist "Kay Grant">}} (voice) & {{<artist "Alex Ward">}} (clarinet)

![](https://www.mopomoso.com/wp-content/uploads/2018/11/kay-and-alex-copy-b-w.jpg)

### {{<artist "Niklas Fite">}} (guitar) & {{<artist "John Russell">}} (guitar) duo

### ![](https://www.mopomoso.com/wp-content/uploads/2018/11/niklas-and-john-copy-b-w.png)

Niklas photo Christina Marx John by Peter Gannushkin

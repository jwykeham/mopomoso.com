---
title: 15 June 2014
categories:
  - Afternoon Sessions
category_sticky_post:
  - '0'
date: "1970-01-01T00:00:00Z"
concert_date: 2014-06-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Seth Bennett"
  - "Shaun Blezard"
  - "Anton Hunter"
  - "Rachel Musson"
  - "Shelly Knotts"
  - "Julie Kjaer"
  - "Steve Beresford"
  - "Arthur Bull"
  - "Pascal Marzan"
  - "John Russell"
sets:
  - - Seth Bennett
    - Shaun Blezard
    - Anton Hunter
    - Rachel Musson
    - Shelly Knotts
    - Julie Kjaer
    - Steve Beresford
  - - Arthur Bull
    - Pascal Marzan
    - John Russell
year: 2014
---

Presenting an intriguing "concert in two halves" with the first half from the Portfolio Improvisers group and the second featuring three fine guitar players from the UK, France and Canada.

## Part One – Portfolio Improvisers:

![Sound and Music](https://www.mopomoso.com/wp-content/uploads/2014/05/sam-small-capture-jpeg1.jpg "Sound and Music")

Seth Bennett (bass), Shaun Blezard (electronics), Anton Hunter (guitar), Rachel Musson (saxophone), Shelly Knotts (electronics), Julie Kjaer (saxophone, flute) led by Steve Beresford (piano)

We are delighted to be hosting this excellent scheme with many thanks to Sound and Music.

## Part Two – Three guitarists:

Arthur Bull (electric), Pascal Marzan (classical), John Russell (archtop)

## Portfolio Improvisers

In the words of [Sound and Music](https://www.soundandmusic.org/ "Sound and Music") their Portfolio scheme provides a key development opportunity for composers to create new work with and for some of the UK?s leading ensembles and presenters of new music. Portfolio enables them to develop their portfolio of compositions and gain vital artistic and practical insight and experience in working with professional ensembles and presenting organisations, and delivering new work to public audiences. We are delighted to host this fine group of improvisers mentored by Steve Beresford.

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/wooda-grab-jpeg3.jpg "wooda grab jpeg")](https://www.mopomoso.com/wp-content/uploads/2014/05/wooda-grab-jpeg3.jpg)

### {{<artist "Seth Bennett">}} (bass)

Begun musical life at Sheffield Cathedral choir as a boy, studying violin, piano and guitar, before concentrating on bass guitar. His twenties saw him touring Europe in various avant garde punk bands and studying Jazz, before taking up the double bass. He now combines study of the bass in improvised music with work as a freelance, including work with folk singer Mary Hampton, Afro/punk Dadaists Orchestre Tout Puissant Marcel Duchamp, the orchestra Sinfonia of Leeds, contemporary jazz with i.d.s.t., and own projects including 7 hertz, Nut Club the Bennett Cole Orchestra.

### {{<artist "Shaun Blezard">}} (electronics)

Currently plays solo under the Clutter name and in a variety of improvising bands – from Hugs Bison (iPad duo) to Kipple (6 piece free jazz/improv). He has played all over the UK and toured the US including concerts at Liverpool Sound City, Manchester Jazz Festival, St Petersburg Festival of Noise and Lancaster Jazz Festival. Shaun is also a community musician and digital artist, helping people to explore their communities using improvisation, animation, film, sound art and has worked for companies such as Sound & Music, BBC & The National Trust. 

[Shaunblezard.net](https://shaunblezard.net/ "shaunblezard.net")

### {{<artist "Anton Hunter">}} (guitar)

From Manchester his music ranges from processing canvas sounds with Takahashi?s Shellfish Concern to the ambient and skronky riffs of HAQ as well as playing in The Beats & Pieces Big Band. He can be found playing in a variety of settings with musicians such as a.o Mick Beck, Sam Andreae, Simon Prince, Rodrigo Constanzo, Cath Roberts, Phil Marks, David Birchall, Keith Jafrate. Improvisation is a core activity and in 2007 he formed The Noise Upstairs. Initially a meeting place and jam session for improvisers, it has grown to include events in Manchester and Sheffield, regular workshops on a range of subjects and a small record label. He co- runs Efpi Records, a contemporary jazz record label, who also run regular gigs in Manchester and have links across Scandinavia.

### {{<artist "Rachel Musson">}} (saxophone)

Involved with a variety of improvisation- based projects, icluding a trio with Liam Noble and Mark Sanders. Also a trio with Danish saxophonist Julie Kjaer and cellist Hannah Marshall, and a duo with bassist Olie Brice. She is a member of clarinetist Alex Ward?s new quintet and Eddie Prevost?s Atmospheric Disturbance She has also written for and recorded with her own band, Skein, which released a highly acclaimed album on F-IRE Records at the end of 2010. She was picked by BBC Jazz on Three to perform at Cheltenham Jazz Festival last year and has performed with a.o Alcyona Mick, Han Bennink, Liam Noble, Gail Brand, Eddie Prevost, Olie Brice, Federico Ughi, Mary Halvorson, John Russell, Adam Linson, Seb Rochford.

### {{<artist "Shelly Knotts">}} (electronics)

Completed a Masters degree in Composition in 2012 at University of Birmingham, now studying for a PhD in Live Computer Music at Durham University. A broad musical involvement ranges from electroacoustic tape music to live-coded dance music performing solo and as a member of various collaborations across the UK and Europe. Including a.o. BEAST (Birmingham ElectroAcoustic Sound Theatre) 2007-2011 and sound art collective SOUNDkitchen 2010-2012. Her work has been published on Chordpunch record label, Absence of Wax netlabel and in Leonardo Music Journal. Recent projects include the 2011 PRSF commissioned commissioned project ‘Interleave’ – combining the traditional Jazz Quartet with experimental live-electronics, network music band BiLE (Birmingham Laptop Ensemble) and live coded generative audio-visual performance ‘Valevari’ with Alo Allik.

[https://soundcloud.com/shelly-knotts](https://soundcloud.com/shelly-knotts "https://soundcloud.com/shelly-knotts")

### {{<artist "Julie Kjaer">}} (saxophone, flute)

Has toured internationally with Django Bates and his band StoRMChaser, and the Danish big-band “Blood Sweat Drum?n Bass” where she has played with amongst others: Dave Douglas, Arve Henriksen and Palle Mikkelborg. She plays with London Improvisers Orchestra and is a leader and sidewoman of several English and Danish ensembles including a new trio project with bass player John Edwards and drummer Steve Noble and on a project with the Danish-English sextet ?Pierette Ensemble?. “Kjær?s playing reached Braxton-esque levels of surreal linguistic expression. This was a synthesis of dark Scandinavian folk with the Art Ensemble of Chicago.” J. Kassman-Todd, Jazzwise (Dec 2009) 

[www.juliekjaer.com](https://www.juliekjaer.com "www.juliekjaer.com")

### [![](https://www.mopomoso.com/wp-content/uploads/2014/05/steve-beresford.jpg "steve beresford")](https://www.mopomoso.com/wp-content/uploads/2014/05/steve-beresford.jpg)

### {{<artist "Steve Beresford">}}

Steve is internationally renowned as a free improviser on piano and electronics. He plays with and conducts the London Improvisers Orchestra and has worked with hundreds of musicians, including Derek Bailey, Han Bennink, Evan Parker, Otomo Yoshihide and John Zorn. Photograph Andy Newcombe.

[![Sound and Music](https://www.mopomoso.com/wp-content/uploads/2014/05/sam-logo-jpeg.jpg "Sound and Music")](https://www.soundandmusic.org/)[](https://www.soundandmusic.org/ "www.soundandmusic.org")

## Three guitarists

### {{<artist "Arthur Bull">}}

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/arthur-bull.jpg "arthur-bull")](https://www.mopomoso.com/wp-content/uploads/2014/05/arthur-bull.jpg)

Arthur has been active on the improvised music scene in Canada since the mid-1970s, when he played with a.o. John Oswald, Michael Snow, Stu Broomer and the CCMC in the early days of the Music Gallery in Toronto. In 1980?s he was a member of the Bill Smith Ensemble, and collaborated with various filmmakers and sound poets, including the Four Horsemen and Bob Cobbing. Since the late 1990?s he has toured and recorded extensively in a duo with fellow guitarist Daniel Heïkalo. He has performed in concert with a.o. Roscoe Mitchell, John Tchicai, Joe McPhee, Roger Turner, Peter Kowald, Paul Rutherford, Fred Anderson, Mike Cooper, Michael Snow, John Oswald, Paul Dutton, Bill Smith, Derek Bailey, Kidd Jordan and John Butcher. He is also a published poet, and an activist in the small-scale fisheries movement. He lives on Digby Neck in Nova Scotia.

### {{<artist "Pascal Marzan">}}

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/pascal-pic1-300x272.jpg "pascal pic")](https://www.mopomoso.com/wp-content/uploads/2014/05/pascal-pic1.jpg)

Pascal is a classically trained guitarist active in Paris, his home city, both as an improviser and organiser of improvised music events. He should be well known to Mopomoso audiences where he has performed on a number of occasions. As well as his duo with John Russell (CD Translations – Emanem 5019) work with other English improvisers includes with a.o. Steve Beresford, Keith Rowe and the London Improvisers Orchestra. His solo performance at Freedom of the City 2007 was one of the highlights of the festival and was rapturously received by the audience at London’s Red Rose club. He has also recorded in a quartet with Philipp Wachsmann, Teppo Hauta-Aho and Roger Turner. His playing has been influenced by his interest in the folk musics of Eastern Europe, Asia and Africa. Photograph Helen Petts.

### {{<artist "John Russell">}}

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/JRussell_pic1-300x199.jpg "JRussell_pic1")](https://www.mopomoso.com/wp-content/uploads/2014/05/JRussell_pic1.jpg)

John got his first guitar in 1965 while living in Kent and began to play in and around London from 1971 onwards. An early involvement with the emerging free improvisation scene (from 1972) followed, seeing him play in such places as The Little Theatre Club, Ronnie Scott’s, The Institute of Contemporary Arts, The Musicians’ Co- Op and the London Musicians’ Collective. From 1974 his work extended into teaching, broadcasts (radio and television) and touring in the United Kingdom and, ever extensively, in other countries around the world . He has played with many of the world’s leading improvisers and his work can be heard on over 50 CDs. In 1981, he founded QUAQUA, a large bank of improvisers put together in different combinations for specific projects and, in 1991, he started MOPOMOSO which has become the UK’s longest running concert series featuring mainly improvised music. Photograph Caroline Forbes

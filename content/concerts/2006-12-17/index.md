---
artists:
  - Dominic Lash
concert_date: 2006-12-17T20:00:00Z
concert_series: At The Red Rose
date: 2006-12-17T20:00:00Z
notes: null
sets:
  - - Dominic Lash
    - Dave Tucker
    - Dave Fowler
    - Yedo Gibson
  - - Dominic Lash
    - Lol Coxhill
    - Alexander Hawkins
  - - Dominic Lash
    - John Russell
    - Alexander Hawkins
    - Stegan Keune
    - Matt Hutchinson
    - Hannah Marshall
    - Kay Grant
    - Chefa Alonso
    - Harry Gilonis
sources:
  - http://dominiclash.blogspot.com/p/gig-archive.html
title: 17 December 2006
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2006
---
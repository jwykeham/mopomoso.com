---
title: "19 May 2024"
date: 2024-04-24T10:58:43+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-05-19 00:00:00 +0100 BST
concert_series: Mopomoso Live
artists:
  - Geoff Heran
  - Olie Brice
  - Milo Fell
  - Sylvia Hallett
  - Susanna Ferrar
  - Maggie Nicols
  - Tracy Lisk
  - Reuben Derrick
  - Charlotte Keeffe
  - Mia Zabelka
sets:
  - - Geoff Heran
    - Olie Brice
    - Milo Fell
  - - Sylvia Hallett
    - Susanna Ferrar
    - Maggie Nicols
    - Tracy Lisk
  - - Reuben Derrick
  - - Mia Zabelka
    - Charlotte Keeffe
    - Tracy Lisk
ticket_link: https://www.ticketweb.uk/event/mopomoso-may-vortex-jazz-club-tickets/13473744
year: 2024
featured_image: images/featured.webp
notes:
- "**Mia Zabelka** was unwell so was unable to play"
---

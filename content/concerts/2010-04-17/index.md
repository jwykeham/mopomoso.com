---
title: 18 April 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-04-18T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Viv Corringham"
  - "Dave Tucker"
  - "Heddy Boubaker"
  - "Chefa Alonso"
  - "Eugene Martynec"
  - "Barbara Meyer"
  - "Henry Lowther"
  - "John Russell"
sets:
  - - Viv Corringham
    - Dave Tucker
  - - Heddy Boubaker
  - - Chefa Alonso
    - Eugene Martynec
    - Barbara Meyer
  - - Henry Lowther
    - John Russell
year: 2010
---
### {{<artist "Viv Corringham">}} ( voice ) / {{<artist "Dave Tucker">}} ( guitar )

Viv Corringham is a British vocalist, currently based in Minneapolis, who has worked internationally since the early 1980s. She makes performances, audio installations and soundwalks. Regular collaborators include Peter Cusack and Charles Hayward in the UK and Milo Fine in the US.

After an early involvement with the Manchester Rock scene, touring and recording with The Fall, Dave Tucker moved to London in the mid 80s where he has performed with musicians as divers as Andy Sheppard, Roger Turner, Dudu Pukwana and Phil Minton. A long term member of The Alan Tomlinson trio he is a major force in the London Improvisers Orchestra and also leads his own groups.

The duo tonight is making its debut in what promises to be an engaging and radical pairing from two fine musicians.

{{<youtube 1_3g11uIbqU>}} 

### {{<artist "Heddy Boubaker">}} (saxophones ) solo

Born in 1963 in Marseille, Heddy Boubaker has played music and experimented with sounds since 1977, starting with electric guitar in a mainly rock/punk even progressive style. Now exclusively playing free improvised music on alto &amp; bass saxophones ‘in an indescribable style, in which we are always able to hear the sea and the wind’, he lives near Toulouse where he manages the improvised music venue “la maison peinte” and is also president of the SonoFages collective and creator of Un Rêve Nu record label.

{{<youtube 875ZNwbS6JI>}} 

### {{<artist "Chefa Alonso">}} (saxophone ) / {{<artist "Eugene Martynec">}} ( electronics ) / {{<artist "Barbara Meyer">}} ( cello )

Saxophonist Chefa Alonso makes a welcome return to London from her native Madrid where she is one of the major figures in the improvised music scene there with her own groups Sin Red, Uz and Aka Free, conducting two orchestras in Galicia and Andalucia and as a member of FOCO. Her publications include the book: Improvisación libre. La composición en movimiento, in Dos Acordes, Spain.

Eugene Martynec performs live improvised electro-acoustic music using a unique software instrument called “Midi-ax”. The virtual instrument has countless sounds provided by samplers and soft synthesizers that are manipulated in real time.

Cellist Barbara Meyer began paying improvised music in Cologne in the mid 80s and has performed throughout Europe, at times in collaboration with other disciplines, such as dance, poetry and film.  
All three musicians have performed with the London Improvisers Orchestra and this trio promises to be a sonic delight. N.B Owing to travel difficulties Eugene Martynec did not appear.

{{<youtube BbFgQIPo4mM>}} 

### {{<artist "Henry Lowther">}} ( trumpet ) / {{<artist "John Russell">}} ( guitar )

Named as one of the top 100 trumpeters of all time, the remarkable Henry Lowther was one of the first musicians on the British jazz scene to experiment with totally free improvisation, notably with Lyn Dobson, Jack Bruce and John Hiseman. He played with the original Mike Westbrook band and with John Dankworth while also working on the rock scene with Manfred Mann, John Mayall and Keef Hartley, with whom he appeared at the Woodstock festival. He is also a frequent player with the London Brass Virtuosi, the London Philharmonic Orchestra, the City of Birmingham Symphony Orchestra, the London Sinfonietta and the Matrix Ensemble. He was for five years the solo flugelhorn player with the strings of the BBC Radio Orchestra. As a session musician he has recorded with Simon Rattle, Elton John, Paul McCartney, George Harrison, Van Morrison, Henry Mancini, Bing Crosby, Nelson Riddle and Talk Talk amongst many others and is one of only two or three players in the world to have had the honour of playing lead trumpet with both Gil Evans and George Russell.  
In the last few years Henry has become increasingly interested in composition and formed his own band “Still Waters” to enable him to pursue this interest further.  
His regular duo with the guitarist John Russell, with its elegant poise and highly refined musical intelligence, is seen as one of improvised music’s not to be missed experiences.

{{<youtube udKmsC0VGDw>}} 

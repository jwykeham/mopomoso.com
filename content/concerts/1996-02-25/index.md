---
artists:
  - Max Eastly
  - Paul Shearsmith
  - John Russell
  - Adam Bohman
  - Chris Burn
concert_date: 1996-02-25T20:00:00Z
concert_series: At The Red Rose
date: 1996-02-25T20:00:00Z
notes: null
sets: []
sources:
  - https://www.newspapers.com/image/790523953
title: 25 February 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
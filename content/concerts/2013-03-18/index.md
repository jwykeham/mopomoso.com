---
title: 21 April 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-04-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Caroline Kraabel"
  - "John Edwards"
  - "Richard E Harrison"
  - "Dario Fariello"
  - "Adam Linson"
  - "John Russell"
sets:
  - - Caroline Kraabel
    - John Edwards
    - Richard E Harrison
  - - Dario Fariello
  - - Adam Linson
    - John Russell
year: 2013
---

### {{<artist "Caroline Kraabel">}} (alto sax/voice), {{<artist "John Edwards">}} (bass) & {{<artist "Richard E Harrison">}} (drums)

{{<youtube vTxDIWSOof0>}}

### {{<artist "Dario Fariello">}} (saxophone)

{{<youtube urKzuxY8DcM>}}

### {{<artist "Adam Linson">}} (bass, electronics) & {{<artist "John Russell">}} (guitar)

{{<youtube NxWzM3BtqKk>}}

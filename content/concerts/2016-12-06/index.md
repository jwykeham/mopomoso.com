---
title: 18 December 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-12-18T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: End of Year Show
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Russell"
  - "Alan Wilkinson"
  - "Marcio Mattos"
  - "Marilza Gouvea"
  - "Jez Parfett"
  - "Michael Giaquinto"
  - "Joe Smith-Sands"
  - "Sam Enthoven"
  - "John Eyles"
  - "Eamon Foreman"
  - "Chris Hill"
  - "Dave Tucker"
  - "Paul Morris"
  - "Charlotte Keefe"
  - "Paul Jolly"
  - "Lee Boyd Allatson"
  - "Martin Hackett"
  - "Lawrence Casserley"
  - "Maggie Nicols"
  - "Kostas Chondros"
  - "John Rangecroft"
  - "Matt Hutchinson"
  - "Stephen Ying"
  - "Chun-Ting Wang"
  - "Sebastian Sterkowicz"
  - "James Malone"
  - "Charlotte Hug"
  - "Veryan Weston"
  - "Sarah Gail Grand"
  - "Rick Jensen"
  - "Martin Vishnick"
  - "Matt Scott"
  - "Simon Littlefield"
  - "Klaus Bru"
year: 2016
---
# It’s the End of Year Show!!!
Starting at [2.00 p.m at The Vortex](https://www.vortexjazz.co.uk/event/mopomoso-christmas-special/) this is our final concert before the holidays and as usual will feature a large cast of musicians playing in different groupings and you are invited to bring food to share.

We already have some musicians confirmed and the list will be added to right up until the day. We usually get about 50 people taking part and it is lots of fun with a chance to hear and meet up with old friends and also meet and make new ones. So far confirmed as taking part are (come back for updates):
- {{<artist "John Russell">}} (guitar)
- {{<artist "Alan Wilkinson">}} (sax)
- {{<artist "Marcio Mattos">}} (cello)
- {{<artist "Marilza Gouvea">}} (voice)
- {{<artist "Jez Parfett">}} (piano)
- {{<artist "Michael Giaquinto">}} (bass)
- {{<artist "Joe Smith-Sands">}} (guitar)
- {{<artist "Sam Enthoven">}} (theremin)
- {{<artist "John Eyles">}} (sax)
- {{<artist "Eamon Foreman">}} (guitar)
- {{<artist "Chris Hill">}} (clarinet)
- {{<artist "Dave Tucker">}} (guitar)
- {{<artist "Paul Morris">}} (banjo)
- {{<artist "Charlotte Keefe">}} (trumpet)
- {{<artist "Paul Jolly">}} (reeds)
- {{<artist "Lee Boyd Allatson">}} (drums)
- {{<artist "Martin Hackett">}} (objects)
- {{<artist "Lawrence Casserley">}} (objects)
- {{<artist "Maggie Nicols">}} (voice)
- {{<artist "Kostas Chondros">}} (poetry)
- {{<artist "John Rangecroft">}} (reeds)
- {{<artist "Matt Hutchinson">}} (piano)
- {{<artist "Stephen Ying">}} (flute)
- {{<artist "Chun-Ting Wang">}} (bass)
- {{<artist "Sebastian Sterkowicz">}} (bass clarinet)
- {{<artist "James Malone">}} (guitar)
- {{<artist "Charlotte Hug">}} (viola)
- {{<artist "Veryan Weston">}} (piano)
- {{<artist "Sarah Gail Grand">}} (trombone)
- {{<artist "Rick Jensen">}} (sax)
- {{<artist "Martin Vishnick">}} (guitar)
- {{<artist "Matt Scott">}} (accordion)
- {{<artist "Simon Littlefield">}} (guitar)
- {{<artist "Klaus Bru">}} (sax)

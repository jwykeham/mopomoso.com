---
title: "24 April 2022"
date: 2023-01-14T16:53:37Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-04-24T00:00:00Z
concert_series: Afternoon Sessions
artists:
  - "John Butcher"
  - "Ute Kanngeiser"
  - "Sothiac"
  - "Pat Moonchy"
  - "Lucky Liguori"
  - "Paul Jolly"
  - "Benedict Taylor"
  - "Sam Bailey"
sets:
  - - "John Butcher"
    - "Ute Kanngeiser"
  - - "Sothiac"
    - "Pat Moonchy"
    - "Lucky Liguori"
    - "Paul Jolly"
  - - "Benedict Taylor"
    - "Sam Bailey"
year: 2022
---

### {{<artist "John Butcher">}} (tenor, soprano saxophones) & {{<artist "Ute Kanngeiser">}} (cello)
### {{<artist "Sothiac">}} – {{<artist "Pat Moonchy">}} (vocals, synth), {{<artist "Lucky Liguori">}} (guitar,synth) & {{<artist "Paul Jolly">}} (bass clarinet, saxophone)
### {{<artist "Benedict Taylor">}} (viola) & {{<artist "Sam Bailey">}} (piano)

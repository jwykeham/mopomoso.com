---
title: 21 August 2021
image: /wp-content/uploads/2021/08/Mopomoso-22l08-750x422-1.jpg
date: "1970-01-01T00:00:00Z"
concert_date: 2021-08-21T14:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
categories:
  - Afternoon Sessions
artists:
  - "Steve Beresford"
  - "Petra Haller"
  - "Charlotte Keeffe"
  - "Paul Jolly"
  - "Martin Vishnick"
  - "Rick Jensen"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - "Steve Beresford"
  - - "Petra Haller"
    - "Charlotte Keeffe"
    - "Paul Jolly"
  - - "Martin Vishnick"
    - "Rick Jensen"
year: 2021
---

### {{<artist "Steve Beresford">}} (Solo)

[https://en.m.wikipedia.org/wiki/Steve\_Beresford](https://en.m.wikipedia.org/wiki/Steve_Beresford)

### {{<artist "Petra Haller">}}, {{<artist "Charlotte Keeffe">}} and {{<artist "Paul Jolly">}}

<http://www.pauljolly.com/>  
<https://www.petrahaller.net/>  
<https://www.charlottekeeffe.com/>

### {{<artist "Martin Vishnick">}} and {{<artist "Rick Jensen">}}

<https://www.mvish.co.uk/>  
<https://thedeleriumofanalogies.wordpress.com/>

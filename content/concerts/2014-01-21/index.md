---
title: 16 February 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-02-16T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Noel Taylor"
  - "Tom Wheatley"
  - "John Garcia"
  - "Tommaso Vespo"
  - "Ian Simpson"
  - "John Jasnoch"
  - "Neil Carver"
  - "Alice Eldridge"
  - "Satoko Fukuda"
  - "John Russell"
sets:
  - - Noel Taylor
    - Tom Wheatley
    - John Garcia
    - Tommaso Vespo
  - - Ian Simpson
    - John Jasnoch
    - Neil Carver
  - - Alice Eldridge
    - Satoko Fukuda
    - John Russell
year: 2014
---

### Trio Rewind – {{<artist "Noel Taylor">}} (bass /soprano clarinets), {{<artist "Tom Wheatley">}} (bass), {{<artist "John Garcia">}} (tiplé) with guest {{<artist "Tommaso Vespo">}} (piano)

{{<youtube -hmrCV3wEi8>}}

### Wire Assembly – {{<artist "Ian Simpson">}} (electric guitar), {{<artist "John Jasnoch">}} (electric guitar) & {{<artist "Neil Carver">}} (electric guitar)

{{<youtube mb2f2vMtc6k>}}

### {{<artist "Alice Eldridge">}} (cello), {{<artist "Satoko Fukuda">}} (violin) & {{<artist "John Russell">}} (guitar)

{{<youtube nJnfHcNMTyc>}}

{{<youtube z1uh2XG0poE>}}

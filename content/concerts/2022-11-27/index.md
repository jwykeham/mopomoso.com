---
title: "27 November 2022"
date: 2022-11-01T00:00:00Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-11-27T00:00:00Z
concert_series: Afternoon Sessions
artists:
  - "Chris Dowding"
  - "Ben Higham"
  - "Dave Amis"
  - "Colin Webster"
  - "Sylvia Hallett"
  - "Danny Kingshill"
  - "Gus Garside"
sets:
  - - "Chris Dowding"
    - "Ben Higham"
    - "Dave Amis"
  - - "Colin Webster"
  - - "Sylvia Hallett"
    - "Danny Kingshill"
    - "Gus Garside"
year: 2022
---
### {{<artist "Hard Edges">}} – {{< artist "Chris Dowding" >}} (trumpet), {{< artist "Ben Higham" >}} (tuba/trumpet) & {{< artist "Dave Amis" >}} (trombone)

### {{< artist "Colin Webster" >}} (saxophone)

### {{<artist "ARC">}} – {{< artist "Sylvia Hallett" >}} (violin/electronics), {{< artist "Danny Kingshill" >}} (cello) & {{< artist "Gus Garside" >}} (double bass/electronics)

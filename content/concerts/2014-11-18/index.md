---
title: 21 December 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-12-21T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Christmas party
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Steve Beresford"
  - "Satoko Fukuda"
  - "Roland Ramanan"
  - "Edward Lucas"
  - "Paul G. Smyth"
  - "Viv Corringham"
  - "Maggie Nichols"
  - "Sylvia Hallett"
  - "Ian McGowan"
  - "Kay Grant"
  - "Martin Speake"
  - "Noel Taylor"
  - "Mark Browne"
  - "Terry Day"
  - "Loz Speyer"
  - "Dave Fowler"
  - "Matt Hutchinson"
  - "Klaus Bru"
  - "Matt Scott"
  - "Tom Jackson"
  - "Dave Tucker"
year: 2014
---
A highlight on the improvising calendar, it’s that time of year again for the delights of our Christmas party. A chance to hook up with old friends and meet new ones, it’s a great way to see some fine musicians in a more informal setting and experience the wide variety to be found in this music.

This year’s event takes place in the afternoon, and heralds a move to afternoon concerts – still on the third Sunday of the month – in 2015.

### {{<artist "Steve Beresford">}} (piano) & {{<artist "Satoko Fukuda">}} (violin)

{{<youtube CC4606ut83I>}}

### {{<artist "Roland Ramanan">}} (trumpet) & {{<artist "Edward Lucas">}} (trombone)

{{<youtube fNPykBLaH5E>}}

### {{<artist "Paul G. Smyth">}} (piano)

{{<youtube KXWf4mUnd9k>}}

### {{<artist "Viv Corringham">}} (voice) & {{<artist "Maggie Nichols">}} (voice)

{{<youtube MqQDr5n_X54>}}

### {{<artist "Sylvia Hallett">}} (violin) & {{<artist "Ian McGowan">}} (trumpet)

{{<youtube QNih_K-oGug>}}

### {{<artist "Kay Grant">}} (voice), {{<artist "Martin Speake">}} (saxophone) & {{<artist "Noel Taylor">}} (clarinet)

{{<youtube X2b012UOpZA>}}

### {{<artist "Mark Browne">}} (saxophone) & {{<artist "Terry Day">}} (drums)

{{<youtube WYldYHxFf5c>}}

### {{<artist "Loz Speyer">}} (trumpet) & {{<artist "Dave Fowler">}} (drums, percussion)

{{<youtube MGhVsSJoKQ0>}}

### {{<artist "Matt Hutchinson">}} (piano) & {{<artist "Martin Speake">}} (saxophone)

{{<youtube 3g5eOXIdgBs>}}

### {{<artist "Paul G. Smyth">}} (piano) & {{<artist "Noel Taylor">}} (clarinet)

{{<youtube Jg52gBK_ifg>}}

### {{<artist "Klaus Bru">}} (saxophone) & {{<artist "Terry Day">}} (drums)

{{<youtube xViZanHsNP0>}}

### {{<artist "Matt Scott">}} (accordion)

{{<youtube 9SG9QiSrFSQ>}}

### {{<artist "Kay Grant">}} (voice) & {{<artist "Tom Jackson">}} (clarinet)

{{<youtube _Qz5cPa7RWo>}}

### Roland Ramanan Big Band: {{<artist "Roland Ramanan">}} (trumpet), {{<artist "Kay Grant">}} (voice), {{<artist "Edward Lucas">}} (trombone), {{<artist "Martin Speake">}} (saxophone), {{<artist "Tom Jackson">}} (clarinet) & {{<artist "Terry Day">}} (drums)

{{<youtube nCosgrmHqCA>}}

### Dave Tucker Quintet: {{<artist "Dave Tucker">}} (piano), {{<artist "Matt Scott">}} (accordion), {{<artist "Ian McGowan">}} (trumpet), {{<artist "Mark Browne">}} (saxophone) & {{<artist "Terry Day">}} (drums)

{{<youtube RLR9t3Nkfgk>}}

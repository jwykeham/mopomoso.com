---
title: 16 September 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-09-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Simon Rose"
  - "Roland Ramanan"
  - "Tom Jackson"
  - "Daniel Thompson"
  - "Claudia Binder"
  - "Stefan Keune"
  - "John Russell"
sets:
  - - Simon Rose
  - - Roland Ramanan
    - Tom Jackson
    - Daniel Thompson
  - - Claudia Binder
  - - Stefan Keune
    - John Russell
year: 2012
---
### {{<artist "Simon Rose">}} (saxophone)

![simon rose](https://www.mopomoso.com/wp-content/uploads/2012/08/simon-rose-jpeg.jpg "simon rose jpeg")

A welcome chance to hear the remarkable saxophonist in a rare solo visit to this country.

‘Rose transforms the instrument into an orchestra; effect is superseded, or rather transmogrified, moving away from virtuosic display and becoming an essential part of the instrumental language..’

‘free but absolutely logical, the best blend of spontaneity and improvisation imaginable…’ Cadence

### {{<artist "Roland Ramanan">}} (trumpet), {{<artist "Tom Jackson">}} (clarinet) & {{<artist "Daniel Thompson">}} (guitar)

![roland ramanan trio](https://www.mopomoso.com/wp-content/uploads/2012/08/roland-trio-jpeg.jpg "roland trio jpeg")

Highly thought of London based trio displaying a keen sense of space and intimacy with spirited invention. Credits include John Stevens, London Improviser’s Orchestra, Peter Brotzmann, Keith and Julie Tippet(s) and Paul Dunmall.

### {{<artist "Claudia Binder">}}

![claudia binder](https://www.mopomoso.com/wp-content/uploads/2012/08/claudiabio100.jpg "claudiabio100")

Zurich based pianist making a welcome return visit. Thoughtful and unforced improvisations invite the listener into an open and naturalistic process of a music unfolding in real time.

### {{<artist "Stefan Keune">}} (sax) & {{<artist "John Russell">}} (guitar)

![stefan keune john russell](https://www.mopomoso.com/wp-content/uploads/2012/08/keune-russell-jpeg.jpg "keune russell jpeg")

Fast reactions and fiery interplay from this long established duo. This, their only UK concert, to be part of an ongoing recording project. Past tours have included Japan, Germany and the UK. Total improvisation without artifice, direct, honest and engaging.

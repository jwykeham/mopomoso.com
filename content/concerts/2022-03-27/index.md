---
title: "27 March 2022"
date: 2023-01-14T13:23:28Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-03-27T00:00:00Z
concert_series: Afternoon Sessions
artists:
  - "Zorya"
  - "Maria Sappho"
  - "Katie Oswell"
  - "Olie Brice"
  - "Cath Roberts"
  - "Rachel Musson"
  - "Hannah Marshall"
  - "Tom Ward"
sets:
  - - "Zorya"
    - "Maria Sappho"
    - "Katie Oswell"
  - - "Olie Brice"
    - "Cath Roberts"
  - - "Rachel Musson"
    - "Hannah Marshall"
    - "Tom Ward"
year: 2022
---
As far as Mopomoso is concerned, International Women’s Day is the whole of March, so we have an extra special Matriarchal Mopomoso TV episode to be premiered on Sunday 20th on the Mopomoso YouTube channel, and on Sunday 27th, Mopomoso (live) is thrilled to be presenting the following mesmerising musicians;
<!--more-->
### {{<artist "Zorya">}} – {{<artist "Maria Sappho">}} (piano) & {{<artist "Katie Oswell">}} (voice, electronics)
### {{<artist "Olie Brice">}} (bass) & {{<artist "Cath Roberts">}} (baritone saxophone)
### {{<artist "Rachel Musson">}} (tenor saxophone) & {{<artist "Hannah Marshall">}} (cello) & {{<artist "Tom Ward">}} (woodwinds)

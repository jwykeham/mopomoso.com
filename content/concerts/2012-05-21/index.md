---
title: 17 June 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-06-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Chris Burn"
  - "Matt Hutchinson"
  - "Mark Browne"
  - "Sonic Pleasure"
  - "Ian Smith"
  - "Steve Beresford"
  - "John Russell"
sets:
  - - Chris Burn
    - Matt Hutchinson
  - - Mark Browne
    - Sonic Pleasure
    - Ian Smith
  - - Steve Beresford
    - John Russell
year: 2012
---

### {{<artist "Chris Burn">}} (trumpet) – {{<artist "Matt Hutchinson">}} (piano)

{{<youtube ghCMAf5eBPw>}}

### {{<artist "Mark Browne">}} (castrato saxophone / percussion) – {{<artist "Sonic Pleasure">}} (bricks / masonry / metal) – {{<artist "Ian Smith">}} (trumpet)

{{<youtube -d-dQRhxyaA>}}

### {{<artist "Steve Beresford">}} (piano) – {{<artist "John Russell">}} (guitar)

{{<youtube D-71DqNII_A>}}

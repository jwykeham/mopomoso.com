---
title: "26 November 2023"
date: 2023-11-13T09:33:06Z
doors: 2pm
categories:
- Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
- key: "Standard"
  value: "£12"
- key: "Vortex Members & Concessions"
  value: "£8"
concert_date: 2023-11-26 00:00:00 +0000 GMT
concert_series: Mopomoso Live
ticket_link: https://www.ticketweb.uk/event/mopomoso-live-november-vortex-jazz-club-tickets/13768348
artists:
- "Martin Speake"
- "Alyson Cawley"
- "Marcio Mattos"
- "Marilza Gouvea"
- "Lawrence Casserley"
- "Adrian Northover"
sets: 
- - Trio CZW
- - Martin Speake
  - Alyson Cawley
- - Marcio Mattos
  - Marilza Gouvea
  - Lawrence Casserley
  - Adrian Northover
---
### Trio CZW 
Formed in 2019, Trio CZW brings together the rich backgrounds of its musicians.

Trio CZW are Maureen Wolloshin (oboe and cor anglais), Paul Cheneour (flutes), and Alistair Zaldua (e-violin). 

The work of all three soloists is critically acclaimed. This is a rare opportunity to hear them together.

> Our improvisations are informed by scores written by each of us. Our album Where the Wind Takes Us was released by Pan y Rosas Discos in 2021. Whereness, a collaboration with artist and multi-instrumentalist Ansuman Biswas together with film by Annie Catford is available on Redgold Music.

### {{<artist "Martin Speake">}} (alto sax) & {{<artist "Alyson Cawley">}} (tenor sax)
Over the past few years Alyson Cawley and Martin Speake have been collaborating closely together on diverse projects ranging from exploring South Indian music to Charlie Parker to free improvisation. Throughout the lockdown period of 2020 two new projects have arisen inspired by a love of Ornette Coleman and Lennie Tristano. Ornette Coleman’s melodies are a perfect vehicle for Alyson and Martin to improvise freely together

### {{<artist "Marcio Mattos">}} (bass, cello), {{<artist "Marilza Gouvea">}} (voice), {{<artist "Lawrence Casserley">}} (electronics) & {{<artist "Adrian Northover">}} (alto sax)
{{<artist/description "Marcio Mattos">}} 

{{<artist/description "Marilza Gouvea">}} 

{{<artist/description "Lawrence Casserley">}} 

{{<artist/description "Adrian Northover">}} 

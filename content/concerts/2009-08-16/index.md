---
title: 17 August 2009
categories:
  - Afternoon Sessions
concert_series: "Afternoon Sessions"
date: "1970-01-01T00:00:00Z"
concert_date: 2009-08-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Sabu Toyozumi"
  - "Luo Chao yun"
  - "John Russell"
  - "Pat Thomas"
  - "Lol Coxhill"
  - "John Butcher"
  - "Jean Borde"
  - "Ute Voelker"
  - "Satoko Fukuda"
  - "Angelika Sheridan"
  - "Shabaka Hutchins"
  - "Hannah Marshall"
  - "Hennry Lowther"
sets:
  - - Henry Lowther
    - John Russell
    - Satoko Fukuda
  - - Pat Thomas
    - Sabu Toyozumi
    - UteVoelker
    - John Butcher
    - Angelika Sheridan
  - - Jean Borde
    - John Russell
    - Hannah Marshall
    - Satoko Fukuda
    - Luo Chao-yun
  - - Lol Coxhill
    - Sabu Toyozumi
    - Pat Thomas
  - - Hannah Marshall
    - Shabaka Hutchings
    - Angelika Sheridan
    - Sabu Toyozumi
  - - Jean Borde
    - John Russell
    - Ute Volker
    - Luo Chao-yun
    - John Butcher
  - - Lol Coxhill
    - Henry Lowther
    - Pat Thomas
    - Shabaka Hutchings
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2009
---
## FETE QUAQUA (Day two)

{{<artist "Sabu Toyozumi">}} (Japan) percussion, {{<artist "Luo Chao yun">}} (Taiwan) pipa, {{<artist "John Russell">}} (UK) guitar, {{<artist "Pat Thomas">}} (UK) keyboards, {{<artist "Lol Coxhill">}} (UK) saxophone, {{<artist "John Butcher">}} (UK) saxophones, {{<artist "Jean Borde">}} (France) bass, {{<artist "Ute Voelker">}} (Germany), accordion {{<artist "Satoko Fukuda">}} (UK) violin, {{<artist "Angelika Sheridan">}} (Germany) flutes, {{<artist "Shabaka Hutchins">}} (UK) saxophone and clarinet, {{<artist "Hannah Marshall">}} (UK) cello, {{<artist "Hennry Lowther">}} (UK) trumpet.

Sets in order of performance. Please allow time to download.
### First Improvisation
{{<youtube 9M-2eE-slyQ>}}
### Henry Lowther, John Russell & Satoko Fukuda
{{<youtube itEbIWwsWmg>}}
### Pat Thomas, Sabu Toyozumi, UteVoelker, John Butcher & Angelika Sheridan 
No video available
### Jean Borde, John Russell, Hannah Marshall, Satoko Fukuda & Luo Chao-yun 
No video available
### Lol Coxhill, Sabu Toyozumi & Pat Thomas
{{<youtube O1H1ikIPWSE>}}
### Hannah Marshall, Shabaka Hutchings, Angelika Sheridan & Sabu Toyozumi
{{<youtube dKszFZ-K_bo>}}
### Jean Borde, John Russell, Ute Volker, Luo Chao-yun & John Butcher
No video available.
### Lol Coxhill, Henry Lowther, Pat Thomas & Shabaka Hutchings
{{<youtube h5_oFepRNqw>}}
### Last Improvisation
{{<youtube puWf1QtOUa8>}}

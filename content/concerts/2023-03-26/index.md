---
title: "26 March 2023"
date: 2023-07-09T13:39:14+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2023-03-26T15:00:00+01:00
concert_series: Afternoon Sessions
artists:
  - "Alistair Zaldua"
  - "Lauren Redhead"
  - "Paulina Owczarek"
  - "Witold Oleszak"
  - "John Bissett"
  - "Jem Finer"
  - "Hywell Jones"
  - "Luigi Marino"
sets:
  - - "Alistair Zaldua"
    - "Lauren Redhead"
  - - "Paulina Owczarek"
    - "Witold Oleszak"
  - - "John Bissett"
    - "Jem Finer"
    - "Hywell Jones"
    - "Luigi Marino"
year: 2023
---
### {{<artist "Alistair Zaldua">}} (violin) & {{<artist "Lauren Redhead">}} (piano, spoken word)

### {{<artist "Paulina Owczarek">}} (saxophones) – supported by the Adam Mickiewicz Institute (culture.pl) & {{<artist "Witold Oleszak">}} (prepared piano, objects)

### {{<artist "John Bissett">}} (lapsteel), {{<artist "Jem Finer">}} (hurdy gurdy), {{<artist "Hywell Jones">}} (euphonium) & {{<artist "Luigi Marino">}} (percussion)

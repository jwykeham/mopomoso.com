---
title: Monday 19 August 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-08-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Russell"
year: 2013
---
### Tutti
{{<youtube jX_Ak1mgrOI>}}
###  Smyth, Solberg, Houtkamp, Casserley
{{<youtube ZRLRXhgVCXg>}}
###  Blunt, Musson, Linson, Russell
{{<youtube 0xUJp9Rn1hc>}}
###  Burn, Lauro. Wassermann, Eldridge
{{<youtube dnJXlwboNAw>}}
###  Blunt, Burn, Solbderg, Linson, Musson
{{<youtube -ycxQFdJAGM>}}
###  Houtkamp, Wassermann, Casserley
{{<youtube C_w2pdy_-2I>}}
###  Eldridge, Blunt, Linson
{{<youtube iYB1HMyDDI8>}}
###  Smyth, Eldridge, Lauro, Russell
{{<youtube o01WGQJkfGY>}}
### Tutti
{{<youtube t7fy8qdqxsE>}}

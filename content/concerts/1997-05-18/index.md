---
artists:
  - Chris Burn
  - Rhodri Davies
  - Phil Durrant
sets:
  - - Chris Burn
    - Rhodri Davies
    - Phil Durrant
concert_date: 1997-05-18T20:00:00+01:00
concert_series: At The Red Rose
date: 1997-05-18T20:00:00+01:00
notes: null
sets: []
sources:
  - https://rhodridavies.bandcamp.com/album/archif-11-red-rose-18-05-1997
title: 18 May 1997
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1997
---

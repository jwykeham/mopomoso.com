---
title: 15 June 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-06-15T01:00:00+01:00
concert_series: "Afternoon Sessions"
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "The Rick Jensen Quartet"
  - "Rick Jensen"
  - "Phil Somervell"
  - "Colin Somervell"
  - "Paul May"
  - "Richard Barrett"
  - "Henry Lowther"
  - "John Russell"
sets:
  - - "The Rick Jensen Quartet"
    - "Rick Jensen"
    - "Phil Somervell"
    - "Colin Somervell"
    - "Paul May"
  - - "Richard Barrett"
    - Ute Wassermann
  - - "Henry Lowther"
    - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

### {{<artist "The Rick Jensen Quartet">}} - {{<artist "Rick Jensen">}} (sax), {{<artist "Phil Somervell">}} (piano), {{<artist "Colin Somervell">}} (bass) & {{<artist "Paul May">}}

{{<youtube v8AUdyw6lm4>}}

### {{<artist "Richard Barrett">}} & {{<artist "Ute Wassermann">}}

{{<youtube 5AsuBx1ZSic>}}

### {{<artist "Henry Lowther">}} & {{<artist "John Russell">}}

{{<youtube WoF9Nw3UG4M>}}

---
title: 17 December 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-12-17T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: End of Year Show
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Martin Speake"
  - "Rick Jensen"
  - "Paul Jolly"
  - "Marcio Mattos"
  - "Marilza Goueva"
  - "Terry Day"
  - "Martin Hackett"
  - "Dave Tucker"
  - "Ross Lambert"
  - "Steve Beresford"
  - "Tania Chen"
  - "Martin Clarke"
  - "Alan Newcombe"
  - "Martin Vishnick"
  - "Adam Kinsey"
  - "Michael Giaquinto"
  - "Stephen Barrett"
  - "John Russell"
  - "Yves Charuest"
  - "Chun Ting Wang"
  - "Charlotte Keeffe"
  - "Kay Grant"
year: 2017
---
## The Mopomoso end of year show!
![mopomoso |-  xmas logo](https://www.mopomoso.com/wp-content/uploads/2017/11/mopomoso-xmas-logo.jpeg)

It’s that time of year again and Mopomoso celebrates all things improvised with our annual extravaganza at the Vortex. As in previous years the number of performers gets added to up until the day itself but typically we expect around 50 musicians playing short sets of 5 – 10 minutes or so. 

A great way to check out the vibrancy of the London free improvisation scene and see old friends and make new ones in an informal and friendly atmosphere. People bring food to share and we usually have a raffle. 

So far confirmed are {{<artist "Martin Speake">}}, {{<artist "Rick Jensen">}}, {{<artist "Paul Jolly">}}, {{<artist "Marcio Mattos">}}, {{<artist "Marilza Goueva">}}, {{<artist "Terry Day">}}, {{<artist "Martin Hackett">}}, {{<artist "Dave Tucker">}}, {{<artist "Ross Lambert">}}, {{<artist "Steve Beresford">}}, {{<artist "Tania Chen">}}, {{<artist "Martin Clarke">}}, {{<artist "Alan Newcombe">}}, {{<artist "Martin Vishnick">}}, {{<artist "Adam Kinsey">}}, {{<artist "Michael Giaquinto">}}, {{<artist "Stephen Barrett">}}, {{<artist "John Russell">}}, {{<artist "Yves Charuest">}}, {{<artist "Chun Ting Wang">}}, {{<artist "Charlotte Keeffe">}}, {{<artist "Kay Grant">}}. 

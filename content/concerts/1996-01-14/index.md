---
artists: []
concert_date: 1996-01-14T20:00:00Z
concert_series: At The Red Rose
date: 1996-01-14T20:00:00Z
notes: null
sets:
  - - John Russell
    - Phil Durrant
    - John Butcher
  - - Matt Hutchinson
    - Phil Wachsmann
sources:
  - https://www.newspapers.com/image/790525089
title: 14 January 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
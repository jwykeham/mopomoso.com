---
artists:
  - Dominic Lash
concert_date: 2005-12-18T20:00:00Z
concert_series: At The Red Rose
date: 2005-12-18T20:00:00Z
notes: null
sets:
  - - Dominic Lash
    - Veryan Weston
    - Paul May
sources:
  - http://dominiclash.blogspot.com/p/gig-archive.html
title: 18 December 2005
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2005
---
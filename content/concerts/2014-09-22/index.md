---
title: 19 October 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-10-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Yoko Miura"
  - "Jean Michel van Shouwburg"
  - "Lawrence Casserley"
  - "Anthony Donovan"
  - "Matt Chilton"
  - "Noura Sanatian"
  - "Guylaine Cosseron"
  - "Phil Minton"
  - "John Russell"
sets:
  - - Yoko Miura
    - Jean Michel van Shouwburg
    - Lawrence Casserley
  - - Anthony Donovan
    - Matt Chilton
    - Noura Sanatian
  - - Guylaine Cosseron
    - Phil Minton
    - John Russell
year: 2014
---

This month’s Mopomoso programme of free improvised music features musicians from Japan, Belgium, France and the UK in three contrasting and inspiring trios.

### {{<artist "Yoko Miura">}} (piano), {{<artist "Jean Michel van Shouwburg">}} (voice) & {{<artist "Lawrence Casserley">}} (electronics) trio

![Yoko Miura, Jean Michel van Schouwburg,Lawrence Casserley](https://www.mopomoso.com/wp-content/uploads/2014/08/ym-jmvs-lc-trio.jpg)

Tokyo based Yoko Miura plays a music of poise and elegance that reflects her feelings on the natural world. With live sound manipulations from the ever attentive (and inventive) Casserley, the whole is annotated by the vocal pyrotechnics of van Schouwburg. A welcome return for three fine musicians who perform for the first time at Mopomoso as a trio.

### {{<artist "Anthony Donovan">}} (electric bass,electronics), {{<artist "Matt Chilton">}} (electronics) & {{<artist "Noura Sanatian">}} (violin) trio

![Anthony Donovan,Noura Sanatian, Matt Chilton](https://www.mopomoso.com/wp-content/uploads/2014/08/AD-three-grab-2-.jpeg "Anthony Donovan,Noura Sanatian, Matt Chilton")

Following on from a short and delightful spot at the Mopomoso Christmas party last year, and after some diary juggling, we are delighted to finally announce the return of this excellent group. With the acoustic purity of the violin and the low resonances of the bass guitar effortlessly framed by the subtle use of electronics, they create a very personal sonic universe for us to share.

### {{<artist "Guylaine Cosseron">}} (voice), {{<artist "Phil Minton">}} (voice) & {{<artist "John Russell">}} (guitar)

![guylaine cosseron, phil minton, john russell](https://www.mopomoso.com/wp-content/uploads/2014/08/guylaine-phil-john.jpg)

Making a return visit to Mopomoso, this trio takes the human voice to quite remarkable levels of expression, interweaving all manner of vocal sounds with the steel string sonorities of the guitar to great effect. It is an engaging and all encompassing music that draws the listener in to a very human and deeply rewarding listening experience.

{{<youtube Yn-qIb0v_zE>}}

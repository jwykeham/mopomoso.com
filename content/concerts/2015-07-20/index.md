---
title: 16-18 August 2015
categories:
  - Fête Quaqua
date: "1970-01-01T00:00:00Z"
concert_date: 2015-08-15T14:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors:
  - 2015-08-16T14:00:00+01:00
  - 2015-08-17T20:00:00+01:00
  - 2015-08-18T20:00:00+01:00
concert_series: Fête Quaqua
costs:
  - key: Single Concert
    value: £8/£5 concessions
  - key: Three day pass
    value: £20/£15 concessions
artists:
  - "Chris Burn"
  - "Max Eastley"
  - "Satoko Fukuda"
  - "Audrey Lauro"
  - "Hannah Marshall"
  - "Henrik Munkeby Nørstebø"
  - "John Russell"
  - "Ståle Liavik Solberg"
  - "Paul G Smyth"
  - "Emil Strandberg"
  - "Alex Ward"
  - "Emi Watanabe"
  - "Portia Winters"
  - "Mia Zabelka"
year: 2015
---

## Fete Quaqua 2015

Fourteen musicians from seven countries appear in different combinations over three concerts. Each concert begins and ends with a Tutti improvisation.

### Set 1: Tutt

{{<youtube QdRFQ495jt8>}}

### Set 2: Eastley / Fukuda / Solberg / Strandberg / Ward

{{<youtube 6XKAlQzUKok>}}

### Set 3: Lauro / Zabelka

{{<youtube bs4vraFBSds>}}

### Set 4: Burn / Marshall / Nørstebø / Watanabe

{{<youtube AAj0fgHq2ZU>}}

### Set 5: Eastley / Lauro / Russell / Solberg / Smyth / Winters

{{<youtube Ga34oU0Zj_U>}}

### Set 6: Burn / Eastley / Russell / Solberg / Ward

{{<youtube QCthul3Asxw>}}

### Set 7: Fukuda / Lauro / Smyth / Strandberg / Winters

{{<youtube Wc9rLeqsNw0>}}

### Set 8: Eastley / Nørstebø

{{<youtube 5eTunsoJ9qQ>}}

### Set 9: Marshall / Russell / Smyth / Zabelka / Watanabe

{{<youtube KSugVIxpqT8>}}

### Set 10: Burn/ Nørstebø/ Strandberg/ Ward/ Watanabe/ Winters/ Zabelka

{{<youtube bN38gdMlog4>}}

### Set 11: Fukuda / Russell

{{<youtube iqtP6DBjbVg>}}

### Set 12: Tutti

{{<youtube rV-1xlqfMh0>}}

***

- {{<artist "Chris Burn">}} (trumpet, piano) UK [www.chrisburnmusician.com](https://www.chrisburnmusician.com/)
- {{<artist "Max Eastley">}} (self built instruments) UK [maxeastley.co.uk/](https://maxeastley.co.uk/)
- {{<artist "Satoko Fukuda">}} (violin) UK/Japan [www.linkedin.com/pub/satoko-fukuda/6a/b28/5ba](https://www.linkedin.com/pub/satoko-fukuda/6a/b28/5ba)
- {{<artist "Audrey Lauro">}} (saxophone) Belgium [www.audreylauro.com](https://www.audreylauro.com/)
- {{<artist "Hannah Marshall">}} (cello) UK [www.hannahmarshall.net](https://www.hannahmarshall.net/)
- {{<artist "Henrik Munkeby Nørstebø">}} (trombone) Norway [henriknorstebo.com](https://henriknorstebo.com/)
- {{<artist "John Russell">}} (guitar) UK [www.john-russell.co.uk](https://www.john-russell.co.uk/)
- {{<artist "Ståle Liavik Solberg">}} (percussion) Norway [hispid.no](https://hispid.no/)
- {{<artist "Paul G Smyth">}} (piano) Ireland [www.paulgsmyth.com/about](https://www.paulgsmyth.com/about)
- {{<artist "Emil Strandberg">}} (trumpet) Sweden [emilstrandberg.bandcamp.com](https://emilstrandberg.bandcamp.com/)
- {{<artist "Alex Ward">}} (clarinet) UK [sites.google.com/site/alexwardmusician/biography](https://sites.google.com/site/alexwardmusician/biography)
- {{<artist "Emi Watanabe">}} (flutes) UK/Japan [interaktionslabor.de/lab13/Team/Emi.htm](https://interaktionslabor.de/lab13/Team/Emi.htm)
- {{<artist "Portia Winters">}} (voice) UK [portiawinters.weebly.com](https://portiawinters.weebly.com/)
- {{<artist "Mia Zabelka">}} (violin,voice) Austria [www.miazabelka.com](https://www.miazabelka.com/)

***

Fete Quaqua gratefully acknowledges the help and assistance of the following in making this event possible:

![Support logos](https://www.mopomoso.com/wp-content/uploads/2015/07/support-logos.jpg)![Norsk Jazzforum logo](https://www.mopomoso.com/wp-content/uploads/2015/07/norsk-jazzforum-logo.jpg)![Konstnarsnamnden logo](https://www.mopomoso.com/wp-content/uploads/2015/07/konstnarsnamnden-logo.gif)![Das Land Steiermark LOGO](https://www.mopomoso.com/wp-content/uploads/2015/07/das-land-steiermark.gif)![Music Norway logo](https://www.mopomoso.com/wp-content/uploads/2015/07/music-norway-logo.gif)

---
title: 17 February 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-02-17T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Ian Simpson"
  - "Derek Saw"
  - "Shaun Blezard"
  - "Herve Perez"
  - "John Jasnoch"
  - "Charlie Collins"
  - "Kay Grant"
  - "Alex Ward"
  - "John Russell"
  - "Phil Wachsmann"
sets:
  - - Ian Simpson
    - Derek Saw
    - Shaun Blezard
    - Herve Perez
    - John Jasnoch
    - Charlie Collins
  - - Kay Grant
    - Alex Ward
  - - John Russell
    - Phil Wachsmann
year: 2013
---

### KIPPLE - {{<artist "Ian Simpson">}} (electronics), {{<artist "Derek Saw">}} (trumpet/flugelhorn), {{<artist "Shaun Blezard">}} (electronics), {{<artist "Herve Perez">}} (saxophone), {{<artist "John Jasnoch">}} (guitars) & {{<artist "Charlie Collins">}} (drums/percussion)

{{<youtube q1x1TXShM9I>}}
{{<youtube eBgqvx0LXTU>}}

### {{<artist "Kay Grant">}} (voice) & {{<artist "Alex Ward">}} (clarinet)

{{<youtube 5qEvaA6qdDE>}}
{{<youtube GL0eqG--M5g>}}

### {{<artist "John Russell">}} (guitar) & {{<artist "Phil Wachsmann">}} (violin)

{{<youtube xMfdvzg6aeY>}}

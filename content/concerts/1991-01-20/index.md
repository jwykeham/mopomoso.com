---
artists:
  - John Russell
  - Roger Turner
  - John Butcher
  - Vanessa Mackness
  - Eddie Prévost
  - Chris Burn
concert_date: 1991-01-20T20:00:00Z
concert_series: At The Red Rose
date: 1991-01-20T20:00:00Z
notes:
  - '>First of a monthly improvised music series to run thoughout 1991. Tonight explores duo interactions between some of London''s [most] resourceful figures on the free scene. - *The Guardian 17th January 1991*'
sets:
  - - John Russell
    - Roger Turner
  - - John Butcher
    - Vanessa Mackness
  - - Eddie Prévost
    - Chris Burn
sources:
  - https://www.newspapers.com/image/260072425
title: 20 January 1991
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
costs:
  - key: "Standard"
    value: "£3"
  - key: "Concessions"
    value: "£2"
year: 1991
---

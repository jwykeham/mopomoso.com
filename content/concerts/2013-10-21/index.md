---
title: 17 November 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-11-17T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: London Jazz Festival
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Russell"
year: 2013
---
To coincide with the 2013 London Jazz Festival a concert from guitarist John Russell’s Quaqua group, this time as a 14 piece international ensemble. The evening starts and finishes with the full ensemble and in between are smaller combinations picked by Russell. The individual musician’s experiences cover an enormous range and there is a similarly wide diversity in their approaches to playing music. The common ground is a shared love of free improvisation and a desire to play music together. ‘When you hear music, after it’s gone, in the air, you can never capture it again’ Eric Dolphy

### Tutti (Intro)
{{<youtube cwiVLvyoNFk>}}
  
### Beresford / Eldridge / Shimada / Solberg
{{<youtube lM_Hmm1LCfk>}}
  
###  Bohman / Burn / Grant / Russell / Gutvik / Shimada
{{<youtube 4mZUBgTv3o4>}}
  
### Linson / Mayes / Musson / Mimmo
{{<youtube Igs7FPCfEfI>}} 
  
### Bohman / Linson / Grant / Mimmo / Shimada
{{<youtube 1DT7GOauYwM>}} 
  
### Beresford / Musson / Gutvik / Burn
{{<youtube JVvtfLitGNQ>}} 
  
### Mayes / Solberg / Russell / Eldridge
{{<youtube sMBKlCd5Bcg>}} 
  
### Tutti (Closing)
{{<youtube G2P-trHvEi8>}}
  

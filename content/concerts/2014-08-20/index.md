---
title: 21 September 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-09-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Emilio Gordoa"
  - "Roland Ramanan"
  - "Dave Tucker"
  - "John Rangecroft"
  - "Marcio Mattos"
  - "Pat Thomas"
  - "Emi Watanabe"
  - "Stefan Keune"
  - "John Russell"
sets:
  - - Emilio Gordoa
    - Roland Ramanan
  - - Dave Tucker
    - John Rangecroft
    - Marcio Mattos
  - - Pat Thomas
    - Emi Watanabe
  - - Stefan Keune
    - John Russell
year: 2014
---

### {{<artist "Emilio Gordoa">}} (vibes) & {{<artist "Roland Ramanan">}} (trumpet)

![Roland Ramanan, Emilio Gordoa](https://www.mopomoso.com/wp-content/uploads/2014/08/ramanan-gordoa.jpg) 

Vibraphonist, percussionist Gordoa is ‘redefining the vibraphone as a source, treating it with preparations and extended techniques’. Based in Berlin, he is busy in many areas of creative music making both as a player and composer. Ramanan is a vital presence on the London improvised music scene. Constantly exploring the boundaries between composition and improvisation as well as the sound possibilities of the trumpet, he has worked with musicians as divers as Peter Brotzmann and Alex Ward and has been an integral part of the London Improvisers Orchestra since its inception. Having worked in Germany this is the duo?s first UK appearance.

### The Paulinus trio – {{<artist "Dave Tucker">}} (guitar), {{<artist "John Rangecroft">}} (tenor sax and clarinet) & {{<artist "Marcio Mattos">}} (bass and electronics)

![Dave Tucker, John Rangecroft, Marcio Mattos](https://www.mopomoso.com/wp-content/uploads/2014/08/tucker-rangecroft-mattos.jpg)

With a background in a wide variety of musics and a solid root in free improvisation these three players are all highly regarded members of the London scene and it is a real pleasure to have them here for a Mopomoso debut as a trio. Intimate group playing of a high order.

### {{<artist "Pat Thomas">}} (piano) & {{<artist "Emi Watanabe">}} (Japanese flutes)

![Pat Thomas, Emi Watanabe](https://www.mopomoso.com/wp-content/uploads/2014/08/thomas-watanabe.jpg)

Watanabe was born in Japan where she trained in traditional flutes. Emi plays three different types of traditional flutes – Ryuteki, Nohkan and Shinobue. She has been invited to perform with numerous musicians since she moved to the UK in 1995 these include from traditional settings to maverick performers like Jah Wobble. She is joined by the ever inventive Thomas who studied classical piano from aged 8 and started playing Jazz from the age of 16. He has since gone on to develop an utterly unique style – embracing improvisation, jazz and new music. A remarkable pairing!

### {{<artist "Stefan Keune">}} (tenor sax) & {{<artist "John Russell">}} (electric guitar)

![John Russell, Stefan Keune](https://www.mopomoso.com/wp-content/uploads/2014/08/russell-keune.jpg)

Keune and Russell have been working as a duo for a number of years now having toured here, on the continent and Japan and have two CDs released featuring Russell on acoustic guitar and Keune on alto and sopranino saxophones. For this performance Keune swaps saxophones to play with Russell?s electric guitar for the first time. Visceral sonic delights!

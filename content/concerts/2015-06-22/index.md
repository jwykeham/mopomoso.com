---
title: 19 July 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-07-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Neil Metcalfe"
  - "John Rangecroft"
  - "Dave Tucker (guitar)"
  - "Hannah Marshal"
  - "Steve Noble"
  - "Veryan Weston"
sets:
  - - Neil Metcalfe
    - John Rangecroft
  - - Dave Tucker (guitar)
    - Hannah Marshal
    - Steve Noble
  - - Veryan Weston
year: 2015
---

### {{<artist "Neil Metcalfe">}} (flute) & {{<artist "John Rangecroft">}} (reeds)

{{<youtube rsrg0ggP7P0>}}

### {{<artist "Dave Tucker (guitar)">}}, {{<artist "Hannah Marshal">}} (cello) & {{<artist "Steve Noble">}} (drums)

{{<youtube ixh2zzYnBqM>}}

### {{<youtube Trevor Watts>}} (sax) & {{<artist "Veryan Weston">}} (piano)

{{<youtube oOqaeSJ1lmo>}}

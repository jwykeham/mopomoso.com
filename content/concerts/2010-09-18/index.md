---
title: 19 September 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-09-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Stefano Giust"
  - "Noel Taylor"
  - "Steve Beresford"
  - "Lisa Ullen"
  - "Dave Solomon"
  - "Garry Todd"
  - "Pascal Marzan"
  - "John Russell"
sets:
  - - Stefano Giust
    - Noel Taylor
    - Steve Beresford
  - - Lisa Ullen
  - - Dave Solomon
    - Garry Todd
  - - Pascal Marzan
    - John Russell
year: 2010
---

### {{<artist "Stefano Giust">}} (drums) / {{<artist "Noel Taylor">}} (clarinet) / {{<artist "Steve Beresford">}} (piano)

A welcome visit from Stefano Giust – a tireless organiser and promoter of improvised music in his native Italy and founder of the Setola di Maiale label. A sensitive and creative improviser who also works as a composer for film and video he is joined by London based musicians Noel Taylor and Steve Beresford in this debut outing.

{{<youtube k1_IPfBIP8A>}} 

### {{<artist "Lisa Ullen">}} (solo piano)

‘New generation’ Swedish pianist Ullen came with her quartet to play for Mopomoso last year and we are pleased that she can return for this solo set. An intelligent and vibrant improvising musician with a firm background in Jazz, it is a rare chance to catch a player who is making waves on both the Swedish and wider European stage.

{{<youtube XM77hd3n4jA>}} 

### {{<artist "Dave Solomon">}} (drums) / {{<artist "Garry Todd">}} (saxophone)

Dave Solomon and Garry Todd were two of the leading ‘second wave’ free improvisers in the early seventies and this current duo has been a fixture for the last couple of years. Highly interactive and full of twists and turns – a music of both intellect and passion.

{{<youtube ajAtzudoH8w>}} 

{{<youtube JUM5NUO6uAI>}} 

### {{<artist "Pascal Marzan">}} (guitar) / {{<artist "John Russell">}} (guitar)

Pascal Marzan is a leading light of the French improvisation scene and one of only a handful of musicians who use the nylon strung classical guitar in free improvisation. The duo with John Russell has been working for over five years, their last appearance being a successful concert in Montmartre earlier this year and a CD of their music is recorded and awaiting release later in the year.

{{<youtube XwycEdU1N_c>}} 

{{<youtube O-KlZp0fnrg>}} 

---
title: 18 September 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-09-18T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Edward Lucas"
  - "Daniel Kordik"
  - "Ken Ikeda"
  - "Ole Mofjell"
  - "Natalie Sandtorv"
  - "Paul Pignon"
  - "Mike Gennaro"
  - "John Russell"
sets:
  - - Edward Lucas
    - Daniel Kordik
    - Ken Ikeda
  - - Ole Mofjell
    - Natalie Sandtorv
  - - Paul Pignon
  - - Mike Gennaro
    - John Russell
year: 2016
---

### {{<artist "Edward Lucas">}} (trombone), {{<artist "Daniel Kordik">}} (electronics) & {{<artist "Ken Ikeda">}} (electronics) trio

![daniel ken ed](https://www.mopomoso.com/wp-content/uploads/2016/08/daniel-ken-ed.jpg)

A finely detailed and expressive group that is both elegant and eloquent. All aspects of sound production are scrutinised and employed as valid musical material which in turn gives an immersive and complete listening experience.

### Not on The Guest list – {{<artist "Ole Mofjell">}} (drums) & {{<artist "Natalie Sandtorv">}} (voice)

![natalie and ole](https://www.mopomoso.com/wp-content/uploads/2016/08/natalie-and-ole.jpg)

This young Norwegian duo have been active for a couple of years now and play with a lot of energy and relish for the<span class="Apple-converted-space"> </span>rough and tumble world of free improvisation. An open style that invites the active listener to enjoy the fun.

### {{<artist "Paul Pignon">}} (bass clarinet)

![paul](https://www.mopomoso.com/wp-content/uploads/2016/08/paulpingon.jpg)

Paul has been playing free improvised music since his early days in Oxford in the 60s. From there he went on to set up the electronic music studio in Belgrade and build the EMS 100 synthesiser . He has been living and working as a musician in Sweden for many years where he plays a variety of single reed and electronic instruments. Having appeared here before with the group SQ4 he returns to play a short solo set on bass clarinet.

### {{<artist "Mike Gennaro">}} (drums) & {{<artist "John Russell">}} (electric guitar)

![mike john ](https://www.mopomoso.com/wp-content/uploads/2016/08/gennaro-russell.jpg)

First time meeting of Toronto based drummer and acclaimed UK guitarist more normally associated with the acoustic instrument, here on electric. Mike has collaborated with many musicians including John Butcher, Mats Gustafsson and Eugene Chadbourne and Toronto indie soul act Mantler, performing at the International Pop Overthrow, Beaches Jazz Festival and the Toronto International Jazz Festival.

---
title: 20 November 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-11-20T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: London Jazz Festival
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Daniel Thompson"
  - "Javier Carmona"
  - "Will Connor"
  - "Ståle Liavik Solberg"
  - "Stine Janvin Motland"
  - "Tom Chant"
  - "Fredi Proell"
  - "Udo Schindler"
  - "Uli Winter"
  - "Dave Tucker"
  - "Pat Thomas"
  - "Phil Minton"
  - "Roger Turner"
  - "Steve Beresford"
  - "Satoko Fukuda"
sets:
  - - Daniel Thompson
    - Javier Carmona
    - Will Connor
  - - Ståle Liavik Solberg
    - Stine Janvin Motland
  - - Tom Chant
  - - Fredi Proell
    - Udo Schindler
    - Uli Winter
  - - Dave Tucker
    - Pat Thomas
    - Phil Minton
    - Roger Turner
  - - Steve Beresford
    - Satoko Fukuda
year: 2011
---

To coincide with this year’s London Jazz Festival we put together an extended concert with musicians from Spain, France, Norway, Austria, Germany and the UK.

### {{<artist "Daniel Thompson">}} (guitar), {{<artist "Javier Carmona">}} (drums), {{<artist "Will Connor">}} (percussion)

{{<youtube LpXk4KvsmFg>}} 

### MOTSOL – {{<artist "Ståle Liavik Solberg">}} (drums/percussion) & {{<artist "Stine Janvin Motland">}} (voice)

{{<youtube e70AH0moNwo>}} 

### {{<artist "Tom Chant">}} (sax)

{{<youtube 6Zjk-L9_cP0>}} 

### Schi.Va – {{<artist "Fredi Proell">}} (drums), {{<artist "Udo Schindler">}} (sop. sax/bass clarinet) & {{<artist "Uli Winter">}} (cello)

{{<youtube Af_o0vAVS4Q>}} 

### SCATTER – {{<artist "Dave Tucker">}} (guitar), {{<artist "Pat Thomas">}} (piano), {{<artist "Phil Minton">}} (voice) & {{<artist "Roger Turner">}} (drums/percussion)

{{<youtube KDLvNsyLTTo>}} 

### {{<artist "Steve Beresford">}} (piano), {{<artist "Satoko Fukuda">}} (violin) duo

{{<youtube FnlL_6A1rsU>}} 

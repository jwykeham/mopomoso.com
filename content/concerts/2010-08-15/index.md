---
title: 16 August 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-08-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 8pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2010
---

# Second day of Fete Quaqua 2010
  {{<youtube TeZ2gQ5AIhE>}} 
  {{<youtube KqfMKz1eP_8>}} 
  {{<youtube DkHxDwc0e5c>}} 
  {{<youtube XzybrcJQHcA>}} 
  {{<youtube iBNzJkZtaQA>}} 
  {{<youtube QTGojbGohoE>}} 
  {{<youtube JwGPV94NPJI>}} 
  {{<youtube K2gig8_YpGI>}} 
  {{<youtube O48sAeECVaQ>}} 

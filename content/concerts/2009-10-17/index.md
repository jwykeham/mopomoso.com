---
title: 18 October 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-10-18T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Alison Blunt"
  - "Hannah Marshall"
  - "Ivor Kallin"
  - "John Jasnoch"
  - "Pat Thomas"
  - "Marilza Gouvea"
  - "Marcio Mattos"
  - "Ricardo Montoya"
  - "Tony Marsh"
  - "Alexander Hawkins"
sets:
  - - "Alison Blunt"
    - "Hannah Marshall"
    - "Ivor Kallin"
  - - "John Jasnoch"
    - "Pat Thomas"
  - - "Marilza Gouvea"
    - "Marcio Mattos"
    - "Ricardo Montoya"
  - - "Tony Marsh"
    - "Alexander Hawkins"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2009
---
###  Barrel – {{<artist "Alison Blunt">}}, {{<artist "Hannah Marshall">}} & {{<artist "Ivor Kallin">}}
{{<youtube je2V2a53RYM>}}

###  {{<artist "John Jasnoch">}} & {{<artist "Pat Thomas">}}
{{<youtube DEuIXQRleP0>}}

###  {{<artist "Marilza Gouvea">}}, {{<artist "Marcio Mattos">}} & {{<artist "Ricardo Montoya">}}
{{<youtube hXuaB1Ub0iQ>}}
###  {{<artist "Tony Marsh">}} & {{<artist "Alexander Hawkins">}}
{{<youtube AOpmnsqBQdk>}}

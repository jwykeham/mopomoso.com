---
title: 20 January 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-01-20T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Stefan Keune"
  - "Steve Noble"
  - "Hannah Marshall"
  - "Paul G. Smyth"
  - "Satkoko Fukuda"
  - "John Russell"
sets:
  - - Stefan Keune
    - Steve Noble
  - - Hannah Marshall
    - Paul G. Smyth
  - - Satkoko Fukuda
    - John Russell
year: 2013
---

Owing to adverse weather conditions the programme became three duos. Dominic Lash unable to join Stefan Keune and Steve Noble and Henry Lowther not appearing with Satoko Fukuda and John Russell.

### {{<artist "Stefan Keune">}} (sax) & {{<artist "Steve Noble">}} (drums)

{{<youtube xx1gx7eX2-o>}}

{{<youtube 0WSydXhVqAw>}}

### {{<artist "Hannah Marshall">}} (cello) & {{<artist "Paul G. Smyth">}} (piano)

{{<youtube OBmg5TY-UHs>}}

### {{<artist "Satkoko Fukuda">}} (violin) & {{<artist "John Russell">}} (guitar)

{{<youtube pL1hbQwpiRQ>}}

---
title: 16 October 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-10-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Noel Taylor"
  - "Noura Sanatian"
  - "Benedict Taylor"
  - "Simon Rose"
  - "Steve Beresford"
  - "Shabaka Hutchings"
  - "Gullaume Viltard"
  - "Loius Moholo–Moholo"
sets:
  - - Noel Taylor
    - Noura Sanatian
    - Benedict Taylor
  - - Simon Rose
  - - Steve Beresford
    - Shabaka Hutchings
    - Gullaume Viltard
    - Louis Moholo–Moholo
year: 2011
---

### Redstart - {{<artist "Noel Taylor">}} / {{<artist "Noura Sanatian">}} / {{<artist "Benedict Taylor">}}

{{<youtube 1C8WlCwpmBE>}} 

### {{<artist "Simon Rose">}}

{{<youtube dgyAaDjP938>}} 

### {{<artist "Steve Beresford">}}, {{<artist "Shabaka Hutchings">}} & {{<artist "Gullaume Viltard">}} plus special guest {{<artist "Louis Moholo–Moholo">}}

{{<youtube n8xXO3EVRO8>}} 

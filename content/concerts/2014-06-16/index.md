---
title: 20 - 21  July 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-07-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors:
  - 2014-07-20T20:00:00+01:00
  - 2014-07-21T20:00:00+01:00
concert_series: Quaqua Anglo Austria
costs:
  - key: Single concert
    value: £8/£5 concessions
  - key: Two night pass
    value: £15/£10 concessions
artists:
  - "Satoko Fukuda"
  - "Henry Lowther"
  - "Maggie Nicols"
  - "Mario Rechtern"
  - "John Russell"
  - "Roger Turner"
  - "Mia Zabelka"
year: 2014
---
# QUAQUA ANGLO AUSTRIA

![](https://www.mopomoso.com/wp-content/uploads/2014/05/july-badge-jp.jpg "july badge jp")

With thanks to the Austrian Cultural Forum we present a two night Anglo Austrian improvised music meeting with Steve Beresford (piano / electronics) / Tanja Feichtmair (sax) / Satoko Fukuda (violin) / Henry Lowther (trumpet) / Maggie Nichols (voice) / Mario Rechtern (sax) / John Russell (guitar) / Roger Turner (drums / percussion) / Mia Zabelka (violin / voice).

Each night will have a different programme of smaller combinations and also feature a large group with everyone playing together.

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/logo-jp.jpg "logo jp")](https://www.mopomoso.com/wp-content/uploads/2014/05/logo-jp.jpg)

## THE MUSICIANS

### Steve Beresford (piano / electronics) UK

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/steve_beresford_r_de_choc-132.jpeg "steve_beresford_r_de_choc-13")](https://www.mopomoso.com/wp-content/uploads/2014/05/steve_beresford_r_de_choc-132.jpeg)

Internationally renowned as a free improviser Steve plays with and conducts the London Improvisers Orchestra and has worked with hundreds of improvising musicians, including Derek Bailey, Han Bennink, Evan Parker, Otomo Yoshihide and John Zorn. He also works with visual artists. being a regualr collaborator with Christian Marclay and has recently toured John Cage’s ‘Indeterminacy’ with comedian Stewart Lee and pianist Tania Chen.

Awarded a Paul Hamlyn Award for Artists in 2012, Steve’s music and his teachings have inspired the musical community in the UK for over a decade.

### Tanja Feichtmair (sax / bass clarinet) Austria

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/Tanja-Foto-2-by-Iztok-Zupan-300x200.jpg "Tanja Foto 2 by Iztok Zupan")](https://www.mopomoso.com/wp-content/uploads/2014/05/Tanja-Foto-2-by-Iztok-Zupan.jpg)

‘I want that the auditory system learns not only the movement of the melody , but also the movement of the substance of the melody in musical and spiritual space …. So that the sounds and their substance become detached and begin to dance.’

Selected discography:

- Tanja Feichtmair Solo – Kaman Wasi ( extra plate )
- Tri -Centric Ensemble in Ulrich mountain Conducted by AnthonyBraxton ( LeoRecords )
- LULL – The Zipper ( LeoRecords )
- Omnixus ( composed by Tanja Feichtmair ) Linz Capital of Culture 09
- Trio Now – Now! ( Leo Records 2013)

Groups and collaborations:
MÜJAZZ, PUDNIK, LULL, Quartet : Tanja Feichtmair / Hugh Livingston / Scott Looney / Damon Smith,TRIO NOW inter alia, cooperation with John Russell, Damon Smith, Gino Robair, Roger Turner, Hannes Löschel , Marco Eneidi , SPIRIT, Francesco Cusa, Weasel Walter, Gerold Mayer, Hannes Wührleitne , Michael Kreuzer, Fredi Proll, Uli Winter, Josef Novotny, Edwin Stöbich, Ali Belhiba, Ben Greenberg, Kjell Nordesen, Steve Adams, Jorrit Deijkstra, Alfred Reiter.

### {{<artist "Satoko Fukuda">}} (violin) UK

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/satoko_fukuda_full-300x240.jpg "satoko_fukuda_full")](https://www.mopomoso.com/wp-content/uploads/2014/05/satoko_fukuda_full.jpg)

Satoko began playing violin in Japan at the age of seven, two years later coming to England to study with Catherine Lord. Her debut was at the age of thirteen for the Anglo Japanese Society. She has since performed and recorded on three continents. Invited, on a full scholarship, to study at the Yehudi Menuhin School w. Natasha Boyarskaya and Lucia Ibragimova she made her television debut in Japan as a soloist, was invited to perform at the Wigmore Hall and Royal Opera House with her Quartet and led the Menuhin School Orchestra broadcasting live for Classic FM and the Menuhin Festival, touring in Switzerland and appearing at the Royal Albert and Queen Elizabeth Halls. Satoko graduated from the Royal College of Music having studied with Itzhak Rashkovsky as a Joseph and Jill Karaviotis Scholar gaining awards such as the Ian Stouker Prize and the English Speaking Union Scholarship. Over the last fifteen years she has established herself as an improviser of great skill and imagination playing with, among others Steve Beresford, John Russell, Henry Lowther, Hannah Marshall and Veryan Weston.

### {{<artist "Henry Lowther">}} (trumpet) UK

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/henry_lowther-2-300x251.jpg "henry_lowther-2")](https://www.mopomoso.com/wp-content/uploads/2014/05/henry_lowther-2.jpeg)

During the sixties Henry was one of the first musicians on the British jazz scene to experiment with totally free improvisation, notably with Lyn Dobson, Jack Bruce and John Hiseman. He played with the original and seminal Mike Westbrook band and also with John Dankworth, including playing on the now legendary Kenny Wheeler album “Windmill Tilter” while also working on the rock scene with Manfred Mann, John Mayall and Keef Hartley, with whom he appeared at the Woodstock festival. His musical breadth is confirmed by his frequent engagements with major symphony orchestras and ensembles, inc. the London Brass Virtuosi, the London Philharmonic and City of Birmingham Symphony Orchestras, the London Sinfonietta and the Matrix Ensemble. Until its demise Henry was for five years the solo flugelhorn player with the Strings of the BBC Radio Orchestra, and as a session musician he has recorded with Simon Rattle, Elton John, Paul McCartney, George Harrison, Van Morrison, Henry Mancini, Bing Crosby, Nelson Riddle and Talk Talk amongst many others. He is one of only two or three players in the world to have had the honour of playing lead trumpet with both Gil Evans and George Russell. In the last few years Henry has become increasingly interested in composition and formed his own band “Still Waters” to enable him to pursue this interest further.

### {{<artist "Maggie Nicols">}} (voice) UK

[![](https://www.mopomoso.com/wp-content/uploads/2014/05/maggie_nichls-jp-300x290.jpg "maggie_nichls jp")](https://www.mopomoso.com/wp-content/uploads/2014/05/maggie_nichls-jp.jpg)

Maggie has been an active participant in the European improvisational community since joining the Spontaneous Music Ensemble in the late ’60s. A truly original musician she is rightly seen as the UK’s finest female improvising vocalist. As a co-founder of the Feminist Improvising Group, she has also worked to further women in improvised music, dancing and other creative arts not only by example, but through workshops and extensive collaborations including with pianist Irene Schweizer and bassist Joelle Leandre as the trio Les Diaboliques. She also plays in Trio Blurb with John Russell and Mia Zabelka.

### {{<artist "Mario Rechtern">}} (saxophone) Austria

![](https://www.mopomoso.com/wp-content/uploads/2014/05/mario-rechter-300x199.jpg "mario rechter")

Mario began playing flute/violin aged ten and clarinet/saxophone aged fourteen. 1961 aged nineteen found him in Berlin where two years later he finished highschool (theatre arts). There followed a period of stage design in Persia and studies of its music continuing in Israel, Turkey, Germany, France, Lux. Italy. 1972 began four years in the U.S.A studying African American studies and palying with the Open Rehearsal Black Music Orchestra. A return to Vienna where he worked as a musician in the free jazz scene while continuing in his main job as a stage designer for theater and opera performances in Austria, the Czech Republic and France. In 1981 he founded in Vienna Formation Laboratory for (H) armonical Basics Products and Open / Open Rehearsals and in 1998 the corresponding label L.ABOP – LSG as a forum for improvisational music. Since 1996 he has played in directcomposing orcestra with Reinhard Ziegerhofer and Bill Elgart, worked with Viennese musician Fritz Novotny in Reform Art Unit together with musicians like Sunny Murray, Paul Fields, Sepp Mitterbauer,Leena Conquest and Linda Sharrock. In trio Shine shape with Georg Edlinger and Volker Kagerer he used “recurrent rhythm, outgoing interwoven and interrupted, minimalist melodies, expanding electronically altered with sounds, improvisations, loops and structured forms that are resolved by sound collages and resulting music images. “while he working in film music. In New York City he worked with Blaise Siwula and drummer Weasel Walter, in Boston with the New Language Collaborative to Eric Zinman.

### {{<artist "John Russell">}} (guitar) UK

![](https://www.mopomoso.com/wp-content/uploads/2014/05/jr-jp-300x225.jpg "jr jp")

John Russell got his first guitar in 1965 while living in Kent and began to play in and around London from 1971 onwards. An early involvement with the emerging free improvisation scene (from 1972) followed, seeing him play in such places as The Little Theatre Club, Ronnie Scott’s, The Institute of Contemporary Arts, The Musicians’ Co-Op and the London Musicians’ Collective. From 1974 his work extended into teaching, broadcasts (radio and television) and touring in the United Kingdom and, ever extensively, in other countries around the world . He has played with many of the world’s leading improvisers and his work can be heard on over 50 CDs. In 1981, he founded QUAQUA, a large bank of improvisers put together in different combinations for specific projects and, in 1991, he started MOPOMOSO which has become the UK’s longest running concert series featuring mainly improvised music.

### {{<artist "Roger Turner">}} (drums / percussion) UK

![](https://www.mopomoso.com/wp-content/uploads/2014/05/roger_turner_e-jp-300x232.jpg "roger_turner_e jp")

Grew up amongst the Canterbury musical life of the 1960’s with a strong jazz foundation and since 1974 work has been concentrated on exploring a more personal percussion language through the processes of improvisation. Solo work, collaborations with experimental rock musics and open-form song, extensive work with dance, film and visual art, involvements in numerous jazz-based ensembles &amp; workshop residencies have formed part of that development. Mostly, however, the pleasures and discoveries have been in music-making with many of the finest European and international musicians in numerous recordings, ad-hoc and group improvising collaborations including Alan Silva, Cecil Taylor, Henry Grimes, Derek Bailey, Evan Parker, Keith Rowe, Toshinori Kondo, Irene Schweitzer and Joelle Leandre. Working currently in konk pack (with Tim Hodgkinson &amp;amp; Thomas Lehn), duo w/ Annette Peacock, the Josef Nadj dance project, the Phil Minton quartet, trio with Michel Doneda and John Russell, duos with John Russell and Phil Minton. Tours and concerts have been throughout Europe, Australia, Canada, USA, Mexico, China and Japan, and have included such remote places as the arctic.

### {{<artist "Mia Zabelka">}} (Violin / Voice) Austria

![](https://www.mopomoso.com/wp-content/uploads/2014/05/Mia-Zabelka-by-Sasa-Felsbach.jpeg "Mia Zabelka  by Sasa Felsbach")

“I do not understand crossover as a constructed emergence of different musical styles. Various musical genres that interest me, from classical music, to free improvised and electronic music, punk, jazz to heavy metal are filtered by me, through my body. From this concentrate I can then create a new language, my very own specific way of expression. This approach enables me to communicate with musicians from a variety of musical contexts and to find a common language. In my solo work I take different avenues by performing on both acoustic and electric violins plus electronic devices, to create two distinct musical languages or sound worlds. Based on the improvisational technique I have developed – ‘automatic playing’ – for me it’s always about sounding out the limits of possibility when playing the violin, and transferring this approach to voice improvisation.”

Mia Zabelka, sound artist, violinist and vocalist from Vienna, with Czech, Jewish and French family background, lives in the Austrian region of southern Styria.

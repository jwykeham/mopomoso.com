---
title: 20 January 2008
categories:
  - Red Rose Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-01-20T20:00:00Z
concert_series: Afternoon Sessions
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
doors: "8pm"
artists:
  - "Alexander Hawkins Quartet"
  - "Alexander Hawkins"
  - "Chris Cundy"
  - "Ollie Brice"
  - "Javier Carmona"
  - "Chefa Alonso"
  - "Tony Marsh"
  - "Henry Lowther"
  - "John Russell"
sets:
  - - "Alexander Hawkins Quartet"
    - "Alexander Hawkins"
    - "Chris Cundy"
    - "Ollie Brice"
    - "Javier Carmona"
  - - "Chefa Alonso"
    - "Tony Marsh"
  - - "Henry Lowther"
    - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

This concert was the last one at The Red Rose where we had promoted concerts for 17 years. It was in fact the last ever live performance there before attempts to turn it into a strip club and snooker hall by the new landlords.

### {{<artist "Alexander Hawkins Quartet">}} - {{<artist "Alexander Hawkins">}}, {{<artist "Chris Cundy">}}, {{<artist "Ollie Brice">}} & {{<artist "Javier Carmona">}}

{{< youtube bagp0gTBTGg >}}

### {{<artist "Chefa Alonso">}} & {{<artist "Tony Marsh">}}

{{< youtube B_gWy-QGjhE >}}

### {{<artist "Henry Lowther">}} & {{<artist "John Russell">}}

{{< youtube Uu9jyLRbdmw >}}

---
title: 16 December 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-12-16T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: End of Year Party
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2012
---

Mopomoso end of year party. Sets posted in order of performance.
There are many videos on this page so it will take time to download.
  
{{<youtube P92FZZhPZAE>}}
{{<youtube n0pFLxB3w8s>}}
{{<youtube RrEOlP9X2v8>}}
{{<youtube l0X9_8BTkGA>}}
{{<youtube LmwBKq7AxLA>}}
{{<youtube FrXXqUmcxSo>}}
{{<youtube 0RVzT6j5t6E>}}
{{<youtube qE6PH1TBjyQ>}}
{{<youtube E1xvFzZVw10>}}
{{<youtube _XnIECSxOTA>}}
{{<youtube YAVOYUv_P68>}}
{{<youtube in5dtcrrZow>}}
{{<youtube 8vQd-RbEKMU>}}
{{<youtube nyQA6Icv52U>}}
{{<youtube s-aVQ1-jMko>}}
{{<youtube AzTWpem7Vtw>}}
{{<youtube 0q3N9Hk6siM>}}

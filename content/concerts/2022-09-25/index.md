---
title: "25 September 2022"
date: 2023-01-14T10:06:32Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-09-25T01:00:00+01:00
concert_series: Afternoon Sessions
artists:
  - "Sylvia Hallett"
  - "Viv Corringham"
  - "mcddavid"
  - "Paul Jolly"
  - "Charlotte Keeffe"
  - "Pascal Marzan"
  - "Bruno Guastalla"
  - "Kostas Chondros"
year: 2022
---
Get ready for a super special screening of wonderful Ebba Jahn’s Miniatures by Improvisers.
<!--more-->
Plus the Mopomoso Orchestra will perform featuring:
- {{<artist "Sylvia Hallett">}} 
- {{<artist "Viv Corringham">}}
- {{<artist "mcddavid">}}
- {{<artist "Paul Jolly">}}
- {{<artist "Charlotte Keeffe">}}
- {{<artist "Pascal Marzan">}}
- {{<artist "Bruno Guastalla">}}
- {{<artist "Kostas Chondros">}} & more

They will blow the roof off with exquisite music made in the moment! We’re looking forward to seeing you there – come & join the celebration!

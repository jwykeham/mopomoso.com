---
title: 18 August 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-08-18T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Russell"
year: 2013
---
### Tutti
{{<youtube oGls63aDOFw>}}

###  Linson, Musson, Smyth
{{<youtube xhJxleR9gpU>}}

### Burn, Casserley, Eldridge, Lauro
{{<youtube CawjXn6E914>}}### Houtkamp, Russell, Solberg
{{<youtube 89Svr392SDY>}}

### Blunt, Wassermann
{{<youtube WA0CNSyZRcg>}}

### Blunt, Burn, Lauro, Linson
{{<youtube pLRGVJ737KI>}}

### Casserley, Houtkamp, Lauro, Musson, Smyth, Solberg
{{<youtube dsNHETH0eow>}}

### Eldridge , Russell, Wassermann
{{<youtube PAGN94XDsf4>}}

### Tutti
{{<youtube eeCXGPe_B7k>}}

---
title: "18 February 2024"
date: 2024-01-29T12:09:09Z
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-02-18 00:00:00 +0000 GMT
concert_series: Mopomoso Live
ticket_link: https://www.ticketweb.uk/event/mopomoso-live-february-vortex-jazz-club-tickets/13471443
sets:
  - - "Chris Dowding"
    - "Ranieri Spina"
    - "Hannah Marshall"
  - - "Martin Hackett"
    - "Julian Faultless"
    - "John Bissett"
  - - "Olga Ksendzovska"
artists:
  - "Chris Dowding"
  - "Ranieri Spina"
  - "Hannah Marshall"
  - "Martin Hackett"
  - "Julian Faultless"
  - "John Bissett"
  - "Olga Ksendzovska"
year: 2024
---
### {{<artist "Chris Dowding">}}, {{<artist "Ranieri Spina">}} & {{<artist "Hannah Marshall">}}
{{<artist "Ranieri Spina">}} is an improviser sound artist playing guitars based in Norwich. He has performed for East Gallery NUA, Firstsite Colchester, and BBC Radio Norfolk. He is a member of the Norfolk- based sonic arts collective Eastern Ear, which, amongst other activities, runs the improv night Plink Plonk and the participatory improvising group The Plank.

{{<artist "Chris Dowding">}} is a trumpeter and workshop leader based in Norwich. He plays regularly around the UK with Natural Causes, Rude 2.0 (with trombonist Annie Whitehead), and leads the Moonrise Trio. He has been commissioned by Durham Brass Festival and Norfolk and Norwich Festival. He is a member of the sonic arts collective Eastern Ear.

Their duo is a conversation of "scratchy exchanges" (John Sharpe), which evolves out of the environment they are playing in. They have performed previously for the Norwich-based organisation Camouflage, Cambridge event Soundhunt and elsewhere in East Anglia. For Mopomoso in February, they will be joined by cellist Hannah Marshall, who has recently worked with Trance Map.

{{<artist "Hannah Marshall">}} makes music and plays the 'cello in improvised performance, cross-arts collaboration, composition and sound design, she also enjoys making films and drawing.

She has participated in new compositions by: {{<artist "Simon Fell">}}, {{<artist "John Butcher">}}, {{<artist "Tim Hodgkinson">}}, {{<artist "Dylan Bates">}}, {{<artist "Julie Kjaer">}}, {{<artist "Eva-Maria Houben">}} & {{<artist "Alex Ward">}}.

Musicians that she has performed with include: {{<artist "Veryan Weston">}}, {{<artist "Alison Blunt">}} & {{<artist "Ivor Kallin">}} (Barrel), {{<artist "Rachel Musson">}}, {{<artist "Angharad Davies">}}, {{<artist "John Edwards">}}, {{<artist "John Russell">}}, {{<artist "Roger Turner">}}, {{<artist "Tony Marsh">}}, {{<artist "Dave Tucker">}}, {{<artist "Evan Parker">}} & {{<artist "Matt Wright">}}'s Trance map+, {{<artist "Nick Malcolm">}}, {{<artist "Lauren Kinsella">}}, {{<artist "Sylvia Hallett">}}, {{<artist "Ansuman Biswas">}}, {{<artist "Ntshuks Bonga">}}, {{<artist "Steve Beresford">}}, {{<artist "Black Top">}}, {{<artist "Xhosa Cole">}}, {{<artist "John Butcher">}}, {{<artist "Jacques Demierre">}}, {{<artist "Luc Ex">}}, {{<artist "Diatribes">}}, {{<artist "Dan Am">}}, {{<artist "Ingrid Laubrock">}}, and many more.

### {{<artist "Martin Hackett">}}, {{<artist "Julian Faultless">}} & {{<artist "John Bissett">}}
{{<artist "Martin Hackett">}} is a member of Oxford Improvisers and the Muzzix collective in Lille, France. He has been playing improvised music of one sort or another for as long as he can remember, for the past thirty years largely devoted to an obsessive exploration of the Korg MS10 synthesiser.

{{<artist "Julian Faultless">}} Having studied the horn at the Royal Academy of Music and the University of California, Los Angeles, he has pursued a varied freelance career including playing in various London orchestras such as the Royal Opera House and the Royal Philharmonic.

For ten years he played first horn in the Mozart Festival Orchestra (dressed in wig and breeches) in their Christmas season at the Albert Hall and currently plays with London Musical Arts, which performs monthly at St Martin's in the Fields in Trafalgar Square. He now regularly plays as guest principal with the Southern Sinfonia amongst other orchestras. He is particularly interested in contemporary music, playing in various specialist ensembles, sometimes broadcast on Radio 3. Julian is also active in Oxford Improvisors.

{{<artist "John Bisset">}} Guitarist, vocalist, improviser and composer, organiser of structures for improvising within (Relay), and film maker.Improvising and composing with the Manchester Musicians Collective in 1976 and ever since; from 2009 he made films, including collaborations with {{<artist "Ivor Kallin">}} as 213TV, and in 2022 returned to live playing; improvising on lap steel & voice, working with {{<artist "Iris Colomb">}}, {{<artist "Andrew Ciccone">}}, {{<artist "Ed Shipsey">}}, {{<artist "Matt Atkins">}}, and other more recent arrivals on the improvised music scene, mostly emanating from {{<artist "Rick Jensen">}}'s SKRONK open sessions; as well as old acquaintances {{<artist "Jem Finer">}}, {{<artist "Phil Durrant">}}, {{<artist "Chris Cundy">}}, {{<artist "Alex Ward">}}, the London Improvisers Orchestra and {{<artist "Fara Afifi">}}'s Noisy People's Improvising Orchestra.
### {{<artist "Olga Ksendzovska">}}
{{<artist "Olga Ksendzovska">}} studied in Moscow. She worked with the theatre group Playback, and with the Ukrainian Psych art rock band {{<artist "La Horsa Bianca">}}. Since moving to London she plays with {{<artist "The Noisy Women">}} and the {{<artist "One Orchestra">}}.

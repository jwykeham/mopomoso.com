---
title: 18 December 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-12-18T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Christmas Party
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2011
---

Something of an institution in the free improvisation calendar, the Mopomoso Christmas party provides an opportunity to listen to the varied and wide ranging approaches to the music in a less formal and friendly atmosphere. Posted in order of performance.

Please note: there are many videos on this page, so it will take time to download

{{<youtube f4L2iOvlV44>}} 

{{<youtube _Ah5ktxx3ak>}} 

{{<youtube VoDjI9l5Nps>}} 

{{<youtube UurlfFkP0Ks>}} 

{{<youtube UhbUSgClBhY>}} 

{{<youtube QPf3PhqOOYI>}} 

{{<youtube oYTVZzSFUa4>}} 

{{<youtube 1EebPpZjrp4>}} 

{{<youtube vfogd4PteTA>}} 

{{<youtube 8H2zE9q73_A>}} 

{{<youtube kIleJjEZ87o>}} 

{{<youtube f5Yh5hJdXO8>}} 

{{<youtube udYgzklV30k>}} 

{{<youtube 17HssRyHyrI>}} 

{{<youtube AxTry6TM6KA>}} 

{{<youtube 6Ha96HwHnNY>}} 

{{<youtube eS-KKIxhVXE>}} 

{{<youtube -EkzVSg2tDM>}} 

{{<youtube BT8FkT4A8Fg>}} 

{{<youtube aT1DgzYXYDg>}} 

{{<youtube wlSzies5jVo>}} 

{{<youtube DP36DU0KH88>}} 

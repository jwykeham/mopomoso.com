---
artists:
  - John Russell
  - Ute Völker
  - Mathieu Werchowski
  - Evan Parker
concert_date: 2003-11-16T20:00:00+01:00
concert_series: At The Red Rose
date: 2003-11-16T20:00:00+01:00
notes: null
sets:
  - - John Russell
    - Ute Völker
    - Mathieu Werchowski
    - Evan Parker
sources:
  - https://www.angharaddavies.com/past-events?page=11
title: 16 November 2003
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2003
---

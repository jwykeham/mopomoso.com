---
title: "20 October 2024"
date: 2024-04-24T10:59:28+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-10-20 00:00:00 +0100 BST
concert_series: Mopomoso Live
ticket_link: https://www.ticketweb.uk/event/mopomoso-october-vortex-jazz-club-tickets/13829893
year: 2024
sets:
- - Yoko Miura
  - Viv Corringham
  - Lawrence Casserley
- - Julian Woods
  - Alex Paxton
  - Terry Day
- - Adam Bohman
  - Pascal Marzan
  - Neil Metcalfe
artists:
- Yoko Miura
- Viv Corringham
- Lawrence Casserley
- Julian Woods
- Alex Paxton
- Terry Day
- Adam Bohman
- Pascal Marzan
- Neil Metcalfe
---

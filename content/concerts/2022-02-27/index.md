---
title: "27 February 2022"
date: 2023-01-14T13:23:43Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-02-27T00:00:00Z
concert_series: Afternoon Sessions
artists:
  - "Viv Corringham"
  - "Alan Wilkinson"
  - "Phil Wachsmann"
  - "David Leahy"
  - "Pete Robson"
  - "Stuart Wilding"
sets:
  - - "Viv Corringham"
    - "Alan Wilkinson"
  - - "Phil Wachsmann"
    - "David Leahy"
  - - "Pete Robson"
    - "Stuart Wilding"
year: 2022
---
### {{<artist "Viv Corringham">}} (voice) & {{<artist "Alan Wilkinson">}} (reeds)
### {{<artist "Phil Wachsmann">}} (violin) & {{<artist "David Leahy">}} (double bass)
### {{<artist "Pete Robson">}} (piano) & {{<artist "Stuart Wilding">}} (percussion)

---
title: 18 August 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-08-18T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Sabu Toyozumi"
  - "Luo Chao yun"
  - "John Russell"
  - "Pat Thomas"
  - "Lol Coxhill"
  - "John Butcher"
  - "Jean Borde"
  - "Ute Voelker"
  - "Satoko Fukuda"
  - "Angelika Sheridan"
  - "Shabaka Hutchins"
  - "Hannah Marshall"
  - "Henry Lowther"
  - "Shabaka Hutchings"
  - "Luo Chao-yun"
sets:
  - - Sabu Toyozumi
    - Angelika Sheridan
    - Henry Lowther
    - Pat Thomas
  - - John Butcher
    - John Russell
    - Luo Chao-yun
  - - Lol Coxhill
    - Jean Borde
    - Hannah Marshall
    - Satoko Fukuda
    - Shabaka Hutchings
  - - Luo Chao-yun
    - Ute Voelker
    - Jean Borde
    - Angelika Sheridan
  - - Lol Coxhill
    - John Russell
    - Sabu Toyozumi
  - - John Butcher
    - Henry Lowther
    - Pat Thomas
    - Hannah Marshall
    - Satoko Fukuda
    - Ute Voelker
  - - Sabu Toyozumi
    - Pat Thomas
    - Shabaka Hutchings
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2009
---
## FETE QUAQUA (DAY three)
{{<artist "Sabu Toyozumi">}} (Japan) percussion, {{<artist "Luo Chao yun">}} (Taiwan) pipa, {{<artist "John Russell">}} (UK) guitar, {{<artist "Pat Thomas">}} (UK) keyboards, {{<artist "Lol Coxhill">}} (UK) saxophone, {{<artist "John Butcher">}} (UK) saxophones, {{<artist "Jean Borde">}} (France) bass, {{<artist "Ute Voelker">}} (Germany), accordion {{<artist "Satoko Fukuda">}} (UK) violin, {{<artist "Angelika Sheridan">}} (Germany) flutes, {{<artist "Shabaka Hutchins">}} (UK) saxophone and clarinet, {{<artist "Hannah Marshall">}} (UK) cello, {{<artist "Henry Lowther">}} (UK) trumpet.

Sets in order of performance
### Tutti
{{<youtube  qCwBmyA-NdE>}}

### {{<artist "Sabu Toyozumi">}}, {{<artist "Angelika Sheridan">}}, {{<artist "Henry Lowther">}} & {{<artist "Pat Thomas">}} 
No video available.
### {{<artist "John Butcher">}}, {{<artist "John Russell">}} & {{<artist "Luo Chao-yun">}} 
{{<youtube wzX4N_A90Ak>}}

### {{<artist "Lol Coxhill">}}, {{<artist "Jean Borde">}}, {{<artist "Hannah Marshall">}}, {{<artist "Satoko Fukuda">}} & {{<artist "Shabaka Hutchings">}} 
No video available.

### {{<artist "Luo Chao-yun">}}, {{<artist "Ute Voelker">}}, {{<artist "Jean Borde">}} & {{<artist "Angelika Sheridan">}} 
No video available

### {{<artist "Lol Coxhill">}}, {{<artist "John Russell">}} & {{<artist "Sabu Toyozumi">}} 
No video available
### {{<artist "John Butcher">}}, {{<artist "Henry Lowther">}}, {{<artist "Pat Thomas">}}, {{<artist "Hannah Marshall">}}, {{<artist "Satoko Fukuda">}} & {{<artist "Ute Voelker">}}
{{<youtube UimUkC3vDyE>}}
### {{<artist "Sabu Toyozumi">}}, {{<artist "Pat Thomas">}} & {{<artist "Shabaka Hutchings">}}
{{<youtube gMLeLnPbt80>}}
### Final Tutti
{{<youtube CnN3TZE78pU>}}

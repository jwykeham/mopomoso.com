---
title: 16 April 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-04-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Steve Beresford"
  - "Mandhira da Saram"
  - "Lee Boyd Allatson"
  - "Phil Minton"
  - "John Russell"
sets:
  - - Steve Beresford
    - Mandhira da Saram
  - - Lee Boyd Allatson
  - - Phil Minton
    - John Russell
year: 2017
---

Free improvisation at its finest in ‘London’s listening jazz club’

### {{<artist "Steve Beresford">}} (piano,electronics) & {{<artist "Mandhira da Saram">}} (violin) 

![steve and mandhira](https://www.mopomoso.com/wp-content/uploads/2017/03/steve-and-mandhira-300x163.jpeg "full width")

Steve Beresford is a name anyone with an interest in free improvisation should be familiar with. Possessor of a considerable musical intelligence, full of wit and with an inventiveness that has seen him play with some of the world’s finest improvisers, he is always a welcome addition to any Mopomoso bill. He is joined here by Mandhira da Saram, a versatile violinist who works as a soloist, chamber musician and orchestral player and is also a founding member and leader of the Ligeti Quartet. Commanding a varied repertoire, consisting of standard classical works and collaborations with contemporary composers, sound artists and musicians from a variety of genres outside classical music she also shares a love of free improvisation. A superb pairing of two fine musicians.

### {{<artist "Lee Boyd Allatson">}} (drums, percussion)

![lee allatson](https://www.mopomoso.com/wp-content/uploads/2017/03/lee-grab-300x163.jpeg "full width")

Lee Allatson’s early percussive years included classical tuition and a keen interest in 1970’s film soundtracks – all fused within the cultural melting pot of Caribbean, Asian and Eastern European influences provided from a Leicester city education. Moving to vocals and electronics he developed his experimental edge through free music playing with regular trio Misterlee and in ad hoc performances with such luminaries as Damo Suzuki (Can), Paul Dunmall and Wildbirds & Peacedrums. In recent years a return to drumming has seen him also teaching percussion and recording solo drum set works. Lee now regularly plays in several free improvising ensembles with, amongst others: John Russell, Bruce Coates and Christopher Hobbs. Lee also founded and runs the South Leicestershire Improvisors Ensemble.

### {{<artist "Phil Minton">}} (voice) & {{<artist "John Russell">}} (guitar)

![phil minton john russell](https://www.mopomoso.com/wp-content/uploads/2017/03/maxresdefault-300x169.jpg "full width")

Two of the UK’s leading improvisers, Phil and John first met in an early version of Gunter Christmann’s Vario and, over time, have cemented their playing relationship to build a duo that can change direction in a split second and one that is guaranteed to keep the listener on the edge of their seat. There is something deeply human and humane about Phil’s use of the voice that directly touches all of us and Russell’s acoustic guitar manages to frame and enhance this, gently pushing the music forwards. Not to be missed!

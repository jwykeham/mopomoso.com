---
title: "22 May 2022"
date: 2023-01-14T13:23:14Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-05-22T01:00:00+01:00
concert_series: Afternoon Sessions
artists:
  - "Phil Wachsmann"
  - "David Leahy"
  - "Neil Metcalfe"
  - "Harrison Smith"
  - "Adam Bohman"
  - "Catherine Pluygers"
sets:
  - - "Phil Wachsmann"
    - "David Leahy"
  - - "Neil Metcalfe"
    - "Harrison Smith"
  - - "Adam Bohman"
    - "Catherine Pluygers"
year: 2022
---
On Sunday 22nd May, Mopomoso (live) is delighted to be presenting the following magical musicians:

### {{<artist "Phil Wachsmann">}} (violin) & {{<artist "David Leahy">}} (double bass)
### {{<artist "Neil Metcalfe">}} (flute) & {{<artist "Harrison Smith">}} (bass clarinet)
### {{<artist "Adam Bohman">}} (amplified objects) & {{<artist "Catherine Pluygers">}} (oboe)

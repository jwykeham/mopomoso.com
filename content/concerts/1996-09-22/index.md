---
artists: []
concert_date: 1996-09-22T20:00:00+01:00
concert_series: At The Red Rose
date: 1996-09-22T20:00:00+01:00
notes:
  - '>Prodigous young tiba avant gardist - *The Guardian 21 Sept 1996*'
sets:
  - - Robin Hayward Group
sources:
  - https://www.newspapers.com/image/790536466
  - https://www.newspapers.com/image/260941422
title: 22 September 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
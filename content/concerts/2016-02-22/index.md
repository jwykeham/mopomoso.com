---
title: 20 March 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-03-20T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Viv Corringham"
  - "Hannah Marshall"
  - "Martin Archer"
  - "Hervé Perez"
  - "Peter Fairclough"
  - "Sergi Sorrentino"
  - "Lee Allatson"
  - "John Russell"
sets:
  - - Viv Corringham
    - Hannah Marshall
  - - Martin Archer
    - Hervé Perez
    - Peter Fairclough
  - - Sergi Sorrentino
  - - Lee Allatson
    - John Russell
year: 2016
---

### {{<artist "Viv Corringham">}} (voice), {{<artist "Hannah Marshall">}} (cello)

{{<youtube f4t5mfho9XM>}}

Two remarkable musicians with an insatiable appetite for playing and a seemingly endless supply of inventiveness. Players with a generous spirit and unafraid to take chances they are two Mopomoso favourites! A delight to have them here again, this time in their Mopomoso debut as a duo.

<https://www.vivcorringham.org/>

<https://www.hannahmarshall.net/>

### Inclusion principle – {{<artist "Martin Archer">}} (electronic keyboards, laptop, saxophones, clarinets, recorders), {{<artist "Hervé Perez">}} (laptop, saxophones) & {{<artist "Peter Fairclough">}} (drums and percussion)

{{<youtube --E1C7VHTDI>}}

A well established Sheffield based trio, Inclusion Principle inhabit a space between electronics, nu-jazz, contemporary electroacoustic music and free improvisation. Tendencies towards a minimalist approach can on occasion lead to more ‘full on’ electronic beats. Soundscapes with attitude!

<https://www.efi.group.shef.ac.uk/marcher.html>

<https://sndsukinspook.wordpress.com/>

[https://www.jazzcds.co.uk/artist\_id\_60/biography\_id\_60](https://www.jazzcds.co.uk/artist_id_60/biography_id_60)

### {{<artist "Sergi Sorrentino">}} (guitar)

{{<youtube qHxfDMlzoyg>}}

After receiving the Academic Guitar Diploma at the Novara Conservatory in 2010, with a thesis on Italian avant-garde guitar music, Sorrentino has become “one of the most important contemporary Italian guitarists” (RAI Radio Tre). As a performer he promotes the classical and electric guitars’ contemporary repertory in both composed and improvised musics. Here on a short visit, so a rare chance to experience this fine musician playing live.  

<https://www.sergiosorrentinoguitarist.com/>

### {{<artist "Lee Allatson">}} (drums), {{<artist "John Russell">}} (guitar)

{{<youtube qBvISnAIN7g>}}

Whilst drummer Lee Allatson has worked across a wide range a musical genres with musicians such as a.o, Prince Buster, Laurel Aitken, Desmond Decker, Paul Dunmall, Han Earl Park, Showaddywaddy, Bill Drummond of KLF, David Thomas and the South Leicestershire Improvisors Ensemble, it is the freer end of things that ‘provide a level of connection with my instrument, music and other musicians I can find nowhere else’. Joined by leading UK guitarist John Russell this is a first encounter.

<https://www.dyehousedrumworks.com/wordpress/lee/>

<https://www.john-russell.co.uk>

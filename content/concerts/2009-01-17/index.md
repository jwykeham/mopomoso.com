---
title: 18 January 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-01-18T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - Terry Day
  - John Edwards
  - Jean-Michel van Schouwburg
  - Marjolaine Charbin
  - Alan Tomlinson
  - John Russell
  - Gianni Mimmo
sets:
  - - Terry Day
    - John Edwards
  - - Jean-Michel van Schouwburg
    - Marjolaine Charbin
  - - Alan Tomlinson
  - - John Russell
    - Gianni Mimmo
year: 2009
---
Three duos and a solo.
### {{<artist "Terry Day">}} & {{<artist "John Edwards">}} 

{{<youtube AuK5yH2GuS8>}} 

### {{<artist "Jean-Michel van Schouwburg">}} & {{<artist "Marjolaine Charbin">}}
{{<youtube 6FGuiw1If60>}} 

### {{<artist "Alan Tomlinson">}} 
{{<youtube BUwyLBzyw_8>}} 

### {{<artist "John Russell">}} & {{<artist "Gianni Mimmo">}} 
{{<youtube Hwpt7DoolKY>}} 

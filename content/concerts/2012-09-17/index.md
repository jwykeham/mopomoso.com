---
title: 21 October 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-10-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Leon Michener"
  - "Neil Metcalf"
  - "Viv Corringham"
  - "Dave Tucker"
  - "Dominic Lash"
  - "Chris Cundy"
  - "Yoko Arai"
  - "John Russell"
sets:
  - - Leon Michener
    - Neil Metcalf
  - - Viv Corringham
    - Dave Tucker
  - - Dominic Lash
    - Chris Cundy
  - - Yoko Arai
    - John Russell
year: 2012
---

### {{<artist "Leon Michener">}} (clavichord/piano) & {{<artist "Neil Metcalf">}} (flute)

{{<youtube 9Tpf1gDJxM8>}}

### {{<artist "Viv Corringham">}} (voice) & {{<artist "Dave Tucker">}} (guitar)

{{<youtube USDvf9HDPdk>}}

### {{<artist "Dominic Lash">}} (bass) & {{<artist "Chris Cundy">}} (clarinets)

{{<youtube AYrOeGKUj9k>}}

### {{<artist "Yoko Arai">}} (piano) & {{<artist "John Russell">}} (guitar)

{{<youtube N4SQU5uEBqA>}}

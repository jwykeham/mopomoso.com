---
artists: []
concert_date: 1996-07-21T20:00:00+01:00
concert_series: At The Red Rose
date: 1996-07-21T20:00:00+01:00
notes: []
sets:
  - - Vanessa Mackness Quartet
  - - Alan Tomlinson
  - - '? Beck'
    - '? Jaznoch'
    - '? Parson'
sources:
  - https://www.newspapers.com/image/260884134
  - https://www.newspapers.com/image/790531716
title: 21 July 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
---
title: 6 February 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-02-06T00:00:00Z
concert_series: "Afternoon Sessions"
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "The Alan Tomlinson Trio"
  - "Alan Tomlinson"
  - "Dave Tucker"
  - "Roger Turner"
  - "Chris Burn"
  - "Steve Beresford"
  - "Satoko Fukuda"
  - "John Russell"
  - "Roger Turner"
  - "Ute Wassermann"
sets:
  - - "The Alan Tomlinson Trio"
    - "Alan Tomlinson"
    - "Dave Tucker"
    - "Roger Turner"
  - - "Chris Burn"
  - - "Steve Beresford"
    - "Satoko Fukuda"
  - - "John Russell"
    - "Roger Turner"
    - "Ute Wassermann"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

### {{<artist "The Alan Tomlinson Trio">}} - {{<artist "Alan Tomlinson">}}, {{<artist "Dave Tucker">}} & {{<artist "Roger Turner">}}

{{< youtube szZKHsG9t2s >}}

### {{<artist "Chris Burn">}}

No video available

### {{<artist "Steve Beresford">}} & {{<artist "Satoko Fukuda">}}

{{< youtube gVIESimLHpk >}}

### {{<artist "John Russell">}}, {{<artist "Roger Turner">}} & {{<artist "Ute Wassermann">}}

{{< youtube DtKk9xYM1-0 >}}


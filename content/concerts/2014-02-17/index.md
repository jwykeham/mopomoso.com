---
title: 16 March 2014
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2014-03-16T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Andrea Caputo"
  - "Massimo Semprini"
  - "Stephen Grew"
  - "Nick Malcolm"
  - "Hannah Marshall"
  - "Lauren Kinsella"
  - "John Russell"
  - "Kay Grant"
sets:
  - - Andrea Caputo
    - Massimo Semprini
  - - Stephen Grew
  - - Nick Malcolm
    - Hannah Marshall
    - Lauren Kinsella
  - - John Russell
    - Kay Grant
year: 2014
---

## **Mopomoso presents ‘Debuts’**

The groups playing on this concert were all making their Mopomoso debuts.

### Utter Chaos – {{<artist "Andrea Caputo">}} (guitar) & {{<artist "Massimo Semprini">}} (sax)

{{<youtube pXBDc2NzUi8>}}

### {{<artist "Stephen Grew">}} (piano) 

{{<youtube VFNLM0p6hvg>}}

### {{<artist "Nick Malcolm">}} (trumpet), {{<artist "Hannah Marshall">}} (cello) & {{<artist "Lauren Kinsella">}} (voice)

https://www.youtube.com/watch?v=op5gzdxv61c

### {{<artist "John Russell">}} (guitar) & {{<artist "Kay Grant">}} (voice)

{{<youtube LHgLukskJkk>}}

---
artists: []
concert_date: 1997-01-19T20:00:00Z
concert_series: At The Red Rose
date: 1997-01-19T20:00:00Z
notes:
  - Newspaper scan is very low quality.
sets:
  - - Ken Hyder
    - Clive Bell
  - - Alan Wilkinson
  - - '? Smith'
    - '? Bissett'
    - '? Edwards'
sources:
  - https://www.newspapers.com/image/260704861
title: 19 January 1997
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1997
---
---
artists:
  - Colin Brady
  - Simon Vincent
  - Dave Pullin
concert_date: 1995-03-19T20:00:00+01:00
concert_series: At The Red Rose
date: 1995-03-19T20:00:00+01:00
sets:
  - - Colin Brady
    - Simon Vincent
    - Dave Pullin
title: 19 March 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
costs:
  - key: "Standard"
    value: "£3.50"
  - key: "Concessions"
    value: "£2.50"
sources:
  - https://archive.org/details/the-wire-magazine-1995-03-cbz/page/n7/mode/2up?q=mopomoso
year: 1995
---

---
artists:
  - John Edwards
  - John Russell
  - Chris Burn
  - Lol Coxhill
  - Phil Minton
concert_date: 2002-11-17T20:00:00Z
concert_series: At The Red Rose
date: 2002-11-17T20:00:00Z
notes: null
sets: null
sources:
  - https://www.discogs.com/release/2056127-Chris-Burn-Lol-Coxhill-John-Edwards-Phil-Minton-John-Russell-Mopomoso-Solos-2002
title: 17 November 2002
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2002
---
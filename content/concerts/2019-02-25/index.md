---
title: 17 March 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-03-17T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Bertrand Denzler"
  - "Phil Durrant"
  - "Chris Dowding"
  - "Sylvia Hallett"
  - "John Russell"
  - "Roger Turner"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Bertrand Denzler
    - Phil Durrant
  - - Chris Dowding
    - Sylvia Hallett
  - - John Russell
    - Roger Turner
year: 2019
---
A continuing series presenting some of the best from the world of free improvisation
<!--more-->

### {{<artist "Bertrand Denzler">}} (tenor saxophone) & {{<artist "Phil Durrant">}} (octave mandola)

![](https://www.mopomoso.com/wp-content/uploads/2019/02/Screenshot-2019-02-25-at-13.58.39.png)

Bertrand Denzler is a Swiss musician active in the fields of contemporary improvised music, new music and free jazz. He is based in France and Switzerland. As a tenor saxophonist, he has toured extensively in Europe, North America, South America, the Middle East and the Far East with regular ensembles, dozens of ad-hoc groups, artists from other disciplines (dance, performance, theatre, sculpture, poetry, video), as well as solo. He is or was a member of ensembles like Trio Sowari, Hubbub, Denzler-Gerbal-Dörner, The Seen, Onceim, Denzler-Grip-Johansson.

He has composed pieces for new music ensembles and musicians including Ensemble ReRe, CoÔ (Cordes de l’Onceim), Bondi-Denzler-D’Incise, Šalter Ensemble, CCP3, Félicie Bazelaire, Ensemble Hodos, Onceim and Horns, as well as for jazz groups like the Umlaut Big Band. He has also worked as a composer for Swiss filmmaker Christoph Kühn.

As an improviser, performer and/or composer, he has participated in over 70 published recordings, released by labels like Potlatch, Umlaut, Confront, Aussenraum, Remote Resonator, Mikroton, Trestle, DDS, Matchless, Creative Sources, Ambiances Magnétiques, Leo, For4Ears, Label Bleu or Unit.

He has published texts about improvisation and experimental music in collaboration with Jean-Luc Guionnet, as well as with Burkhard Beins and Phil Durrant. He also gives improvisation workshops.

Phil Durrant is a multi-instrumentalist improviser/composer/sound artist who currently performs solo and group concerts.

As a violinist (and member of the Butcher/Russell/Durrant trio), he was one of the key exponents of the “group voice approach” style of improvised music. In the late 90s, his trio with Radu Malfatti and Thomas Lehn represented a shift to a more “reductionist” approach.

Recently, he has been performing solo and duo concerts with Phil Maguire, Bill Thompson, Mark Sanders, Lee Patterson and drone concerts with Mark Wastell, Phi Julian, Rhrodri Davies, Bill Thompson, using an analogue/digital modular synthesizer system. As a mandolinist, he has been performing with guitarist Martin Vishnick and a quartet with Sue Lynch, Hutch Demouilpied and Dave Fowler.

Durrant still performs regularly with the acoustic/electronic group Trio Sowari (with Bertrand Denzler and Burkhard Beins) and Mark Wastell’s The SEEN.

### Sylvia and I – {{<artist "Chris Dowding">}} (trumpet, piccolo trumpet, flugelhorn & electronics) & {{<artist "Sylvia Hallett">}} (violin, voice, bicycle wheel, saw & electronics)

![](https://www.mopomoso.com/wp-content/uploads/2019/02/44678908_2184847648423659_6794401626753859584_n.jpg)

The duo Sylvia & I use overlapping loops and other effects to create a sonic dream-world, both haunting and beautiful, with excursions into the dark side. The music unfurls slowly, drawing the listener into an immersive sea of melodic fragments and dreamy soundscapes.

Over the last three years Sylvia & I have performed at improvised music nights and venues around the UK, such as Hundred Years Gallery, The Klinker, Flimflam, and in Norwich. They launched the album with a performance at Wymondham Music Festival, in South Norfolk, and are touring Britain in 2019.

Chris Dowding is a moving force on the Norwich experimental music scene, and co-founded the Norfolk and Norwich Sonic Arts Collective. He performs regularly in the bands Natural Causes (recently reviewed by the Wire), Rude 2.0 (with the trombonist Annie Whitehead), and leads the Moonrise Trio. He has toured the UK with these bands, performing at Union Chapel, Dartington, and regularly at The Vortex Jazz Club. In 2010, he performed on flugelhorn for ‘Songs at a Year’s End’ – an Opera North production about the miner’s strike, composed by Hugh Nankivell, with words by Ian McMillan. Since moving back to Norfolk (where he grew up) in 2014, he has explored a wide range of projects, often with brass, including performing with The Brass Monkeys, the “arresting trumpet duo” some things, and was featured at BBC Radio 3’s Exposure event in Norwich with the “immersive textures” of hymn.

Sylvia Hallett is a multi-instrumentalist and composer. Her work grew out of the London Musicians Collective, where she played in a trio with Lol Coxhill and Susannah Ferrar, and was a regular contributor on NATO records with British Summer Time Ends. Recently she has collaborated with Evan Parker, David Toop, Anna Homler (The Many Moods of Bread and Shed CD), Clive Bell (The Geographers CD) Mike Adcock (Reduced CD), and regularly plays with the London Improvisers Orchestra. She also performs solo and has released 3 solo CDs on MASH and EMANEM. Site specific commissions include a semi-composed work which she performed for the Livio Felluga Wine Company on a hillside vineyard in Italy.  
As a composer she has worked extensively with the choreographers Miranda Tufnell, Jacky Lansley, h2dance, Wonderful Beast Theatre Company, and has composed music for many BBC Radio dramas.

### {{<artist "John Russell">}} (guitar) & {{<artist "Roger Turner">}} (drums, percussion)

![](https://www.mopomoso.com/wp-content/uploads/2019/02/maxresdefault.jpg)

A classic pairing of free improvised music the duo of Russell and Turner really should not be missed. Two musicians of the highest calibre who have played together for many years and have a remarkable empathy, playing an agile, lively music that engages at every twist and turn. A rare chance to hear them live in the UK.

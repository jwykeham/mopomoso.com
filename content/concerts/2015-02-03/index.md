---
title: 15 March 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-03-15T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Emmanuel Cremer"
  - "Lionel Garcin"
  - "Dominic Lash"
  - "Alex Ward"
  - "Chris Burn"
  - "Kay Grant"
  - "Martin Speake"
sets:
  - - Emmanuel Cremer
    - Lionel Garcin
    - Dominic Lash
    - Alex Ward
  - - Chris Burn
  - - Kay Grant
    - Martin Speake
year: 2015
---

### {{<artist "Emmanuel Cremer">}} (cello), {{<artist "Lionel Garcin">}} (sax), {{<artist "Dominic Lash">}} (bass) & {{<artist "Alex Ward">}} (clarinet)

{{<youtube 93qXTpLhZ5E>}}

### {{<artist "Chris Burn">}} (piano)

{{<youtube MWQQCHutEqQ>}}

### {{<artist "Kay Grant">}} (voice) & {{<artist "Martin Speake">}} (sax) duo

{{<youtube i5L6R05MP58>}}

---
title: 20 February 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-02-20T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Olie Brice"
  - "Neil Metcalfe"
  - "Alison Blunt"
  - "Annette Giesriegel"
  - "Elisabeth Harnink"
  - "John Butcher"
  - "Matthew Ostrowski"
  - "Stefan Keune"
  - "John Russell"
year: 2011
---

With the very best from the world of free improvisation our February concert highlights both new and established collaborations from a broad spectrum of backgrounds and approaches.

### {{<artist "Olie Brice">}} (bass) / {{<artist "Neil Metcalfe">}} (flute)

Growing out of out of quartet concerts in 2009 with drummer Tony Marsh and saxophonist Mark Hanslip, and then with Tony Marsh and Evan Parker, this duo greatly enjoyed playing together, and shared a desire to explore the possibilities of operating as an acoustic duo, allowing them to explore dynamic areas made impossible by amplification and free jazz drumming! A debut CD ‘brackish’ is released this Spring on FMR.

{{<youtube d5ToUh1FHa8>}} 

### {{<artist "Alison Blunt">}} (violin) / {{<artist "Annette Giesriegel">}} (voice) / {{<artist "Elisabeth Harnink">}} (piano)

The unceasingly creative Alison Blunt leads this much awaited trio’s UK debut with two of Austria’s finest! Both Harnik and Giesriegel are major players in their native country and last appeared together in London as part of the Alpenglow festival. Lively and inventive improvisers with a wealth of experience.

{{<youtube 6lo5MVe5zjY>}} 

### {{<artist "John Butcher">}} (sax) / {{<artist "Matthew Ostrowski">}} (electronics)

Rightly regarded as one of the world’s leading saxophone innovators John Butcher’s highly individual saxophone techniques are equalled by a fine ear and musicality. He is joined by the New York based electronic/computer music wizard Matthew Ostrowski on a rare UK visit.

{{<youtube 6TMkrn7gY_s>}} 

### {{<artist "Stefan Keune">}} (sax) / {{<artist "John Russell">}} (guitar)

Russell and Keune have been playing together for many years, appearing in the UK, Europe and Japan both as a duo and with other musicians including Evan Parker, Paul Lovens and Hans Schneider. Articulate and inventive exchanges from a long term improvising partnership.

{{<youtube zTWy-aOCLXA>}} 

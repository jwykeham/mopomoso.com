---
title: 23 August 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-08-23T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 8pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Adam Bohman"
  - "Chris Burn"
  - "Luo Chao yun"
  - "Lol Coxhill"
  - "Satoko Fukuda"
  - "Nicola Guazzaloca"
  - "Adam Linson"
  - "Henry Lowther"
  - "Hannah Marshall"
  - "Thomas Rohrer"
  - "John Russell"
  - "Roger Turner"
  - "Alex Ward"
  - "Ute Wassermann"
year: 2011
---

Third day of John Russell’s annual Fete Quaqua which took place on the 21st, 22nd and 23rd of August 2011.

Mopomoso presents John Russell’s annual three day ‘Fete Quaqua’ in an unfolding celebration of the best in free improvisation. A major event in the improvised music calendar and one not to be missed!

‘The basic idea behind all Quaqua projects is to extend existing collaborations in juxtaposition with new groupings and thus provide a fertile ground for free improvisation. The music is very much about the here and now of performance and this uniqueness is reflected in the fact that almost all Quaqua line-ups are never repeated after the event for which they were assembled so I can truly say you won’t have heard this music before!

The 2011 ‘Fete Quaqua’ featured fourteen musicians from three continents, some of whom had worked together for many years, and others who had never played together before. Their musical backgrounds stretched across an enormous variety of styles and settings, the common denominator being shared interest in free improvisation.’ – John Russell

Each concert starts and finishes with a short ensemble piece, the main part of the evening comprising smaller groupings from the larger ensemble, decided immediately prior to performance.

Fete Quaqua 2011 musicians: 
### {{<artist "Adam Bohman">}} (electronics), {{<artist "Chris Burn">}} (trumpets/piano), {{<artist "Luo Chao yun">}} (pipa), {{<artist "Lol Coxhill">}} (saxophone), {{<artist "Satoko Fukuda">}} (violin), {{<artist "Nicola Guazzaloca">}} (piano/accordion), {{<artist "Adam Linson">}} (bass), {{<artist "Henry Lowther">}} (trumpet), {{<artist "Hannah Marshall">}} (cello), {{<artist "Thomas Rohrer">}} (rabeca), {{<artist "John Russell">}} (guitar), {{<artist "Roger Turner">}} (percussion), {{<artist "Alex Ward">}} (clarinet), {{<artist "Ute Wassermann">}} (voice).

{{<youtube NkvhH24oHis>}} 

{{<youtube ZaT43Tw2qDw>}} 

{{<youtube 3EF4tKaCqOE>}} 

{{<youtube F70lxcLpdrw>}} 

{{<youtube 37ZoPxBn7mA>}} 

{{<youtube 4nXzSzpYD4w>}} 

{{<youtube F60rIifyHTI>}} 

{{<youtube 8drVtXyMTIU>}} 

{{<youtube 06HsK1lkfws>}} 

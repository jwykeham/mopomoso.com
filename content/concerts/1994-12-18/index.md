---
artists:
  - Maggie Nicholls
  - Alan Tomlison
  - Steve Noble
  - John Russell
concert_date: 1994-12-18T20:00:00+01:00
concert_series: At The Red Rose
date: 1994-12-18T20:00:00+01:00
notes:
  - '>Mopomoso present the cream of contemporary Vortecists, freeformers and hairshirted impro-heads. - *The Guardian - Sat December 17 1994*'
sets: []
sources:
  - https://www.newspapers.com/image/261162796
title: 18 December 1994
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
costs:
  - key: "Standard"
    value: "£7"
  - key: "Concessions"
    value: "£5"
year: 1994
---

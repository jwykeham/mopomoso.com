---
artists:
  - John Russell
  - Roger Turner
  - Paul Rutherford
concert_date: 1995-09-03T20:30:00Z
concert_series: At The Red Rose
date: 1995-09-03T20:30:00Z
notes: null
sets:
  - - John Russell
    - Roger Turner
  - - Paul Rutherford
sources:
  - https://www.newspapers.com/image/790512689
  - https://www.newspapers.com/image/261182678
title: ' 3 September 1995'
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1995
---
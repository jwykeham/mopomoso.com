---
title: 15 March 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-03-15T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - Steve Noble
  - Alex Ward
  - Martin Speake
  - Oren Marshall
  - Mark Sanders
  - "Kay Grant"
  - "Matt Hutchinson"
  - "Hannah Marshall"
  - "John Russell"
sets:
  - - Steve Noble
    - Alex Ward
  - - Martin Speake
    - Oren Marshall
    - Mark Sanders
  - - "Kay Grant"
    - "Matt Hutchinson"
    - "Hannah Marshall"
    - "John Russell"
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2009
---
Duo, trio, quartet.
### {{<artist "Steve Noble">}} & {{<artist "Alex Ward">}}
{{<youtube qE-WUt9eoKk>}} 

### {{<artist "Martin Speake">}}, {{<artist "Oren Marshall">}} & {{<artist "Mark Sanders">}}
{{<youtube JjaOrFzitLM>}} 

### {{<artist " Kay Grant">}}, {{<artist "Matt Hutchinson">}}, {{<artist "Hannah Marshall">}} & {{<artist "John Russell">}}

---
title: 21 November 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-11-21T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: London Jazz festival
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Yoko Arai"
  - "Max Eastley"
  - "Satoko Fukuda"
  - "Dominic Lash"
  - "Henry Lowther"
  - "Hannah Marshall"
  - "Phil Minton"
  - "John Russell"
  - "Hideaki Shimada"
  - "Roger Turner"
sets:
  - - Yoko Arai
  - - Max Eastley
  - - Satoko Fukuda
  - - Dominic Lash
  - - Henry Lowther
  - - Hannah Marshall
  - - Phil Minton
  - - John Russell
  - - Hideaki Shimada
  - - Roger Turner
year: 2010
---

Following on from the success of Fête Quaqua and to coincide with this year´s London Jazz festival, Mopomoso host an extended concert with a distinctly Anglo-Japanese flavour, again under the banner of guitarist John Russell´s Quaqua (note the early start). With cutting edge music from a changing pool of some of the finest and most innovative improvisers around, playing in an environment where creativity is given full reign, this marks the UK debut appearances of violinist Hideaki Shimada and pianist Yoko Arai.

Also taking part are Roger Turner (percussion), Satoko Fukuda (violin), sound artist and instrument builder Max Eastley, Dominic Lash (bass), Hannah Marshall (cello), Phil Minton (voice) and Henry Lowther (trumpet). An unrepeatable, not to be missed experience! Booking recommended.

### {{<artist "Yoko Arai">}} (piano)

Yoko majored in piano performance, at the same time studying composition with composer Joji Yuasa. Beside Western music, she also studied and practiced Asian classical music, Javanese gamelan and Japanese esoteric Buddhist chant, Shomyo.

### {{<artist "Max Eastley">}} (self made instruments)

Max combines kinetic sound sculpture and music in a unique art form. He has exhibited his sound installations internationally, and collaborated with artists, musicians and film-makers including Brian Eno, David Toop and Peter Greenaway. His installations have been included in Sonic Boom at the Hayward Gallery, London, Sound as Media, Tokyo, and more recently in Riga, Latvia, in collaboration with sound engineer Dave Hunt.

### {{<artist "Satoko Fukuda">}} (violin)

Satoko started the violin at the age of seven and at nine came over to England to study with Catherine Lord. In 1997, she was invited to study at the Yehudi Menuhin School on full scholarship with Natasha Boyarskaya and Lucia Ibragimova. During this time as a soloist she made her television appearance in Japan, was invited to perform at the Wigmore Hall and the Royal Opera House with her Quartet and led the Menuhin School orchestra through Live Broadcast for Classic FM and the Menuhin Festival tour in Switzerland as well as at the Royal Albert and Queen Elizabeth Halls. She was awarded the Concordia Serena Nevill prize for 2007.

### {{<artist "Dominic Lash">}} (bass)

Dominic began playing bass guitar in 1994 and took up the double bass in 2001. Since then he has been in much demand for his improvising skills, performing extensively in the UK, Europe and the USA. Festival appearances include Akbank Jazz Festival (Istanbul), Huddersfield Contemporary Music Festival, Konfrontationen (Nickelsdorf), Manchester Jazz Festival and Tampere Jazz Happening. His work has been broadcast on a number of radio stations, including BBC Radios 1 and 3 and Germany´s SWR2.

### {{<artist "Henry Lowther">}} (trumpet)

Henry was one of the first musicians on the British jazz scene to experiment with free improvisation with a.o Lyn Dobson, Jack Bruce and John Hiseman. He played in the original Mike Westbrook band (which included Mike Osborne and John Surman), John Dankworth and on the Kenny Wheeler album ‘Windmill Tilter’ while also working on the rock scene with Manfred Mann, John Mayall and Keef Hartley, with whom he appeared at the Woodstock festival.

Henry´s musical breadth is confirmed by frequent engagements with major symphony orchestras and ensembles, including the London Brass Virtuosi, the London Philharmonic Orchestra, the City of Birmingham Symphony Orchestra, the London Sinfonietta and the Matrix Ensemble. Henry was for five years the solo flugelhorn player with the strings of the BBC Radio Orchestra, and as a session musician he has recorded with Simon Rattle, Elton John, Paul McCartney, George Harrison, Van Morrison, Henry Mancini, Bing Crosby, Nelson Riddle and Talk Talk amongst many others. He is one of only two or three players in the world to have had the honour of playing lead trumpet with both Gil Evans and George Russell. In the last few years Henry has led his own band ‘Still Waters’.

### {{<artist "Hannah Marshall">}} (cello)

Since graduating from the Guildhall in 1996 Hannah has been devising music &amp; sound within theatre and dance and working extensively as a much respected improviser on the European scene. Amongst others she plays in a trio with Veryan Weston and Satoko Fukuda, a sextet with Alexander Hawkins, Orphy Robinson, Dominic Lash, Javier Carmona, Otto Fisher and a nquartet with Liam Noble, Mark Sanders, and Chris Batchelor. She played on Polar Bear album ‘Held on the Tips of Fingers’.

### {{<artist "Phil Minton">}} (vocals)

Phil joined the Mike Westbrook group in 1963 as trumpeter / vocalist followed by a stint in dance bands before rejoining Westbrook in 1972 until 1984. For the last 30 years he has worked with most of the world´s leading improvising musicians and been guest singer for many composers´ pieces. He was a Nesta awardee in 2005 and in the last 15 years has traveled to many countries with his ‘Feral Choir’ – a workshop and concert for all people who want to sing.

### {{<artist "John Russell">}} (guitar)

John got his first guitar in 1965 while living in Kent and beginning to play in and around London from 1971 onwards. An early involvement with the emerging free improvisation scene (from 1972) followed, seeing him play in such places as The Little Theatre Club, Ronnie Scott&amp;acut;s, The Institute of Contemporary Arts, The Musicians´ Co-Op and the London Musicians´ Collective. From 1974 his work extended into teaching, broadcasts (radio and television) and touring in the United Kingdom and abroad. He founded Quaqua in 1981.

### {{<artist "Hideaki Shimada">}} (violin)

Hideaki lives on the West coast of Japan, in Kanazawa. He received violin tuition from infancy through to his mid-teenage years, when, in the late 1970s he became interested in the improvised music scene in Europe (Evan Parker, Derek Bailey and others). In or around 1982 Shimada met Masami Akita (Merzbow), associating Shimada with arguably some of the most challenging art and artists active in Japan at this time. His solo work since 1986 has been described as both Noise and Music; Avant-Garde and Minimalist; nervous and mellow; frantic and still; hectic and calm.

### {{<artist "Roger Turner">}} (drums, percussion)

Roger has been working as an improvising musician since the early 1970´s : solo work, collaborations with experimental rock musics and open-form songs, extensive work with dance, film and visual artists. workshop formations and the hands-on politics of independent record labels and work situations, plus involvements in numerous jazz based ensembles have all contributed to this process.

He has collaborated with many of the finest European and international musicians from Annette Peacock to Phil Minton, Cecil Taylor to Keith Rowe; tours and concerts have been throughout Europe, Australia, Canada, U.S.A. Mexico, China and Japan.

## About Quaqua

Originally used as a name for a weekly club for improvised music, the first actual Quaqua groups performed in the early 1980’s under the heading ‘Fête Quaqua’ at the now defunct London Musicians´ Collective building in Camden Town. The basic idea then, and behind most Quaqua projects now, is to extend existing collaborations / relationships between musicans in juxtaposition with new ones and thus provide a fertile ground for free improvisation. This was an extension of the way a number of us had been working at the Little Theatre Club and the London Musicians´ Co-Op concerts at the Unity Theatre from around 1973 onwards, where personnel changed from concert to concert from a pool of musicians with different approaches, who shared a love of free improvisation. (One notable exception to this modus operandi was the group ‘News from the Shed’ which initially was named ‘Quaqua’ for a UK tour.) ‘Quaquaversality’ means to point in all directions and the name ‘Quaqua’ is Latin for ‘whithersoever’.

## Videos in order of performance

{{<youtube TwKEdkV6d2A>}} 
{{<youtube dgdD6hN2iNI>}} 
{{<youtube GrcZl4lkMOw>}} 

## Yoko, Roger, Henry, Lol, Hannah, Satoko
(no video available)

{{<youtube h1NTMVcANt0>}} 
{{<youtube wVWtxxVla8w>}} 
{{<youtube YWrl-1jPQPo>}} 
{{<youtube 6B_eLSLrwKQ>}} 
{{<youtube aBh2V-AUnMI>}} 
{{<youtube 7jJnbQ36iHU>}} 
{{<youtube njkNzXa6ciY>}} 
{{<youtube ARGXj1yr9p4>}} 
{{<youtube uC8OAywrKOE>}} 

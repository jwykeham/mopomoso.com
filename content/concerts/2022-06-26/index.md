---
title: "26 June 2022"
date: 2023-01-14T13:23:02Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-06-26T01:00:00+01:00
concert_series: Afternoon Sessions
artists:
  - "Daniel Thompson"
  - "Dominic Lash"
  - "Terry Day"
  - "Charlotte Keeffe"
  - "Aurelie Freoua"
  - "Hannah Marshall"
  - "Veryan Weston"
  - "Pei Ann Yeoh"
sets:
  - - "Daniel Thompson"
    - "Dominic Lash"
  - - "Terry Day"
    - "Charlotte Keeffe"
    - "Aurelie Freoua"
  - - "Hannah Marshall"
    - "Veryan Weston"
    - "Pei Ann Yeoh"
year: 2022
---
On Sunday 26th June, Mopomoso (live) is delighted to be presenting the following magical musicians;

### {{< artist "Daniel Thompson" >}} (guitar) &  {{< artist "Dominic Lash" >}}  (bass)
### {{< artist "Terry Day" >}} (drums) & {{< artist "Charlotte Keeffe" >}} (trumpet, flugelhorn) + {{< artist "Aurelie Freoua" >}} (live painting)
### {{< artist "Hannah Marshall" >}} (cello), {{< artist "Veryan Weston" >}} (piano) &  {{< artist "Pei Ann Yeoh" >}} (violin)

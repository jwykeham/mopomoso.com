---
artists:
  - Alan Tomlinson
  - Richard Barrett
concert_date: 2000-03-18T20:00:00Z
concert_series: At The Red Rose
date: 2000-03-18T20:00:00Z
notes: []
sets: []
sources:
  - https://www.newspapers.com/image/719964352
title: 18 March 2000
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2000
---
---
title: 15 April 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-04-15T14:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Michael Giaquinto"
  - "Chris Hill"
  - "Alec Kronacker"
  - "Veryan Weston"
  - "Pei Ann Yeo"
  - "Terry Day"
  - "John Russell"
sets:
  - - Michael Giaquinto
    - Chris Hill
    - Alec Kronacker
  - - Veryan Weston
    - Pei Ann Yeo
  - - Terry Day
    - John Russell
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2018
---
Three contrasting sets showing the diversity to be found in the world of free improvisation in the friendly intimacy of London's famous "listening Jazz club".

### {{<artist "Michael Giaquinto">}} (e.bass), {{<artist "Chris Hill">}} (clarinet/electronics/amplified objects) & {{<artist "Alec Kronacker">}} (e.guitar)

![trio april](https://www.mopomoso.com/wp-content/uploads/2018/03/trio-april.jpeg)

DiscountGnostic was set up by Chris Hill (clarinet, soprano saxophone and amplified objects) as a recording vehicle to explore the potential of sound art, namely field recordings and sound manipulation along with live improvised material, as artistic practice.

This is the first live performance featuring Michael Giaquinto, electric bass guitar and Alec Kronacker, guitars and effects and promises to combine noise and abstract sound with fragments of melody and rhythm.  
Recent collaborations include a soundtrack composed with Cult of Crimpelene for a book launch, ‘GefDrift’.

### {{<artist "Veryan Weston">}} (piano) & {{<artist "Pei Ann Yeo">}} (violin)

![pei veryan](https://www.mopomoso.com/wp-content/uploads/2018/03/pei-veryan.jpeg)

Highly respected piano player and a Mopoomoso favourite Veryan Weston is joined by Pei Ann Yeo, a Malaysian violinist, currently completing her PhD at King’s College London. She leads her own ensemble TriYeoh with Birmingham-based musicians, and plays regularly with the London Improviser’s Orchestra. A delight.

### {{<artist "Terry Day">}} (percussion/self made instruments) & {{<artist "John Russell">}} (guitar)

![terry john](https://www.mopomoso.com/wp-content/uploads/2018/03/terry-john.jpeg)

Terry and John first played together at Ronnie Scotts in 1972 as members of Mal Dean’s Amazing Band. Since then they have continued to play together in a number of formats both in the UK and abroad, never running out of new musical avenues to explore and enjoy. A close relationship built on years of experience brings a rare closeness and understanding to the stage.

---
title: 17 January 2010
categories:
  - Afternoon Sessions
date: "2010-01-17T00:00:00Z"
concert_date: 2010-01-17T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Mila Dores"
  - "Yvonna Magda"
  - "Derek Saw"
  - Charlie Collins
  - "Yoko Miura"
  - "Evan Parker"
  - "John Edwards"
  - "John Russell"
sets:
  - - "Mila Dores"
    - "Yvonna Magda"
    - "Derek Saw"
    - Charlie Collins
  - - "Yoko Miura"
  - - "Evan Parker"
    - "John Edwards"
    - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2010
---

### {{<artist "Mila Dores">}} (vocals), {{<artist "Yvonna Magda">}} (violin), {{<artist "Derek Saw">}} (tuba) & {{<artist "Charlie Collins">}} (drums)
This new quartet unites two of the rising generation of UK based improvisors from Leeds (Dores & Magda) with two “old hands” from Sheffield (Saw & Collins) in what promises to be a probing and far reaching affair that reflects their divers musical backgrounds. Portuguese vocalist Dores is a member of LIMA and is a graduate of Leeds College of Music’s Jazz course and now teaches and researches vocal techniques from around the world. Yvonna Magda is a member of a.o Swift are the Winds of Life and the Nickoladze Family Georgian Choir and provides music for dance and film collaborations.

Both Derek Saw and Charlie Collins are members of Sonny Simmons’ UK quartet and are two of the leading figures on the Sheffield music scene having played in a variety of settings from Rock, New Orleans Jazz, Blues and R’n B.

{{<youtube znsQTPjkrI0>}}

### {{<artist "Yoko Miura">}} (piano)
{{<artist/description "Yoko Miura">}}

{{<youtube qVJ8QSbwEsw>}}

### {{<artist "Evan Parker">}} (saxophones), {{<artist "John Edwards">}} (bass) & {{<artist "John Russell">}} (guitar)
Three great musicians who have worked together in numerous permutations over many years and in many countries. Put together by Evan Parker, this particular group has been performing as a trio for sometime now. Freely improvised, constantly changing and closely interactive; drawing on a wealth of experience, they invite the audience into a unique sound world full of warmth and surprise. Check out the new CD "House Full of Floors" on Tzadik.

{{<youtube QcZ7Bcwte8E>}}

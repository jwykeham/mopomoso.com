---
title: 28 February 2010
categories:
  - Afternoon Sessions
date: "2010-02-28T00:00:00Z"
concert_date: 2010-02-28T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Stephen Grew"
  - "Mick Beck"
  - "Derek Bailey"
  - "Andy Sheppard"
  - "Keith Tippett"
  - "Evan Parker"
  - "Louis Moholo"
  - "LULL"
  - "Tanja Feichtmair"
  - "Josef Novotny"
  - "Uli Winter"
  - "Fredi Proll"
  - "John Russell"
  - "Walter Wierbos"
  - "John Edwards"
  - "Harri Sjoestrom"
  - "John Russell"
  - "Cecil Taylor"
sets:
  - - "Stephen Grew"
    - "Mick Beck"
  - - "LULL"
    - "Tanja Feichtmair"
    - "Josef Novotny"
    - "Uli Winter"
    - "Fredi Proll"
  - - "John Russell"
    - "Harri Sjoestrom"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2010
---

### {{<artist "Stephen Grew">}} (piano) & {{<artist "Mick Beck">}} (reeds)
 Stephen and Mick are two fine musicians and prime movers in their respective towns of Lancaster and Sheffield. Between them they have worked in the UK and abroad with musicians such as {{<artist "Derek Bailey">}}, {{<artist "Andy Sheppard">}}, {{<artist "Keith Tippett">}}, {{<artist "Evan Parker">}} and {{<artist "Louis Moholo">}}. Grew has developed his own series of scales and motifs which he uses as building blocks in conjunction with Beck’s continuing re-invention of the saxophone and unusually in improvisation, the bassoon. They have issued one CD as a duo, ‘Picture August’ Bruces Fingers BF 34.
 
"Uncompromising, intense" and "ridiculously entertaining" Jazzwise review for Discus CD "Its Morning" Stephen Grew trio with Mick Beck.

{{<youtube EbQXYEGjkLM>}}

### {{<artist "LULL">}} - {{<artist "Tanja Feichtmair">}} (sax), {{<artist "Josef Novotny">}} (piano – electronics), {{<artist "Uli Winter">}} (cello) & {{<artist "Fredi Proll">}} (drums)
Formed in 2001, LULL are an energetic and joyful group of improvisers from Austria, here to promote their new CD "The Zipper", now available on Leo Records. Tanja has played Mopomoso before in her ongoing duo with {{<artist "John Russell">}} and she has toured with the group alongside {{<artist "Walter Wierbos">}} and {{<artist "John Edwards">}}. An imaginative and dynamic quartet who play a far reaching music and a rare chance to hear them in the UK in this their only appearance.

{{<youtube AJOf51z69N4>}}

### {{<artist "Harri Sjoestrom">}} (sax) & {{<artist "John Russell">}} (guitar)
Harri is a major figure on the European free improvised music scene and is probably best known for his many collaborations with {{<artist "Cecil Taylor">}}. He is also co-founder of the European Contemporary Improvisers Orchestra. Together with Russell they have played in many different groups but this is the first time they have appeared as a duo. Here to make a recording, this is their only London concert and should provide a fascinating insight into the music of two very experienced and inventive musicians.

{{<youtube cY_qPnzVEfo>}}

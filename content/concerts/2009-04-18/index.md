---
title: 19 April 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-04-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - Jim Dvorak
  - Neil Metcalfe
  - John Edwards
  - Dave Fowler
  - Treehouse
  - Ute Kanngiesser
  - Guillaume Viltard
  - Jamie Coleman
  - Phil Minton
  - John Russell
sets:
  - - Jim Dvorak
    - Neil Metcalfe
    - John Edwards
    - Dave Fowler
  - - Treehouse
    - Ute Kanngiesser
    - Guillaume Viltard
    - Jamie Coleman
  - - Phil Minton
    - John Russell
year: 2009
---
Quartet. trio, duo.
### {{<artist "Jim Dvorak">}}, {{<artist "Neil Metcalfe">}}, {{<artist "John Edwards">}} & {{<artist "Dave Fowler">}}
{{<youtube Rz4PBv84PZA>}} 
### Treehouse - {{<artist "Ute Kanngiesser">}}, {{<artist "Guillaume Viltard">}} & {{<artist "Jamie Coleman">}}
{{<youtube 9A6GxONP_T0>}} 
### {{<artist "Phil Minton">}} & {{<artist "John Russell">}}
{{<youtube RxLUtRbir8s>}} 

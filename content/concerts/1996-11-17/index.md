---
artists: []
concert_date: 1996-11-17T20:00:00Z
concert_series: At The Red Rose
date: 1996-11-17T20:00:00Z
notes: null
sets:
  - - Louis Moholo
    - John Russell
  - - Phil Durrant
  - - Viv Corringham
    - Sylvia Hallett
sources:
  - https://www.newspapers.com/image/790535142
title: 17 November 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
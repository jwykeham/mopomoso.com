---
title: 16 June 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-06-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Phil Marks"
  - "Paul Obermayer"
  - "Rex Casswell"
  - "Audrey Lauro"
  - "Gunter Christmann"
  - "Elke Schipper"
  - "Le Quan Ninh"
sets:
  - - Phil Marks
    - Paul Obermayer
    - Rex Casswell
  - - Audrey Lauro
  - - Gunter Christmann
    - Elke Schipper
    - Le Quan Ninh
year: 2013
---

### BARK – {{<artist "Phil Marks">}} (drums), {{<artist "Paul Obermayer">}} (electronics) & {{<artist "Rex Casswell">}} (guitar)

{{<youtube 381lTtH_Bjo>}}

### {{<artist "Audrey Lauro">}} (saxophone)

{{<youtube UgoqngE9eI0>}}

### {{<artist "Gunter Christmann">}} (trombone,cello), {{<artist "Elke Schipper">}} (sound poetry), {{<artist "Le Quan Ninh">}} (percussion) & John Russell (guitar)

{{<youtube lscFdwBe2Vw>}}

{{<youtube W4rjRIp5zV0>}}

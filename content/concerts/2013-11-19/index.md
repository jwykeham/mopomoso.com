---
title: 15 December 2013
categories:
  - Afternoon Sessions
# ‘Another good year – bring a cake!’

date: "1970-01-01T00:00:00Z"
concert_date: 2013-12-15T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: End of Year Celebration
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Russell"
year: 2013
---
# An annual end of year celebration of all things improvised with a ‘cast of thousands’.
Since its inception some years ago Mopomoso’s end of year improvisorama has become a focal point for musicians to come and play in a series of short sets; to meet old friends and to make new ones. Certainly ‘the best party in town’ where audiences get a chance to experience a wide range of music in a less formal setting than our regular concert series. 
### Fernandez / Kim / Collins
{{<youtube ZNwWMC4wlsk>}}
### Ross
{{<youtube Z9vP7cspsQA>}}
### Gilonis
{{<youtube H8SSpxaiP0A>}}
### Sanderson / Thompson
{{<youtube sUrY7erkgK0>}}
### McDonald / Tejero / Hutchinson
{{<youtube TgrqXl4z5CE>}}
### Parfett
{{<youtube y_Q7V88kigI>}}
### Taylor / Kjaer / Caputo
{{<youtube 6wp2xVKWWC4>}}
### Eldridge / Musson / Bennett
{{<youtube IsaBE3lMTS0>}}
### Bohman / Northover
{{<youtube g3c6Gn82HoI>}}
### Mattos / Gouvea / Dvorak
{{<youtube j6RUq-dRytU>}}
### Tomlinson / Wilkinson
{{<youtube wZdMkSebddQ>}}
### Corringham / Marshall / Adato
{{<youtube Dj0Mld6Zt7I>}}
### Tucker / Lambert / Morris / Rueda / Scott
{{<youtube X_5HEYcPxBQ>}}
### Shearsmith / Jensen / Hutchinson / Fowler
{{<youtube 6qO0KaYeQG8>}}
### Donovan / Chilton / Sanatian
{{<youtube Q14qrWIp5_I>}}
### Thompson / Jackson / Lyle
{{<youtube okfhdyHxEdI>}}
### Beresford / Tomlinson
{{<youtube Eqkjrs1tpHc>}}
### Eldridge / Lowther / Russell
{{<youtube qs2zx04wfA8>}}
 

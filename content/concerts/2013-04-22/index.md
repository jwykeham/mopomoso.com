---
title: 19 May 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-05-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Jorn Erik Ahlsen"
  - "Stian Larsen"
  - "Cyril Bondi"
  - "d’incise"
  - "Phil Minton"
  - "John Russell"
sets:
  - - Jorn Erik Ahlsen
    - Stian Larsen
  - - Cyril Bondi
    - d’incise
  - - Phil Minton
    - John Russell
year: 2013
---

### KOOK – {{<artist "Jorn Erik Ahlsen">}} (guitar, live electronics) & {{<artist "Stian Larsen">}} (guitar, live electronics)

{{<youtube aHysZmft7Zk>}}

[https://www.kookmusic.co.nr/ ](https://www.kookmusic.co.nr/  "https://www.kookmusic.co.nr/ ")

### Diatribes – {{<artist "Cyril Bondi">}} (floor tom, objects) & {{<artist "d’incise">}} (laptop, objects)

{{<youtube uTWkFQQLIQI>}}

[https://www.dincise.net/diatribes/diatribes.html ](https://www.dincise.net/diatribes/diatribes.html)

### {{<artist "Phil Minton">}} (voice) & {{<artist "John Russell">}} (guitar) duo

{{<youtube gRta8gepLOY>}}

[https://www.philminton.co.uk/](https://www.philminton.co.uk/ "https://www.philminton.co.uk/") 
[https://www.john-russell.co.uk](https://www.john-russell.co.uk "https://www.john-russell.co.uk")

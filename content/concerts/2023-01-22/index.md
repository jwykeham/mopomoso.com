---
title: "22 January 2023"
date: 2023-01-13T16:10:00Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
tickets: https://www.ticketweb.uk/event/mopomoso-live-january-vortex-jazz-club-tickets/12843575
concert_date: 2023-01-22T00:00:00Z
concert_series: Afternoon Sessions
artists:
  - "Paul Jolly"
  - "Mike Adcock"
  - "Sylvia Hallett"
  - "John Butcher"
  - "Pascal Marzan"
  - "Matt Clark"
  - "James Edmunds"
  - "Charlotte Keeffe"
year: 2023
---
### {{<artist "Paul Jolly">}} (sax/clarinets), {{<artist "Mike Adcock">}} (accordion/bits&pieces) & {{<artist "Sylvia Hallett">}} (violin/saw etc)

### {{<artist "John Butcher">}} (sax) & {{<artist "Pascal Marzan">}} (guitar)

### MC3 – {{<artist "Matt Clark">}} (guitar), {{<artist "James Edmunds">}} (drums) & {{<artist "Charlotte Keeffe">}} (trumpet & flugelhorn)


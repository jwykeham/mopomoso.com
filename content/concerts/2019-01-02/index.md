---
title: 20 January 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-01-20T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Matthew Grigg"
  - "Mark Hewins"
  - "Ross Lambert"
  - "Pascal Marzan"
  - "John Russell"
  - "Dave Tucker"
  - "Alex Ward"
  - "Joshua Weitzel"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2019
---
**GUITARS!**

![](https://www.mopomoso.com/wp-content/uploads/2019/01/gtr-spec-jan-2019.png)

Electric, acoustic, micro-tonal, plectrum, prepared, finger style, steel, nylon, hollow and solid body. Showcasing a number of approaches to the instrument Mopomoso starts the New Year with an all guitar special featuring {{<artist "Matthew Grigg">}}, {{<artist "Mark Hewins">}}, {{<artist "Ross Lambert">}}, {{<artist "Pascal Marzan">}}, {{<artist "John Russell">}}, {{<artist "Dave Tucker">}}, {{<artist "Alex Ward">}} and {{<artist "Joshua Weitzel">}}. 

> "It don’t mean a thing if it ain’t got a string!"

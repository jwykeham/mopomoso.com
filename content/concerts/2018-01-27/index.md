---
title: 18 February 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-02-18T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Sonic Pleasure"
  - "Mark Browne"
  - "Ian Smith"
  - "Bruce Coates"
  - "Ian Simpson"
  - "Mark Hanslip"
  - "John Jasnoch"
  - "John Russell"
  - "Alice Eldridge"
  - "Beibei Wang"
year: 2018
---
### CRUSH!!!

![CRUSH!!!](https://www.mopomoso.com/wp-content/uploads/2018/01/CRUSH.jpeg)

CRUSH!!! are {{<artist "Sonic Pleasure">}}, {{<artist "Mark Browne">}} and {{<artist "Ian Smith">}}. The group utilises a vast array of found and home-made sound making devices combined with more traditional instruments. Sonic Pleasure is famed for her unique crafting of sound from bricks and masonry, reducing much of her instrumentation to dust by the end of a concert. Mark Browne plays castrato saxophone, broken glass, percussion, whistles and bones. Ian Smith will play trumpet in an extraordinary manner.



### LINEA 4

![LINEA 4](https://www.mopomoso.com/wp-content/uploads/2018/01/LINEA-4.jpeg)

LINEA 4 are {{<artist "Bruce Coates">}} (sax) {{<artist "Ian Simpson">}} (guitar) {{<artist "Mark Hanslip">}} (sax) and {{<artist "John Jasnoch">}} (guitar) This sax and guitar double-duo quartet, some of whom are well known to regular Mopomoso visitors, have played a number of concerts in Birmingham and Manchester and we are delighted to have them visit us again as part of a small tour.



### NARRATIVE

![narrative three](https://www.mopomoso.com/wp-content/uploads/2018/01/narrative-three.jpeg)

NARRATIVE is the working title for the start of a new initiative from {{<artist "John Russell">}} (electric guitar) with {{<artist "Alice Eldridge">}} (feedback cello) and {{<artist "Beibei Wang">}} (drums) Starting with this trio concert the aim is to see where the narrative takes us. With a firm base in the electric end of things and in the hands of three great innovatory players this could just be the set to blow away any remaining winter blues.

***
Now in its 28th year Mopomoso has, amongst a number of other projects, presented an unbroken monthly series of concerts focusing solely on freely improvised music making it the longest running such series in the UK. Founded by guitarist John Russell it is run entirely by volunteers.

The word Mopomoso was coined from the phrase MOdernism – POstMOdernism – SOwhat?

www.mopomoso.com

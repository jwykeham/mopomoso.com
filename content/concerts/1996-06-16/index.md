---
artists: []
concert_date: 1996-06-16T20:00:00+01:00
concert_series: At The Red Rose
date: 1996-06-16T20:00:00+01:00
notes: null
sets:
  - - Lol Coxhill's Recedents
  - - Evan Parker
    - John Russell
  - - Chris Burn
sources:
  - https://www.newspapers.com/image/790530845
  - https://www.newspapers.com/image/260857284
title: 16 June 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
---
title: "23 January 2022"
date: 2023-01-14T13:23:51Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-01-23T00:00:00Z
concert_series: Afternoon Sessions
artists:
  - "Sylvia Hallett"
  - "Douglas Benford"
  - "Caroline Kraabel"
  - "John Edwards"
  - "Meg Morley"
sets:
  - - "Sylvia Hallett"
    - "Douglas Benford"
  - - "Caroline Kraabel"
    - "John Edwards"
  - - "Meg Morley"
year: 2022
---
To kick off 2022, Mopomoso is delighted to present the following exquisite improvising musicians:

### {{<artist "Sylvia Hallett">}} (violin) and {{<artist "Douglas Benford">}} (little instruments)
### {{<artist "Caroline Kraabel">}} (sax) and {{<artist "John Edwards">}} (double bass)
### {{<artist "Meg Morley">}} (piano)

---
title: "21 April 2024"
date: 2024-03-27T08:44:37Z
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-04-21 14:00:00 +0100 BST
concert_series: Mopomoso Live
ticket_link: https://www.ticketweb.uk/event/mopomoso-april-vortex-jazz-club-tickets/13372804
sets:
  - - "John Butcher"
    - "Phil Durrant"
    - "Mark Wastell"
  - - "N.O. Moore"
    - "Dominic Lash"
    - "Roger Turner"
  - - "Mike Adcock"
    - "Stuart Wilding"
artists:
  - "John Butcher"
  - "Phil Durrant"
  - "Mark Wastell"
  - "N.O. Moore"
  - "Dominic Lash"
  - "Roger Turner"
  - "Mike Adcock"
  - "Stuart Wilding"
year: 2024
---

---
artists: []
concert_date: 1996-01-07T20:00:00Z
concert_series: At The Red Rose
date: 1996-01-07T20:00:00Z
notes: null
sets:
  - - Phil Minton
    - Roger Turner
  - - Vanessa Mackness
    - John Butcher
sources:
  - https://www.newspapers.com/image/790521371
title: ' 7 January 1996'
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
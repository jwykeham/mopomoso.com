---
title: 17 January 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-01-17T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: 25th Anniversary
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Terry Day
    - Billy Steiger
  - - Kay Grant
    - Marcio Mattos
  - - Tanja Feichtmair
    - John Russell
year: 2016
---

Now celebrating our 25th year of an unbroken concert series dedicated solely to free improvisation the first concert of this anniversary year features three fascinating duos, each containing one of the three surviving members from the last version of Mal Dean’s Amazing Band, a group that first performed at Ronnie Scotts in 1973.

### {{<artist "Terry Day">}} (drums) & {{<artist "Billy Steiger">}} (violin)

{{<youtube tVZZyddTtBk>}}

Terry Day should need no introduction to followers of improvised music. An ever-creative force whose collaborations with some of the music’s finest players and as a founder member of The People Band are well documented. Always innovative and keen to encourage and nurture creativity he plays here with regular partner Billy Steiger.

### {{<artist "Kay Grant">}} (voice) & {{<artist "Marcio Mattos">}} (cello)

{{<youtube Bj2Ri3wL4s4>}}

Described by The Wire magazine as “Totally in the here and now… intimate and musically flirtatious” Kay Grant’s vocals can switch between the ethereal and the visceral in a matter of moments. Partnered here by the skittering cello and electronics of Marcio Mattos who provides a continuous narrative that is at once supportive and also provocatory.

### {{<artist "Tanja Feichtmair">}} (sax) & {{<artist "John Russell">}} (guitar)

{{<youtube 2nhisO-RXOs>}}

After over ten years of playing together Feichtmair and Russell’s duo exhibits a feiry intensity that delights in taking risks and playing on the edge while firmly built on their shared sense of musical possibilities.

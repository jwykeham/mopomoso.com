---
artists:
  - Uki Sobotta
  - Vanessa Mackness
concert_date: 1995-08-20T20:00:00+01:00
concert_series: At The Red Rose
date: 1995-08-20T20:00:00+01:00
notes:
  - '>Euphonium and voice anarchy followed by improv choas. - *The Guardian - Sat August 19 1995*'
sets: []
sources:
  - https://www.newspapers.com/image/261162796
title: 20 August 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1995
---
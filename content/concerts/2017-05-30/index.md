---
title: 18 June 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-06-18T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Phil Durrant"
  - "Rob Flint"
  - "Kev Hopper"
  - "Richard Sanderson"
  - "Mark Browne"
  - "Ian McGowan"
  - "Sonic Pleasure"
  - "John Russell"
  - "Roger Turner"
sets:
  - - Phil Durrant
    - Rob Flint
    - Kev Hopper
    - Richard Sanderson
  - - Mark Browne
    - Ian McGowan
    - Sonic Pleasure
  - - John Russell
    - Roger Turner
year: 2017
---
### TICKLISH – {{<artist "Phil Durrant">}} (modular synth), {{<artist "Rob Flint">}} (video), {{<artist "Kev Hopper">}} (bass guitar & effects) & {{<artist "Richard Sanderson">}} (amplified melodeon).

![Ticklish by Adam Brett](https://www.mopomoso.com/wp-content/uploads/2017/05/Ticklish-by-Adam-Brett-300x225.jpg)

These veterans of the electronic music and improvisation scene are on this occasion wielding slightly different instruments while still retaining the distinctive eccentricities of the Ticklish sound; gurgling body noises, startling contrasts in texture and quirky, live film making.

The four band members come from a variety of musical backgrounds including left-wing pop, punk, underground, and free improvisation and have worked with musicians as different as Derek Bailey and Grooverider so we can expect a wide ranging and constantly moving sound field.

### CRUSH – {{<artist "Mark Browne">}} (sax,plus), {{<artist "Ian McGowan">}} (trumpet) & {{<artist "Sonic Pleasure">}} (objects)

![crush](https://www.mopomoso.com/wp-content/uploads/2017/05/crush-225x300.jpg)

Sonic Pleasure is famed for her unique use of bricks and masonry to make sound and usually the materials are reduced to dust by the end of a concert. She is ably abetted in this by Mark Browne who apart from saxophone also uses bones, glass, whistles and a variety of percussion. The third member is trumpet player Ian McGowan, an extraordinary musician adept at coaxing a wide range of sounds from his instrument. It is quite a journey and a lot of fun!

### {{<artist "John Russell">}} (guitar) & {{<artist "Roger Turner">}} (percussion) duo

![russell turner grab](https://www.mopomoso.com/wp-content/uploads/2017/05/russell-turner-grab-300x173.jpeg)

A much loved classic duo, whose first release ‘Artless Sky’ with Japanese trumpeter Toshinori Kondo came out in 1979; a Russell / Turner duo concert is not to be missed. Leaving no stone unturned and playing with an intuitive feel developed from years of working closely together, this duo never stands still. Engaging and exciting music at its best!

***
Mopomoso has, among other things, hosted the UK’s longest running series dedicated solely to freely improvised music in a series of monthly concerts going back 26 years, of which this is the latest.

As George says: "Good atmosphere. Good music played by good musicians. I can’t think of a better way to spend a Sunday afternoon!"

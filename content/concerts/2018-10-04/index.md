---
title: 21 October 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-10-21T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Alex Maguire"
  - "Simon Picard"
  - "Yoko Miura"
  - "Stefan Keune"
  - "Mia Zabelka"
  - "Axel Dörner"
  - "Sture Erikson"
  - "John Edwrads"
  - "Raymond Strid"
  - "Isadora Edwards"
  - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Alex Maguire
    - Simon Picard
  - - Yoko Miura
    - Stefan Keune
    - Mia Zabelka
  - - Axel Dörner
    - Sture Erikson
    - John Edwrads
    - Raymond Strid
  - - Isadora Edwards
    - John Russell
year: 2018
---
Welcoming in the Winter season with a stunning international line up of four sets, featuring some of the finest music from the world of free improvisation from around the world.


### {{<artist "Alex Maguire">}} (piano) & {{<artist "Simon Picard">}} (sax)
![](https://www.mopomoso.com/wp-content/uploads/2018/10/PICARD.NEW__0.jpg)
A deeply rooted sense of harmony and melody are tightly locked into the improvisations from this respected and much loved duo. Delighted to have them on a return visit to Mopomoso.
***
### {{<artist "Yoko Miura">}} (piano), {{<artist "Stefan Keune">}} (sax) & {{<artist "Mia Zabelka">}} (violin)
![](https://www.mopomoso.com/wp-content/uploads/2018/10/yoko-stefan-mia.jpeg)
We play host to a first time meeting for this international trio coming here from Germany, Japan and Austria.The music promises to move between the delicate use of space and a dense, visceral expressionism
***
### {{<artist "Axel Dörner">}} (trumpet), {{<artist "Sture Erikson">}} (sax), {{<artist "John Edwrads">}} (bass) & {{<artist "Raymond Strid">}} (drums)
![](https://www.mopomoso.com/wp-content/uploads/2018/10/sture-4tet.jpeg)

"Should be recognized without qualifictions as an identifying archetype of top- flight improvisation", "A true Northern European super group" – Ken Waxman, Jazz Weekly on The Electrics Jazz, free jazz, improv and contemporary music in a melting pot, a highly original musical profile created by four internationally acknowledged improvisers regular bassist Joe Williamson is replaced for this gig only by John Edwards.
***
### {{<artist "Isadora Edwards">}} (cello) & {{<artist "John Russell">}} (guitar)
![](https://www.mopomoso.com/wp-content/uploads/2018/10/isadora-edwards-john-russell.jpeg)
Fiery and spirited cellist arrives from Chile to play with renowned guitarist Russell. We gratefully acknowledge help and support from the Chilean Ministry of Culture for this concert.

![](https://www.mopomoso.com/wp-content/uploads/2018/10/Screenshot-2018-10-13-at-14.47.20.png)

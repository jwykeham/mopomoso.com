---
title: 19 March 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-03-19T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Kay Grant"
  - "Marcio Mattos"
  - "Lawrence Casserley"
  - "Alan Wilkinson"
  - "Viv Corringham"
  - "John Russell"
sets:
  - - Kay Grant
    - Marcio Mattos
    - Lawrence Casserley
  - - Alan Wilkinson
  - - Viv Corringham
    - John Russell
year: 2017
---
This month Mopomoso brings you an afternoon of performances by musicians who are all recognised as stalwarts of this country’s free improvisation scene.

### {{<artist "Kay Grant">}} (voice), {{<artist "Marcio Mattos">}} (bass and/or cello) & {{<artist "Lawrence Casserley">}} (electronics trio)

![marcio kay lawrence](https://www.mopomoso.com/wp-content/uploads/2017/02/marcio-kay-lawrence-300x160.jpeg)

The first set promises to be fascinating as it features three musicians who will be familiar to all our regulars, but in a new combination. American vocalist Kay Grant has established herself as a firm Mopomoso favourite since settling in London, and here she is joined by two venerable figures. Over the last four decades Marcio Mattos has been a mainstay of so many projects that listing them would be an impossible task. It is staggering to realise that his career goes back to the very early concerts of Derek Bailey’s “Company”, and he should always be remembered for being a member of the ground-breaking London Bass Trio if nothing else. Composer, instrument maker and electronics pioneer Lawrence Casserley came to regular live performance much later – following his retirement as professor at the Royal College of Music. in the 1990’s. Naturally he was soon playing in the highest company and is probably best known generally for his many recordings with Evan Parker, and to us in particular for his frequent participation in Mopomoso and Fete Quaqua events.

### {{<artist "Alan Wilkinson">}} (solo saxophone(s))

![alan wilkinson by Simon Godley](https://www.mopomoso.com/wp-content/uploads/2017/02/alan-wilkinson-by-Simon-Godley-300x184.jpg)

Next up is a solo set from another favourite, – curator of the Flim Flam, and surely the country’s most explosive saxophonist – Alan Wilkinson. Al tore the place up and got a standing ovation at a Mopomoso event just before Christmas, and, put quite simply, we couldn’t wait to have him back!


### {{<artist "Viv Corringham">}} (voice) & {{<artist "John Russell">}} – (guitar)

![john russell viv corringham](https://www.mopomoso.com/wp-content/uploads/2017/02/john-russell-viv-corringham-300x167.jpeg)

As always, the final set features Mopomoso host John Russell. This month he renews his collaboration with vocalist Viv Corringham. We haven’t seen this one for some time. Viv used to be seen quite regularly but made the opposite journey to Kay Grant and settled in New York making this a rare appearance that should not be missed.

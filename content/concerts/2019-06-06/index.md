---
title: 16 June 2019
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2019-06-16T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Matt Hutchinson"
  - "Sue Lynch"
  - "Hutch Demouilpied"
  - "Emil Karlsen"
  - "Alan Tomlinson"
  - "Dave Tucker"
  - "Phil Marks"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
sets:
  - - Matt Hutchinson
    - Sue Lynch
    - Hutch Demouilpied
  - - Emil Karlsen
  - - Alan Tomlinson
    - Dave Tucker
    - Phil Marks
year: 2019
---
Three sets from the world of free improvisation to enlighten and inspire.

### {{<artist "Matt Hutchinson">}} (piano), {{<artist "Sue Lynch">}} (sax) & {{<artist "Hutch Demouilpied">}} (trumpet)

![](https://www.mopomoso.com/wp-content/uploads/2019/06/matt-sue-hutch-trio.png)

Another chance to catch the longstanding duo of Sue Lynch – sax/flute and Hutch Demouilpied – trumpet , augmented on this occasion by keyboard player Matt Hutchinson on piano. He brings a further dynamic and perhaps a certain ‘wildcard’ element to their excellent and sometimes uncanny musical rapport.

### {{<artist "Emil Karlsen">}} (drums)

![](https://www.mopomoso.com/wp-content/uploads/2019/06/emil-crop.png)

Emil Karlsen’s electro-acoustic solo project is inspired by his philosophy of using the drum kit as a solo instrument. Being born and raised in a small village deep in the Norwegian forest has given him the opportunity to be inspired and respect the raw force of nature, and work organically with the drum kit. A root in the organic and acoustic, but also working with a blend of acoustic and electronics has given him an opportunity to explore textures, rhythm and loops to create a personal world of musical expression. This project is a portrait of many years thinking compositionally and texturally about the art of drumming.

### {{<artist "Alan Tomlinson">}} (trombone), {{<artist "Dave Tucker">}} (guitar) & {{<artist "Phil Marks">}} (drums)

![](https://www.mopomoso.com/wp-content/uploads/2019/06/at-three.png)

Much loved trio led by trombonist Tomlinson bring a joyful sense of anarchic play and fine musicianship in a return visit to the series. Expect the unexpected when you least expect it. Engaging and fun.

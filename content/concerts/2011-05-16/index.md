---
title: 19 June 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-06-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Audrey Lauro"
  - "Isabelle Sainte-Rose"
  - "Yoko Miura"
  - "Lawrence Casserley"
  - "Raymond McDonald"
  - "Maggie Nicols"
  - "John Russell"
  - "Mia Zabelka"
sets:
  - - Audrey Lauro
    - Isabelle Sainte-Rose
  - - Yoko Miura
    - Lawrence Casserley
    - Raymond McDonald
  - - Maggie Nicols
    - John Russell
    - Mia Zabelka
year: 2011
---

### Life is Knife – {{<artist "Audrey Lauro">}} (sax) / {{<artist "Isabelle Sainte-Rose">}} (cello)

{{<youtube iAMSAKwWRJE>}} 

### {{<artist "Yoko Miura">}} (piano) / {{<artist "Lawrence Casserley">}} (electronics) / {{<artist "Raymond McDonald">}} (sax)

{{<youtube kAm64l125ac>}} 

### Trio Blurb – {{<artist "Maggie Nicols">}} (voice) / {{<artist "John Russell">}} (guitar) / {{<artist "Mia Zabelka">}} (violin)

{{<youtube 75-quWCAQ58>}} 

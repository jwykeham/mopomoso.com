---
artists:
  - Evan Parker
concert_date: 1995-10-21T20:00:00+01:00
concert_series: At The Red Rose
date: 1995-10-21T20:00:00+01:00
notes: []
sets:
  - - Alex Maguire Trio
sources:
  - https://www.newspapers.com/image/260698215
title: 21 October 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1995
---
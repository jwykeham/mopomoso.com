---
title: "21 January 2024"
date: 2024-01-01T14:00:00:00Z
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-01-21 00:00:00 +0000 GMT
concert_series: Mopomoso Live
artists:
  - "John Butcher"
  - "Pascal Marzan"
  - "Si Paton"
  - "Douglas Benford"
  - "Roland Ramanan"
  - "Ecka Mordecai"
  - "Sylvia Hallett"
sets:
  - - "John Butcher"
    - "Pascal Marzan"
  - - "Si Paton"
  - - "Douglas Benford"
    - "Roland Ramanan"
    - "Ecka Mordecai"
    - "Sylvia Hallett"
ticket_link: https://www.ticketweb.uk/event/mopomoso-january-vortex-jazz-club-tickets/13415343
year: 2024
---
### {{<artist "John Butcher">}} & {{<artist "Pascal Marzan">}}
Born in Brighton and living in London, {{<artist "John Butcher">}} is a saxophonist whose work ranges through improvisation, his own compositions, multitracked pieces and explorations with feedback, unusual acoustics and non-concert locations. He is well known as a solo performer who attempts to engage with a sense of place. Resonant Spaces, for example, is a collection of performances recorded during a tour of unusual locations in Scotland and the Orkney Islands.
Since 1982 he has collaborated with hundreds of artists, to name a few: {{<artist "Derek Bailey">}}, {{<artist "Akio Suzuki">}}, {{<artist "John Stevens">}}’ Spontaneous Music Ensemble, {{<artist "Rhodri Davies">}}, Last Dream of the Morning (with {{<artist "John Edwards">}} & {{<artist "Mark Sanders">}}), {{<artist "Steve Beresford">}}, {{<artist "Matthew Shipp">}}, {{<artist "Gerry Hemingway">}}, {{<artist "Chris Burn">}}, {{<artist "Magda Mayas">}}, {{<artist "Gino Robair">}}, Thermal ({{<artist "Andy Moor">}} & {{<artist "Thomas Lehn">}}), {{<artist "Christian Marclay">}}, {{<artist "Eddie Prévost">}}, {{<artist "Okkyung Lee">}}, {{<artist "John Russell">}}, {{<artist "Ståle Liavik Solberg">}} and {{<artist "Phil Minton">}}.

---

Currently living in London, {{<artist "Pascal Marzan">}} performs on a 10 string microtonal guitar.
After studying classical guitar – mainly devoted to the twentieth century repertoire – and teaching guitar in several music schools in Budapest, Hungary and France, Pascal has dedicated himself to free improvisation and the exploration of microtonal tunings of his instrument. His playing has been influenced by his interest in the folk musics of Eastern Europe, Asia and Africa…
He has recorded guitar duets with {{<artist "John Russell">}}, {{<artist "Roger Smith">}} (Emanem records) and a trio with {{<artist "Sabu Toyozumi">}} and {{<artist "Dan Warburton">}} (Improvising Beings records)

### {{<artist "Si Paton">}}
{{<artist "Si Paton">}} is a composer, bassist, academic and promoter. He is commencing his PhD at Birmingham Conservatoire under the supervision of Ed Bennett and Seán Clancy, researching composition for improvising large ensembles within DIY and punk influenced contexts. Past projects he has been involved in include leading the prog-jazz group Selectric, punk-funk big band Pack of Wolves Arkestra and the acoustic-grindcore suite Gillberg Variations. As an improviser, he has performed with the likes of {{<artist "Nils Økland">}}, {{<artist "Øyvind Skarbø">}}, {{<artist "Chris Sharkey">}}, {{<artist "Maria Chiara Argirò">}}, {{<artist "Cath Roberts">}} and regularly plays with Apocalypse Jazz Unit. Currently, he is working the groups Machete Squad, Phame and Huh? while performing several additional gigs of improvised music.

### {{<artist "Douglas Benford">}}, {{<artist "Roland Ramanan">}}, {{<artist "Ecka Mordecai">}} & {{<artist "Sylvia Hallett">}}

As a composer and sound artist, {{<artist "Douglas Benford">}} has been involved in various audio genres since the late 1980s, performing at many institutions/venues in the UK (Bristol’s Arnolfini, London’s Science Museum, Tate Modern, The Roundhouse, ICA and Glasgow’s CCA), festivals worldwide and had installation work in numerous UK art spaces. He is a regular contributor to the London Improvisers Orchestra, as well as playing with Confront Recordings’ The Seen collective. His collaborators in recent years have included {{<artist "Blanca Regina">}}, {{<artist "Dominic Lash">}}, {{<artist "Martin Vishnick">}}, {{<artist "Crystabel Riley">}}, poet {{<artist "Tamar Yoseloff">}}, {{<artist "Olivia Moore">}}, {{<artist "Matt Atkins">}}, {{<artist "Lina Lapelyte">}}, {{<artist "Jem Finer">}}, {{<artist "Clive Bell">}}, {{<artist "John Edwards">}}, {{<artist "Jennifer Allum">}}, {{<artist "Sue Lynch">}}, sculptor {{<artist "Rob Olins">}} and many more.

---

### {{<artist "Ecka Mordecai">}} is a British artist based in London. Situated between sonic, performative and olfactory disciplines, her work is driven by sensation: entwining cello, horsehair harp, voice, eggflute, scent and improvisation into time-based objects expressive of emotional complexity.

Performing since 2010, Ecka has appeared alongside the likes of {{<artist "David Toop">}}, {{<artist "Malvern Brume">}}, {{<artist "Thurston Moore">}}, {{<artist "Keeley Forsyth">}}, {{<artist "Ilan Volkov">}}, Ex-Easter Island Head, {{<artist "Greta Buitkute">}}, {{<artist "Dave Birchall">}} and {{<artist "Kate Armitage">}}. She has played at Cafe OTO, BBC Glasgow, Islington Mill and inside a Berlin wasserturm, amongst others.

She has projects with Revox tape performer {{<artist "Valerio Tricoli">}} in the duo Mordecoli (The Addiction, Hedione 2022), and in the trio Circæa with {{<artist "Andrew Chalk">}} and {{<artist "Tom James Scott">}} (The Bridge of Dreams, Faraway Press, 2019).

---

Since his first experiences playing with legendary drummer {{<artist "John Stevens">}} in 1989, {{<artist "Roland Ramanan">}} has been a major presence on the London improvised music scene. Constantly exploring the boundaries between composition and improvisation as well as the sound possibilities of the trumpet, he has worked with {{<artist "Peter Brotzmann">}}, {{<artist "Eddie Prevost">}}, {{<artist "Evan Parker">}}, {{<artist "Simon Fell">}}, {{<artist "Marcio Mattos">}}, {{<artist "Alex Ward">}}, {{<artist "Alex Hawkins">}} and {{<artist "Tony Marsh">}}.

Ramanan has been a conductor and stalwart of the London Improvisers Orchestra since its inception.

He has two recordings on Emanem records (Shaken and Caesura); his latest record release and project is the free jazz tentet on Leo records. Ramanan also plays in the punk, funk, improv trio Vole - soon to be released on Babellabel.

---

{{<artist "Sylvia Hallett">}} is a multi-instrumentalist and composer moving between violin, Norwegian Hardanger fiddle, saw, accordion, electronics, bowed bicycle wheel, and bowed branches. Collaborators have included {{<artist "Chris Dowding">}} (sylvia & I), {{<artist "Delphine Dora">}}, {{<artist "Ansuman Biswas">}}, {{<artist "Mike Cooper">}}, {{<artist "David Toop">}}, {{<artist "Michael Ormiston">}}, {{<artist "Jah Wobble">}}, {{<artist "Anna Homler">}}, {{<artist "Clive Bell">}}, {{<artist "Colourscape">}}, {{<artist "The London Improvisers Orchestra">}}, {{<artist "The Heliocentrics">}}, {{<artist "LaXula">}}, {{<artist "Opera North">}}, {{<artist "h2dance">}}, {{<artist "Miranda Tufnell">}}, {{<artist "Eva Karczag">}}, {{<artist "Wonderful Beast Theatre Company">}}, and {{<artist "The Royal Shakespeare Company">}}. She has released six solo albums. Performances include a commission by the Livio Felluga Wine Company to create and perform a solo site-specific work on a hill in an Italian vineyard.


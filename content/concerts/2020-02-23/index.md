---
title: 15 March 2020
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2020-03-15T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
costs:
  - key: "Standard"
    value: "£8"
  - key: "Concessions"
    value: "£6"
artists:
  - "Sothiac"
  - "Pat Moonchy"
  - "Lucky Liguori"
  - "Paul Jolly"
  - "Adrian Northover"
  - "Neil Metcalfe"
  - "Elisa Ulian"
  - "Phil Durrant"
  - "Emil Karlsen"
sets:
  - - "Sothiac"
    - "Pat Moonchy"
    - "Lucky Liguori"
    - "Paul Jolly"
  - - "Adrian Northover"
    - "Neil Metcalfe"
    - "Elisa Ulian"
  - - "Phil Durrant"
    - "Emil Karlsen"
year: 2020
---
### {{<artist "Sothiac">}}

![Sothiac](https://www.mopomoso.com/wp-content/uploads/2020/02/Sothiac.png)

SOTHIAC are {{<artist "Pat Moonchy">}} (Voice) and {{<artist "Lucky Liguori">}} (Guitar/Gongs) Ft. special guest {{<artist "Paul Jolly">}} (reeds).

Based in London they have toured China, Europe and Japan. Their most recent albums are Sothis (2016) and Erebia Christi (2017) which blend elements of free jazz, landscape, industrial\_noise and psychedelic/doom. Over the last twenty years they have played with Faust, Sawada, Angelo Avogadri, Lino Liguori, Angelo Contini, Dirk Dhonau, Lars Nicolaysen.

### {{<artist "Adrian Northover">}} (Saxophone), {{<artist "Neil Metcalfe">}} (Flute) and {{<artist "Elisa Ulian">}} (Voice)

![Adrian Northoliver, Neil Metacalfe and Elisa Ulian](https://www.mopomoso.com/wp-content/uploads/2020/02/March-2020-Adrian-Neil-Elisa.png)

ADRIAN NORTHOVER is based in London, and plays alto and soprano saxophone, and is also involved in sound production.

He can currently be heard playing on the London club scene with a wide range of musicians, as well as doing solo saxophone performances.

He has made recordings with B Shops for the Poor, The Remote Viewers, Sonicphonics (with BillyBang), The London Improvisers Orchestra, Ensemble Trip-Tik, Anna Homler,John Edwards, The Custodians, and various duo CD’s with Adam Bohman,Tasos Stamou, Daniel Thompson and many others.He has performed at many international festivals, including the Montreal Jazz Festival, SoundSymposium, Victoriaville, Leipzig Jazz Festival, Tallin Festival, Arkangel Jazz, Freedom Of TheCity, Bari Jazz and others.

NEIL METCALFE has been active on London improvising scene since the 70’s.He has played with, amongst many others ;Evan Parker, Roscoe Mitchell, Paul Dunmall, Lol Coxhill and Tony Marsh and was a member of theSpontaneous Music Ensemble, considered by many to be the flagship European Improvising Ensemble.His current output includes Cds with Paul Dunmall, Alison Blunt, Hanna Marshall and the RuncibleQuintet.

ELISA ULIAN has been working for years in the vocal experimentation and research field.A graduate in piano, she was worked as a pianist accompanying singers, as avocalist, a choir master and vocal coach since she was young.In 2007 she began studies at the School of Modern Music in Lugano(Switzerland) attending the course ‘VocalSound’ held by Anna Bacchia, doingfurther research with her in vocal experimental work, new cognitive approachesand bioresonance. In the same year she strated to collaborate with the trio vocalgroup ‘Vocinconsuete’ with whom she made and recorded MILADISE projectinvolving vocal traditions, experimentation and improvisation.

### {{<artist "Phil Durrant">}} (Octave Mandola) and {{<artist "Emil Karlsen">}} (Drums)

![Phil Durrant and Emil Karlsen](https://www.mopomoso.com/wp-content/uploads/2020/02/Phil-and-Emil.png)

EMIL KARLSEN (b.1998), originally from Norway, is a young drummer/improviser currently based in Leeds, UK. He is active on the UK improvised music scene establishing himself as a new, original voice. In 2019 he released his solo record “Flux” followed by a UK tour. His playing highlights dynamics, timbre and textures, all done with an organic mindset. His work as improviser consists of doing acoustic solo performances, working in ensembles spanning from duos to octets and beyond, exploring graphic scores with an interest in audio visuals using both digital and analogue medias, all done with a focus on creating boundary-pushing music. His mentality of exploring the sonic possibilities of the drum kit is brought into all of his projects forming his unique expression. He is seen across the UK with various large and small ensembles ranging from being a solo performer to percussionist in the London Improvisers Orchestra. Apart from being a busy performer, he also run the label noumenon – a label for improvised music with a Scandinavian presence

PHIL DURRANT is a multi-instrumentalist improviser/composer/sound artist who currently performs solo and group concerts.As a violinist (and member of the Butcher/Russell/Durrant trio), he was one of the key exponents of the “group voice approach” style of improvised music. In the late 90s, his trio with Radu Malfatti and Thomas Lehn represented a shift to a more “reductionist” approach.Recently, he has been performing solo and duo concerts with Phil Maguire, Bill Thompson, Mark Sanders, Lee Patterson and drone concerts with Mark Wastell, Phi Julian, Rhrodri Davies, Bill Thompson, using an analogue/digital modular synthesizer system. As a mandolinist, he has been performing with guitarist Martin Vishnick and a quartet with Sue Lynch, Hutch Demouilpied and Dave Fowler.Durrant still performs regularly with the acoustic/electronic group Trio Sowari (with Bertrand Denzler and Burkhard Beins) and Mark Wastell’s The SEEN.

---
title: 19 June 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-06-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: 25th Anniversary
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Faith Brackenbury"
  - "Martin Speake"
  - "Alex Maguire"
  - "Fang-Yi Liu"
  - "John Russell"
sets:
  - - Faith Brackenbury
    - Martin Speake
  - - Alex Maguire
  - - Fang-Yi Liu
    - John Russell
year: 2016
---

Our 25th anniversary series continues with a close look at solo and duo playing

### {{<artist "Faith Brackenbury">}} (violin) & {{<artist "Martin Speake">}} (sax)

![faith and martin jjj](https://www.mopomoso.com/wp-content/uploads/2016/05/faith-and-martin-jjj-1024x527.jpg)

Faith and Martin have backgrounds in jazz, folk and classical music and share a delight in improvising. Martin has been described as ‘one of the most interesting and rewarding alto saxophonists now playing jazz on any continent.” (Thomas Conrad – Jazz Times) and together with Faith their music takes the listener on a lyrical journey of timeless melodic invention.

### {{<artist "Alex Maguire">}} (piano)

![alex 2 jjj](https://www.mopomoso.com/wp-content/uploads/2016/05/alex-2-jjj.jpg)

Maguire is interested in ‘pre-literate music, anything original rather than imitative (irrespective of idiom)’. He has studied with classical composers John Cage and Michael Finnissy and his arsenal of techniques and figures, stretch from avant garde classical piano to jazz and from kwela to R&amp;B. One of the UK’s true originals in a rare solo appearance.

### {{<artist "Fang-Yi Liu">}} (voice) & {{<artist "John Russell">}} (guitar)

![jr fan yi jjj](https://www.mopomoso.com/wp-content/uploads/2016/05/jr-fan-yi-jjj.jpg)

Based in Kaohsiung the southern Taiwan, Liu’s work concentrates on tonal improvisation and field recordings. His performances are mostly based on the use of varied materials and methods to explore the boundaries of expression of human voice and environment sound. He is also the founder of Cochlea, an online platform and group for promoting sound art and experimental music. He has released two CD albums, Koujie- Koujie and Bathroom Meteorology, in 2014 and 2015. Here he teams up with renowned UK guitarist Russell for a set of close intuitive interplay.

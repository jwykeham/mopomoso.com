---
title: "17 November 2024"
date: 2024-04-24T10:59:33+01:00
doors: 2pm
draft: true
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2024-11-17 00:00:00 +0000 GMT
concert_series: Mopomoso Live
year: 2024
---

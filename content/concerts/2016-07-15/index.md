---
title: 21 - 23 August 2016
categories:
  - Fête Quaqua 2016
date: "1970-01-01T00:00:00Z"
concert_date: 2016-08-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors:
  - 2016-08-21T14:00:00+01:00
  - 2016-08-22T20:00:00+01:00
  - 2016-08-23T20:00:00+01:00
concert_series: Fête Quaqua 2016
costs:
  - key: Single Concert
    value: £8 / £6 concessions
  - key: Three day pass
    value: £20 / £15 concessions
artists:
  - "Ståle Liavik Solberg"
  - "Adam Linson"
  - "Paul G. Smyth"
  - "John Russell"
  - "Alice Eldridge"
  - "Stefan Keune"
  - "Julia Kjaer"
  - "Chris Burn"
  - "Martin Mayes"
  - "Edward Lucas"
  - "Charlotte Hug"
  - "Mia Zabelka"
  - "Ute Wassermann"
  - "Maggie Nicols"
  - "Lawrence Casserley"
year: 2016
---

# Fête Quaqua 2016

Both the audience and the musicians share in the discovery of a music as it happens, making for a uniquely rewarding experience. An international cast of musicians will be playing in a variety of different combinations, which won’t be repeated, ensuring that no two shows will be the same.

- {{<artist "Ståle Liavik Solberg">}} (drums/percussion)
- {{<artist "Adam Linson">}} (bass, electronics)
- {{<artist "Paul G. Smyth">}} (piano)
- {{<artist "John Russell">}} (guitar)
- {{<artist "Alice Eldridge">}} (cello, feedback cello)
- {{<artist "Stefan Keune">}} (saxophone)
- {{<artist "Julia Kjaer">}} (saxophone, flute)
- {{<artist "Chris Burn">}} (trumpet, piano, electronics)
- {{<artist "Martin Mayes">}} (french horn, alphorn)
- {{<artist "Edward Lucas">}} (trombone)
- {{<artist "Charlotte Hug">}} (viola, voice)
- {{<artist "Mia Zabelka">}} (violin, voice)
- {{<artist "Ute Wassermann">}} (voice & birdcall whistles/resonating objects)
- {{<artist "Maggie Nicols">}} (voice)
- {{<artist "Lawrence Casserley">}} (electronics)

![fete quaqua 2016 final grab j](https://www.mopomoso.com/wp-content/uploads/2016/07/fete-quaqua-2016-final-grab-j.jpg "full width")

## ABOUT THE MUSICIANS

### {{<artist "Ståle Liavik Solberg">}} (drums/percussion)

Ståle Liavik Solberg has made a base for himself within Oslo’s thriving improvised music scene where he plays a central role as one of the driving forces behind the series Blow Out! and co-curating the festival with the same name with fellow drummer / percussionist Paal Nilssen-Love.. He plays with ensembles VCDC, Will it float? (with John Russell, Steve Beresford &amp; John Edwards), Silva-Rasmussen-Solberg trio and in duos with Fred Lonberg-Holm and John Russell. His open and attentive drumming style has garnered many positive reactions from musicians and audiences alike in both Europe and the USA.

### {{<artist "Adam Linson">}} (bass, electronics)

Adam Linson is active internationally as a double bassist, improvisor, and composer, who performs acoustically and with live electronics, solo and in a wide variety of ensembles. He can be heard on several critically acclaimed albums, which also feature the real-time interactive computer music systems that he designs and develops. He is also a scholar whose interdisciplinary research on improvisation uniquely combines the arts, humanities, sciences and technology. His publications focus on perception, cognition, and interaction, and span a range of topics in philosophy, cognitive science, music psychology, and artificial intelligence/robotics.

### {{<artist "Paul G. Smyth">}} (piano)

Paul G. Smyth is a musician, visual artist and designer, based in Dublin. A member of The Jimmy Cake (described by the Irish Times as “the most powerful musical force in Ireland”), Smyth is also Ireland’s foremost free-improvising pianist. Since 1999, he has worked with Charles Gayle, Derek Bailey, Evan Parker, Peter Brötzmann, Keiji Haino, Barry Guy, Wadada Leo Smith, John Russell, John Edwards, Chris Corsano, Lol Coxhill, Charles Hayward, John Butcher, Hannah Marshall, Mark Sanders and Damo Suzuki, among many others, been a member of synth trio Boys Of Summer (“the sound of John Carpenter being buried alive” – Le Cool Magazine), a featured composer in the National Concert Hall, written music for theatre, and performed in 17 countries.

### {{<artist "John Russell">}} (guitar)

A leading figure on the UK improivised music scene and a highly respected guitarist John Russell first started events under the Quaqua banner thirty five years ago. He is greatly looking forward to this anniversary event which coincidentally also marks fifty years since he began playing the guitar and twenty five years of promoting freely improvised music as Mopmoso.

### {{<artist "Alice Eldridge">}} (cello, feedback cello)

Alice Eldridge is a Research Fellow in Digital Humanities and Digital Performance at the Sussex Humanities Lab, Department of Music, University of Sussex and a Member of the MIPTL – School of Music, Film and Media. Her current research includes collaborations with conservation biologists investigating the potential for acoustic approaches to biodiversity assessment and with musicians to explore the use of networking technologies in ensemble music making. She also performs and composes with the group Collectress and has taken part in a number of Quaqua projects in the past.

### {{<artist "Stefan Keune">}} (saxophone)

In 1982 Stefan began studying classical technique with private teachers moving quickly to free improvisation before playing for a time in a trio with Martin Blume and, in the early 1990s, in a quartet with Dietmar Diesner, Matthias Bauer and Paul Lytton. In 1991 he formed his first group as leader, the Stefan Keune Trio, with bassist Hans Schneider and drummer Paul Lytton, recording the album ‘Loft’. In 1993 he began working with British guitarist John Russell , forming the quartet ‘Beware of Art’ (with Lovens and Schneider) and from 1997 they began duo performances, releasing two CDs and touring Japan. In 1997 he co-ounded with Schneider, Lytton, Thomas Lehn , Gunda Gottschalk , Melvyn Poore , Mark Charig , Erhard Hirt , Ute Voelker and Philippe Micol the ensemble ‘Real Time’. In addition Stefan has played with many musicians of the British free improvisation scene including Roger Turner, Phil Durrant, John Butcher, Dominic Lash and Steve Noble and with fellow Europeans Mats Gustafsson, Raymond Strid, Radu Malfatti and Peter Kowald.

### {{<artist "Julia Kjaer">}} (saxophone, flute)

Julie has toured internationally and recorded with Django Bates and StoRMChaser. Currently her main focus is on her trio, Julie Kjær 3, with bass player John Edwards and drummer Steve Noble with a debut album ‘Dobbeltgænger’ being released earleir this year on Clean Feed Records. She tours internationally with Norwegian drummer Paal Nilssen-Love and his Large Unit, plays with London Improvisers Orchestra and is a leader and side woman of several other English and Danish ensembles. In 2014 Julie was chosen to be a Sound and Music “New Voice” Artist and was chosen as a featured composer by the British Music Collection. She associates herself with prized performers like Mark Sanders, Dave Douglas, Louis Moholo-Moholo, John Russel, Dave Liebman, Laura Jurd, ‘Leafcutter’ John, Mira Calix, Veryan Weston and Steve Beresford.

### {{<artist "Chris Burn">}} (trumpet, piano, electronics)

Although known mostly as a pianist of some repute, Chris Burn began playing trumpet at the age of 6. In many of his earlier projects, such as the Chris Burn Group, he did so as both pianist and trumpeter and throughout the 1980s and ’90s he continued to perform on trumpet albeit on an occasional basis. A regular duo with Matt Hutchinson on electronics and synthesiser, encouraged him to bring the instrument to the fore and he participated as a trumpeter in early rehearsals of the London Improvisers Orchestra, taking part in the recording of their first CD ‘Proceedings’. He now works predominantly as a composer, improviser and pianist who specialises in new and extended techniques including electronics.

### {{<artist "Martin Mayes">}} (french horn, alphorn)

Resident in Turin, Martin Mayes started his career on the London experimental music scene of the seventies, where he was a founder member of the London Musicians’ Collective as well as a performer with street theatre and visual arts groups. He has played and recorded with the European improvising orchestras of Radu Malfatti, Franz Koglmann, Giancarlo Schiaffini, Hannes Zerbe, Cecil Taylor and the Italian Instabile Orchestra. His projects, which are often site-specific, range from medieval music and the traditions of the French trompe de chasse to radical improvisation.His music has taken him from mountain tops to underground caves, stone quarries and the sea; from village gardens to ancient cave churches; from abandonded buildings to royal palaces. As a workshop leader he has created and conducted workshops and projects with all types of people of all ages, from 5-year-old children to music college students, from dancers to diabetology medical professionals.

### {{<artist "Edward Lucas">}} (trombone)

Edward began palying the trombone in school, coming to improvisation later in life as a way of getting back into playing after a substantial break. Now almost exclusively what he does, his work has included groupings together with Tom Wheatley, Daniel Thompson, Eddie Prévost, Roland Ramanan, and James O?Sullivan, among many others. He also has a long running duo with Slovakian analogue synthesist Daniel Kordik, a collaboration that extends to running the Earshots concert series and label, which they have been developing over the last three years.

### {{<artist "Charlotte Hug">}} (viola, voice)

Charlotte is known for her musical-visual solo performances at specific locations performing for instance in the tunnels of the Rhône glacier, a subterranean former prison in London, in the semi-demolished bunker Humboldthain Berlin, the hot, healing springs of Baden or the Dockyard in Coph Ireland. With an instrument built in 1763 by the Viennese violin maker JG Thir, from the year 1763, using ‘soft bow’technique’ and specialising in combining the sounds of viola and voice the result is an unmistakable and distinct tonal language. Her life is spent between her home of Zurich and continual touring around the world.

### {{<artist "Mia Zabelka">}} (violin, voice)

Mia Zabelka is an Austrian contemporary classical composer as well as an improvising violinist and vocalist. She has collaborated with a string of musicians from around the world and her electric groups include Mia Zabelka / Gavino Canu, Medusa’s Bed with Lydia Lunch &amp; Zahra Mani, ASTMA, Zabelka /Petit and Redshift Orchestra. Acoustically she works as a soloist and withTrio Blurb (w.Maggie Nicols/ John Russell) duos Gino Robair, Audrey Lauro and Nicola Hein and groups ALLIGATOR and the ZZM Trio. She also curates KlangHaus Untergreith and Phonofemme in Austria.

### {{<artist "Ute Wassermann">}} (voice & birdcall whistles/resonating objects)

Ute Wassermann, composer, performer, soundartist, improvisor is known for her extraordinary, many-voiced and extreme vocal sound-language, which she has brought into experimental/contemporary music in diverse ways. She has developed unique techniques to “mask” the voice using bird whistles, palate whistles, speaker objects, resonators. She is interested in the area between composition and improvisation, music and performance art as well as in free improvisation. She has performed as a vocal soloist in festivals, galleries and clubs throughout Europe, Australia and Asia. Premieres of numerous works by composers especially written for her voice. Performances with different bands and adhoc with musicians like Birgit Ulher, Phil Minton, Thomas Lehn, Martin Blume, John Russel, Raed Yassin, Les Femmes Savantes.

### {{<artist "Maggie Nicols">}} (voice)

Maggie is a totally original vocalist. At the age of fifteen she left school and started to work as a dancer at the Windmill Theatre before her first singing enaggement a year later. In 1968 she joined the legendary Spontaneous Music Ensemble and in the early 70´s she continued to work with such people as Keith Tippett, Robert Wyatt, Johnnny Dyani, Alan Skidmore, Julie Tippetts and John Stevens. She founded the Feminist Improvising Group with Lindsay Cooper and is currently working in ‘Les Diaboliques’ with Irene Schweizer and Joelle Leandre. She now lives in Wales and continues performing and recording challenging and beautiful work, in music and theatre.

### {{<artist "Lawrence Casserley">}} (electronics)

Best known for his work in free improvised music, particularly real-time processing of other musicians’ sound Lawrence was one of the first students of Electronic Music at the Royal College of Music where he later became Professor-in-Charge of Studios and Adviser for Electroacoustic Music. He has worked with many fine improvisers, particularly Evan Parker, with whom he works as a duo partner, in various larger groupings and in the Evan Parker Electro-Acoustic Ensemble. He also works as a soloist, processing sounds from voice, percussion and home-made instruments. His is an “instrumental” approach to live computer sound processing; the Signal Processing Instrument allowing him to use physical gestures to control processing and direct the morphology of the sounds.

## Quaqua 35th Anniversary

Originally used as a name for a weekly club for improvised music begun in 1981 and quickly evolved into its present shifting format. The basic idea behind most Quaqua projects is to extend existing collaborations / relationships between musicans in juxtaposition with new ones and so provide a fertile ground for free improvisation.

An extension of the way a number of us had been working at the Little Theatre Club and the London Musicians’ Co-Op concerts at the Unity Theatre from around 1973 onwards, where personnel changed from concert to concert from a pool of musicians with different approaches, who shared a love of free improvisation this was a major influence on Derek Bailey’s Company groups.

![fetequaqua1982](https://www.mopomoso.com/wp-content/uploads/2016/07/fetequaqua1982.jpg)

‘Quaquaversality’ means to point in all directions and the name ‘Quaqua’ is Latin for ‘whithersoever’ which seems a suitable ‘catch all’ term for the adventures of free improvisation.

---
title: 18 October 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-10-18T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "John Butcher"
  - "Emilio Gordoa"
  - "Nicola Hein"
  - "Sture Ericson"
  - "John Russell"
  - "Raymond Strid"
sets:
  - - John Butcher
    - Emilio Gordoa
    - Nicola Hein
  - - Sture Ericson
    - John Russell
    - Raymond Strid
year: 2015
---

Continuing to present the very finest of musical free improvisation this month’s Mopomoso concert is a first rate international double bill with musicians from Argentina, Germany, Sweden and the UK. Engaging music from the UK’s longest running series dedicated to free improvisation, now approaching our 25th year.

### {{<artist "John Butcher">}} (saxophone), {{<artist "Emilio Gordoa">}} (vibes) & {{<artist "Nicola Hein">}} (guitar)

{{<youtube fHylyftsYRw>}}

All three of these highly respected musicians have expressed an interest in form and texture as important elements in their music and each has their own view on using them. Coupled with an openness to new thoughts and ideas, as a trio, this juxtaposition should produce something both delightful and unexpected. A debut performance and a rare chance to enjoy cutting edge music at its best.

<https://nicolahein.com/>
<https://www.johnbutcher.org.uk/>
<https://emiliogordoa.tumblr.com/>

### {{<artist "Sture Ericson">}} (saxophone), {{<artist "John Russell">}} (guitar) & {{<artist "Raymond Strid">}} (drums)

{{<youtube 1e4QHSiD4W0>}}

Having worked with Strid and saxophonist Mats Gustafsson and Ericsson with drummer Stale Liavik Solberg this is, for Russell, either a change of drummer or a change of saxophonist to make this trio debut. Either way we can expect a decidedly no holds barred set with all three musicians employing the full range of their respective instruments and revelling in sharing the moment. Don’t miss it!

<https://www.ilkmusic.com/artist/artist/sture-ericson>
<https://www.efi.group.shef.ac.uk/musician/mstrid.html>
<https://www.john-russell.co.uk/>

---
title: 15 July 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-07-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Marjolaine Charbin"
  - "Frans Van Isacker"
  - "Alan Tomlinson"
  - "Dave Tucker"
  - "Phil Marks"
  - "John Russell"
  - "Phil Wachsmann"
sets:
  - - Marjolaine Charbin
    - Frans Van Isacker
  - - Alan Tomlinson
    - Dave Tucker
    - Phil Marks
  - - John Russell
    - Phil Wachsmann
year: 2012
---

### Kryscraft - {{<artist "Marjolaine Charbin">}} (piano) & {{<artist "Frans Van Isacker">}} (sax)

{{<youtube SSLh3FPyz7I>}}

### Alan Tomlinson trio - {{<artist "Alan Tomlinson">}} (trombone), {{<artist "Dave Tucker">}} (guitar) & {{<artist "Phil Marks">}} (drums)

{{<youtube 27nFp81tENc>}}
{{<youtube xE9ZH8sFmaY>}}

### {{<artist "John Russell">}} (guitar) & {{<artist "Phil Wachsmann">}} (violin) duo

{{<youtube z_tUjnDG9zI>}}
{{<youtube XBdj8MabseM>}}

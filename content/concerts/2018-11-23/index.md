---
title: 16 December 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-12-16T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2018
---
Mopomoso presents our end of year
**Christmas Party**

Every year Mopomoso hosts a variety of musicians from across the free improvised music scene to come and play in a series of five to ten minute cameo sets. It is a wonderful way to catch up with old favourites and to check out new players in a relaxed and friendly atmosphere. As well as the music there will be refreshments and a raffle. You too are welcome to bring some food to share.
Let’s celebrate another good year of making great music together.

Of course there will be some silly stuff as well!
Here’s Steve Beresford, Kay Grant, {{<artist "John Russell">}} and Roger Turner playing toy laptops at our party in 2007

{{<youtube bWsJqk4Onv8>}}

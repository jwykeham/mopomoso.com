---
title: 20 - 22 August 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-08-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors:
  - 2017-08-20T14:00:00+01:00
  - 2017-08-21T20:00:00+01:00
  - 2017-08-22T20:00:00+01:00
concert_series: Fête Quaqua
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
  - key: Three Day Pass
    value: "£20 / £15 Concessions"
artists:
  - "Alice Eldridge"
  - "Satoko Fukuda"
  - "Matt Hutchinson"
  - "Ken Ikeda"
  - "Paul Jolly"
  - "Charlotte Keeffe"
  - "Henry Lowther"
  - "Pascal Marzan"
  - "Rachel Musson"
  - "John Russell"
  - "Gina Southgate"
  - "Roger Turner"
  - "Jennifer Walshe"
  - "Alex Ward"
year: 2017
---

# Fête Quaqua 2017

![fete quaqua 2017 group pic](https://www.mopomoso.com/wp-content/uploads/2017/07/fete-quaqua-2017-group-pic.jpeg)

Three days of improvised music with 14 world class musicians. The Sunday concert is at 2.00 pm and the Monday and Tuesday concerts both start at 8.00 pm. A three day pass is £20 or £15 concessions and individual concerts are £8 or £6 concessions. Tickets purchases / reservations from The Vortex or from the door on the day.

Every year the guitarist John Russell puts together a project involving a group of highly creative and individual musicians to work together, sometimes for the first time. The idea is to provide a fertile ground for free improvisation, strengthening existing ties and making new ones.

There are three concerts, each starting and finishing with the whole ensemble. In between there will be smaller ensembles taken from the larger group and every night the performers will play at least once in one of the smaller groups. Each concert will have a different programme and nothing is repeated. The idea being that the whole event can be seen as a single piece of music that offers a glimpse into the creative potential of the individual participants within the ensemble.

### The musicians taking part are
- {{<artist "Alice Eldridge">}} (feedback cello)
- {{<artist "Satoko Fukuda">}} (violin)
- {{<artist "Matt Hutchinson">}} (digital synthesiser/piano)
- {{<artist "Ken Ikeda">}} (electronics)
- {{<artist "Paul Jolly">}} (sax/reeds)
- {{<artist "Charlotte Keeffe">}} (trumpet/flugel horn)
- {{<artist "Henry Lowther">}} (trumpet/violin)
- {{<artist "Pascal Marzan">}} (guitar)
- {{<artist "Rachel Musson">}} (sax)
- {{<artist "John Russell">}} (guitar)
- {{<artist "Gina Southgate">}} (bric a brac)
- {{<artist "Roger Turner">}} (percussion)
- {{<artist "Jennifer Walshe">}} (voice)
- {{<artist "Alex Ward">}} (clarinet/guitar)

~~More details on [John Russell’s website](https://www.john-russell.co.uk/2017/07/17/fete-quaqua-2017-and-other-things/)~~

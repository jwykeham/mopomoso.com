---
title: 16 May 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-05-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Angel Faraldo"
  - "Yolanda Uriz"
  - "Ricardo Tejero"
  - "Will Connor"
  - "Elisabeth Harnik"
  - "Nicola Guazzaloca"
  - "Hannah Marshall"
  - "Gianni Mimmo"
  - "Tony Marsh"
  - "John Russell"
sets:
  - - Angel Faraldo
    - Yolanda Uriz
    - Ricardo Tejero
    - Will Connor
  - - Elisabeth Harnik
  - - Nicola Guazzaloca
    - Hannah Marshall
    - Gianni Mimmo
  - - Tony Marsh
    - John Russell
year: 2010
---
### WARY - {{<artist "Angel Faraldo">}} (computer) / {{<artist "Yolanda Uriz">}} (flute) / {{<artist "Ricardo Tejero">}} (reeds) / {{<artist "Will Connor">}} (percussion)

Cutting edge computer powerhouse Angel Faraldo’s latest project brings together sensitive flute, angular melodic reeds and textural percussion in a far reaching sonic landscape from this intriguing UK and Netherlands based quartet, here to record their first CD.

{{<youtube -JKwBKS839Y>}} 

{{<youtube CL9pAAJMea0>}} 

### {{<artist "Elisabeth Harnik">}} solo piano

Austrian pianist making a debut appearance for Mopomoso, Elisabeth has been awarded many prizes for her compelling work, with festival appearances as far afield as Europe, the USA and China.  

>“Harnik makes sound sculptures in wire. They flex, sometimes break, change with the light, but they always seem to work.” (Brian Morton, THE WIRE 06)

{{<youtube DQKrUEkbPRs>}} 

### {{<artist "Nicola Guazzaloca">}} (piano) / {{<artist "Hannah Marshall">}} (cello) / {{<artist "Gianni Mimmo">}} (soprano saxophone)

Guazzaloca’s ever intelligent and inventive piano playing with fellow Italian, Gianni Mimmo’s lyrical and deeply rooted saxophone are joined by the remarkable Hannah Marshall in this, the trio’s second UK visit. A music with a multiplexity of colour and line, rhythmic surprise and engaging warmth.

{{<youtube PO0WNXOUCqo>}} 

{{<youtube SnQlCKrjRK8>}} 

### {{<artist "Tony Marsh">}} (drums) / {{<artist "John Russell">}} guitar

Tony Marsh is rightly regarded as one of the UK’s leading drummers, having appeared with many of the finest musicians around. His continuing exploration into drums and the art of drumming, matched with an extremely acute ear and an ability to ‘get inside the music’ is matched by a constant inventiveness. He is joined by Mopomoso host, guitarist John Russell for this debut duo performance.

{{<youtube mkWBROvKHoc>}} 

{{<youtube QnbA_XCJ_I8>}} 

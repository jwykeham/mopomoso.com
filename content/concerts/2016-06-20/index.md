---
title: 17 July 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-07-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Terry Day"
  - "David Panton"
  - "Paul G Smyth"
  - "Jennifer Walshe"
  - "John Russell"
  - "Roger Turner"
sets:
  - - Terry Day
    - David Panton
  - - Paul G Smyth
    - Jennifer Walshe
  - - John Russell
    - Roger Turner
year: 2016
---

### {{<artist "Terry Day">}} (drums) & {{<artist "David Panton">}} (saxophone / piano)

![dave panton and terry day](https://www.mopomoso.com/wp-content/uploads/2016/06/dave-and-terry-day.jpg)

Two highly motivated and individual musicians who first met on the emergent free music scene in the early seventies. Over the  
years Terry and David have played with an international Who’s Who of improvised music whilst constantly developing and nurturing their own unique work. To much acclaim they recently renewed their acquaintance at the Mopomoso end of year show and we are delighted that they have returned to play a full set. Expect lots of energy and invention from two absolute originals. Not to be missed!

### {{<artist "Paul G Smyth">}} (piano) & {{<artist "Jennifer Walshe">}} (voice)

![paul g smyth and jennifer walshe](https://www.mopomoso.com/wp-content/uploads/2016/06/paul-and-jennifer.jpg)

Hailing from Dublin, The Goatstown Improvisers Orchestra is the smallest such orchestra in the world, comprising only two members! Paul is a favourite of Mopomoso audiences, having played here a number of times now, both as a soloist and also as part of various Quaqua groups. A fine musician his playing is full of twists and turns and always has something unexpected in it. Jennifer is a composer , vocalist and multimedia artist of rare wit and talent whose work is performed, and who performs herself, around the world. This should be a far reaching roller coaster of a set, from darkly brooding depths to humorous interchanges.

### {{<artist "John Russell">}} (guitar) & {{<artist "Roger Turner">}} (percussion)

![john russell and roger turner](https://www.mopomoso.com/wp-content/uploads/2016/06/john-and-roger-1024x576.jpg)

An uncompromising and much loved duo, John and Roger have an instant rapport built upon years of playing together. Their music takes all aspects of sound production as a starting point as they engage the audience in a journey of discovery that will never be repeated. Always new and always invigorating – one of improvised music’s great pairings. If you know their music you will want to come anyway and if you don’t know it then you really should come and see what you have been missing!

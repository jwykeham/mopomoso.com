---
title: 15 April 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-04-15T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Matt Hutchinson"
  - "Dave Solomon"
  - "Viv Corringham"
  - "Alison Blunt"
  - "Pascal Marzan"
  - "John Russell"
sets:
  - - Matt Hutchinson
    - Dave Solomon
  - - Viv Corringham
    - Alison Blunt
  - - Pascal Marzan
    - John Russell
year: 2012
---

### {{<artist "Matt Hutchinson">}} (piano) / {{<artist "Dave Solomon">}} (drums)

{{<youtube 7UIaeNCUfOU>}}

### {{<artist "Viv Corringham">}} (voice) / {{<artist "Alison Blunt">}} (violin)

{{<youtube Jd55q84wVnY>}}

### {{<artist "Pascal Marzan">}} (guitar) / {{<artist "John Russell">}} (guitar)

{{<youtube 9yk3PSR3AFc>}}

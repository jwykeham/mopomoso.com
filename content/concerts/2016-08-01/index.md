---
title: 17 October 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-10-17T20:00:00+01:00
venue: Sevilla Mia, 22 Hanway St, London, W1T 1UQ
doors: 8pm
concert_series: Workshop Concerts
costs:
  - key: Full Price
    value: £4
artists:
  - "Mopomoso workshop group"
  - "Natalie Sandtorv"
  - "Ole Mofjell"
  - "Per Gardin"
year: 2016
---

After the success of the first in our series of workshop concerts featuring the {{<artist "Mopomoso workshop group">}} with the duo of {{<artist "Natalie Sandtorv">}} (voice) and {{<artist "Ole Mofjell">}} (drums) the group welcomes saxophonist {{<artist "Per Gardin">}} to join in the fun. 

The residents will play the first set followed by a set from Per and then they will play together.

![mopomoso-sept-192](https://www.mopomoso.com/wp-content/uploads/2016/08/mopomoso-sept-192-1024x768.jpg)


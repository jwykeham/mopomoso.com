---
title: 21 August 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-08-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 8pm
concert_series: Fête Quaqua
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2012
---
# Fête Quaqua Day Three

{{<youtube chEXCBSUZTE>}}
{{<youtube Mtrvxb0PzRs>}}
{{<youtube JhDn9Hm9c-c>}}
{{<youtube 8RjUyc2vWpM>}}
{{<youtube E_g0Lp2YL98>}}
{{<youtube F3FllthVqR4>}}
{{<youtube ypJd2PEQaAA>}}
{{<youtube 2AMZoeaReVI>}}

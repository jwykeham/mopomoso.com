---
title: 21 January 2018
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2018-01-21T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Jim Dvorak"
  - "Harrison Smith"
  - "Joshua Weitzel"
  - "Beibei Wang"
  - "Steve Beresford"
  - "John Russell"
sets:
  - - Jim Dvorak
    - Harrison Smith
  - - Joshua Weitzel
    - Beibei Wang
  - - Steve Beresford
    - John Russell
year: 2018
---
### {{<artist "Jim Dvorak">}} & {{<artist "Harrison Smith">}}

![Harrison Smith and Jim Dvorak ](https://www.mopomoso.com/wp-content/uploads/2018/01/Harrison-and-Jim-grab.jpeg)

A long established and highly respected partnership, Harrison Smith’s first public outings were with the Herrington Colliery band in Durham, leading to later experiences in dance bands and soul/rock groups around the North East of England, whereas Jim Dvorak (great nephew of the composer Antonin) hails from Brooklyn, where he took private tuition, before graduating from the prestigious Eastman school of music. From such different beginnings, and following a shared interest in improvising, the two musicians ended up in London in the late sixties / early seventies as collaborators in some of the leading groups of the time. The strength of their shared musical bond over such a time means that they can play with a closeness and understanding that is only to be found amongst old friends. A great opportunity to experience two excellent musicians enjoying the fun of making music together.

### {{<artist "Joshua Weitzel">}} & {{<artist "Beibei Wang">}}

![Joshua Weitzel and Beibei Wang](https://www.mopomoso.com/wp-content/uploads/2018/01/Joshua-and-Beibei.jpeg)

Guitarist and shamisen player Joshua Weitzel is from Kassel, Germany and works mostly in the field of free improvisation, experimental composition and contemporary Jazz. His focus is on the use of different materials and dynamics while contrasting and juxtaposing musical parameters. He has played in Europe and Asia with a wide range of contrasting artists including: Akiyama Tetuzi, Christian Wolff, Toshimaru Nakamura, Matthias Schubert, Eddie Prévost, Hayashi Eiichi, Alfred 23 Harth, Haco and Nicola Hein. For this concert he is joined by Beibei Wang an acclaimed virtuoso percussionist who has enjoyed a meteoric rise in the classical music world. Following double Master Degrees from the Central Conservatory of Music (China) and the Royal Academy of Music (UK) in 2014 she was listed in the top 50 Chinese musicians in the “Sound of East” project by the Chinese Ministry of Culture. In 2015, endorsed as an Exceptional Talent by the Arts Council, England and following a successful recital at SOAS, University of London she started teaching traditional Chinese percussion there. She was recently featured on the cover of Music Life magazine in China in 2017.

### {{<artist "Steve Beresford">}} & {{<artist "John Russell">}}

![Steve Beresford and John Russell](https://www.mopomoso.com/wp-content/uploads/2018/01/steve-and-john.jpeg)

‘Teatime’ an Incus LP released in 1975 was, in retrospect, a very influential recording in the world of free improvisation, giving rise to some aspects of Derek Bailey’s Company projects whilst also providing a spur for Davey Williams and LaDonna Smith’s Trans Museq in Alabama and Eugene Chadbourne who said, ‘When I heard that record I thought, ‘If those guys can get away with it then so can I.’ Since then two of the musicians from that record Steve Beresford and John Russell have not only continued to maintain and develop their playing within the free improvisation world but have continued to work together in a number of projects; the most recent being a quartet – ‘Will it Float?’ (with drummer/percussionist Stale Liavik Solberg and bassist John Edwards). Their eponymous first vinyl LP being followed by a CD ‘The Sorter’ in 2017. Both ‘Teatime’ and ‘The Sorter’ will be for sale at the event. The continuing musical dialogue from these two musicians will be happening on the day. Don’t miss it!

***
Mopomoso is now in its 28th year and is run solely on a voluntary basis. The name stands for MOdernism POst MOdernism SO what? and its aims are to promote and nurture understanding of freely improvised music making in all its forms and on an international, national, regional and local basis through concerts and workshops involving both established and emerging players and listeners.

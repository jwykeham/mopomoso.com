---
artists:
  - John Butcher
  - Steve Beresford
  - Fabienne Audeoud
concert_date: 1998-10-18T20:00:00Z
concert_series: At The Red Rose
date: 1998-10-18T20:00:00Z
notes: []
sets: []
sources:
  - https://www.newspapers.com/image/260658597
title: 18 October 1998
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1998
---
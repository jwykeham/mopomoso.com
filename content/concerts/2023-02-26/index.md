---
title: "26 February 2023"
date: 2023-07-09T13:17:17+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2023-02-26T00:00:00Z
concert_series: Afternoon Sessions
artists:
  - "Gina Southgate"
  - "Charles Hayward"
  - "Aurelie Freoua"
  - "Terry Day"
  - "Gwendolyn Kassenaar"
  - "Meg Morley"
  - "Hannah Marshall"
sets:
  - - "Gina Southgate"
    - "Charles Hayward"
  - - "Aurelie Freoua"
    - "Terry Day"
  - - "Gwendolyn Kassenaar"
    - "Meg Morley"
    - "Hannah Marshall"
year: 2023
---
### {{<artist "Gina Southgate">}} & {{<artist "Charles Hayward">}} (percussion)

### {{<artist "Aurelie Freoua">}} & {{<artist "Terry Day">}} 

### {{<artist "Gwendolyn Kassenaar">}}, {{<artist "Meg Morley">}} (piano) & {{<artist "Hannah Marshall">}} (cello)


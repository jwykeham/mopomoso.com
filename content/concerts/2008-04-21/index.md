---
title: 20 April 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-04-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Furt Richard Barrett"
  - "Paul Obermayer"
  - "Sarah Gail Brand"
  - "Mark Sanders"
  - "John Russell"
  - "John Butcher"
sets:
  - - "Furt Richard Barrett"
    - "Paul Obermayer"
  - - "Sarah Gail Brand"
    - "Mark Sanders"
  - - "John Russell"
    - "John Butcher"
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

### {{<artist "Furt Richard Barrett">}} & {{<artist "Paul Obermayer">}}

{{< youtube -5eI9g5jTNg >}}

### {{<artist "Sarah Gail Brand">}} & {{<artist "Mark Sanders">}}

{{< youtube HFHTKMUMLBE >}}

### {{<artist "John Russell">}} & {{<artist "John Butcher">}}

{{< youtube AJeYGnmQYRg >}}


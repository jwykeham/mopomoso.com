---
title: 15 January 2017
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2017-01-15T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Martin Hackett"
  - "Daniel Thompson"
  - "Phil Wachsmann"
  - "The Stellari String Quartet"
  - "Charlotte Hug"
  - "John Edwards"
  - "Marcio Mattos"
  - "Maggie Nichols"
  - "John Russell"
sets:
  - - Martin Hackett
    - Daniel Thompson
    - Phil Wachsmann
  - - The Stellari String Quartet
  - - Charlotte Hug
    - John Edwards
    - Marcio Mattos
    - Phil Wachsmann
  - - Maggie Nichols
    - John Russell
year: 2017
---
More quality free improvisation as we continue our afternoon concert series at The Vortex (details below)

### {{<artist "Martin Hackett">}} (electronics), {{<artist "Daniel Thompson">}} (guitar) & {{<artist "Phil Wachsmann">}} (violin)

![phil Martiu](https://www.mopomoso.com/wp-content/uploads/2016/12/phil-martin-daniel-300x99.jpeg)

A new trio put together by synthesizer player Martin Hackett and, in Phil Wachsmann, featuring one of the finest improvising violinists to be found anywhere. Thompson is a younger guitar player who has over the last few years created quite a buzz with his sensitive, measured playing. Surely a fine group.

### {{<artist "The Stellari String Quartet">}}

### {{<artist "Charlotte Hug">}} (viola), {{<artist "John Edwards">}} (bass), {{<artist "Marcio Mattos">}} (cello) & {{<artist "Phil Wachsmann">}} (violin) quartet

![150418_stellari](https://www.mopomoso.com/wp-content/uploads/2016/12/150418_Stellari-300x135.jpg)

Not quite the traditional string quartet line up and certainly not traditional music! Four superb palyers, all well known and highly respected on the international circuit, coming together to create a music of fire, passion and elegance.

### {{<artist "Maggie Nichols">}} (voice) & {{<artist "John Russell">}} (guitar)

![maggie-and-john](https://www.mopomoso.com/wp-content/uploads/2016/12/maggie-and-john-300x133.jpeg)

Two thirds of ‘Trio Blurb’ John and Maggie have been working together since the early seventies when they first met at the legendary Little Theatre Club. Maggie is, like John, someone who believes in organising events and introducing new audiences and participants to the music. Her seemingly boundless energy is carried through into the music, where her highly individual vocal techniques never cease to amaze audiences around the world.

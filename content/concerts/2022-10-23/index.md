---
title: "23 October 2022"
date: 2022-10-01T00:00:00Z
doors: 2pm
categories:
  - Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2022-10-23T01:00:00+01:00
concert_series: "Afternoon Sessions"
artists:
  - "David Leahy"
  - "Paloma Carrasco Lopez"
  - "Henry McPherson"
  - "Petra Haller"
  - "Paul Jolly"
  - "Charlotte Keeffe"
sets:
  - - "David Leahy"
    - "Paloma Carrasco Lopez"
  - - "Henry McPherson"
  - - "Petra Haller"
    - "Paul Jolly"
    - "Charlotte Keeffe"
year: 2022
---
### NUNC – {{<artist "David Leahy" >}} (double bass and dance) & {{<artist "Paloma Carrasco Lopez">}} (cello and dance)

### {{<artist "Henry McPherson">}} (multi-disciplinary performance)

### {{<artist "Petra Haller">}} (tap dance), {{<artist "Paul Jolly">}} (reeds) & {{<artist "Charlotte Keeffe">}} (trumpet)


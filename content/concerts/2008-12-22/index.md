---
title: 21 December 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-12-21T00:00:00Z
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - - "Rick Jensen"
    - "Dominic Lash"
    - "Dave Solomon"
  - - "Noel Taylor"
    - "Gabriel Keen"
    - "Bob Peachey"
  - - "Adam Bohman"
    - "Adrian Northover"
  - - "Meat Xylophone"
    - "Simon Littlefield"
    - "Nigel Summerly"
  - - "Viv Corringham"
    - "Lol Coxhill"
  - - "Andrea Caputo"
  - - "Marcio Mattos"
    - "Marilza"
    - "Alison Blunt"
    - "Rodrigo Montoya"
  - - "Daniel Thompson"
    - "Keith Thompson"
  - - "Ian Diplock"
    - "John Russell"
  - - "Swift are The Winds of Life"
    - "Charlie Collins"
    - "Yvonna Magda"
    - "Beatrix Ward-Fernandez"
  - - "Steve Beresford"
  - - "Jeff Cloke"
    - "Terry Day"
    - "Max Reed"
  - - "Pat Thomas"
    - "John Coxon"
    - "Roger Turner"
    - "Alex Ward"
  - - "Phil Minton"
    - "Ute Wassermann"
  - - "Karl D’Silva"
    - "Dominic Lash"
  - - "Martin Speake"
    - Dave Solomon
  - - Leila Adu
    - Steve Beresford
    - Hannah Marshall
  - - Harry Gilonis
    - Richard Barrett
    - John Russell
  - - Kay Grant
    - Hannah Marshall
  - - Scatter - Pat Thomas
    - Phil Minton
    - Roger Turner
    - Dave Tucker
  - - Alexander Hawkins
    - Tony Marsh
  - - Lol Coxhill
  - - Ute Kanngieser
    - Guillaume Viltard
  - - Alan Tomlinson
  - - Ricardo Tejero
    - Will Connors
    - Adam Bohman
    - Alison Blunt
  - - Satoko Fukuda
    - John Russell
    - Fabrizio Spera
  - - Conductivisation
    - Steve Beresford
    - Dominic Lash
    - Dave Solomon
    - Ricardo Tejero
    - Rick Jensen
    - Marcio Mattos
    - Dave Tucker
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

The complete set list for our 2008 end of year get together.<!--more--> Owing to camera problems not all sets were recorded. However there are still a lot of recordings here so the page will take a little time to download.

### {{<artist "Rick Jensen">}}, {{<artist "Dominic Lash">}}, {{<artist "Dave Solomon">}} & {{<artist "Noel Taylor">}}

No video available

### {{<artist "Gabriel Keen">}}, {{<artist "Bob Peachey">}}

No video available

### {{<artist "Adam Bohman">}} & {{<artist "Adrian Northover">}}

{{<youtube NTndAj-uUK8>}}

### {{<artist "Meat Xylophone">}} - {{<artist "Simon Littlefield">}} & {{<artist "Nigel Summerly">}}

No video available

### {{<artist "Viv Corringham">}} & {{<artist "Lol Coxhill">}}

No video available

### {{<artist "Andrea Caputo">}}

No video available

### {{<artist "Marcio Mattos">}}, {{<artist "Marilza">}}, {{<artist "Alison Blunt">}}, {{<artist "Rodrigo Montoya">}}

No video available

### {{<artist "Daniel Thompson">}} & {{<artist "Keith Thompson">}}

No video available

### {{<artist "Ian Diplock">}} & {{<artist "John Russell">}}

{{<youtube T36-cQcSLHk>}}

### {{<artist "Swift are The Winds of Life">}} - {{<artist "Charlie Collins">}}, {{<artist "Yvonna Magda">}} & {{<artist "Beatrix Ward-Fernandez">}}

No video available

### {{<artist "Steve Beresford">}}

{{<youtube jj5ty6OOehw>}}

### {{<artist "Jeff Cloke">}}, {{<artist "Terry Day">}} & {{<artist "Max Reed">}} trio

{{<youtube ge4oh5LY4Zk>}}

### {{<artist "Pat Thomas">}}, {{<artist "John Coxon">}}, {{<artist "Roger Turner">}} & {{<artist "Alex Ward">}}

{{<youtube j6xILy6IKIc>}}

### {{<artist "Phil Minton">}}, {{<artist "Ute Wassermann">}}

{{<youtube rMDe63W5s8Q>}}

### {{<artist "Karl D’Silva">}} & {{<artist "Dominic Lash">}}

{{<youtube 12zjDTEBZFw>}}

### {{<artist "Martin Speake">}} & {{<artist "Dave Solomon">}}

{{<youtube DV1KsehX5J4>}}

### {{<artist "Leila Adu">}}, {{<artist "Steve Beresford">}} & {{<artist "Hannah Marshall">}} trio

No video available.

### {{<artist "Harry Gilonis">}}, {{<artist "Richard Barrett">}} & {{<artist "John Russell">}}

No video available

### {{<artist "Kay Grant">}} & {{<artist "Hannah Marshall">}}

No video available

### {{<artist "Scatter">}} - {{<artist "Pat Thomas">}}, {{<artist "Phil Minton">}}, {{<artist "Roger Turner">}} & {{<artist "Dave Tucker">}}

{{<youtube XZwqkiHberE>}}

### {{<artist "Alexander Hawkins">}} & {{<artist "Tony Marsh">}}

{{<youtube AOpmnsqBQdk>}}

### {{<artist "Lol Coxhill">}}

{{<youtube eRbQxLQg5FI>}}

### {{<artist "Ute Kanngieser">}} & {{<artist "Guillaume Viltard">}}

{{<youtube g9Sg2-f7OiA>}}

### {{<artist "Alan Tomlinson">}}

No video available

### {{<artist "Ricardo Tejero">}}, {{<artist "Will Connors">}}, {{<artist "Adam Bohman">}} & {{<artist "Alison Blunt">}}

{{<youtube Tx1M1_4l9rU>}}

### {{<artist "Satoko Fukuda">}}, {{<artist "John Russell">}} & {{<artist "Fabrizio Spera">}}

{{<youtube yDJdjgx2ABg>}}

### {{<artist "Conductivisation">}} - {{<artist "Steve Beresford">}}, {{<artist "Dominic Lash">}}, {{<artist "Dave Solomon">}}, {{<artist "Ricardo Tejero">}}, {{<artist "Rick Jensen">}}, {{<artist "Marcio Mattos">}} & {{<artist "Dave Tucker">}}

No video available

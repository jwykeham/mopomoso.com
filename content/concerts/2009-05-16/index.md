---
title: 17 May 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-05-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artist:
  - Andrea Caputo
  - Noel Taylor
  - Lisa Ullen Quartet
  - Lisa Ullén
  - Mats Äleklint
  - Nils Ölmedal
  - Andreas Axelsson
  - Roger Turner
  - John Russell
sets:
  - - Andrea Caputo
    - Noel Taylor
  - - Lisa Ullen Quartet
    - Lisa Ullén
    - Mats Äleklint
    - Nils Ölmedal
    - Andreas Axelsson
  - - Roger Turner
    - John Russell
year: 2009
---
One quartet and two duos.
### {{<artist "Andrea Caputo">}} & {{<artist "Noel Taylor">}}
{{<youtube hbtXdbu96w8>}} 
### Lisa Ullen Quartet - {{<artist "Lisa Ullén">}}, {{<artist "Mats Äleklint">}}, {{<artist "Nils Ölmedal">}} & {{<artist "Andreas Axelsson">}} 
{{<youtube JpZyCb6n4jM>}} 
### {{<artist "Roger Turner">}} & {{<artist "John Russell">}}
{{<youtube 1bvkQv_TSwI>}} 

---
title: 20 September 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-09-21T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "The Vultures"
  - "The Messy Anne Trio"
  - "John Butcher"
  - "Pascal Marzan"
  - "John Russell"
  - "Peter Urpeth"
  - "Philipp Wachsmann"
sets:
  - - "The Vultures"
  - - "The Messy Anne Trio"
  - - "John Butcher"
    - "Pascal Marzan"
  - - "John Russell"
    - "Peter Urpeth"
    - "Philipp Wachsmann"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2009
---
### {{<artist "The Vultures">}}
{{<youtube 2_R8giWOTGI>}}
### {{<artist "The Messy Anne Trio">}}
{{<youtube 8zLE5volbb8>}}
### {{<artist "John Butcher">}}, {{<artist "Pascal Marzan">}}
{{<youtube 1g2ldpDqTUo>}}
### {{<artist "John Russell">}}, {{<artist "Peter Urpeth">}} & {{<artist "Philipp Wachsmann">}}
{{<youtube SSwPIf8E9FA>}}

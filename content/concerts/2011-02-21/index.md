---
title: 20 March 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-03-20T00:00:00Z
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Roberto Sassi"
  - "Ricardo Tejero"
  - "Steve Beresford"
  - "Satoko Fukuda"
  - "Guillaume Viltard"
  - "Guylaine Cosseron"
  - "Phil Minton"
  - "John Russell"
sets:
  - - Roberto Sassi
    - Ricardo Tejero
  - - Steve Beresford
    - Satoko Fukuda
    - Guillaume Viltard
  - - Guylaine Cosseron
    - Phil Minton
    - John Russell
year: 2011
---

Mopomoso’s March concert brings you the best in free improvisation with a duo and two

trios of impeccable musicianship from the UK, France, Italy and Spain.

### {{<artist "Roberto Sassi">}} (guitar) / {{<artist "Ricardo Tejero">}} (sax)

From fiery explosions to delicate nuance this Italian / Spanish pairing draws influences from Jazz, Rock, Noise,Electronic and Contemporary music. Perfectly mixed in an integrated push/pull of inventiveness. Now resident in London they have become an integral part of the local scene.

{{<youtube CqM56QU4uMY>}} 

### {{<artist "Steve Beresford">}} (piano) / {{<artist "Satoko Fukuda">}} (violin) / {{<artist "Guillaume Viltard">}} (bass)

A musician whose references are from an enormous range of music, with the musicianship and creativity to match, Beresford is joined by the astonishingly talented Fukuda and French bass wizard Viltard for this debut trio performance which promises to be a sheer delight

{{<youtube xTZMdhzLsH4>}} 

### {{<artist "Guylaine Cosseron">}} (voice) / {{<artist "Phil Minton">}} (voice) / {{<artist "John Russell">}} (guitar)

French vocalist Cosseron, here for this concert only, is a phenomenal vocalist, highly respected in her native country. With Minton vocal boundaries are pushed to the extreme whilst retaining a deep musical understanding. They are joined by renowned guitarist Russell for an unmissable trio!

{{<youtube Yn-qIb0v_zE>}} 

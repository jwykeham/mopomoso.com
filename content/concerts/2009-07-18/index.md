---
title: 19 July 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-07-19T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - Nusch Werchowska
  - Guylaine Cosseron
  - Kay Grant
  - Sarah Gail Brand
  - Oren Marshall
  - Pat Thomas
  - Martin Hackett
  - Tanja Feichtmair
  - John Russell
sets:
  - - Nusch Werchowska
    - Guylaine Cosseron
  - - Kay Grant
    - Sarah Gail Brand
    - Oren Marshall
  - - Pat Thomas
    - Martin Hackett
  - - Tanja Feichtmair
    - John Russell
year: 2009
---
Four sets – Three duos one trio in order of performance.
Unfortunate and unintentional lighting for the trio.
### {{<artist "Nusch Werchowska">}} & {{<artist "Guylaine Cosseron">}}
{{<youtube TEY-TWhrvMo>}} 
### {{<artist "Kay Grant">}}, {{<artist "Sarah Gail Brand">}} & {{<artist "Oren Marshall">}}
{{<youtube HKm9htzabaA>}} 
### {{<artist "Pat Thomas">}} & {{<artist "Martin Hackett">}}
{{<youtube ZwOdeDtfIxE>}} 
### {{<artist "Tanja Feichtmair">}} & {{<artist "John Russell">}}
{{<youtube vgJHwXaEixY>}} 

---
title: Tuesday 18 June 2013
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2013-06-18T01:00:00+01:00
venue: Arch 1
doors: 2pm
concert_series: Arch 1
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Gunther Christmann"
  - "Elke Schipper"
  - "John Russell"
sets:
  - - Gunther Christmann
    - Elke Schipper
    - John Russell
year: 2013
---
### {{<artist "Gunther Christmann">}}, {{<artist "Elke Schipper">}} and {{<artist "John Russell">}} at Arch 1 presenting a programme of music, sound poetry and film

An extra concert from our usual monthly series.
{{<youtube MN4A1Fk8D6E>}}
{{<youtube JIvqPS5ZEPo>}}
{{<youtube aNY-k_kcDKs>}}
{{<youtube 6j8Jbt8VDN4>}}
{{<youtube L_6y35X0JP8>}}
{{<youtube pvgnxeSmm3o>}}
{{<youtube VT7bnUldcf8>}}

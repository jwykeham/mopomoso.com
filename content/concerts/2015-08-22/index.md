---
title: 27 September 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-09-27T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Viv Corringham"
  - "Shaun Blezard"
  - "Hannes Buder"
  - "Clive Bell"
  - "Richard Scott"
  - "David Ross"
  - "Charlie Collins"
  - "Yoko Miura"
sets:
  - - Viv Corringham
    - Shaun Blezard
  - - Hannes Buder
  - - Clive Bell
    - Richard Scott
    - David Ross
  - - Charlie Collins
    - Yoko Miura
year: 2015
---

Continuing to present the very finest of musical free improvisation, Mopomoso brings you four sets – of which one is a UK debut, two are new to the club and the fourth is the return of an old favourite. Engaging music from the UK’s longest running series dedicated to free improvisation, now approaching its 25th year.

### {{<artist "Viv Corringham">}} (voice) & {{<artist "Shaun Blezard">}} (electronics)

{{<youtube PmihzQQrdKo>}}

**NB: John Russell appeared instead of Shaun Blezard who was ill.**

British vocalist Corringham is a composer and sound artist based in New York, here in a new pairing with Cumbrian based musician Blezard. Both have appeared at Mopomoso before and we look forward to hearing the new duo which promises to be full of intriguing twists and turns.

"Corringham's enchanting musical journey… possessed a quality that set the pulses racing." – Edwin Pouncey, *The Wire*

### {{<artist "Hannes Buder">}} (guitar)

{{<youtube JruZrn2C5ko>}}

Hannes Buder (born 1978 in the former GDR) is a musician, improviser and composer in the field of experimental music. His works concentrate on issues of movement, authenticity, intuition, minimalism, density and slowness.

"…wonderfully balanced sound-art, versatile, lively and uncompromising in its musical character." – *Süddeutsche Zeitung*

### Twinkle3 – {{<artist "Clive Bell">}} (shakuhachi, khene etc.), {{<artist "Richard Scott">}} (electronics) & {{<artist "David Ross">}} (electronics)

{{<youtube tKNUHkkG5hg>}}

A Mopomoso debut for this fine electro acoustic trio that mixes the digital and analogue electronics of Ross and Scott with the acoustic instruments of Bell. A delightful blend.

"…the three put together a real personal cosmogony, a spacey example of global folklore abstractions between experimental and more compelling electro-acoustic narratives…" – Nicola Catalano, *Blow Up*

### {{<artist "Charlie Collins">}} (percussion) & {{<artist "Yoko Miura">}} (piano)

{{<youtube 3qf9oMLU7zQ>}}

A welcome return to Mopomoso for Japanese pianist Miura and percussionist Collins whose music can be described as ‘a glimpse into the gaps between’ and has strong affinities to the natural world in its sense of space and tranquility. Two musicians with a wide range of experiences and interests who achieve the task of keeping the music direct and simple. Sharing an awareness of silence and a close listening bond the music is given air to develop at its own pace. Highly skilful and engaging playing.

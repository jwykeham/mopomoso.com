---
title: "17 December 2023"
date: 2023-12-13T14:50:24Z
doors: 12pm
categories:
- Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
- key: "Standard"
  value: "£12"
- key: "Vortex Members & Concessions"
  value: "£8"
concert_date: 2023-12-17 00:00:00 +0000 GMT
concert_series: Mopomoso Christmas Party
ticket_link: https://www.ticketweb.uk/event/mopomoso-christmas-party-vortex-jazz-club-tickets/13327573
artists:
- "Ebba Jahn"
- "Paul Pignon"
- "Alan Wilkinson"
- "Lawrence Casserley"
- "Martin Hackett"
- "John Bissett"
- "George Garford"
- "Julian Woods"
- "Olga Ksendzovska"
- "Eli Evdokimova"
- "Chris Hill"
- "Alan Newcombe"
- "Steve Beresford"
- "Faradena Afifi"
- "Maham Suhail"
- "Pascal Marzan"
- "Roger Turner"
- "Paul Jolly"
- "Geoff Eales"
- "Maggie Nicols"
- "Phil Minton"
- "Lisa Reim"
- "Matt Hutchinson"
- "Chris Holley"
- "Nicky Smith"
- "Jane Hollingworth"
- "Bettina Schroeder"
- "Susanna Ferrar"
- "Henry McPherson"
- "Jakub Rokita"
- "Paul Taylor"
- "Ivor Kallin"
- "John Eyles"
- "Edward Shipsey"
- "Jerry Wigens"
- "Neil Metcalfe"
- "Dave Tucker"
---
And it's the Mopomoso Christmas Party again!

Mopomoso is the UK's longest running concert series dedicated to freely improvised music!
<!--more-->
Come and join us on Sunday 17th December, kicking off earlier than usual at 12pm!

There'll be around 37 musicians this month, plus a silent film (which musicians will play to LIVE) collated by {{<artist "Ebba Jahn">}}, including 17 international film-makers.

Musicians include: 
- {{<artist "Paul Pignon">}}
- {{<artist "Alan Wilkinson">}}
- {{<artist "Lawrence Casserley">}} / {{<artist "Martin Hackett">}}
- {{<artist "John Bissett">}} / {{<artist "George Garford">}} / {{<artist "Julian Woods">}}
- {{<artist "Olga Ksendzovska">}} / {{<artist "Eli Evdokimova">}}
- {{<artist "Chris Hill">}} / {{<artist "Alan Newcombe">}}
- {{<artist "Steve Beresford">}} / {{<artist "Faradena Afifi">}} / {{<artist "Maham Suhail">}}
- {{<artist "Pascal Marzan">}} / {{<artist "Roger Turner">}}
- {{<artist "Paul Jolly">}} / {{<artist "Geoff Eales">}} / {{<artist "Maggie Nicols">}}
- {{<artist "Phil Minton">}}
- {{<artist "Lisa Reim">}}
- {{<artist "Matt Hutchinson">}}
- {{<artist "Chris Holley">}} / {{<artist "Faradena Afifi">}} / {{<artist "Nicky Smith">}} / {{<artist "Jane Hollingworth">}} / {{<artist "Bettina Schroeder">}}
- {{<artist "Susanna Ferrar">}}
- {{<artist "Pascal Marzan">}} / {{<artist "Henry McPherson">}} / {{<artist "Jakub Rokita">}}
- {{<artist "Paul Taylor">}}
- {{<artist "Ivor Kallin">}}
- {{<artist "Paul Jolly">}}
- {{<artist "John Eyles">}}
- {{<artist "Edward Shipsey">}}
- {{<artist "Jerry Wigens">}} / {{<artist "Pascal Marzan">}} / {{<artist "Neil Metcalfe">}}
- {{<artist "Dave Tucker">}}

There'll be party food.

Please come and support!

We wish you all a Merry Mopomoso Christmas!

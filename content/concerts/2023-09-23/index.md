---
title: "23 September 2023"
date: 2023-09-18T08:01:14+01:00
doors: 2pm
categories:
- Concert
venue: St. Mary’s, Church Street, LU1 3JF
costs:
- key: "Standard"
  value: "£5"
concert_date: 2023-09-23 14:00:00 +0100 BST
concert_series: Equinoks
ticket_link: https://www.eventbrite.co.uk/e/equinoks-arts-festival-at-st-marys-church-in-luton-tickets-673011273837?utm-campaign=social&utm-content=attendeeshare&utm-medium=discovery&utm-term=listing&utm-source=cp&aff=ebdsshcopyurl
artists:
- "Faye Munroe"
- "Claire Davies"
- "Anna Fairchild"
- "Viv Corringham"
- "Paul Jolly"
- "Sylvia Hallett"
- "Tomasz Glazik"
- "Yoko Miura"
- "Mark Browne"
- "Jakub Rokita"
- "Lawrence Casserley"
- "mcddavid"
- "Petra Haller"
- "Ebba Jahn"
- "Ahmed Salvador"
- "Scott McMahon"
---
An innovative pop-up art show is coming to Luton on the day of the Autumnal equinox - set against the historical interiors of St. Mary's church in Luton town centre. Featuring acoustic performances by acclaimed British and international free improvisers, many of whom members of the longest running improvised event Mopomoso, established by the late  **John Russell**, as well as several Luton's improvisers.
<!--more-->

The free spirited, musical performances using a variety of classical, improvised and electronic instruments will be accompanied by a display of ceramic and photographic work by Luton and Hertfordshire artists: {{<artist "Faye Munroe">}}, {{<artist "Claire Davies">}} and {{<artist "Anna Fairchild">}} and interwoven by screenings of short films and animations by film-makers both local and international.

### Musicians
- {{<artist "Viv Corringham">}} (USA, Voice + Effects)
- {{<artist "Paul Jolly">}} (UK, Clarinets)
- {{<artist "Sylvia Hallett">}} (UK, Branches and a bicycle wheel)
- {{<artist "Tomasz Glazik">}} (UK, Sax)
- {{<artist "Yoko Miura">}} (Japan, Piano / Melodica)
- {{<artist "Mark Browne">}} (UK, Sax + Collected Objects)
- {{<artist "Jakub Rokita">}} (UK, Harmonica) 
- {{<artist "Lawrence Casserley">}} (UK, SPU + Sounders)
- {{<artist "mcddavid">}} (Germany, Guitar)
- {{<artist "Petra Haller">}} (UK, Dance)

### Film-makers
- {{<artist "Ebba Jahn">}} (Germany)
- {{<artist "Ahmed Salvador">}} and {{<artist "Scott McMahon">}} (USA)
- {{<artist "Jakub Rokita">}} and {{<artist "Tomasz Glazik">}} (UK)
- {{<artist "mcddavid">}} (Germany) 


### Artists
- {{<artist "Anna Fairchild">}} (Photography)
- {{<artist "Claire Davies">}} (Ceramics)
- {{<artist "Faye Munroe">}} (Ceramics)

---
artists:
  - John Russell
  - Phil Durrant
  - John Butcher
concert_date: 1998-03-15T20:00:00+01:00
concert_series: At The Red Rose
date: 1998-03-15T20:00:00+01:00
notes:
  - A recording of John Butcher, Phil Durrant & John Russell's set was included on *The Scenic Route* (EMANEM 4029)
sets:
  - - John Russell
    - Phil Durrant
    - John Butcher
sources:
  - https://www.audiocircle.com/index.php?topic=153460.40
title: 15 March 2003
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1998
---

---
title: 17 May 2015
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2015-05-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Steve Beresford"
  - "Beibei Wang"
  - "Paul Pignon"
  - "Thomas Bjelkeborn"
  - "John Russell"
sets:
  - - Steve Beresford
    - Beibei Wang
  - - Paul Pignon
    - Thomas Bjelkeborn
  - - John Russell
year: 2015
---

### {{<artist "Steve Beresford">}} (piano), {{<artist "Beibei Wang">}} (percussion)

{{<youtube 0L1WQmJSAE4>}}

### SQ – {{<artist "Paul Pignon">}} (single reeds, voice and electronics), {{<artist "Thomas Bjelkeborn">}} (electronics)

{{<youtube HbEo5nyuC3k>}}

### {{<artist "John Russell">}} (guitar)

{{<youtube 797b38--qWA>}}

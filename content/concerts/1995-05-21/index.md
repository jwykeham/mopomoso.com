---
artists: []
concert_date: 1995-05-21T20:30:00+01:00
concert_series: At The Red Rose
date: 1995-05-21T20:30:00+01:00
notes: []
sets:
  - - Chris Burn
    - John Butcher
  - - Vanessa Mackness
    - Phil Wachsmann
    - Alex Frangenheim
sources:
  - https://www.newspapers.com/image/260798890
title: 21 May 1995
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1995
---
---
title: "27 August 2023"
date: 2023-08-01T19:57:14+01:00
doors: 2pm
categories:
- Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
- key: "Standard"
  value: "£12"
- key: "Vortex Members & Concessions"
  value: "£8"
concert_date: 2023-08-27 00:00:00 +0100 BST
concert_series: Mopomoso Live
artists:
- "Mark Browne"
- "Adam Bohman"
- "Noisy Women"
- "Julian Woods"
- "Maggie Nicols"
- "Marion Treby"
- "Joanne Morrison"
- "Faradena Afifi"
sets:
- - "Mark Browne"
- - "Adam Bohman"
- - "Noisy Women"
  - "Julian Woods"
  - "Maggie Nicols"
  - "Marion Treby"
  - "Joanne Morrison"
  - "Faradena Afifi"
ticket_link: https://www.ticketweb.uk/event/mopomoso-august-vortex-jazz-club-tickets/13472938
---
### {{<artist "Mark Browne">}} (saxophone, objects etc)

### {{<artist "Adam Bohman">}} (amplified objects etc)

### {{<artist "Noisy Women">}}
- {{<artist "Julian Woods">}} (microtonal guitar etc) 
- {{<artist "Maggie Nicols">}} (voice) 
- {{<artist "Marion Treby">}} (piano, voice)
- {{<artist "Joanne Morrison">}} (voice, movement, objects) 
- {{<artist "Faradena Afifi">}} (voice, movement, strings)

### {{<artist "Riley Mackenzie">}} (Electronics)

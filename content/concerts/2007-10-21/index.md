---
artists:
  - Imaginary String Trio
  - Dominic Lash
concert_date: 2007-10-21T20:00:00Z
concert_series: At The Red Rose
date: 2007-10-21T20:00:00Z
notes: null
sets: []
sources:
  - http://dominiclash.blogspot.com/p/gig-archive.html
title: 21 October 2007
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 2007
---
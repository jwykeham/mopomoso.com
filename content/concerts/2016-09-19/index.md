---
title: 16 October 2016
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2016-10-16T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Gus Garside"
  - "Annie Kerr"
  - "Per Gardin"
  - "Sarah Gail Brand Quartet"
  - "Sarah Gail Brand"
  - "Mark Sanders"
  - "Phil Minton"
  - "Veryan Weston"
  - "Yoko Miura"
  - "John Russell"
sets:
  - - Gus Garside
    - Annie Kerr
  - - Per Gardin
  - - Sarah Gail Brand Quartet
    - Sarah Gail Brand
    - Mark Sanders
    - Phil Minton
    - Veryan Weston
  - - Yoko Miura
    - John Russell
year: 2016
---

### {{<artist "Gus Garside">}} (bass) & {{<artist "Annie Kerr">}} (violin)

![gus-garside-annie-kerr](https://www.mopomoso.com/wp-content/uploads/2016/09/gus-garside-annie-kerr-1024x684.jpg)

As well as this duo Gus Garside plays with string trio Arc and in Static Memories. He is also instrumental in running the highly regarded Safehouse music events in Brighton. Annie also lives in Brighton playing in a wide variety of settings and styles and where she also teaches music. Their music ranges between the intense to the pastoral. From the lyrical to the dramatic.

### {{<artist "Per Gardin">}} (sax)

![per-gardin-grab](https://www.mopomoso.com/wp-content/uploads/2016/09/per-gardin-grab.jpg)

Delighted to have a visit from Stockholm based saxophonist Per Gardin. An assured improviser whose musical skills are always employed to the demands of the music as it unfolds in real time. Per will approach phrases from different directions, constantly disecting and building on ideas with embellishments designed to elucidate and engage the listener.

### {{<artist "Sarah Gail Brand Quartet">}} – {{<artist "Sarah Gail Brand">}} (trombone), {{<artist "Mark Sanders">}} (drums), {{<artist "Phil Minton">}} (voice) & {{<artist "Veryan Weston">}} (piano)

![sarah-gail-brand-quartet](https://www.mopomoso.com/wp-content/uploads/2016/09/sarah-gail-brand-quartet.jpg)

The new Sarah Gail Brand Quartet is quite an amazing line up of old friends and should need no introduction, not only to followers of Mopomoso but improvised music listeners everywhere. Dramatic and inspiring music from some the most respected improvisers around to lift the lowest of spirits!

### {{<artist "Yoko Miura">}} (piano) & {{<artist "John Russell">}} (guitar)

![john-and-yoko-m-2](https://www.mopomoso.com/wp-content/uploads/2016/09/john-and-yoko-m-2.jpg)

A welcome return from Tokyo based pianist Miura whose delicate and enigmatic approach to the piano uses areas of silent contemplation to emphasise the natural world. Joined by guitarist Russell this is their second ever duo meeting and should make for a listening experience immersed in detail and dramatic anticipation.

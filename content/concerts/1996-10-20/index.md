---
artists:
  - Sascha Demand
  - Roger Turner
concert_date: 1996-10-20T20:00:00+01:00
concert_series: At The Red Rose
date: 1996-10-20T20:00:00+01:00
notes: []
sets:
  - - Thomas Lehn Trio
sources:
  - https://www.newspapers.com/image/260397440
title: 20 October 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
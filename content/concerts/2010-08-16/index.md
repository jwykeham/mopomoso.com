---
title: 17 August 2010
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2010-08-17T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 8pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2010
---
Third and final day of Fete Quaqua 2010. Sets in order of preformance.
  
  {{<youtube ab5klNdtWMs>}} 
  {{<youtube yrabfooKYk8>}} 
  {{<youtube _jPE1A9Hyho>}} 
  {{<youtube shiGN0elgos>}} 
  {{<youtube FOlGaGP6zDM>}} 
  {{<youtube YYMa7XpwbVU>}} 
  {{<youtube o8RiPoBtcHs>}} 
  {{<youtube MyYcQ38ivpE>}} 
  {{<youtube 7JmH17-DFSU>}} 

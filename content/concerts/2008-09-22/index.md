---
title: 21 September 2008
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2008-09-21T01:00:00+01:00
concert_series: Afternoon Sessions
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
artists:
  - "Jeff Cloke"
  - "Dave Draper"
  - "Dominic Lash"
  - "Dave Solomon"
  - "Garry Todd"
  - "Phil Minton"
  - "Veryan Weston"
  - "Ketil Gutvik"
  - "John Russell"
sets:
  - - "Jeff Cloke"
    - "Dave Draper"
  - - "Dominic Lash"
    - "Dave Solomon"
    - "Garry Todd"
  - - "Phil Minton"
    - "Veryan Weston"
  - - "Ketil Gutvik"
    - "John Russell"
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
year: 2008
---

### {{<artist "Jeff Cloke">}}, {{<artist "Dave Draper">}}

{{<youtube 0J1rPWQY38s>}}


### {{<artist "Dominic Lash">}}, {{<artist "Dave Solomon">}}, {{<artist "Garry Todd">}}

{{<youtube CFruoP8Y4oU>}}


### {{<artist "Phil Minton">}}, {{<artist "Veryan Weston">}}

{{<youtube ZdipuAEOsw8>}}


### {{<artist "Ketil Gutvik">}}, {{<artist "John Russell">}}

{{<youtube KqKPfp0eYSM>}}

---
title: 20 May 2012
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2012-05-20T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Anton Mobin"
  - "Giorgio Albanese"
  - "Ricardo Tejero"
  - "Sylvia Hallett"
  - "Anna Homler"
  - "Kay Grant"
  - "Matthew Ostrowski"
  - "John Russell"
  - "Dave Solomon"
sets:
  - - Anton Mobin
  - - Giorgio Albanese
    - Ricardo Tejero
  - - Sylvia Hallett
    - Anna Homler
  - - Kay Grant
    - Matthew Ostrowski
    - John Russell
    - Dave Solomon
year: 2012
---

### {{<artist "Anton Mobin">}} (electronics)

{{<youtube -ivqEcVM290>}}

### {{<artist "Giorgio Albanese">}} (accordion) / {{<artist "Ricardo Tejero">}} (sax) / duo

{{<youtube DYia84T35_c>}}

### {{<artist "Sylvia Hallett">}} (violin plus) / {{<artist "Anna Homler">}} (voice) duo

{{<youtube dD2rJorkMgw>}}

### {{<artist "Kay Grant">}} (voice) / {{<artist "Matthew Ostrowski">}} (electronics) / {{<artist "John Russell">}} (guitar) / {{<artist "Dave Solomon">}} (drums)

{{<youtube Xdlnmfgm8lA>}}

---
title: 21 June 2009
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2009-06-21T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - Tania Chen
  - Shabaka Hutchings
  - Mark Sanders
  - Forgiving July
  - Gianni Mimmo
  - Angelo Contini
  - Stefano Pastor
  - John Butcher
  - Dominic Lash
  - John Russell
sets:
  - - Tania Chen
    - Shabaka Hutchings
    - Mark Sanders
  - - Forgiving July
    - Gianni Mimmo
    - Angelo Contini
    - Stefano Pastor
  - - John Butcher
    - Dominic Lash
    - John Russell
year: 2009
---
Three trios.
### {{<artist "Tania Chen">}}, {{<artist "Shabaka Hutchings">}} & {{<artist "Mark Sanders">}}
{{<youtube Hmdotrl7AWM>}} 
### Forgiving July - {{<artist "Gianni Mimmo">}}, {{<artist "Angelo Contini">}} & {{<artist "Stefano Pastor">}}
{{<youtube nvfDdttn9zw>}} 
### {{<artist "John Butcher">}}, {{<artist "Dominic Lash">}} & {{<artist "John Russell">}}
{{<youtube iuQFZC_y9cU>}} 

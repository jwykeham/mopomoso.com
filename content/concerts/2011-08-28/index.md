---
title: 18 September 2011
categories:
  - Afternoon Sessions
date: "1970-01-01T00:00:00Z"
concert_date: 2011-09-18T01:00:00+01:00
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
doors: 2pm
concert_series: Afternoon Sessions
costs:
  - key: Full Price
    value: £8
  - key: Concessions
    value: £5
artists:
  - "Portia Winters"
  - "Illi Adato"
  - "Oren Marshall"
  - "Sylvia Hallett"
  - "Alison Blunt"
  - "Anna Kaluza"
  - "Horst Nonnenmacher"
  - "Manuel Miethe"
  - "Nikolai Meinhold"
sets:
  - - Portia Winters
    - Illi Adato
    - Oren Marshall
  - - Sylvia Hallett
  - - Alison Blunt
    - Anna Kaluza
    - Horst Nonnenmacher
    - Manuel Miethe
    - Nikolai Meinhold
year: 2011
---

Consistently bringing the very best from the free improvised end of things Mopomoso’s September programme has three distinct and intriguing sets.

### {{<artist "Portia Winters">}}, {{<artist "Illi Adato">}} & {{<artist "Oren Marshall">}}

![Adato Marshall Winters trio](https://www.mopomoso.com/wp-content/uploads/2011/08/omw-three-jpeg-1.jpg "Adato Marshall Winters")

The fine voice of {{<artist "Portia Winters">}} and the percussion/electronics forays of {{<artist "Illi Adato">}} are joined by the astounding and much in demand tuba player {{<artist "Oren Marshall">}}. A Mopomoso debut for this established trio, well known to the London free improvisation scene, they promise a music of energy, colour and lightness of touch.

### {{<artist "Sylvia Hallett">}}

![sylvia hallett](https://www.mopomoso.com/wp-content/uploads/2011/08/sylvia_hallett2.jpg "sylvia_hallett2")

Sylvia Hallett is a violinist who works both as a composer and as an improviser, and has had pieces performed in Britain and Europe including work for a.o BBC Radio Plays, The Royal Shakespeare Company, The Young Vic and with dancer/choreographers Miranda Tufnell, From Here to Eternity, Eva Karczag and live art puppeteers DooCot. Her last appearance to a packed house at Mopomoso received a standing ovation from the appreciative crowd. It’s great to have her back!

### The HAMAN Quintet – {{<artist "Alison Blunt">}} violin, {{<artist "Anna Kaluza">}} sax, {{<artist "Horst Nonnenmacher">}} bass, {{<artist "Manuel Miethe">}} sax & {{<artist "Nikolai Meinhold">}} piano

![HAMAN Quintet](https://www.mopomoso.com/wp-content/uploads/2011/08/HAMAN-Quintet-collage.jpg "HAMAN Quintet collage")

Welcome UK debut for this Berlin based quintet whose collective CVs range from Sam Rivers to the Linchopinger Symphoniker and Paul Bley to the Berlin Improvisers Orchestra taking in cabaret, dance, film and theatre music along the way. Intelligent broad canvas free improvisation to delight the imagination.

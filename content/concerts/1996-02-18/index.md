---
artists:
  - '? Klapper'
  - '? Biskup'
  - Chris Burn
  - John Butcher
  - Phil Durrent
  - '? Wagner'
  - '? Hafner'
concert_date: 1996-02-18T20:00:00Z
concert_series: At The Red Rose
date: 1996-02-18T20:00:00Z
notes:
  - '>Certifiable floor, toy and electronic-banging improv mavericks. - *The Guardian - Sat Feb 17 1996*'
sets: []
sources:
  - https://www.newspapers.com/image/260872275
title: 18 February 1996
venue: Red Rose, 129 Seven Sisters Rd, Finsbury Park, London N7 7QG
year: 1996
---
---
title: "28 May 2023"
date: 2023-05-28T14:00:00+01:00
doors: 2pm
categories:
  - Concert
venue: Vortex Jazz Club, 11 Gillett Square, London, N16 8AZ
costs:
  - key: "Standard"
    value: "£12"
  - key: "Vortex Members & Concessions"
    value: "£8"
concert_date: 2023-05-28T14:00:00+01:00
concert_series: Afternoon Sessions
artists:
  - "Phil Durrant"
  - "Cath Roberts"
  - "Andrew Lisle"
  - "Charlotte Keeffe"
  - "Daniel Thompson"
  - "Martin Hackett"
sets:
  - - "Phil Durrant"
    - "Cath Roberts"
    - "Andrew Lisle"
  - - "Charlotte Keeffe"
    - "Daniel Thompson"
  - - "Martin Hackett"
year: 2023
---
### {{<artist "Phil Durrant">}}, {{<artist "Cath Roberts">}} & {{<artist "Andrew Lisle">}}
{{<artist/description "Phil Durrant">}} 

{{<artist/description "Cath Roberts">}} 

{{<artist/description "Andrew Lisle">}} 

### {{<artist "Charlotte Keeffe">}} & {{<artist "Daniel Thompson">}}
{{<artist/description "Charlotte Keeffe">}} 

{{<artist/description "Daniel Thompson">}} 

### {{<artist "Martin Hackett">}}
{{<artist/description "Martin Hackett">}} 

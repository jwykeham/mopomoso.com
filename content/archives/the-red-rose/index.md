---
title: At The Red Rose
date: 2023-07-29T06:53:00+01:00
categories:
- archive
- the red rose
featured_image: images/Red-Rose-Club.jpg
---

The Following is a list of concerts from the Red Rose that we've pulled together from various sources.
<!--more-->
If you can help with dates, sets, photos, video, posters etc and/or corrections please get in touch <a href="mailto:hello@mopomoso.com?subject=archive">hello@mopomoso.com</a>


{{<concerts-to-table>}}

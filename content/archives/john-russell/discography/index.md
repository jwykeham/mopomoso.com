---
title: Discography
date: 2023-07-29T06:53:00+01:00
categories:
- archive
- John Russell
slug: /john-russell/discography
featured_image: images/hyste-cover-150x150.jpg
summary: John Russell's incomplete discography
---
## John Russell's Incomplete Discography
- 1974/1975 *Teatime* **Incus 15** with Garry Todd / Dave Solomon / Nigel Coombes / Steve Beresford
- 1978 *Home Cooking* Incus 31 Solo LP shared with Richard Coldman
- 1979 *Artless Sky* CAW 001 with Toshinori Kondo and Roger Turner
- 1980 *Forward of Short Leg* Dossier ST 7529. With various Jon Rose groups.
- 1980 *Requiem* Impetus IMP LP 18405 Quiet Violence with Junja Kawasaki and Gillian McGregor
- 1980 *Vario II* Moers Music 01084 with Gunter Christmann / Maarten Altena / Paul Lovens / Maggie Nicols
- 1981 *The Fairly Young Bean* Emanem 4036 With Maarten Altena and Terry Day
- 1983/1985 *Vario* Moers Music 02048 leader Gunter Christmann
- 1987 *Conceits* Acta 1 with John Butcher and Phil Durrant
- 1988 *Wild Pathway Favourites* Ladder rung002 with the Martin Archer group and Nick Evans
- 1989 *News from the Shed* Acta 4 with John Butcher / Phil Durrant / Paul Lovens and Radu Malfatti
- 1989 *News from the Shed* Emanem 4121 Re-issue of Acta 4 with additional material
- 1990 *Cultural Baggage* Acta 5 with the Chris Burn Ensemble
- 1991 *The Place 1991* Emanem 4056 with the Chris Burn Ensemble.
- 1991/1992 *Concert Moves* Random Acoustics RA 011 with John Butcher and Phil Durrant
- 1992 *Ohrkiste* ITM Classics 950013 led by Radu Malfatti
- 1994 *Strakt* Discus 5CD Brief solo track on Network CD.
- 1996 *Birthdays* Emanem 4010 Duo with Roger Turner
- 1996 *London Air Lift* FMP CD89 with Evan Parker / John Edwards / Mark Sanders
- 1997 *Navigations* Acta 12 with the Chris Burn Ensemble
- 1997 *Interplay* FMR CD39 V0697 Hugh Davies in trio with Russell/Turner.
- 1997 *Improvisation: exhibit A* Avant BGS sampler. Solo track on cover CD of Avant, no. 3.
- 1997/8 *Strings with Evan Parker* Emanem 4302.
- 1998 *The Scenic Route* Emanem 4029 with John Butcher and Phil Durrant
- 1998 *Refrain* MS 41 with Mario Schiano / Sylvia Hallett / Roger Turner / Penazzi
- 1998 *The Duo Recordings* The Field Recordings 7, FR7. Duo with Luc Houtkamp.
- 2000 *Excerpts & Offerings* Acta 14 duo with Stefan Keune
- 2000 *The All Angels Concerts* Emanem 4209 Solo on compilation 2-CD.
- 2001 *The Second Sky* Emanem 4058. duo with Roger Turner
- 2001 *Deluxe improvisation series vol. 2* ASE_03/Deluxe improvisation series vol. 2. Quartet track with Stefan Keune on
 compilation CD
- 2001 *Freedom of the City 2001: large groups* Emanem 4206 Strings with/without Evan Parker.
- 2001 *Freedom of the City 2001: small groups* Emanem 4205
- 2001 *Horizontals White* Emanem 4080 with the Chris Burn Ensemble
- 2001 *Grain* DotDotDot Music 003 Short track on compilation CD.
- 2001/2002 *From Next to Last* Emanem 4071 Solo
- 2002 *Sangeraku* May 2nd Creative in Hodogaya volume 2 with Shu Oyama / Kemmy Nishioka / Sabu Toyozumi / Osamu
 Nomura
- 2002 *Freedom of the City 2002: small groups* Emanem 4210 duo with Evan Parker on compilation CD.
- 2002 *Frequency of Use* NURNICHTNUR BERSLTON 102 12 31 duo with Stefan Keune
- 2002 *Mopomoso solos 2002* Emanem 4100 with Chris Burn / Lol Coxhill / John Edwards / Phil Minton
- 2003 *Freedom of the City 2003: small groups* Emanem 4212
- 2003 *Three Planets* Emanem 4106 Ute Völker / Matthieu Werchowski
- 2004 *Freedom of the City 2004: small groups* Emanem 4214
- 2004 & 2006 *Analekta* Emanem 413 various groups
- 2005 *Crossing the River* psi 06.02. Evan Parker Octet
- 2005 *More Together than Alone* Emanem 4136 Lol Coxhill
- 2005/2006 *The Mercelis Concert* Inaudible CD 006 with Jean Demey / Jean-Michel Van Schouwburg
- 2006 *Duos* Emanem 4137 Terry Day
- 2006 *Vario-44* Edition Explico 15 with John Butcher / Roger Turner / Thomas Lehn / Dorothea Schürch / Günter Christmann
- 2008 *House Full of Floors* Tzadik Key Series 6732 with Evan Parker / John Edwards/Aleks Kolkowski
- 2009 *No Room for Doubt* Amirani Records with Gianni Mimmo / Angelo Contini / Jean Michel van Schouwburg / Andreas
 Serrapiglio plus Paolo Falascone
- 2009 *Duet* Another Timbre at27 duo Martine Altenberger
- 2010 *Hyste* Psi 10.06 solo guitar
- 2010 *The Cigar That Talks* Collection PiedNu PN0110 with Michel Doneda / Roger Turner
- 2010 *Teatime* Emanem 5009 Re-issue of Incus 15 with extra track from 1973.
- 2011 *Translations* Emanem 5019 Duo with guitarist Pascal Marzan
- 2012 *Birds* dEN Records 008 Trio with Mats Gustafsson and Raymond Strid
- 2012 *Kosai Yujyo* Inaudible 008 One octet track on Sabu Toyozumi 2 CD set
- 2012 *Home* Peira 15 Duos with Fred Lonberg Holm
- 2013 *Trio Blurb* Extraplatte EX 821-2 Trio with Maggie Nicols and Mia Zabelka
- 2014 *No Step* Hispid HSPD002 Duo with Stale Liavik Solberg
- 2015 *A Duo(S)* Bocian Records vinyl LP BCLO Mats Gustafsson with Phil Minton and John Russell
- 2015 *Will it Float?* Va Fongool vinyl LP VAFLP010 with Stale Liavik Solberg, Steve Beresford and John Edwards
- 2015 *With…* Emanem CD 5037 Live 60th birthday concert with Henry Lowther, Satoko Fukuda, Phil Minton, Evan Parker, John
 Edwards and Thurston Moore.
- 2016 *Chasing the Peripanjandra* one CD with Evan Parker and John Edwards of the 4 CD boxed set *Making Rooms* Weekertoft 1
 – 4
- 2017 *In Search of Wasabi* digital download with Ken Ikeda and Eddie Prevost Weekertoft Digital 2
- 2017 *Ditch School* digital download with Paul G Smyth Weekertoft Digital 4
- 2017 *The Art of the Duo* Günter Christmann’s Vario 50 compilation Edition Explico
- 2017 *Reunion* Ian Brighton project including duo with Evan Parker CD FMR records
- 2017 *Friends and Heroes:Guitar duets Henry Kaiser with…* own label CD
- 2017 *The Sorter* with Steve Beresford, John Edwards and Stale Liavik Solberg CD Va Fongool
- 2017 *Walthamstow (61 revisited)* with Evan Parker and John Edwards vinyl LP ByrdOut

### Links
- https://www.discogs.com/artist/292491-John-Russell

---
title: Some guitars over the years
date: 2014-08-06T00:00:00
summary: In response to an enquiry about my guitar history I have quite a few guitars but these are the ones that represent my main instrument over the years. I  have not kept any of the early electrics as I upgraded each time I got a new one.
featured_image: 4-course_guitar,_c1570.jpg
---
## Some guitars over the years

In response to an enquiry about my guitar history I have quite a few guitars but these are the ones that represent my main instrument over the years. I  have not kept any of the early electrics as I upgraded each time I got a new one.

I started playing with a cheap steel strung acoustic and the "Tune a Day" guitar book, graduating to my first electric after a couple of months. A *Rossetti Lucky Seven* with a white plastic Egmond pick up.

This was followed by a solid body three pick up *Hofner Galaxie*.

The last of these earlier electrics was a *Grimshaw GS30 Les Paul* which I used when I first started playing free improvisation and it can be heard on the Incus LP *Teatime*.

After this I switched to acoustic playing and used an *Epiphone Zenith* made in 1936.[^1]

A fine guitar it finally "wore out" and I bought the guitar I use today. The *Radiotone F7812*, again built in 1936, it is the top guitar in this catalogue and at the time cost £6 15s 0d or £6.75.

These days I occasionally play electric guitar as well and use an *Epiphone Broadway* with a natural finish like the one in the picture.

[^1]: Sold to Ross Lambert https://www.thewire.co.uk/in-writing/essays/floating-sounds-john-russell

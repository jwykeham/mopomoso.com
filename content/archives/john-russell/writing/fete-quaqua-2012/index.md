---
title: Fete Quaqua 2012
date: 2012-08-02T00:00:00
slug: john-russell/writing/fete-quaqua-2012
featured_image: fetequaqua1982.jpg
---

Gearing up for this year's Fete Quaqua. Always a challenge and made more so by the fact that there is no funding for it apart from the
generosity of the **Swedish Arts Council** who have made it possible for the two Swedish musicians to attend. I really hope we get a
good audience. It is very difficult to make an assumption about this as we have the Olympics in London at the moment and people just
don’t have much money (me included!). After overheads are paid all the door money is divided equally among the musicians but it
will be a very small amount in professional terms (probably just covering individual per diem expenses). I have kept the admission
price to the normal Mopomoso one of £8 and £6 concessions and there is also a saving on a three day pass. What would be great is if
lots of people went for the three day pass option and followed how the music develops over the course of the event. It’s also good
when audience members decide to take a chance and experience something new. I think as an event Fete Quaqua is particularly well
suited for this as the evening takes the form of relatively short sets held together by a developing narrative thread.

This "narrative thread"" is the second aspect of the composition, the first being to invite the musicians, and I spend a lot of time working
out all the possible combinations and permutations before coming up with a programme for each night. Each concert always begins and
ends with the whole ensemble playing together. These tutti generally seem to act as a musical barometer to the whole thing and
develop accordingly.

I’m very excited by the musicians playing this year. All wonderful players from diverse backgrounds who share a love of free
improvisation. Every year alliances emerge and new musical friendships are made and it is great for me to see that some of the
musicians go on to work together in other contexts around the world. Although all the musicians are "stars" there is never any "star like
behaviour" and this genuine openness to collaboration and honesty to music is heart warming. The music always comes first!

And the audience! We always try to make an inviting atmosphere for the music to thrive in and The Vortex is a great space. Known as
"London's listening Jazz club" it provides an intimate and accessible platform. I really hate the term "punters" and believe that, in the
context of our information rich world and all its pressures, if someone has travelled to see you and paid hard earned money your job is
to do the very best you can for them. So back to the original paragraph – I do hope we get a good audience. I know we have some good
musicians!

More information about Fete Quaqua 2012 can be found https://fetequaqua.wordpress.com/

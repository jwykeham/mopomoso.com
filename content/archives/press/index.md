---
title: Press
date: 2023-07-29T06:53:00+01:00
categories:
- archive
- press
featured_image: images/Press1520.png
---
### They chose freedom - Chiswick, Linton - *The Observer* 2 May 1993
> "Free improvisation is the most direct way of communicating with the active listerner" - John Russell
source: <https://www.newspapers.com/image/258395407>
### John Russell & Mopomoso - *The Wire* July 2020 Issue 437
source: <https://www.thewire.co.uk/issues/437>

---
title: Hundred Years Gallery
where: 13 Pearson Street, London, E2 8JD
urls:
- http://hundredyearsgallery.co.uk/events/upcoming/
directory:
- venue
---
> Hundred Years Gallery has a strong commitment to presenting new composed, improvised and experimental music and sound works in an excellent acoustic setting. Weekly concerts feature international artists and the best of of the UK’s new and most creative musicians, low admission prices, and accessible to all.

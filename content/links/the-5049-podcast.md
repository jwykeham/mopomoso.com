---
title: The 5049 Podcast
where: Online
urls:
- https://podcasts.apple.com/us/podcast/5049-records/id645649986
directory:
- podcast
---
> The 5049 Podcast is a weekly interview show hosted by composer/clarinetist Jeremiah Cymerman, broadcast from the Lower East Side of New York City to the Web every Monday morning. Each week Jeremiah sits down with a musician to talk music, life, art, philosophy and shit. Lots of shit. Join us each Monday morning.

---
title: Jazz Rumours
when: First Sunday of the month.
where: 12 Pine Grove, Finsbury Park, N4 3LL
urls:
- https://www.facebook.com/people/Jazz-Rumours/100063933940221/
directory:
- gig
---
> North London's premium improvised music event

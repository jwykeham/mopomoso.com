---
title: Cafe Oto
where: 18-22 Ashwin Street, London, E8 3DL
urls:
- https://www.cafeoto.co.uk
directory: 
- venue
---
> Cafe OTO provides a home for creative new music that exists outside of the mainstream with an evening programme of adventurous live music seven nights a week.

---
title: BRÅK
where: waterintobeer, Shop Unit 2, Mantle Court, 209-211 Mantle Road, Brockley, London, SE4 2EW
urls:
- https://brakbrakbrak.co.uk
directory: 
- gig
---
> an improvised music series taking place at waterintobeer in Brockley, South East London, hosted by {{<artist "Cath Roberts">}}, {{<artist "Tom Ward">}} and {{<artist "Colin Webster">}}. Each night comprises three duo sets, in which the hosts are joined by guest musicians from the UK scene and beyond.

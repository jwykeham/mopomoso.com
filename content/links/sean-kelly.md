---
title: Seán Kelly
where: London
urls:
- https://www.seankellyphotos.com/
directory: 
- photographer
---
> Excellent photographer, who kindly lets us use some of his work for the Mopomoso website.

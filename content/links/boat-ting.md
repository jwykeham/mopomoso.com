---
title: Boat Ting
when: First Monday of the month.
where: Bar & Co, Temple Pier, Embankment, WC2R 2PN
urls: 
- http://www.boat-ting.co.uk/
directory: 
- gig
---
> London's hottest new music and poetry club of experimental music and poetry

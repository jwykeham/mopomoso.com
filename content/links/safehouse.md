---
title: Safehouse
urls: 
- https://safehousebrighton.co.uk/
when: First Wednesday of the month.
where: Rose Hill, 70-71 Rosehill Terrace, Brighton BN1 4JJ
directory: 
- gig
---
> A regular open session, which is open to all musicians of all levels to improvise, sonically, in a conversational way.

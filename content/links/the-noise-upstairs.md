---
title: The Noise Upstairs
urls:
- https://www.thenoiseupstairs.com/
where: Todmorden and Manchester
directory: 
- organisation
---
> The Noise Upstairs is a collective dedicated to the practice of improvised music. Based in Manchester.

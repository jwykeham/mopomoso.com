---
title: The Horse Improvised Music Club
when: Twice monthly on a Tuesday
where: IKLECTIK Art-Lab, Old Paradise Yard, 20 Carlisle Lane, SE1 7LG
urls:
- https://horseimprovclub.wordpress.com/
- https://www.facebook.com/thehorseimprovclub/
directory:
- gig
---
> The Horse Improvised Music Club was established in March 2012, when it held a series of gigs at The Horse pub in Waterloo, London SE1. The club is organised by {{<artist "Adam Bohman">}}, {{<artist "Sue Lynch">}} and {{<artist "Hutch Demouilpied">}}, who are all musicians on the London Improvised Music Scene.

---
title: Confront Recording
where: London, UK
urls:
- https://www.confrontrecordings.com/
- https://confrontrecordings.bandcamp.com/
directory:
- label
---
> Confront Recordings : proudly publishing adventurous music since 1996.

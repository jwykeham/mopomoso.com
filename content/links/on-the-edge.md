---
title: On The Edge (safehouse)
urls: 
- https://safehousebrighton.co.uk/
when: Last Wednesday of the month.
where: Rose Hill, 70-71 Rosehill Terrace, Brighton BN1 4JJ
directory: 
- gig
---
> Safehouse performance night

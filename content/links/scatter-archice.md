---
title: scatterArchive
where: Glasgow, UK
urls:
- https://scatterarchive.bandcamp.com/
directory: 
- label
---
> Digital record label, featuring release from Mopomoso regulars including Dave Tucker, Sylvia Hallett, Adam Bohman and many more.

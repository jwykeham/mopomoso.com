---
title: The Vortex Jazz Club
where: 11 Gillett Sqaure, London, N16 8AZ
urls:
- https://www.vortexjazz.co.uk/
directory:
- venue
---
> The Vortex Jazz Club is one of the UK’s premier jazz venues, programming almost 400 performances a year in an intimate space. We were winner of the Live Jazz Award category at the 2013 Parliamentary Jazz Awards.As a volunteer-led jazz club in North London, Dalston, we have been given the accolade of being one of the world’s best, and have even been singled out by the prestigious Downbeat magazine as one the top 150 jazz venues in the world.

---
title: "N.O. Moore"
name: "Moore, N.O."
date: 2024-03-27T09:02:40Z
categories:
- Artist
tags:
- "no-moore"
artists: "No Moore"
slug: no-moore
quotes:
- text: "Moore shifts fluidly from argumentatively fractured jazz licks to spacey atmospherics to mad cat hisses; the appositeness of his contributions belies the sparseness of his recorded discography"
  attribute: Bill Meyer, THE WIRE
- text: "Moore unpacks an impressive bag of tricks."
  attribute: Daniel Spicer, JAZZWISE
- text: "Guitarist N.O. Moore would likely attract some attention in any fit company, for he brings a highly personal conception to an instrument often sullied by redundancy."
  attribute: Stuart Broomer, FREEJAZZBLOG
links:
- https://dxdyrecordings.com
featured_image:
  path: images/img_4860LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- guitar
---
**N.O. Moore** is an electric guitarist with a parallel interest in electronics and drum machines. As an improviser, he has played with people such as Eddie Prévost, John Butcher, Rachel Musson, John Edwards, Sue Lynch, Alan Wilkinson, Steve Noble, and Steve Beresford. He can now be heard on a number of recordings, including Nous (with Prévost and Jason Yarde) on Matchless, and The Secret Handshake with Danger (with Henry Kaiser, Binker Golding, Olie Brice, and Prévost) on 577.
<!--more--> 
He has recently launched the DXDY Recordings label to present improvised and electronic musics: dxdyrecordings.com

Moore is interested in the relationship between automation and autonomy, and how this affords fabrications of human sensibility and affect. His first album of purely electronic music will be released by Orbit577 later in 2021.



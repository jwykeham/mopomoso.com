---
title: "Alex Ward"
name: "Ward, Alex"
date: 2024-07-25T07:58:36+01:00
categories:
- Artist
tags: 
- "alex-ward"
artists: "Alex Ward"
instruments:
- clarinet
- guitar
---
**Alex Ward** is a composer, improviser and performing musician (primarily on clarinet and guitar) based in London. He leads various ensembles under the heading of the Item Series to perform his compositions for improvising musicians, and he is a member of many other groups including legendary rock band Pere Ubu.
<!--more-->

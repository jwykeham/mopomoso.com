---
title: "Adam Bohman"
name: "Bohman, Adam"
date: 2023-08-01T20:09:41+01:00
categories:
- Artist
tags: 
- "adam-bohman"
artists: "Adam Bohman"
featured_image: 
  path: images/IMG_7924LR_3000x2000-X3.jpg
  credit: Seán Kelly
links:
- https://adambohman.bandcamp.com/
instruments:
- home-built instruments
- found objects
- tape cut-ups
---
**Adam Bohman** has been operating on the outer fringes of underground music for decades. 

Working with home-built instruments, found objects, tape cut-ups, collages, ink drawings and graphic scores. Favouring acoustic sounds over electronics, he explores the minute tendrils of sounds coaxed from any number of non-musical instruments and objects. 
<!--more-->
He is a member of British experimental groups, Morphogenesis, The Bohman Brothers, Secluded Bronte, and The London Improvisers Orchestra. 

Adam's music is unique and experimental, incorporating Fluxus japery, musique concrete, sound poetry and free improvisation.



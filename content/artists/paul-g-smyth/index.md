---
title: "Paul G Smyth"
name: "Smyth, Paul G"
date: 2023-07-22T15:58:51+01:00
categories:
- Artist
tags: 
- "paul-g-smyth"
artists: 
- Paul G Smyth
- Paul G. Smyth
instruments:
- piano
featured_image:
  path: images/IMG_2108LR_2000x3000-X3.jpg
  credit: Seán Kelly

---
Paul G Smyth is Ireland’s leading improviser where he has played and toured with a.o {{<artist "Charles Gayle">}}, {{<artist "Derek Bailey">}}, {{<artist "Keiji Haino">}}, {{<artist "Evan Parker">}}, {{<artist "Barry Guy">}} and {{<artist "Damo Suzuki">}}.

Outside of improvisation, his group The Jimmy Cake was described in The Irish Times as being "the most powerful musical force in Ireland". 

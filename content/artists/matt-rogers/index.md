---
title: "Matt Rogers"
name: "Rogers, Matt"
date: 2024-06-27T09:56:22+01:00
categories:
- Artist
tags: 
- "matt-rogers"
artists: "Matt Rogers"
instruments:
- voice
links:
- https://www.youtube.com/watch?v=bmo4lO_5Eps
---
**Matt Rogers** is a British composer who has written for and with a host of leading instrumentalists, ensembles and technologists, creating music and installations for concert halls, theatres and galleries.
<!--more-->
He studied for his Masters Degree with Gordon McPherson at the Royal Scottish Academy of Music and Drama (now the Royal Conservatoire of Scotland), graduating in 2001, and continued his education with Oliver Knussen and Magnus Lindberg at the Britten-Pears School for Advanced Musical Study.

Matt has received the Paul Hamlyn Foundation Award for Composers and has been Artist in Residence at the Southbank Centre and Tokyo Wonder Site. He was also the first composer to be commissioned by Art on the Underground.

Other commissions include The Virtues of Things for Royal Opera, Aldeburgh Music and Opera North, and La Celestina, an opera installation for Opera Erratica commissioned by the New York Metropolitan Museum of Art.

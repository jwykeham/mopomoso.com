---
title: "Roger Turner"
name: "Turner, Roger"
date: 2024-03-27T09:02:28Z
categories:
- Artist
tags: 
- "roger-turner"
artists: "Roger Turner"
featured_image:
  path: images/IMG_4502LR_2000x3000-X3.jpg
  credit: Seán Kelly
instruments:
- percussion
---
**Roger Turner** has been working as an improvising percussionist since the early 1970’s, collaborating in numerous established and ad hoc configurations.
<!--more-->
Solo work, work with electro-acoustic ensembles & open--form song, extensive work with dance and visual artists, plus specific jazz-based ensembles have brought collaborations with the most interesting european & international musicians and performers from Annette Peacock to Phil Minton, Cecil Taylor to Masahiko Satoh, Charles Gayle to Lol Coxhill, Derek Bailey to Otomo Yoshihide,  Alan Silva to Keith Rowe, Josef Nadj to Min Tanaka, Toshinori Kondo to Axel Dorner, etc. etc.

He has toured and played concerts worldwide from Sydney to the Arctic, Tokyo to Belfast, New York to Beirut.

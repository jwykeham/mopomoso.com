---
title: "Maggie Nicols"
name: "Nicols, Maggie"
date: 2023-08-01T20:10:21+01:00
categories:
- Artist
tags: 
- "maggie-nicols"
artists: "Maggie Nicols"
links:
- https://www.cafeoto.co.uk/shop/maggie-nichols-creative-contradiction-poetry-story/
featured_image:
  path: images/IMG_2298LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- voice
---
**Maggie Nicols** joined London's legendary {{<artist "Spontaneous Music Ensemble">}} in 1968 as a free improvisation vocalist. 

She then became active running voice workshops with an involvement in local experimental theatre. 

She later joined the group Centipede, led by {{<artist "Keith Tippets">}} and in 1977, with musician/composer {{<artist "Lindsay Cooper">}}, formed the remarkable {{<artist "Feminist Improvising Group">}}. 
<!--more-->
She continues performing and recording challenging and beautiful work, in music and theatre, either in collaborations with a range of artists ({{<artist "Irene Schweitzer">}}, {{<artist "Joelle Leandre">}}, {{<artist "Ken Hyder">}}, {{<artist "Caroline Kraabel">}}) as well as solo.


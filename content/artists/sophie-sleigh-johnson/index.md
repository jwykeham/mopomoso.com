---
title: "Sophie Sleigh-Johnson"
name: "Johnson, Sophie Sleigh"
date: 2024-06-27T09:56:08+01:00
categories:
- Artist
tags: 
- "sophie-sleigh-johnson"
artists: "Sophie Sleigh-Johnson"
links:
- https://www.sophiesleigh-johnson.co.uk/
instruments:
- voice
- performance
---
Sophie Sleigh-Johnson is a Southend-on-Sea based writer, artist and performer.
<!--more-->
Her ongoing investigations concern the magico-materialist temporality of place and the esoteric ground rock of comedy, in work distributed across text, cassette, sonic environments, lino prints and local newspapers.

She is currently working on a book about landscape as the mythic substrate of the sitcom, read at the intersection of the arcane and the post-industrial.

Her latest release is the cassette album Nuncio Ref! released by Crow Versus Crow.

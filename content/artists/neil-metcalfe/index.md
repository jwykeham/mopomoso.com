---
title: "Neil Metcalfe"
name: "Metcalfe, Neil"
date: 2023-07-29T06:33:46+01:00
categories:
- Artist
tags: 
- "neil-metcalfe"
artists: "Neil Metcalfe"
instruments:
- flute
featured_image:
  path: images/IMG_8045LR_3000x2000-X3.jpg
  credit: Seán Kelly
---
**Neil Metcalfe** (flute), has been active on the London improvising scene since the 70’s.
<!--more-->
He has played with, amongst many others:

{{<artist "Evan Parker">}}, {{<artist "Roscoe Mitchell">}}, {{<artist "Paul Dunmall">}}, {{<artist "Lol Coxhill">}} and {{<artist "Tony Marsh">}} and was a member of the 
{{<artist "Spontaneous Music Ensemble">}}, considered by many to be the flagship European Improvising Ensemble.

His current output includes CDs with {{<artist "Paul Dunmall">}}, {{<artist "Alison Blunt">}}, {{<artist "Hanna Marshall">}} and the {{<artist "Runcible Quintet">}}.

---
title: "Harry Gilonis"
name: "Gilonis, Harry"
date: 2023-10-01T08:43:06+01:00
categories:
- Artist
tags: 
- "harry-gilonis"
artists: "Harry Gilonis"
links:
- https://www.veerbooks.com/Harry-Gilonis-eye-blink
---
Harry is a poet, editor, publisher and critic writing on art, poetry and music. 

Recent books include *Forty Fungi*, poems about common English mushrooms illustrated by the artist Erica van Horn (Coracle Press) and *eye-blink*, from Veer Books (deliberately ‘faithless’ translations of T’ang Dynasty Chinese poetry). 

He is the only living poet published on the lawn of London’s Serpentine Gallery.

---
title: "Viv Corringham"
name: "Corringham, Viv"
date: 2023-07-27T17:17:05+01:00
categories:
- Artist
tags: 
- "viv-corringham"
artists: "Viv Corringham"
featured_image: images/viv-corringham-cropped.webp
links:
- http://www.vivcorringham.org/
- https://www.thewire.co.uk/audio/tracks/an-audio-introduction-to-viv-corringham
quotes:
- text: "a vocalist of stunning virtuosity"
  attribute: Louise Gray, The WIRE
- text: "Ululating, humming and chanting to produce not language but a present soundscape"
  attribute: The WIRE
instruments: 
- voice
- electronics
discogs_id: 924071
---
**Viv Corringham** (voice) is a US based British vocalist and sound artist, who has been described as "a vital force in improvised music since the late 1970s" (Corey Mwamba, BBC Radio 3). 

She has been cutting her own distinctive path ranging across free improvisation, Greek Rembetika, soundscapes and spontaneously created songs. Work includes concerts, soundwalks, workshops and installations. She is a certified teacher of Deep Listening, having studied with composer {{<artist "Pauline Oliveros">}}, and holds an MA in Sonic Art from Middlesex University, London. 
<!--more-->
Awards include two McKnight Composer Fellowships through the American Composers Forum. Her work has received international recognition and been presented in venues in 26 countries, including Fonoteca Nacional de Mexico, Issue Project Room New York, Hong Kong Arts Centre, Onassis Centre Athens, Institute of Contemporary Art London, Serralves Contemporary Art Museum Portugal and Tempo Reale Festival Florence. 

Articles about her work appear in many books and publications. 





---
title: "mcddavid"
name: "Mcddavid, "
date: 2024-05-27T07:25:17+01:00
categories:
- Artist
tags: 
- "mcddavid"
artists: "mcddavid"
---
Hanover based **mcddavid** is part of the Mopomoso Team. As an improviser mcddavid developed techniques without touching the instrument. He makes chaos poems and subversive performances with balloons and pipe-cleaners. He has composed two operas involving audience participation for Mopomoso.
<!--more-->

---
title: "Alati"
name: "Alati, "
date: 2024-06-27T09:55:41+01:00
categories:
- Artist
tags: 
- "alati"
artists: "Alati"
links:
- https://www.youtube.com/watch?v=2VLfd8hcEFY
---
**Alati** is a trio led by Norwich-based trumpeter Chris Dowding, and featuring the wonderful vocals of Brigitte Beraha and the versatile piano playing of Dave O'Brien. They have performed around the UK and released their debut album, 'Ascending the Morning' on the Luton-based 33Jazz label.
<!--more-->
They will be performing settings of poems by Colin Simms about the hen harrier, and the tour promotes their debut release of this material, the EP 'Wonderstruck', which features three bucolic songs woven together with improvisation, alongside three-part harmony and more...

Individually, the three members are known nationally and internationally for their own projects as leaders, including Brigitte Beraha's Lucid Dreamers, Dave O'Brien's work with his band Porpoise Corpus, and Chris' Moonrise Trio.

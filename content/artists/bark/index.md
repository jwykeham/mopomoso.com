---
title: "BARK!"
name: "BARK!"
date: 2024-05-27T07:24:41+01:00
categories:
- Artist
tags: 
- "bark"
artists: "BARK!"
instruments:
- guitar
- percussion
- sampler
---
**BARK!** comprises Rex Caswell on guitar,  Phillip Marks on percussion and Paul Obermayer on sampler.
<!--more-->
Energy Music for and from electronics, the BARK! trio proves that exemplary free sounds are more the product of commitment and conception than instrumentation. Manchester-based, the group has gone through various line-ups over the years with percussionist Phillip Marks and guitarist Rex Caswell constant. Sampler player Paul Obermayer, also a member of FURT, joined the band in 1999, and his swift, beat-oriented electronic riffs banish any processed preciousness from the interface.

Like most cooperative groups, Bark's whole is greater than its parts. Although Marks' influences are Jazz, Caswell's Pop-Rock and Obermayer's notated music – and all have flirted with reductionism. Confirming the trio's populism, its name references a neighbourhood in Manchester, UK. Marks also works with pianist Stephen Grew and reedist Mick Beck, while Caswell often collaborates with choreographer Lene Boel.

With stentorian sequences of motor-driven buzzes, signal-processed ricochets and quivering reverb, the three create the sort of collective improvisational polyphony that usually results from Free Jazz's expected lung and muscle power. Yet here, alongside textures that resembles a soundtrack of breaking glass and car crashes, Marks' clip-clopping drum beats are still heard as are Caswell's string slaps.  Alongside Obermayer's envelopes of droning vibrations, ring modulator-like gongs and flanged, vibraharp-like pulses from Marks' percussion is Caswell repetitively strumming what could be an oversized rubber band. 

Collective sound creation can be more delicate as well, as when the guitarist's closely miked licks squeak and resonate, or the percussionist sounds as if he's slapping a plastic swizzle stick on his drum tops as he rattles and rubs the instruments' wood and rims. Plus the three are canny is their use of deliberate pauses, hollow tunnel echoing patterns or precise trimbral placement.  Instantaneous polyrhythmic responses from Marks underline many of these outbursts.  - (by Ken Waxman).

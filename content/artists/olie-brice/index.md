---
title: "Olie Brice"
name: "Brice, Olie"
date: 2024-04-25T09:39:32+01:00
categories:
- Artist
tags: 
- "olie-brice"
artists: "Olie Brice"
instruments:
- Double Bass
featured_image:
  path: images/IMG_3803LR_2000x3000-X3.jpg
  credit: Seán Kelly
links:
- https://oliebrice.com/
---
**Olie Brice** is a double bassist, improviser and composer. Raised in London and Jerusalem, he now lives by the sea in Hastings.
<!--more-->
Olie Brice leads and composes for two groups, a trio (with Tom Challenger & Will Glaser) and an Octet (with Alex Bonney, Kim Macari, Jason Yarde, Rachel Musson, George Crowley, Cath Roberts & Johnny Hunter). Both of these groups were featured on the critically acclaimed double album ‘Fire Hills’. Previously Brice lead a quintet – “one of the most interesting and satisfying bands on the current UK scene” – which released two albums, ‘Immune to Clockwork’ and ‘Day After Day’.  He has also composed a piece for improvising string quartet, ‘From the Mouths of Lions’, which will be released in 2024.

Brice is a committed free improviser, who has performed, toured and recorded with many of the leading names in the music. Frequent collaborators include Mark Sanders, Paul Dunmall, Rachel Musson, Tobias Delius, Cath Roberts and Luis Vicente, and he has also appeared with the likes of Evan Parker, Tony Malaby, John Butcher, Ingrid Laubrock, Ken Vandermark, Eddie Prevost and Louis Moholo. 

Brice is also in demand as a bass player in creative ensembles led by many artists, including Dee Byrne’s Outlines and Out Front. He regularly performs at venues and festivals across Europe. Brice has been the recipient of Arts Council England funding multiple times and in 2021 received a composition commission from Jazz South.




---
title: "Milo Fell"
name: "Fell, Milo"
date: 2024-04-28T07:31:26+01:00
categories:
- Artist
tags: 
- "milo-fell"
artists: "Milo Fell"
instruments:
- drums
links:
- https://www.milofell.com/
featured_image: 
  path: images/milo-fell.jpeg
  credit: Mike Stemberg
---
**Milo Fell** lived and played in the Manchester for seven years, playing with many local bands and musicians such as John Ellis' Big Bang, Jon Thorne, Pocket Central, Isthmus and Rare Birds. He also played with some of the best jazz soloists in the UK -  Don Weller, Bobby Wellins, Peter King, Alan Barnes and Jean Toussaint.
<!--more-->
He moved to London in 1999 to play for a week at Ronnie Scott's Club with saxophonist Tim Whitehead
He played and recorded with The Cinematic Orchestra, the great US jazz singer Mark Murphy, nuevo tango group Tango Siempre, Tony Woods Project, Italian pianist Giovanni Mirabassi,  children's poet Laureate Michael Rosen, Nostalgia 77, Julie Sasoon and singers Juliet Kelly and Karen Lane amongst many more. He has toured a lot  - gigs in Russia and Poland with Dutch based group Dalgoo, and gigs in jazz clubs, festivals and concert halls of Europe

After moving to Brighton in 2013, he became immersed in the thriving local jazz scene, playing regularly with Sussex's finest, including Terry Seabrook, Mark Bassey, John Donaldson, Gabriel Garrick and Jack Kendon. He plays with The Mingus Underground Octet (Charles Mingus tribute), Terry Pack's Trees (unfeasably large ensemble), Rabbit Foot (afro-blues duo) as well as continuing to play gigs in London and beyond.

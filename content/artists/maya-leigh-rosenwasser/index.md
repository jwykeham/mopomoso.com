---
title: "Maya-Leigh Rosenwasser"
name: "Rosenwasser, Maya-Leigh"
date: 2024-03-05T09:33:56Z
categories:
- Artist
tags: 
- "maya-leigh-rosenwasser"
artists: "Maya-Leigh Rosenwasser"
links:
- https://www.mayaleighrosenwasser.com/ 
- https://flxnflx.bandcamp.com/track/live-in-an-attic
instruments:
- piano
featured_image:
  path: images/e23e9e_2332588ca63b4a3f90d4112d969aebee~mv2-cropped.webp
---
**Maya-Leigh Rosenwasser** (she/they) is a performer, improviser and composer and researcher specialising in performing and composing new works for piano (solo and chamber), live electronics and multi-disciplinary ensembles. 

Notable performances, compositions and broadcasts include *Huddersfield Contemporary Music Festival 2023*, *BBC Radio 3's Freeness: Circular Wanderings* 'Harbringers of Change'-  Cafe Oto (One Orchestra New performance), Made at the Red House residency with Wild Plum Arts (**PLASTIC BODIES**), and residency and performance with experimental duo **flxnflx** at Kirkos Ensemble's Unit 44. 
<!--more-->

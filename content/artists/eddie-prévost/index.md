---
title: "Eddie Prévost"
name: "Prévost, Eddie"
date: 2023-10-01T06:19:40+01:00
categories:
- Artist
tags: 
- "eddie-prévost"
artists: "Eddie Prévost"
links:
- https://matchlessrecordings.com/
- url: https://en.wikipedia.org/wiki/Eddie_Pr%C3%A9vost
  name: https://en.wikipedia.org/wiki/Eddie_Prévost
featured_image:
  path: images/IMG_7261LR_3000x2000.jpg
  credit: Seán Kelly
instruments:
-  percussion
---
Drummer Eddie Prévost was born on June 22, 1942 in England. As a teenager he played drums in Skiffle and trad jazz bands before showing signs of the creative musician that was forming. 

After his initial exposure to his greatest influences Max Roach and Ed Blackwell, Prévost became fascinated with the endless possibilities of improvisation. He began incorporating all sorts of non-traditional percussion with his standard drum kit. In 1965 he co-founded the English improv ensemble AMM with saxophonist Leslie Gare and guitarist Keith Rowe. He also began recording with free jazz musicians Evan Parker, Marilyn Crispell, and Paul Rutherford. 

In the '70s and '80s, Prévost led his own quartet and recorded with Supersession, Resoundings, and Free Jazz Quartet. By 1990 he had formed the Masters of Disorientation with AMM alumni Gare and Rowe while also entering the techno/ambient realm, performing with GOD, Main, and EAR. 

When he isn't performing with any combination of these ensembles, Prévost conducts workshops, gives lectures, and writes about improv for several magazines. He also runs his record label, Matchless Records.


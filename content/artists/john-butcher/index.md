---
title: "John Butcher"
name: "Butcher, John"
date: 2024-01-02T10:45:30Z
categories:
- Artist
tags: 
- "john-butcher"
artists: "John Butcher"
instruments: 
- saxophone
links:
- https://johnbutcher.org.uk
featured_image:
  path: images/IMG_5138LR_3000x2000-X3.jpg
  credit: Seán Kelly
---
Born in Brighton and living in London, **John Butcher** is a saxophonist whose work ranges through improvisation, his own compositions, multitracked pieces and explorations with feedback, unusual acoustics and non-concert locations. He is well known as a solo performer who attempts to engage with a sense of place. 
<!--more-->
Resonant Spaces, for example, is a collection of performances recorded during a tour of unusual locations in Scotland and the Orkney Islands.

Since 1982 he has collaborated with hundreds of artists, to name a few: Derek Bailey, Akio Suzuki, John Stevens’ Spontaneous Music Ensemble, Rhodri Davies, Last Dream of the Morning (with John Edwards & Mark Sanders), Steve Beresford, Matthew Shipp, Gerry Hemingway, Chris Burn, Magda Mayas, Gino Robair, Thermal (Andy Moor & Thomas Lehn), Christian Marclay, Eddie Prévost, Okkyung Lee, John Russell, Ståle Liavik Solberg and Phil Minton.
Compositions have been varied. Three for his own large ensembles at HCMF, two saxophone quartets, a piece for Futurist Intonarumori, Tarab Cuts (based on early Arabic recordings), and pieces for the London Sinfonietta and CEPRO (created for a XVI convent in Mexico City).

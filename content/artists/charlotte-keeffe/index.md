---
title: "Charlotte Keeffe"
name: "Keeffe, Charlotte"
date: 2023-01-16T17:05:46Z
categories:
- Artist
tags: 
- "charlotte-keeffe"
artists: charlotte keeffe
featured_image: images/charlotte-keeffe-cropped.webp
links:
- https://www.charlottekeeffe.com
instruments:
- trumpet
- flugelhorn
---
British musician, trumpeter and flugelhorn player **Charlotte Keeffe** wears her love for free improvisation, jazz and experimental music on her sleeve. Whether performing regularly as a soloist (Sound Brush), or leading a variety of different ensembles, including her Right Here, Right Now Quartet, she carefully carves out spaces for the free movement of ideas and individual expression.
<!--more-->
 
Inspired by painters, Charlotte refers to her instruments as "Sound Brushes". Her debut album "Right Here, Right Now" is where you’ll find her exhibiting a passion for vibrant soundscapes rendered in live spaces. Released in 2021 on Discus Music, Charlotte earned critical acclaim and carved out a niche on the imprint, also composing and performing for a number of the roster’s artists including {{<artist "Hi Res Heart">}}, French singer {{<artist "Carla Diratz">}}, {{<artist "Julie Tippetts">}} and {{<artist "Anthropology Band">}}, with Jazz FM's Gold Award winner {{<artist "Orphy Robinson">}} MBE among others. 

To date, her music has been featured on Jazz FM, BBC Radio 3 and BBC Radio 6, where she's been described as a 'prolific', 'dynamic' and 'excellent improviser!', by the likes of {{<artist "Corey Mwamba">}}, {{<artist "Stuart Maconie">}} and {{<artist "Jez Nelson">}}. She is a Serious Artist and part of the Take Five 2022 cohort. Charlotte performed a duet with the mighty City of London as part of world renowned trumpeter {{<artist "Dave Douglas">}}' Festival of New Trumpet Music 2022.

Harnessing the power of art for social change is a crucial part of Charlotte’s musical identity: she has served as Assistant Musical Director of the London Gay Big Band, champions gender and diversity equality, as part of the Parliamentary award-winning Women in Jazz Media team, and played in {{<artist "Marin Alsop">}}'s Taki Concordia Orchestra at the World Economic Forum 2019, in front of world leaders and celebrities including Sir David Attenborough.

From stepping on Glastonbury and Wilderness stages alongside {{<artist "Charlotte Church">}}, {{<artist "Laura Mvula">}} and {{<artist "Kate Nash">}}, to broadcasting to an international audience live from her bathroom during the global pandemic, Charlotte understands that embracing individuality and letting go of inhibitions is the surest way to grasp the transformative power of music.


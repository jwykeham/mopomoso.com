---
title: "Dominic Lash"
name: "Lash, Dominic"
date: 2024-03-27T09:02:51Z
categories:
- Artist
tags: 
- "dominic-lash"
artists: "Dominic Lash"
links: 
- https://dominiclash.blogspot.com/
- https://dominiclash.bandcamp.com/
featured_image:
  path: images/IMG_2347LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- Double Bass
- guitar
---
**Dominic Lash** is a freely improvising double bassist and electric guitarist who the Free Jazz Collective has called an "exceptional creative musician". He also both writes and performs composed music and, as an academic, researches and writes about film.
<!--more-->
He plays or has played with musicians such as John Butcher, Joe Morris, Evan Parker, Pat Thomas, and Nate Wooley, and performed with the late Tony Conrad, Steve Reid, and John Russell.

Among his most regular collaborators are Angharad Davies, Matthew Grigg, Tim Hill, Josh Sinton, and Alex Ward. 

Solo double bass pieces have been written for him by composers including Jürg Frey, Richard Glover, and Éliane Radigue. 

He has also performed with ensembles including Apartment House, the Bozzini Quartet, and Parkinson Saunders.
Lash has performed in the UK, Austria, Canada, Finland, France, Germany, the Netherlands, Norway, Portugal, Spain, Switzerland, Turkey, and the USA.

For nearly a decade he was based in Oxford and played a central role in the activities of Oxford Improvisers; much of 2011 was spent living in Manhattan. He was based in Bristol from 2011 to 2022, where he was involved in organising concerts under banners including Bang the Bore, Insignificant Variation, and Several 2nds. 

Since the beginning of 2022 he has been based in Cambridge, where he organizes the Soundhunt improvised music series, with the guitarist N.O. Moore, on the last Sunday of every month at Thrive on Norfolk Street.
In 2015 he started a digital label for some of his more uncategorisable work, called Spoonhunt. 

---
title: "Benedict Taylor"
name: "Taylor, Benedict"
date: 2024-06-27T09:56:46+01:00
categories:
- Artist
tags: 
- "benedict-taylor"
artists: "Benedict Taylor"
instruments:
- viola
links:
- https://benedicttaylormusic.com/
- https://www.youtube.com/watch?v=1A9WAzmuOg8&t=9s
featured_image:
  path: images/IMG_1000LR_3000x2000-X3.jpg
  credit: Seán Kelly
---
**Benedict Taylor** is a leading figure within contemporary composition, modern string performance and improvised music in the British and European new music world. The central focus of his work is in new composition for live performance, film, theatre, contemporary dance, art installation and electro-acoustic composition. In performance, he predominantly works within improvisation, new composition and 20th/21st century music.  
<!--more-->
Described as a virtuoso violist,  through his performance work there is a focus on solo performance as a creative and investigative process. In 2012 he initiated an ongoing commission series and solo label Subverten (For Viola Solo). The first works premiered in autumn 2013 and continue several times each year.  

An award winning composer for film, theatre and television, he has composed for over 35 feature films, shorts and television/online series with his work showing at many leading international film festivals and theatre venues worldwide.

As an improviser he has performed and recorded closely with many artists through the UK, EU, North America and Asia, including   Keith Tippett, Evan Parker, Terry Day, Lauren Kinsella, Lawrence Upton, Alex Ward, Cath Roberts, Tom Jackson, Renee Baker, Paul Dunmall, Jean-Michel Van Schouwburg, Dirk Serries, Steve Beresford, Angharad Davies, Neil Luck, John Edwards, Ivor Kallin, Anton Mobin, Hannah Marshall, David Leahy, Adam de la Cour, Alison Blunt, Chris Cundy, Daniel Thompson, Kit Downes, Yves Charuest, Jost Drasler, Alexander Hawkins, Tom Challenger, Miya, Tetsu Saito, Erika Sofia Sollo, Gianni Mimmo, Stephen Crowe, Marcello Magliocchi.

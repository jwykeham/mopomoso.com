---
title: "John Bisset"
name: "Bisset, John"
date: 2023-07-09T14:55:33+01:00
categories:
- Artist
tags: 
- "john-bisset"
artists: john bisset
instruments:
- guitar
- voice
featured_image:
  path: images/IMG_4956LR_2000x3000-X3.jpg
  credit: Seán Kelly
---
Guitarist, vocalist, improviser and composer, organiser of structures for improvising within (Relay), and film maker.
<!--more-->
Improvising and composing with the Manchester Musicians Collective in 1976 and ever since; from 2009 he made films, including collaborations with {{<artist "Ivor Kallin">}} as 213TV, and in 2022 returned to live playing; improvising on lap steel & voice, working with {{<artist "Iris Colomb">}}, {{<artist "Andrew Ciccone">}}, {{<artist "Ed Shipsey">}}, {{<artist "Matt Atkins">}}, and other more recent arrivals on the improvised music scene, mostly emanating from {{<artist "Rick Jensen">}}'s SKRONK open sessions; as well as old acquaintances {{<artist "Jem Finer">}}, {{<artist "Phil Durrant">}}, {{<artist "Chris Cundy">}}, {{<artist "Alex Ward">}}, the London Improvisers Orchestra and {{<artist "Fara Afifi">}}'s Noisy People's    Improvising Orchestra.              

---
title: "Colin Somervell"
name: "Somervell, Colin"
date: 2024-07-25T07:59:04+01:00
categories:
- Artist
tags: 
- "colin-somervell"
artists: "Colin Somervell"
instruments:
- double bass
- bass
links:
- https://music.metason.net/artistinfo?name=Colin%20Somervell

---
**Colin Somervell** is as happy performing melodic free improvisation or playing in neo rembetica bands (Trio Tekke) and is also in demand as a free jazz musician.
<!--more-->
He shows an interest in both lyrical and free/collective improvisation, with an approach that uses technique and conceptual approaches in accessible and embraceable ways.

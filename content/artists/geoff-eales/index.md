---
title: "Geoff Eales"
name: "Eales, Geoff"
date: 2023-10-01T08:44:06+01:00
categories:
- Artist
tags: 
- "geoff-eales"
artists: "Geoff Eales"
links:
- https://en.wikipedia.org/wiki/Geoff_Eales
featured_image:
  path: images/IMG_4526LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- piano
---
Born in Aberbargoed, pianist, composer and improviser Geoff Eales has enjoyed an incredibly varied music career. At Cardiff University he studied composition with Alun Hoddinott and piano with Martin Jones. He was awarded a Ph.D for his large-scale work An American Symphony and his setting of Dylan Thomas’ poem In the Beginning for tenor, horn and piano. Geoff has also written a piano concerto, string quartet, brass quintet and A Sussex Rhapsody, the latter commissioned by the BBC.

As well as his love of classical music, Geoff is passionate about jazz and has 13 highly acclaimed albums to his name. He has performed at many of the world’s leading concert halls, festivals and jazz clubs. Geoff is currently writing a musical depicting the rise and fall of the Welsh coal mining Industry.

 In recent years he has developed a passion for the written word, his writing manifesting itself in the form of song lyrics, poems, satire and musical criticism.




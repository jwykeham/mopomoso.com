---
title: "Luft"
name: "Luft, "
date: 2024-08-26T10:06:34+01:00
categories:
- Artist
tags: 
- "luft"
artists: "Luft"
links:
- https://scatterarchive.bandcamp.com/album/luft
instruments:
- clarinet
- ukulele
- saxes
- guitar
---
Over the years {{<artist "Chris Hill">}}, {{<artist "Alan Newcombe">}} and {{<artist "James O'Sullivan">}} have played as regulars at Eddie Prevost's Workshop and as performers in their own right on the London improvisation and sound art scene. More recently pursuing their own brand of maximal improvisation as **LUFT**, the trio thrives on a no compromise approach that may lurch between cinematic rumblings, fractured keening, or melancholic disarray at the drop of a hat.
<!--more-->


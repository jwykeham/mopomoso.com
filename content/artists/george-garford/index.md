---
title: "George Garford"
name: "Garford, George"
date: 2023-07-09T14:51:59+01:00
categories:
- Artist
tags: 
- "george-garford"
artists: george garford
instruments:
- saxophone
featured_image:
  path: images/IMG_3745LR_3000x2000-X3.jpg
  credit: Seán Kelly
---
George is a saxophonist, composer and improviser active in the UK's contemporary jazz and free improvisation scenes.
<!--more-->
George leads and writes for his working band Ideasthesia and co-leads experimental double guitar + saxophone trio Solmonath.

Hailing from Cambridgeshire, UK and now based in London, George is quickly making a name for himself as a prolific and reliable collaborator and sideman. {{<artist "Julien Durand">}}'s 'Dreamscapes', {{<artist "Jonno Gaze Quintet">}}, {{<artist "Myra Brownbridge Quartet">}}, {{<artist "Olivia Murphy Jazz Orchestra">}}, {{<artist "Sam Eastmond">}}, {{<artist "John Bisset">}}, {{<artist "Cameron Scott">}}.

He can regularly be heard playing a diversity of music(s) around the UK, at venues including The Vortex, Cafe OTO, The Jazz Cafe Camden, Oliver's Jazz Bar, Birmingham Symphony Hall and Ronnie Scott's Jazz Club.

Having developed an intricately personal voice as an altoist, George approaches the saxophone with a 'free' sensibility - embracing evocative cries, husky noise and dissonance in a variety of playing contexts. 

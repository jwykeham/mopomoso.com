---
title: "Simon McCorry"
name: "McCorry, Simon"
date: 2023-07-09T14:31:32+01:00
categories:
- Artist
tags: 
- "simon-mccorry"
artists: simon mccorry
links:
- https://linktr.ee/simonmccorry
instruments:
- cello
---
Simon has been creating music and sound design for theatre & contemporary dance for 20 years years, working with companies such as Headlong, Old Vic Productions, Tiata Fahodzi, House of Absolute, Javaad Alipoor Company & Fabula Collective. 
<!--more-->
He is an accomplished cellist and uses the instrument in his own work often processing & combining it with electronics. 

More recently he has concentrated on his own solo output with performance & recording under his own name. His work has been described as "electro-orchestral drone-scapes of, by turns, gauzy intimacy and soaring grandeur", "highly evocative work of the otherworldly transforms the recognizable into something mysterious, even on occasion, the supernatural". 

Being of mixed British/ Indian ethnicity & recently diagnosed with epilepsy these experiences inform & guide his practice, exploring themes of otherness and fragility.

Simon's latest release, *Mouthful of Dust* is out on on **Polar Sea Recordings**: https://polarseasrecordings.bandcamp.com/album/mouthful-of-dust



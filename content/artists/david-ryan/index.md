---
title: "David Ryan"
name: "Ryan, David"
date: 2024-08-26T10:13:26+01:00
categories:
- Artist
tags: 
- "david-ryan"
artists: "David Ryan"
links:
- https://soundcloud.com/david-ryan
---
**David Ryan** is a visual artist and musician.  He has long been associated with experimental music and improvisation, having performed with many distinguished musicians including Anton Lukoszevieze, John Edwards, Ian Mitchell, Alberto Popolla, John Tilbury, Luca Venitucci, Gianni Trovalusci amongst others.
<!--more-->
In the 1990s/early 2000s he curated many concert series featuring both improvisors and composers, including Bunita Marcus, Kaija Saariaho, Ken Vandermark and many others as well as concert collaborations with Earle Brown, David Behrman, Phill Niblock and Christian Wolff.  In 2016 he was an Abbey Fellow at the British School at Rome, and in 2020 was a judge for the Franco Evangelisti prize in composition, Rome.  Recent pieces have been broadcast on Radiophrenia, CCA Glasgow and also at the Video Poetry festival, Athens (Institute of Experimental Arts). 

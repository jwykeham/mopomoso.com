---
title: "Phil Durrant"
name: "Durrant, Phil"
date: 2024-03-27T09:03:03Z
categories:
- Artist
tags: 
- "phil-durrant"
artists: "Phil Durrant"
links:
- https://www.youtube.com/watch?v=SK9d5ngKZd0
featured_image:
  path: images/IMG_9687LR-2_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- mandolin
- violin
---
Born near London in 1957, **Phil Durrant** is a multi-instrumentalist improviser/composer/sound artist who currently performs solo and group concerts.
<!--more-->
As an acoustic or electric mandolinist, he has been performing duos with guitarists Martin Vishnick, and Daniel Thompson. He also performs in trios with Maggie Nicols and Emil Karlsen, Olie Brice and Andrew Lisle as well as Marjolaine Charbin and Jackie Walduck.

Durrant still performs regularly with the acoustic/electronic group Trio Sowari (with Bertrand Denzler and Burkhard Beins) and Mark Wastell’s The SEEN as well as the international electronic ensemble MIMEO with Keith Rowe, Kaff Matthews, Thomas Lehn a.o. With Mark Wastell, Steve Beresford and David Toop, he is involved in a new audio/visual project celebrating the work of the celebrated Scottish artist, Alan Davie.

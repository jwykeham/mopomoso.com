---
title: "Jörg Hufschmidt"
name: "Hufschmidt, Jörg"
date: 2024-05-27T07:25:44+01:00
categories:
- Artist
tags: 
- "j%C3%B6rg-hufschmidt"
artists: "Jörg Hufschmidt"
instruments:
- guitar
---
The sound artist **Jörg Hufschmidt** (Hanover) is known for his experimental/artistic dialogue between painting, sound performance, drawing and music.
<!--more-->

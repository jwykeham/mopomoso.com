---
title: "Marion Treby"
name: "Treby, Marion"
date: 2023-08-01T20:10:38+01:00
categories:
- Artist
tags: 
- "marion-treby"
artists: "Marion Treby"
links:
- https://cambridgefolkclub.co.uk/
featured_image:
  path: images/L1290643.jpeg
  credit: Chris Freeman
instruments:
- piano
---
Marion Treby is involved in the organisation of the Cambridge Folk Club, but has been playing with {{<artist "The Noisy Women">}} for a number of years now.

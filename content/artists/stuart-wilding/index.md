---
title: "Stuart Wilding"
name: "Wilding, Stuart"
date: 2024-03-27T09:03:32Z
categories:
- Artist
tags: 
- "stuart-wilding"
artists: "Stuart Wilding"
links:
- https://stuartwilding.bandcamp.com/
featured_image:
  path: images/img_5399LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- percussion
---
**Stuart Wilding** is an improvising percussionist, playing a mixture of junk percussion, lap harp and cello. He is active in several current collaborations: Ghost Mind, Longstone, Lo-fi Ensemble, Mike Adcock duo, Pete Robson duo, Professori Sognare, Madrona, the Cheltenham Improvisers Orchestra and Chris Cundy’s Speed Hobby. He currently runs the Lo-fi Improvised music Workshops in Camberwell. In the past he has worked in a number of smaller improvising ensembles and with the Flying Down Trio, Hither Green Drone Orchestra and Makeshift. He was also a performer and musician with the Hand in Glove Performance Group.
<!--more-->

---
title: "Alan Tomlinson"
name: "Tomlinson, Alan"
date: 2023-09-09T07:29:23+01:00
categories:
- Artist
tags: 
- "alan-tomlinson"
artists: "Alan Tomlinson"
links: 
- https://britishmusiccollection.org.uk/composer/alan-tomlinson
instruments:
- trombone
featured_image:
  path: images/IMG_0365LR_3000x2000-X3.jpg
  credit: Seán Kelly
---
Alan has toured all over Europe and as far afield as North America and Siberia. 

His first solo album, Still Outside, came out in 1980 and several composers have written solo pieces for him. 

He currently works with his own trio with {{<artist "Dave Tucker">}} (guitar) and {{<artist "Phil Marks">}} (drums). He is a member of the contemporary music groups Sounds Positive and the New Wind Chamber Group.




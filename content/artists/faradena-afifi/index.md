---
title: "Faradena Afifi"
name: "Afifi, Faradena"
date: 2023-08-01T20:09:56+01:00
categories:
- Artist
tags: 
- "faradena-afifi"
artists: "Faradena Afifi"
featured_image: 
  path: images/IMG_0688LR_3000x2000-X3.jpg
  credit: Seán Kelly
links:
- https://faradenaafifi.bandcamp.com/album/flash-of-blue-ep
- https://www.youtube.com/watch?v=ipHvlLdVtQE
instruments:
- voice
- bowed string instruments
- piano
- percussion
---
Faradena Afifi: Founder of The Noisy Women Present, and The Noisy People’s Improvising Orchestra, Fara is also an Initiator, Connector and Performer. Faradena is a person with neurodiversity who has mixed Afghan/British heritage. She is a T’ai Chi Chuan practitioner/instructor, folk singer and improvising community musician who plays bowed string instruments, piano and percussion. She also specialises in healing music and T’ai Chi-based exercises for people with learning differences and brain injuries/conditions.

During lockdown 2020, through jamming online with {{<artist "Maggie Nicols">}}, Fara joined the Glasgow Improvisers Orchestra (GIO) and the Improvising Ensemble (IE). This led to performing with the London Improvisers Orchestra (LIO), and with Maggie Nicol’s Creative Liberation Orchestra in Stockholm 2021 and various musicians since. She co-leads the International Online Improvising Workshop with {{<artist "Tony Hardie Bick">}}, the online sister of The London Improvising Workshop, originally started by {{<artist "Eddie Prévost">}}.

When not teaching or performing on stage, she is out busking with Cambridge musician Banjo Nick.

---
title: "Sue Lynch"
name: "Lynch, Sue"
date: 2024-03-05T09:38:35Z
categories:
- Artist
tags: 
- "sue-lynch"
artists: "Sue Lynch"
links:
- https://bluetapes.bandcamp.com/album/blue-forty-seven.
instruments:
- Tenor saxophone
- Clarinet
- Flute

featured_image:
  path: images/img_1312LR_2000x3000.jpg
  credit: Seán Kelly
---
**Sue Lynch** studied fine art at Coventry College of Art. She currently runs **The Horse Improvised Music Club** with Adam Bohman and Adrian Northover.

Performs with **Adam Bohman**, **Eddie Prevost**, **Anna Homler**, **Richard Sanderson**, **Steve Noble**, **Crystabel Riley**, **Caroline Kraabel**, **Sharon Gal** and **N.O.Moore**.

Recently performed as part of **Tarek Atoui**'s *Reverse Collection* at The Tate Modern and performs with the Psychedelic Afro Beat Sudanese band **The Scorpios**.

Formed, *Paradise Yard* in 2016-an electro accoustic ensemble, featuring women improvisers, performing at Iklectik, Cafe Oto and The ICA.In 2018, performed at 3 Klange Tag Music &Word Festival, Switzerland with Hildegard Kleeb, at Womad BBC Stage and, Tusk Festival and Guess Who Festival (Utrecht) with The Scorpios. Solo performance at Pied Nu Festival, Le Havre 2019.
<!--more-->
Recent release "Secant/Tangent" (dxdyrecordings) with Crystabel Riley and Nathan Moore, "Blue Tapes-Minaru", with Anna Homler, Dave Tucker and Adrian Northover as featured in an interview in October 2023 Wire Magazine.


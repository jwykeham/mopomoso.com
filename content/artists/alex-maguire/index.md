---
title: "Alex Maguire"
name: "Maguire, Alex"
date: 2023-07-09T14:43:34+01:00
categories:
- Artist
tags: 
- "alex-maguire"
artists: alex maguire
instruments:
- piano

---
British pianist Alex Maguire remembers improvising on the piano before he received any lessons, which started at age eight. 
<!--more-->
He researched music on the College or university of London and obtained a BA. He also took lessons from {{<artist "Wanda Jeziorska">}}, {{<artist "Andrew Ball">}}, and {{<artist "Howard Riley">}}. 

He brands as his motivation several respected figures within the free improvising scene, such as {{<artist "Tony Oxley">}} and {{<artist "Evan Parker">}}, in addition to jazz players {{<artist "Cecil Taylor">}} and {{<artist "Eric Dolphy">}}.

Maguire says he’s thinking about "pre-literate music, anything first instead of imitative (regardless of idiom)". 

He studied with traditional composers {{<artist "John Cage">}} and {{<artist "Michael Finnissy">}} and his arsenal of methods and statistics, which extend from avant garde traditional piano to jazz, from kwela to R&B, made him very popular. 

He had an ongoing musical relationship with drummer {{<artist "Steve Noble">}} and has contributed to {{<artist "Tony Oxley">}}’s Special event Orchestra and {{<artist "Sean Bergin">}}’s MOB organizations. 

He has toured with his personal nine-piece, the {{<artist "Kitty O'Nine Tails">}}, with drummer {{<artist "Louis Moholo">}} and saxophonist {{<artist "Alan Wilkinson">}}. 

Of particular note are his recordings with reed player {{<artist "Michael Moore">}}, 1999’s Mt. Olympus and 2001’s White colored Widow. Maguire has also played with {{<artist "Elton Dean">}} (saxophone), {{<artist "Fred T Baker">}} (bass) and {{<artist "Liam Genockey">}} (drums) within the jazz quartet Psychic Warrior.

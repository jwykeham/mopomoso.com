---
title: "Veryan Weston"
name: "Weston, Veryan"
date: 2024-07-25T07:57:59+01:00
categories:
- Artist
tags: 
- "veryan-weston"
artists: "Veryan Weston"
links:
- https://veryanweston.weebly.com/
- https://en.wikipedia.org/wiki/Veryan_Weston
instruments:
- piano
---
**Veryan Weston** is best-known for his work with such British musicians as saxophonist Lol Coxhill, vocalist Phil Minton and Trevor Watts who he is still working with in the 'Dialogues' duo as well as their current trio – 'Eternal Triangle' having recently released two CDs - "Gravity" and in Netherlands "Moving On". 
<!--more-->
Own projects have included research in to pentatonic scale relationships with various compositions reflecting this research called 'Tessellations'.

---
title: "Tracy Lisk"
name: "Lisk, Tracy"
date: 2024-04-28T07:31:41+01:00
categories:
- Artist
tags: 
- "tracy-lisk"
artists: "Tracy Lisk"
instruments:
- drums
featured_image: images/tracy-lisk.jpeg
---
**Tracy Lisk** is a percussionist who mainly performs on drum set, based in Philadelphia PA. Lisk’s history as a painter informs the substance of her improvisations which contain references to rhythmic structures while maintaining a fluid, suspended continuity. 
<!--more-->
Lisk has a background in Brazilian percussion and has worked with master drummers in Salvador, Bahia and Rio de Janeiro, Brazil, and performed in, as well as lead percussion ensembles in Philadelphia. She has collaborated with William Parker, saxophonist Gary Hassay, Andrea Pensado (electronics/voice), dancer Ryuzo Fukuhara (JP, SI), Mia Zabelka (AU), and cellist Helena Espvall (PT), among others.

In September 2023 she played with Susanna Ferrar, Maggie Nicols and Sylvia Hallett at the ImproMonday Festival in Zagreb.

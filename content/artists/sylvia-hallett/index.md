---
title: "Sylvia Hallett"
name: "Hallett, Sylvia"
date: 2023-01-14T08:27:00Z
categories:
  - Artist
tags:
  - "sylvia-hallett"
instruments:
  - "violin"
  - "electronics"
artists: sylvia hallett
featured_image: 
  path: images/IMG_1056LR_3000x2000-X3.jpg
  credit: Seán Kelly 
---
**Sylvia Hallett** is a multi-instrumentalist and composer. Her work grew out of the London Musicians Collective, where she played in a trio with {{<artist "Lol Coxhill">}} and {{<artist "Susannah Ferrar">}}, and was a regular contributor on NATO records with British Summer Time Ends. 
<!--more-->
Recently she has collaborated with {{<artist "Evan Parker">}}, {{<artist "David Toop">}}, {{<artist "Anna Homler">}} (The Many Moods of Bread and Shed CD), Clive Bell (The Geographers CD) Mike Adcock (Reduced CD), and regularly plays with the London Improvisers Orchestra. She also performs solo and has released 3 solo CDs on MASH and EMANEM. 

Site specific commissions include a semi-composed work which she performed for the Livio Felluga Wine Company on a hillside vineyard in Italy.

As a composer she has worked extensively with the choreographers Miranda Tufnell, Jacky Lansley, h2dance, Wonderful Beast Theatre Company, and has composed music for many BBC Radio dramas.

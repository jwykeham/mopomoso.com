---
title: "Barry John Edwards"
name: "Edwards, Barry John"
date: 2024-07-25T07:58:49+01:00
categories:
- Artist
tags: 
- "barry-john-edwards"
artists: "Barry John Edwards"
instruments:
- guitar
links: 
- www.bazmatronics.com
---
Born in Chester, **Barry John Edwards**, also known as Bazmatronics is a musician, composer and improviser based in Wales, U.K. He is a graduate of the Royal Birmingham Conservatoire in jazz and regular contributor to the improvisational scene in both the U.K and Spain. Barry is known for his music contribution to the Netflix series Spy Ops.
<!--more-->

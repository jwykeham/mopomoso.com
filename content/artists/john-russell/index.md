---
title: "John Russell"
name: "Russell, John"
date: 2023-01-12T09:51:43Z
dob: 1954-12-19
dod: 2021-01-19
categories:
  - Afternoon Sessions
tags:
  - "john-russell"
  - "guitar"
artists:
  - "john russell"
summary: "John Russell got his first guitar in 1965 while living in Kent and  began to play in and around London from 1971 onwards."
featured_image: "images/john-russell-cropped.webp"
quotes:
- text: "He finds new tones in the same place, new relationships in the same gesture. A second trip across the fingerboard is always a different excursion. The harmonic is a transparent sound: silence and ambient sound pass through it. It accounts for Russell’s unhurried pace and his sense of order, even when he’s playing fast: there’s simply so much going on"
  attribute: Stuart Broomer, Point of Departure
instruments:
- guitar
discogs_id: 292491
featured_image:
  path: images/IMG_9859LR_3000x2000-X3.jpg
  credit: Seán Kelly
---

Born 1954, London.

John Russell got his first guitar in 1965 while living in Kent and  began to play in and around London from 1971 onwards.

An early  involvement with the emerging free improvisation scene (from 1972)  followed, seeing him play in such places as The Little Theatre Club,  Ronnie Scott’s, The Institute of Contemporary Arts, **The Musicians' Co-Op** and the **London Musicians' Collective**.

From 1974 his work extended  into teaching, broadcasts (radio and television) and touring in the  United Kingdom and, ever extensively, in other countries around the world . He has played with many of the world's leading improvisers and his work can be heard on over 50 CDs and albums.

In 1981, he founded **QUAQUA**, a large bank of  improvisers put together in different combinations for specific  projects and, in 1991, he started **MOPOMOSO** which has become the UK’s  longest running concert series featuring mainly improvised music.

A leading figure in the UK improvised music scene John Russell is internationally respected for his guitar playing and continuing work within the field. He initiated and is responsible for running Mopomoso which, apart from hosting the UK’s longest unbroken concert series of freely improvised music, also organises special events and workshops. In 2016 he co-founded **Weekertoft**, a label specialising in high quality releases of improvised music, with Dublin based pianist {{<artist "Paul G Smyth">}}.


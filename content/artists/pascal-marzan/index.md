---
title: "Pascal Marzan"
name: "Marzan, Pascal"
date: 2023-01-16T17:05:32Z
categories:
- Artist
tags: 
- "pascal-marzan"
artists: "Pascal Marzan"
instruments:
- guitar
featured_image:
  path: images/IMG_1215LR_3000x2000-X3.jpg
  credit: Seán Kelly
---
Currently living in London, **Pascal Marzan** performs on a 10 string microtonal guitar.

After studying classical guitar – mainly devoted to the twentieth century repertoire – and teaching guitar in several music schools in Budapest, Hungary and France, Pascal has dedicated himself to free improvisation and the exploration of microtonal tunings of his instrument. His playing has been influenced by his interest in the folk musics of Eastern Europe, Asia and Africa.
<!--more-->
He has recorded guitar duets with John Russell, Roger Smith (Emanem records) and a trio with Sabu Toyozumi and Dan Warburton (Improvising Beings records).

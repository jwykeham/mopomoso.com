---
title: "Susanna Ferrar"
name: "Ferrar, Susanna"
date: 2024-04-25T09:50:23+01:00
categories:
- Artist
tags: 
- "susanna-ferrar"
artists: "Susanna Ferrar"
featured_image:
  path: images/IMG_4791LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- violin
---
**Susanna Ferrar** is a violinist with a special interest in site-specific improvisation. She is a Fellow of the Royal Geographical Society, and one of HT Ferrar’s grandchildren. She undertook her Postgraduate Certificate in Antarctic Studies (PCAS) at the University of Canterbury, New Zealand in 2011-12, and, during this time, saw many of the places first visited and named by the men of "Discovery". She has played her violin in Scott's hut in Antarctica. 
<!--more-->
She organised 36 concerts in May 1982 for the "Women Live" festival at the old London Musicians Collective premises in Gloucester Avenue, Camden, one of which involved her quartet with Sylvia Hallett, Maggie Nicols and Julie Tippetts: 2 violins and 2 voices. Using the initials of their children, they became "MADLI's mums" but life swept them away in various other directions and it wasn't until they were invited to Zagreb last year that they had the opportunity to reconvene. Sadly by then Julie wasn't travelling, but Tracy Lisk was available and was recruited as a fourth member. 

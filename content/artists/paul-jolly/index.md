---
title: "Paul Jolly"
name: "Jolly, Paul"
date: 2023-01-16T17:05:18Z
categories:
- Artist
tags: 
- "paul-jolly"
artists: paul jolly
links:
- https://www.pauljolly.com
featured_image: 
 path: images/IMG_5297LR_3000x2000-X3.jpg
 credit: Seán Kelly
instruments:
- saxophone
- clarinet
- flute
---
Paul Jolly is a saxophonist, clarinettist and flutist working within the fields of contemporary
improvised since 1969, when he joined the iconic **People Band** with whom he still performs.
<!--more-->
Currently he is involved with the Monk influenced band **SNC**, his own quartet ‘PJQ’ and the
experimental band **House of Five**. Projects include a duo with the poet Paula Rae Gibson, a duo with pianist Mike Adcock, and with Ben Higham’s large ensemble **The Brass Monkeys**. 

His main focus recently has been with the contemporary Italian group **Sothiac**, with whom he co-produced the critically acclaimed short film **Superluna** which showcased live at the 2021 Venice Biennale. 
{{< youtube OpH9P5sO90s >}} 

His love of working with dancers is continued with occasional performances with the award winning contemporary dance group **House of Absolute**, where he first met and performed with cellist **Simon McCorry**, and with the UK based German dancer **Petra Haller**.

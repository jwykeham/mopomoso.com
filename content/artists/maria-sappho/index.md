---
title: "Maria Sappho"
name: "Sappho, Maria"
date: 2023-07-30T17:15:53+01:00
categories:
- Artist
tags: 
- "maria-sappho"
artists: "Maria Sappho"
featured_image: "images/maria-sappho-cropped.webp"
links:
- https://mariasappho.com
instruments:
- piano
---

Maria Sappho is a Puerto Rican American, currently working as an improviser, artist and researcher in the UK. 

She is a member of the Glasgow Improvisers Orchestra (GIO) and founding member of the Noisebringers ensemble (CH/UK). She is a winner of the  BBC
Radiophophonic Daphne Oram award (2021), Dewar awards (2018) and New  Piano Stars Competition (2015). She has worked with a number of large
ensembles; International Contemporary Ensemble (US), BBC Scottish symphony orchestra, Australian Art Orchestra, and the Instant Composers Pool (ND). 

Maria works from a unique practice of multi-species improvisation working frequently with her core collaborators Chimere (a multi-modal AI) and mushrooms. 



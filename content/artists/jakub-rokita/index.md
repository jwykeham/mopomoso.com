---
title: "Jakub Rokita"
name: "Rokita, Jakub"
date: 2023-07-09T14:36:15+01:00
categories:
- Artist
tags: 
- "jakub-rokita"
artists: jakub rokita
links:
- https://linktr.ee/Rokita
featured_image:
  path: images/IMG_4725LR_2000x3000-X3.jpg
  credit: Seán Kelly
---
Jakub is an audio-visual installation artist, performer and an arts-activist working out of Luton. 
<!--more-->
His work focuses on collaborative and audience-participatory art-action through pop-up shows, workshops and jam sessions. 

His solo practice merges photography, performance, sound and video-art bringing out the uncanny and the liminal found in everyday phenomena and common surroundings. 

His work has been exhibited in the UK and internationally and he teaches media at the University of Bedfordshire where he is also one of the AA2A artists in residence.


---
title: "Lawrence Casserley"
name: "Casserley, Lawrence"
date: 2023-09-09T07:32:44+01:00
categories:
- Artist
tags: 
- "lawrence-casserley"
artists: "Lawrence Casserley"
links:
- https://www.lcasserley.co.uk/
featured_image:
  path: images/IMG_0455LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- electronics
---
**Lawrence Casserley** is best known for his work in free improvised music, particularly real-time processing of other musicians' sound, and he has devised a special computer processing instrument for this work. 
<!--more-->
He has worked with many of the finest improvisers, particularly {{<artist "Evan Parker">}}, with whom he works frequently as a duo partner, in various larger groupings and in the Evan Parker Electro-Acoustic Ensemble. He also works as a soloist, processing sounds from voice, percussion and home-made instruments. CDs have been released by ECM, Konnex, Leo Records, Psi, Sargasso and Touch. 

Much of Casserley's work has involved collaboration with other art forms, including visual artist the late {{<artist "Peter Jones">}} in his Colourscape installations. He is a Director of the Colourscape Music Festivals, presenting contemporary music in the unique environment of the Colourscape walk-in sculpture. He has also collaborated with {{<artist "Peter Jones">}} on other sound/light installations.



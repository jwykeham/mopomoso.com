---
title: "Marilza Gouvea"
name: "Gouvea, Marilza"
date: 2024-03-05T09:30:51Z
categories:
- Artist
tags: 
- "marilza-gouvea"
artists: "Marilza Gouvea"
discogs_id: 4322460
featured_image:
  path: images/IMG_3728LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- voice
---
**Marilza** comes from a musical family in Brazil, her father played classical guitar and her grandfather was a maestro and music teacher. From an early age she got involved with music: bossa nova and Brazilian popular music.

At an early age she got involved with music: bossa nova

and Brazilian popular music.

Becoming involved in improvised music after moving to Europe, she has worked with many exponents of its artists such as  {{<artist "Adrian Northover">}}, {{<artist "Jim Dvorak">}}, {{<artist "Terry Day">}}, {{<artist "Phil Minton">}} and {{<artist "Marcio Mattos">}}.
<!--more-->
She has participated in various improvised music festivals such as the Acéfalo in Valparaíso, Chile and in Malaga, invited by Javier Carmona, the Casa Invisible festival. In Rio de Janeiro at the Audio Rebel festival. She has performed several times at the Mopomoso club of the late friend John Russell.

Recently she has released two records in trio with Márcio Mattos and Adrian Northover  : "MAM" and "Cajula" (FMR).

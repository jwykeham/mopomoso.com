---
title: "Steve Beresford"
name: "Beresford, Steve"
date: 2023-09-09T07:25:08+01:00
categories:
- Artist
tags: 
- "steve-beresford"
artists: "Steve Beresford"
links:
- https://en.wikipedia.org/wiki/Steve_Beresford
- url: https://www.youtube.com/watch?v=3uXDSVV4zMs
  name: "In conversation with Steve Beresford - Unpredictable: Conversations with Improvisers"
featured_image:
  path: images/IMG_1233LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- piano
- electronics
- trumpet
- euphonium
- bass guitar
- toys
---
Steve is a British musician who graduated from the University of York. 

He has played a variety of instruments, including piano, electronics, trumpet, euphonium, bass guitar and a wide variety of toy instruments, such as the toy piano. 

He has also played a wide range of music. He is probably best known for free improvisation, but has also written music for film and television and has been involved with a number of pop music groups.


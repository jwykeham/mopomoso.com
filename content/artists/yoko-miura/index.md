---
title: "Yoko Miura"
name: "Miura, Yoko"
date: 2023-09-09T07:35:47+01:00
categories:
- Artist
tags: 
- "yoko-miura"
artists: "Yoko Miura"
featured_image:
  path: images/IMG_0414LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- piano
quotes:
- text: I am trying to make a kind of different universes interweaving and exchanging with different process, values and ways, with people, not only the musicians but also painters and dancers.
---
Tokyo based **Yoko Miura** trained as a classical pianist before moving into the worlds of Jazz and popular music. She began free improvisation with the Shakuhachi in a workshop setting and since 2002 has been performing as an improvisor in her native Japan and in Europe, having played with a.o {{<artist "Osamu Karuda">}}, {{<artist "Jean Borde">}}, {{<artist "Hans Koch">}} and {{<artist "Yoshihiro Goseki">}}. She gains her inspiration from natural scenery and nature and her music contains elements of peace and tranquility.

<!--more-->
Musicians with whom she has played:

In Japan: {{<artist "Noribumi Uchida">}} (shamisen, bass), {{<artist "Nakao">}} (sax, trombone, clarinet), {{<artist "Osamu Kuroda">}} (performance), {{<artist "Kyoko Oikawa">}} (violin), {{<artist "Ryouichi Saito">}} (guitar), {{<artist "SACHI-A">}} (drs), {{<artist "Jyunzo Tateiwa">}} (percussion), {{<artist "Michi Kinugasa">}} (sax, clarinet)

In Europe: {{<artist "Teppo Hauta-Aho">}} (bass), {{<artist "Gianni Mimmo">}} (sax), {{<artist "John Russell">}} (guitar), {{<artist "Lawrence Casserley">}} (electro), {{<artist "Jean-Michel Van Schouwburg">}} (voice), {{<artist "Mia Zabelka">}} (violin), {{<artist "Stefan Keune">}} (sax), {{<artist "Charlie Collins">}} (drs), {{<artist "Derek Saw">}} (trumpet), {{<artist "Ove Volquarlz">}} (bass clarinet), {{<artist "Jean Demey">}} (bass), {{<artist "Jacque Foschia">}} (bass-clarinet), {{<artist "Hans Koch">}} (sax), {{<artist "Emmanuel Rébus">}} (mathematical sounds), {{<artist "Harold Schellinx">}} (mathematical sounds), {{<artist "Claude Perle">}} (accordion), {{<artist "Judith Kan">}} (vocal), {{<artist "Anthony Carcone">}} (guitar), {{<artist "Jean Borde">}} (bass), {{<artist "Raymond MacDonald">}} (sax), {{<artist "Simon Berz">}} (drs), {{<artist "Paed Conca">}} (bass, Clarinet), {{<artist "Margrit Rieben">}} (drs), {{<artist "Raed Yassin">}} ( el.bass), {{<artist "Eugene Darredeau">}} (art), {{<artist "Alison Blunt">}} (violin), {{<artist "Jean-Marc">}} (synth), {{<artist "Thierry Waziniak">}} (drs), {{<artist "Matt Wilson">}} (drs) 

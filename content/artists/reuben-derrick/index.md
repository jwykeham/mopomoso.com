---
title: "Reuben Derrick"
name: "Derrick, Reuben"
date: 2024-04-28T07:32:07+01:00
categories:
- Artist
tags: 
- "reuben-derrick"
artists: "Reuben Derrick"
instruments:
- saxophone
- clarinet
links:
- https://soundcloud.com/reubenderrick
featured_image: images/reuben-derrick.jpeg
---
Saxophonist, clarinettist, sonic artist and composer **Reuben Derrick** grew up in Christchurch, New Zealand. His doctoral dissertation ‘Acoustic illuminations: recorded space as soundscape composition’ (2014) draws upon field recording and improvisation.
<!--more-->
This work featured locations in New Zealand, Australia and Sri Lanka.Ongoing projects include punk-jazz collective 'Poltroon', sound design and music for Free Theatre (recently for 'The Black Rider' and 'Frankenstein') , as well as multi-disciplinary collaborations with dance (such as Movement Art Practice, Julia Harvey and Kyungmi Kim), sculpture, circo-arts, poetry and film. In 2017 he coordinated the Party Of Special Things To Do festival. Other notable festival appearances have been elsewhere in New Zealand, Australia and Sri Lanka. In 2015 he was awarded a fellowship to the Music Omi residency in New York. 

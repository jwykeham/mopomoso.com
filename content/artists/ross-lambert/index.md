---
title: "Ross Lambert"
name: "Lambert, Ross"
date: 2024-05-27T07:24:33+01:00
categories:
- Artist
tags: 
- "ross-lambert"
artists: "Ross Lambert"
instruments:
- guitar
links:
- https://rosslambert.net
featured_image:
  path: images/IMG_0004LR_3000x2000-X3.jpg
  credit: Séan Kelly
---
**Ross Lambert** is an Irish free improvisation guitarist and "magnetic and vibrating sources" player. Visual artist. Writer. Designer.
<!--more-->
Northern Irish, based in London, UK.

"I must prepare to be open, an open book that absorbs and transmits as much as it speaks. In the same way that instrumental practice removes the lag of the circuit-board that is arms, hands and fingers, allowing the ear to connect directly to projection device and its receiver in that information-rich feedback loop. I am a process as much as a thing.

Sometimes the focus is more on innovation in the present but it also can be on historical references, sometimes shared ones. Mostly, these things work together, parallel streams that feed off each other in repeating patterns of coalescence, separation, mistake, coincidence. Reflecting, shadowing; binaries of the design process, pre-building whilst building: innovating, testing, filtering, repeating; the created one about to die, playing like a baby while knowing I die when the sound ends. Which is a dilemma. The sense of doubt, of an ending, of form that coalesces and structures itself with little intervention from the conscious side, but nonetheless seeks structure."

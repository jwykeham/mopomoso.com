---
title: "Alex Paxton"
name: "Paxton, Alex"
date: 2024-09-18T17:41:12+01:00
categories:
- Artist
tags: 
- "alex-paxton"
artists: "Alex Paxton"
links:
- https://alexpaxtonmusic.com/
instruments:
- trombone
quotes:
- text: "[A] Brit who defies every conceivable genre boundary…an extremely modern and future-oriented style."
  attribute: Paul Hindemith Prize winner 2023
- text: "[A] highly innovative...of exceptional creative imagination and musical energy, packed with life force unlike anything else."
  attribute: Ivor Novello British Composer Awards. Winner 2021
- text: "Unbridled joy…sophisticated, passionate music full of pulsating energy and stylistic diversity."
  attribute: Ernst von Siemens Composition Prize 2023
---
**Alex Paxton** (1990) is an award-winning composer & jazz-trombonist. His scores are published by Ricordi (Berlin).
<!--more-->

---
title: "Terry Day"
name: "Day, Terry"
date: 2024-09-18T17:35:25+01:00
categories:
- Artist
tags: 
- "terry-day"
artists: "Terry Day"
links:
- http://www.terryday.co.uk/
instruments:
- Bamboo Reed Flutes
- Drums
- Sopranino
- Recorders
- Balloons
---
**Terry Day** is a Multi-instrumentalist, Improvisation Pioneer, Song Writer, Tune-Smith, Lyricist, Poet, Painter, Conductor even.
<!--more-->
> I am an early 60's First Generation Pioneer of Improvisation, Free Jazz & experimental music. Since the 1960s I have collaborated with many improvising musical Luminaries, Groups, dancers, painters, poets, Artists from around the world, & performed / acted in Alternative Theatre, Events, Rock & Roll, and Nigerian High Life band.

> Depending on who I am working with, I now play Bamboo Reed Flutes, Drums, Sopranino, Recorders, Balloons & Improvise with my Lyrics, Prose, Verse ( which many call poems - but for me They Are Lyrics ).

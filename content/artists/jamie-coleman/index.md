---
title: "Jamie Coleman"
name: "Coleman, Jamie"
date: 2024-05-27T07:24:25+01:00
categories:
- Artist
tags: 
- "jamie-coleman"
artists: "Jamie Coleman"
links:
- https://www.youtube.com/watch?v=9A6GxONP_T0
instruments:
- trumpet
---
**Jamie Coleman** is an accomplished trumpet player and has been playing with Nathaniel Catchpole, Alex James, John Edwards and Eddie Prévost, Guillaume Viltard and many other improvisers.
<!--more-->


---
title: "Caroline Kraabel"
name: "Kraabel, Caroline"
date: 2024-07-25T07:58:12+01:00
categories:
- Artist
tags: 
- "caroline-kraabel"
artists: "Caroline Kraabel"
links:
- https://en.wikipedia.org/wiki/Caroline_Kraabel
instruments:
- saxophone
---
**Caroline Kraabel** has performed and recorded with improvisers including Robert Wyatt, Louis Moholo, Cleveland Watkiss, Hyelim Kim, Annie Lewandowski, Susan Alcorn, Mark Sanders and Veryan Weston (the trio CD – Playtime, and a duo CD project on Emanem - Five Shadows.
<!--more-->

Active groups: duo with cellist Khabat Abas, Transitions Trio (with Charlotte Hug and Maggie Nicols, OTO-Moers Quartet, with Bex Burch, Simon Camatta and Raïssa Mehner, Fit To Burst, with Sarah Washington and John Edwards; a duo with Pat Thomas and the Poetry Quartet with Rowland Sutherland, John Edwards and Sofia Vaisman Maturana.



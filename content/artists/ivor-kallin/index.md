---
title: "Ivor Kallin"
name: "Kallin, Ivor"
date: 2024-08-26T10:17:23+01:00
categories:
- Artist
tags: 
- "ivor-kallin"
artists: "Ivor Kallin"
instruments:
- viola
- electric violin
- bass
- voice
links:
- https://linearobsessional.bandcamp.com/track/turnips-in-my-trousers
---
**Ivor Kallin** plays viola with the London Improvisers Orchestra, electric violin with Electrio, bass and voice with the Glowering Figs, and hosts a weekly radio show on Resonance FM as Ambrosia Rasputin, and the last time he had his hair cut, it cost 30p.
<!--more-->

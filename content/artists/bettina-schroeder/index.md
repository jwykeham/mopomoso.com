---
title: "Bettina Schroeder"
name: "Schroeder, Bettina"
date: 2024-08-26T10:19:11+01:00
categories:
- Artist
tags: 
- "bettina-schroeder"
artists: "Bettina Schroeder"
links:
- https://linearobsessional.bandcamp.com/album/shatter-resistant
- https://wormholeworld.bandcamp.com/album/up-beat
- https://www.youtube.com/watch?v=l4ZVIdgNalU&t=79s
- https://wormholeworld.bandcamp.com/album/versus
featured_image: 
    path: images/featured.jpg
    credit: MRMC Robinson
---
**Bettina Schroeder** is a London-based multimedia artist. Her works include painting, installation, sound art, music, poetry and video.
<!--more-->
Recent performances/shows include: 
- London Improvisers Orchestra concerts
- Oooh International Improvisation Festivals – Performances with The Noisy Women group
- Exhibitions Frauenmuseum Bonn, Germany
- Cafe Oto music events
- Poetry & Exhibition events at *Crunch*, *Skibabble*, *Paper Tiger Poetry* and others
- MOPOMOSO events, Vortex Jazz Club
- üF-Beat Fringe Music Club night at Crouch End Literary Festival
- Otolith group exhibition & music, Greengrassi Gallery, London
- *BOA-TING* music event with Steve Beresford
- Neil Marsh's Vanishing Point at AMP Studios – Exhibition, Anna Göldi Museum, Switzerland
- Exhibitions with The Tunnel artist group, Hundred Years Gallery, London 
– Album releases with air play at BBC3, BBC Introducing, WFMU New York, 2BOB Radio, Australia, Resonance Radio.

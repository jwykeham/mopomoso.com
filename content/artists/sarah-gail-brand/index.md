---
title: "Sarah Gail Brand"
name: "Brand, Sarah Gail"
date: 2023-07-09T14:38:41+01:00
categories:
- Artist
tags: 
- "sarah-gail-brand"
artists:
- "sarah gail brand"
quotes:
- text: the most exciting trombone player for years
  attribute: The Wire
featured_image:
  path: images/IMG_7465LR_3000x2000.jpg
  credit: Seán Kelly
instruments:
- trombone
---
Described by The Wire magazine as "the most exciting trombone player for years" Sarah Gail Brand has recorded and performed on the international Improvised Music and Jazz scene since the early 1990s with {{<artist "Mark Sanders">}}, {{<artist "John Edwards">}}, {{<artist "Elton Dean">}}, {{<artist "Evan Parker">}}, {{<artist "Phil Minton">}}, {{<artist "Veryan Weston">}}, {{<artist "Lol Coxhill">}}, {{<artist "Maggie Nicols">}}, {{<artist "Wadada Leo Smith">}}, {{<artist "Steve Beresford">}}, {{<artist "Georg Graewe">}}, writer and comedian {{<artist "Stewart Lee">}} and countless others.
<!--more-->
Sarah fronts her own tunes quartet (Sarah Gail Brand Sextet), has a long standing duo with drummer {{<artist "Mark Sanders">}}, and a trio with {{<artist "John Edwards">}} and {{<artist "Steve Beresford">}} and continues to work as a soloist and in ad hoc ensembles. As well as being a composer, Sarah’s trombone work ranges from playing Improvised Music and Jazz, studio session work to arranging & playing in pop and rock music. 

Sarah is a music therapist and a professor of Improvisation at the Guildhall School of Music & Drama, London and leads workshops on improvisation around the world. 

Recordings of Sarah Gail’s work can be found on the Emanem and Regardless record labels.

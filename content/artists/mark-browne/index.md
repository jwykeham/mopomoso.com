---
title: "Mark Browne"
name: "Browne, Mark"
date: 2023-08-01T20:09:29+01:00
categories:
- Artist
tags: 
- "mark-browne"
artists: "Mark Browne"
featured_image: "images/mark-browne-cropped.webp"
links:
- https://bandcamp.com/markvbrowne
instruments:
- saxophone
---
Mark Browne is a saxophonic symbolic system of soprano and sopranino. His metaphor exists in the terrible deaths of dog-toys, cockpit, Cockshill, Coxhill music Bohman, Bohman, Crow. In the fetishisation of vibrations on copper. His instruments are coerced in a sault-somer of malapert systems. He works within a fine power-mesh that destroys the puritanicaldynamics of accepted sound. A fanatic of disaster. A fastidious amateur of grief. He destabilises both the physical and psychological through his art. Music is not privileged here; it is the interaction of hand on duck-call, copper drum, flowers of saxophone and breath-beats that underlie his rejection of homogenised sound ideologies and the alpha male collection of objects. Pluralism is encouraged and embraced in Mark’s work – it is not clock-work it is duree. A pulsing bricolage of bark-rips and gusset-faced machinations. Crush!!! Crush!!! He is a beautiful, liberating anti-Enlightenment machine. As with all defiance, soprano and sopranino destroy the blowing syntax and all is readily free as the folding of the 53rd noisy little cloud. Mark is a riff-hope-shy, bed singing with a clear now-self.


*Text by Molly Bloom*



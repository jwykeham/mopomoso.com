---
title: "Mark Wastell"
name: "Wastell, Mark"
date: 2024-03-27T09:03:16Z
categories:
- Artist
tags: 
- "mark-wastell"
artists: "Mark Wastell"
links:
- https://confrontrecordings.bandcamp.com/
featured_image:
  path: images/img_5569LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- cello
- double bass
- electronics
- tam tam
- percussion
---
**Mark Wastell** is a versatile improvising musician who has played a central role in the British improvised music scene for over a quarter of a century.<!--more--> He has performed and recorded extensively and his varied resume includes projects with Derek Bailey, Phil Durrant, John Butcher, Lasse Marhaug, Rhodri Davies, Simon H. Fell, Burkhard Beins, John Tilbury, Mattin, Mark Sanders, Tony Conrad, Evan Parker, Tim Barnes, Bernhard Günter, Keith Rowe, John Zorn, Peter Kowald, Joachim Nordwall, Otomo Yoshihide, Paul Dunmall, David Toop, Alan Wilkinson, Max Eastley, Hugh Davies, Julie Tippetts, Alan Skidmore, Mike Cooper, Chris Abrahams, Stewart Lee, Clive Bell, Arild Andersen, Jan Bang, Maggie Nicols, Thurston Moore and David Sylvian.


---
title: "Geoff Hearn"
name: "Hearn, Geoff"
date: 2024-04-28T07:31:11+01:00
categories:
- Artist
tags: 
- "geoff-hearn"
artists: "Geoff Hearn"
instruments:
- saxophone
links:
- https://www.geoffhearn.com/
featured_image: images/Geoff-Hearn-1200px.jpeg
---
Based in the South of England, **Hearn** is one of Britain's most vibrant musicians on the creative music scene today. 
<!--more-->
He is seriously overlooked and has led many inspired bands including Hoboko, Kasanga , Ten Men, Hipnosis, Up Down and Strange, Amigos, Planet Earth Ensemble, Jump Street, Tetragon, Steam, Torque, Organ-ics, and AKIMA who sometimes collaborated with a troupe of African Yoruba bata' drum masters with great success. Stints with America jazz singer Joe Lee Wilson, Vibraphonist Johnny Lytle and r'n'b legend Tommy Brown. One of his main interests is improvisational 'sonic sculptures' which is most evident in his work with his trio TORQUE with legendary drummer, the late Steve Harris and the extraordinary Canadian guitarist Jim Black, and of course the ground-breaking 'shape shifting' ensemble ZAUM, and recently witha new sonic adventure Shindo! 



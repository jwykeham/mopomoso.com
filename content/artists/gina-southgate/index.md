---
title: "Gina Southgate"
name: "Southgate, Gina"
date: 2024-08-26T10:22:18+01:00
categories:
- Artist
tags: 
- "gina-southgate"
artists: "Gina Southgate"
instruments:
- live painting
- performance
links:
- https://www.artistginasouthgate.com/
---
**Gina Southgate** is best known on the international jazz scene where she produces spectacular, qualitative, real time paintings.  For over three decades she's painted at gigs and festivals capturing the vitality and nuance in her unique portraits of world class musicians.
<!--more-->
She also performs creating real time audio/visual interactive artworks with improvising musicians.

Coming up through the world of freely improvised music and spontaneous site specific performance happenings on the avant garde fringe in the 90's Southgate was encouraged by the inclusivity of that scene to perform herself. 

In this role she creates and manipulates site specific artworks with paint, props and objects. These are chosen for their absurdist qualities as well as for their visual and sculptural potential and their sonic abilities. She explores in performance the futility and irony of domesticity and labour. 

She performs with musicians mainly from the world of freely improvised music including Maggie Nicols, Charles Hayward, Cath Roberts, Alan Wilkinson and had a longstanding duo with the late John Russell

Southgate has collaborated with, been resident artist for and exhibited at (among others); Riverside Studios, Unpredictable, Lume, EFG London Jazz Festival, Gateshead International Jazz festival, All Tomorrows Parties, Jazz Leeds, Jazz North East, Birmingham Jazz, Cambridge Jazz Festival, We Out Here festival, Jazz in the Round, Jazz on 3, Vortex Jazz, UK. Jazzahead!, Konfrontationen, and Kaleidophon Festivals, Europe. Vision Festival, USA.



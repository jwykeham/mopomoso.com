---
title: "Mike Adcock"
name: "Adcock, Mike"
date: 2024-03-27T09:03:23Z
categories:
- Artist
tags: 
- "mike-adcock"
artists: "Mike Adcock"
links:
- https://mikeadcock.bandcamp.com/music
featured_image:
  path: images/img_5400LR_3000x2000-X3.jpg
  credit: Seán Kelly
instruments:
- piano
- accordion
---
**Mike Adcock**'s music embraces both composition and improvisation. He has made numerous solo, duet and band recordings including three with Accordions Go Crazy, 'Lost for words', 'Moment of discovery', 'Sleep it off' (with Clive Bell), 'In the case of darkness', 'Reduced' (with Sylvia Hallett), 'Accord', 'Wanderlust' and 'Risky furniture' (with Paul Jolly).
<!--more-->

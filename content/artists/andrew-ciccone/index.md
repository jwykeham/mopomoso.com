---
title: "Andrew Ciccone"
name: "Ciccone, Andrew"
date: 2024-08-26T10:15:10+01:00
categories:
- Artist
tags: 
- "andrew-ciccone"
artists: "Andrew Ciccone"
links:
- https://omphalopticon.bandcamp.com/
---
**Andrew Ciccone** is a multimedia artist and trash collector from New York. Since migrating to London Andrew has doubled down on his artistic output with deep ventures into live improvisation, paper collage, metalwork, abstract videos and prose. He also runs the Navel-Gazers interview series with recording artists and the No Computers improv event series.
<!--more-->

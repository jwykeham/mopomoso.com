---
title: "Julian Woods"
name: "Woods, Julian"
date: 2023-07-09T14:48:43+01:00
categories:
- Artist
tags: 
- "julian-woods"
artists: julian woods
instruments:
- guitar
featured_image: 
  path: images/IMG_4633LR_3000x2000-X3.jpg
  credit: Seán Kelly
---
**Julian Woods** is a guitarist, bassist and composer based in London, currently studying a postgraduate degree at the Guildhall School of Music.
<!--more-->
Julian is interested in exploring new and unusual musical colours by making use of microtonality, polystylism and maximalism in the context of improvised music.

A graduate of Oxford University, he was prolifically active during his time as a student, leading the University Jazz Society house band, and playing in everything from a jazz orchestra to a classical guitar quartet. He has studied with pioneer {{<artist "Philipp Gerschlauer">}} and other microtonal theorists/composers and is a current student of the Julian Joseph Jazz Academy. 

Current projects include adapting Arabic and Turkish traditional music and Ives quartertone piano works onto modified guitars and performing with the London Improvisers Orchestra.

---
title: "Mark Hewins"
name: "Hewins, Mark"
date: 2024-07-25T08:02:02+01:00
categories:
- Artist
tags: 
- "mark-hewins"
artists: "Mark Hewins"
links:
- https://markhewins.bandcamp.com/album/an-electric-guitar
instruments:
- guitar
---
**Mark Hewins** has worked extensively with various Canterbury scene musicians, including Elton Dean and Hugh Hopper. 
<!--more-->
His collaboration with Dean continued when they both played as members of Soft Heap with Pip Pyle and John Greaves. He has also played with Gong Global Family which featured Hopper, Chris Cutler and Daevid Allen and was Gong's guitar player on their 30th Anniversary tour. He has toured with Lou Reed and played guitar in the Bob Geldof band. In addition, he had a long collaboration with Casio USA, when he wrote software and sound samples for their professional and domestic music instruments.

Trevor Watts asked him to join Amalgam after Keith Rowe left.

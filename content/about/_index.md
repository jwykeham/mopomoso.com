---
title: "About"
date: "2020-02-16T00:00:00Z"
featured_image: /uploads/2010/11/Red-Rose-Club.jpg
layout: single
weight: 100
---
Mopomoso was founded in 1991 by guitarist {{<artist "John Russell">}} and pianist, trumpeter and composer {{<artist "Chris Burn">}} to promote freely improvised music in all its forms and where applicable its relationship to other forms of contemporary music making. From the outset the aim has been to present the music on a local, regional national and international basis and to involve relative newcomers as well as more established musicians.


For seventeen years, monthly concerts were at The Red Rose Comedy Theatre, a venue that was to become an important one for a wide range of improvising projects. It was at The Red Rose that we started our annual end of year extravaganza, initially called *A Galaxy of Stars*, a half party half concert to celebrate all things improvised, which has become a major event in the London improviser's calendar.

Here is a tongue in cheek response to the then current ubiquity of laptop computers from the 2007 version.

{{< youtube bWsJqk4Onv8 >}}

The situation became untenable when a new landlord wanted to change the use of the venue. We were very fortunate to be able to move, without a break, to The Vortex *London's listening jazz club* in February 2008, where we have been ever since. As the UK's longest running concert series devoted to this area of music we have presented hundreds of concerts and a number of special events and workshops. Mopomoso also operates a kind of unofficial clearing house, offering help and advice to organisers and musicians both here and abroad. Initiated by {{<artist "Helen Petts">}} and with the help of some other very fine film makers, we have created what must be one of the largest on-line archives of improvised music film in the world. Here is the last set from John Russell's annual *Fete Quaqua* 2010 which takes place for three days each August.

{{< youtube 7JmH17-DFSU >}}

Run by a team of volunteers we hold a passionate belief that improvised music, sometimes called free music or free improvisation offers many exciting opportunities both to players and listeners alike. We hope you enjoy some of the wonderful music to be found on this site and that you will be able to attend some of our live events.

---
title: Live and Luscious in Luton
date: 2023-07-31T00:00:00+01:00
featured_image: images/St_Mary,_Luton_-_geograph.org.uk_-_3263631.jpg
featured_image_credits:
  author: John Salmon
  source: 
    url: https://commons.wikimedia.org/wiki/File:St_Mary,_Luton_-_geograph.org.uk_-_3263631.jpg
    name: Wikimedia Commons
  license: 
    url: https://creativecommons.org/licenses/by-sa/2.0/deed.en
    name: Attribution-ShareAlike 2.0 Generic
categories:
- news
---
Mopomoso will be live and luscious in Luton on Saturday 23rd September for the Equinoks site-specific event, from 2 till 6. 
<!--more-->
It will be a pop-up art event with fine art, short form video and of course tons of improvised music.

The venue is St. Mary's church - a 900 year old site with fabulous acoustics, located in the town centre.

With over 11 performers - dancers, musicians, artists and video artists, performers include {{<artist "Lawrence Casserley">}}, {{<artist "Paul Jolly">}}, {{<artist "Charlotte Keeffe">}}, {{<artist "Viv Corringham">}}, {{<artist "Tomasz Glazik">}} (sax), {{<artist "Sylvia Hallett">}}, {{<artist "Petra Haller">}}, {{<artist "Yoko Miura">}}, {{<artist "Mark Browne">}}, {{<artist "Jakub Rokita">}}, {{<artist "Maria Weiner">}}, with sculptors {{<artist "Anna Fairchild">}}, {{<artist "Claire Davies">}} and {{<artist "Faye Munroe">}}, with video artists {{<artist "Ahmed Salvador">}} and Scott McMahon, {{<artist "Ebba Jahn">}}, and {{<artist "Kristy M Chan">}}.

---
title: "John Russell 1954 - 2021"
categories:
  - News
date: "2021-01-19T00:00:00Z"
draft: true
---

Mopomoso founder guitarist {{<artist "John Russell">}} died today.

### Links
- https://www.theguardian.com/music/2021/mar/07/john-russell-obituary
- https://londonjazznews.com/2021/01/20/a-tribute-to-john-russell-1954-2021-by-evan-parker/
- https://www.vortexjazz.co.uk/2021/01/20/john-russell-1954-2021/


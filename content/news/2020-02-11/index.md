---
title: "John's Health and the Future of Mopomoso"
draft: true
categories:
  - News
date: "2020-02-16T00:00:00Z"
---
Despite some recent set backs (see John’s statement is below), this month’s MOPOMOSO will be going ahead as usual with an array of great musician.

Dear Friends

The day after I received a letter from the DWP to say I was ineligible for a Personal Independence Payment (PIP), I was asked to visit my GP to see how I was progressing with antibiotic treatment for tonsillitis. The antibiotics were found not to be working so the doctor booked me into hospital for the drugs to be administered intravenously and my condition monitored.

The on-call doctor examined my throat and said that nurses were worried about my SATS (oxygen levels), although this is normal for me with my COPD (chronic obstructive pulmonary disease). After examination by a Senior Registrar, I was found to have a variant of throat cancer and there seems to be secondary activity in the lymph glands. I do not have further information so am now at home awaiting a trip to hospital for an MRI scan and a biopsy, so there can be a fuller diagnosis and prognosis.

Luckily, Joanna has been able to come to London to help me with many pressing matters, some concerning the future of MOPOMOSO. I intend to continue with MOPOMOSO and as much musical work as I possibly can. I have so many ideas, so please excuse lack of website updates, etc.

I would like my friends and MOPOMOSO supporters for their advice and consideration. Here are some of the things I would like help with, please:

1. Can anyone recommend a good solicitor who would visit me at home and help me with legacies?  
2. Is there someone who could give expert help on setting up a MOPOMOSO trust fund, enabling us to maintain this important platform that our music deserves, particularly in the current political climate?
3. I would also like volunteers to assist the programming and running of the venture and support James, Kostas and myself with maintaining a continuing internet presence.

Peace and love.

John Russell


